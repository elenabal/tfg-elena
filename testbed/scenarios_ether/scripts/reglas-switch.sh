#!/bin/bash

if [ ! $# -eq 3 ]
then
    echo "Usage error: $0 <switchName> <IPAddress> <controllerIP>"
    exit -1
fi

SWITCH_NAME=$1
IP_ADDRESS=$2
CONTROLLER_IP=$3

# ------
# TABLA 0
# ------

# cuando esté managed esta regla es tapada por otra de mayor prioridad
# para cada puerto.E sta regla está para que al arrancar funcione el
# mecanismo de reg9 para prender dónde hay un controlador.
ovs-ofctl add-flow $SWITCH_NAME "table=0,priority=65532,actions=resubmit(,1),resubmit(,2)"


# ------
# TABLA 2
# ------

ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=39999,in_port=LOCAL,arp,actions=ALL"



# Cuando esté managed el controlador instala regla con prio 40000 que tapa a estas dos para insertar el grafo
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=39999,in_port=LOCAL,ipv4,nw_dst=$CONTROLLER_IP,tcp,tp_dst=6633,reg9=0,actions=drop"
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=39998,in_port=LOCAL,ipv4,nw_dst=$CONTROLLER_IP,tcp,tp_dst=6633,actions=output:NXM_NX_REG9[0..15]"



#
# Cuando estoy unmanaged aprendo el puerto en el que hay un controlador a partir del tráfico que recibo de probe controller
#
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=39997,reg9=0,in_port=LOCAL,actions=drop"

# Sonda UDP activa reg9, cualquier sonda vale, una vez que el switch esté managed, esta regla debe quedar tapada para que
# sólo las sondas que no son de mi controlador, almacenen el valor de reg9
ovs-ofctl -OOpenFlow14  add-flow $SWITCH_NAME "table=2,priority=39996,reg9=0,ipv4,udp,tp_src=55555,tp_dst=55556,actions=learn(table=1,priority=24999,hard_timeout=2,load:in_port->NXM_NX_REG9[0..15])"
# LLDP activa reg9
ovs-ofctl -OOpenFlow14  add-flow $SWITCH_NAME "table=2,priority=39996,reg9=0,dl_type=0x88cc,actions=learn(table=1,priority=24999,hard_timeout=2,load:in_port->NXM_NX_REG9[0..15])"

# mensaje arp dirigido a este switch se entrega en local y activa reg9 si no lo estaba 
# podría haber oscilaciones si no se comprueba que reg9=0, cuando el controlador tiene varias ifaces, responde por todas a ARP
ovs-ofctl -OOpenFlow14 add-flow $SWITCH_NAME "table=2,priority=39999,arp,arp_tpa=$IP_ADDRESS,reg9=0,actions=learn(table=1,priority=24999,hard_timeout=2,load:in_port->NXM_NX_REG9[0..15]),LOCAL"
ovs-ofctl -OOpenFlow14 add-flow $SWITCH_NAME "table=2,priority=39998,arp,arp_tpa=$IP_ADDRESS,actions=LOCAL"


# Una vez managed (reg9 != 0), tráfico lldp recibido se envía al controlador
#ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=39995,dl_dst=01:80:c2:00:00:00/ff:ff:ff:00:00:00,actions=CONTROLLER"



# tráfico dirigido al switch, se entrega al switch
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=39996,ipv4,nw_dst=$IP_ADDRESS,actions=LOCAL"





#DHCP
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=65000,in_port=LOCAL,ipv4,udp,tp_src=68,tp_dst=67,dl_dst=ff:ff:ff:ff:ff:ff,actions=output:ALL"

#Cuando envíe mensajes IPv6mcast_node_04 lo tira
ovs-ofctl add-flow $SWITCH_NAME "table=2,dl_dst=33:33:00:00:00:04,actions=drop"
ovs-ofctl add-flow $SWITCH_NAME "table=2,dl_dst=33:33:00:00:00:16,actions=drop"



