#!/bin/bash
if [ ! $# -eq 1 ]
then
    echo "Usage error: $0 <switchName>"
    exit -1
fi

SWITCH_NAME=$1

./scripts/deleteBrSdn.sh $SWITCH_NAME 1
./scripts/stopOvs.sh $SWITCH_NAME
./scripts/stopOvsDb.sh $SWITCH_NAME
