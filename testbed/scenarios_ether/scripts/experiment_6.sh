# Args del script (ojo, no confundir con args de campos de awk)
# $1: fichero en el que está el log en el que se busca
# $2: fichero que se genera con la búsqueda
# $3: buscar sólo a partir de la marca de tiempo $3. Almacenado en
#     MARCA_TIEMPO para que el script awk no confunda con sus parámetros
#     $???

# Se queda con la primera aparición de cada controlador que se anuncia
# al controlador cuyo fichero de log nos pasan en $1


# egrep se queda las líneas "informing of new" que son en las que se
# recibe un anuncio de un controlador

# Estas líneas son procesadas por el script awk, que almacena en el
# array IP la primera aparición de cada nuevo controlador: primer
# campo ($1) marca de tiempo y último campo ($NF) dir IP


egrep "informing of new"  $1  | awk -v MARCA_TIEMPO=$3 '{if ($1 > MARCA_TIEMPO && !IP[$NF]) IP[$NF]=$1;} END{for (var in IP) print IP[var] " : \t" var}'  >> $2
