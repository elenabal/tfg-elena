#!/bin/bash


if [[ $# -ne 3 && $# -ne 4 ]]
then
    echo "Usage error: $0 <switchName> <if_number> <ip_address> [BFD min_tx in ms]"
    exit -1
fi


CONTROLLER_IP="10.0.0.1"
BFD="False"

if [[ $# -eq 4 ]]
then
    BFD=$4
else
    echo "No BFD..."
fi

SWITCH_NAME=$1
SWITCH_IF_NUMBER=$2
SWITCH_IP_ADDRESS=$3

echo "Starting $SWITCH_NAME $SWITCH_IP_ADDRESS"

PID_NAMESPACE=`ps -feaww | grep "mininet:$SWITCH_NAME" | head -1 | awk '{print $2}'`
echo "PID_NAMESPACE=$PID_NAMESPACE"


nsenter -t $PID_NAMESPACE -n ./scripts/startOvsDb.sh $SWITCH_NAME 
echo "Start OVS $SWITCH_NAME"
nsenter -t $PID_NAMESPACE -n ./scripts/startOvs.sh $SWITCH_NAME
nsenter -t $PID_NAMESPACE -n ./scripts/createBrSdn.sh $SWITCH_NAME $SWITCH_IF_NUMBER $CONTROLLER_IP
nsenter -t $PID_NAMESPACE -n ifconfig $SWITCH_NAME inet $SWITCH_IP_ADDRESS
nsenter -t $PID_NAMESPACE -n ./scripts/reglas-switch.sh $SWITCH_NAME $SWITCH_IP_ADDRESS $CONTROLLER_IP
nsenter -t $PID_NAMESPACE -n ip route change 10.0.0.0/8 dev $SWITCH_NAME advmss 1000


# BFD

if [[ ${BFD} -ne "False" ]]
then
   val=`expr $SWITCH_IF_NUMBER - 1`
   for i in $(seq 0 $val);
   do
      echo "Activating BFD interface eth$i"
      nsenter -t $PID_NAMESPACE -n ./scripts/startBFD-sw-iface.sh $SWITCH_NAME eth$i $BFD
   done
fi

echo "------------------------------------------------------"
echo "Hora de creación del switch $SWITCH_NAME"
date +"%H:%M:%S,%N"

