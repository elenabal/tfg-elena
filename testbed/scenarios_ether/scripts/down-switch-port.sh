#!/bin/bash

if [ ! $# -eq 2 ]
then
    echo "Usage error: $0 <switchName> <numberofPort>"
    exit -1
fi

SWITCH_NAME=$1
N_PORT=$2
ALT_PORT_DECIMAL=`expr $2 + 8`
printf -v ALT_PORT '%x' $ALT_PORT_DECIMAL




ovs-ofctl -O OpenFlow14 del-flows $SWITCH_NAME "table=1,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=$N_PORT"
ovs-ofctl -O OpenFlow14 del-flows $SWITCH_NAME "table=1,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x$ALT_PORT,nsh_ttl=1"
ovs-ofctl -O OpenFlow14 del-flows $SWITCH_NAME "table=1,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x$ALT_PORT"
ovs-ofctl -O OpenFlow14 del-flows $SWITCH_NAME "table=1,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[24..31]=0xf$N_PORT,nsh_ttl=0"
ovs-ofctl -O OpenFlow14 del-flows $SWITCH_NAME "table=1,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[24..31]=0xf$ALT_PORT"


ovs-ofctl -O OpenFlow14 add-flow $SWITCH_NAME "table=1,priority=57997,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x$N_PORT,actions=drop"
ovs-ofctl -O OpenFlow14 add-flow $SWITCH_NAME "table=1,priority=57999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x$ALT_PORT,nsh_ttl=1,actions=drop"
ovs-ofctl -O OpenFlow14 add-flow $SWITCH_NAME "table=1,priority=57998,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x$ALT_PORT,actions=drop"
ovs-ofctl -O OpenFlow14 add-flow $SWITCH_NAME "table=1,priority=57999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[24..31]=0xf$N_PORT,nsh_ttl=0,actions=drop"
ovs-ofctl -O OpenFlow14 add-flow $SWITCH_NAME "table=1,priority=57998,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[24..31]=0xf$ALT_PORT,actions=drop"



