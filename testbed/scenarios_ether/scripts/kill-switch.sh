#!/bin/bash

if [ ! $# -eq 2 ]
then
    echo "Usage error: $0 <switchName> <number_of_interfaces>"
    exit -1
fi

SWITCH_NAME=$1
SWITCH_IF_NUMBER=$2


echo "Kill switch $SWITCH_NAME"
#kill -9 `cat /tmp/mininet-$SWITCH_NAME/*.pid`


PID_NAMESPACE=`ps -feaww | grep "mininet:$SWITCH_NAME" | head -1 | awk '{print $2}'`
echo "PID_NAMESPACE=$PID_NAMESPACE"

nsenter -t $PID_NAMESPACE -n ./scripts/deleteBrSdn.sh $SWITCH_NAME $SWITCH_IF_NUMBER; ./scripts/stopOvs.sh $SWITCH_NAME; ./scripts/stopOvsDb.sh $SWITCH_NAME

echo "--------------------------------------------------" 
echo "Hora de muerte del switch $SWITCH_NAME" 
date +"%H:%M:%S,%N"

