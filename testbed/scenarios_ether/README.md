# Requiere:
- Instalar mininet
- Instalar python3 y python3-pip
- Instalar virtualenv: pip3 install virtualenv
  
- Crear virtualenv 
   python3 -m venv /home/sdn/sdnenv
- Activarlo
   source /home/sdn/sdnenv/bin/activate

- Instalar ryu
   git clone https://github.com/yurekten/ryu
   cd ryu
   pip3 install .
   pip3 install eventlet==0.30.2
   pip3 install networkx
   pip3 install matplotlib
   pip3 install bitarray



####################################################
# Maqueta de bootstrap con controlador sin grafos
####################################################



Topología definida en el fichero escenario-bucle.py

 10.0.0.1      10.0.0.101      10.0.0.102         10.0.0.103       10.0.0.2
    s0 ----------- s1 -------------- s2 -------------- s3 ------------ h2
                    \
                     \
                      \
                       --------------s4 ------------- h3
                               10.0.0.104          10.0.0.3


Los switches se arrancan en modo secure, out-of-band i
Nosotros queremos un plano de control in-band, pero en ovs out-of-band significa sin entradas en tablas de flujos. El modo in-band de ovs implica que mete entradas de las tablas para poder alcanzar al controlador. Nosotros metemos las nuetras por lo que queremos el modo out-of-band

## Modo de arranque

- En un terminal arrancar mininet con la topología
  sudo python3 ./scenarios/sin_grafos/escenario.py

- En xterm de s0  arrancar controlador:
  s0~:> sudo ryu-manager --verbose ./miControlador.py 

- En el CLI de mininet:
  mininet > source ./scenarios/sin_grafos/start-scenario.py

## Interrumpir ejecución
- Interrumpir el controlador
- En el CLI de mininet:
  mininet > source ./scenarios/sin_grafos/stop-scenario.py


## up-switch-port.sh y down-switch-port.sh levantan/tiran puertos
- En el terminal del switch donde se desea deshabilitar un puerto, por ejemplo en s1 desactivar el puerto 2:
  s1~:> ./scripts/down-switch-port.sh s1 2
- En el terminal del switch donde se desea habilitar un puerto, por ejemplo en s1 activar el puerto 2:
  s1~:> ./scripts/up-switch-port.sh s1 2
   

## Consultar estado

Para consultar la información de un switch, por ejemplo s1, ejecutamos en su terminal:
   s1:~> /dumpAllFlows.sh s1


####################################################
# Maqueta de bootstrap con controlador con grafos
####################################################

## Ejemplo de ejecución bucle1

 Topologia
                                                              
       1         1    2              1   2            1    2          1
    s0 ----------- s1 --------------- s2 -------------- s3 ------------ h2
 10.0.0.1           \ 3            3 /                               10.0.0.2
                     \              /
                     \          3 /           1
                       -------- s4 ------------- h3
                              1    2            10.0.0.3

- En un terminal arrancar mininet con la topología
  sudo python3 ./scenarios/bucle1/scenario_bucle1.py

- En xterm de s0  arrancar controlador:
  mininet> xterm s0
  s0~:> source ../../venv/bin/activate
  s0~:> sudo ryu-manager --verbose ./miControlador.py 

- En el CLI de mininet:
  mininet > source ./scenarios/bucle1/start_scenario_bucle1.py

- Prueba de camino alternativo
  mininet> s2 wireshark&                  # capturamos en s2-eth0
  mininet> s2 wireshark&                  # capturamos en s2-eth2

  Vemos que todo el tráfico va por el camino principal (s2-eth0)
  Ahora tiramos el enlace s1-s2 en ambos sentidos:

  mininet> s2 ./scripts/down-switch-port.sh s2 1
  mininet> s1 ./scripts/down-switch-port.sh s1 2
  Vemos que todo el tráfivo va por el camino alternativo (s2-eth2)

- Para interrumpir la ejecución: interrumpir el controlador e interrumpir el escenario
  mininet > source ./scenarios/bucle1/stop_scenario_bucle1.py


## up-switch-port.sh y down-switch-port.sh levantan/tiran puertos
- En el terminal del switch donde se desea deshabilitar un puerto, por ejemplo en s1 desactivar el puerto 2:
  s1~:> ./scripts/down-switch-port.sh s1 2
- En el terminal del switch donde se desea habilitar un puerto, por ejemplo en s1 activar el puerto 2:
  s1~:> ./scripts/up-switch-port.sh s1 2
   

## Ejemplo de ejecución linea15_bucle
 Topologia
                                                              
       1         1    2              1   2      1    2    1   2     1  2     1  2         1   2
    s0 ----------- s1 --------------- s2 -------- s3 ------s4 ------ s5 ----- s6 -- ... -- s16 ------ h2
 10.0.0.1           \ 3            3 /                                                             10.0.0.2
                     \              /
                     \          3 /           1
                       -------- s4 ------------- h3
                              1    2            10.0.0.3

- En un terminal arrancar mininet con la topología
  sudo python3 ./scenarios/linea15_bucle/scenario_linea15_bucle.py

- En xterm de s0  arrancar controlador:
  mininet> xterm s0
  s0~:> source ../../venv/bin/activate
  s0~:> sudo ryu-manager --verbose ./miControlador.py 

- En el CLI de mininet:
  mininet > source ./scenarios/linea15_bucle/start_scenario_linea15_bucle.py

- Para interrumpir la ejecución: interrumpir el controlador e interrumpir el escenario
  mininet > source ./scenarios/linea15_bucle/stop_scenario_linea15_bucle.py


## up-switch-port.sh y down-switch-port.sh levantan/tiran puertos
- En el terminal del switch donde se desea deshabilitar un puerto, por ejemplo, en s1 desactivar el puerto 2:
  s1~:> ./scripts/down-switch-port.sh s1 2
- En el terminal del switch donde se desea habilitar un puerto, por ejemplo en s1 activar el puerto 2:
  s1~:> ./scripts/up-switch-port.sh s1 2
   

# Topology Visualizer
No funciona con firefox

Para que funcione, suponiendo que ryu está en $RYU:
1) copiar topology-visualizer/d3.v3.min.js a $RYU/ryu/app/gui_topology/html
2) copiar topology-visualizer/ibc.html a $RYU/ryu/app/gui_topology/html
3) ryu-manager  --observe-links --app-lists ryu.app.ofctl
_rest ryu.app.gui_topology.gui_topology InBandController.py
4) instalación de brave-browser: wget  https://brave.com/linux/
5) arrancar brave-browser en s0 sin ser root, y cargar http://localhost:8080/ibc.html 

API REST docs: https://github.com/faucetsdn/ryu/blob/master/ryu/app/rest_topology.py


###########
# Testing
###########
Example: 
 sudo python3 -m unittest test_scenarios.ScenariosTestCase.test_kill_node_scenario_bucle


###############
# Activate BFD
###############
https://ryu-devel.narkive.com/5xenac6s/enabling-bfd-from-the-controller

Activate in file params.conf setting True in param use_bfd_param:
---
[DEFAULT]
use_bfd_param = True
--- 
Then launch controller passing the params-controller-s0.conf file as parameter to the --config-file option:

sudo ryu-manager --verbose InBandController.py --config-file params-controller-s0.conf > /tmp/salida 2>&1

############
# LLDP
###########
LLDP is required to rediscover lost links, so you must launch with --observe-links:
sudo ryu-manager --observe-links --verbose InBandController.py --config-file params-controller-s0.conf > /tmp/salida 2>&1


#####
# iperf
####

En scenario mesh_4controllers arrancamos 2 controladores en s20 y 230

servidor udp en 11.0.0.2
iperf -u -i 1 -s

cliente udp en 11.0.0.3
iperf -u -B 11.0.0.3 -c 11.0.0.2 -l 1200 -b 300M

-------

servidor tcp en 11.0.0.2
ip route change 11.0.0.0/8 dev s20 advmss 1200
iperf  -i 1 -s


cliente en 11.0.0.3
ip route change 11.0.0.0/8 dev s30 advmss 1200
iperf  -i 1  -B 11.0.0.3 -c 11.0.0.2  -b 200M