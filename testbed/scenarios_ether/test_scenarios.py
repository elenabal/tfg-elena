#!/usr/bin/python

import subprocess

from unittest import TestCase, main

import sys
import time
import os

from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info

# -------------------------
# launch_controller
# -------------------------
def launch_controller(node, config_file, log_file):
    # Launch controller in node
    #node.cmd("source ../../../sdnenv/bin/activate")
    node.cmd("source /home/sdnwifi/sdnenv/bin/activate")
    ryu_command=f'ryu-manager --observe-links --verbose   ./InBandController.py  --config-file {config_file} >{log_file} 2>&1 &'
    print(ryu_command)
    node.cmd(ryu_command)


# -------------------------
# scenario_test
# -------------------------
def scenario_test(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None, LAUNCH_CONTROLLER_MODE="End"):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    start_controller_switch_sh = f'{SCENARIO_DIR}/start_controller_switch_{scenario_name}.sh' 
    start_other_switches_sh = f'{SCENARIO_DIR}/start_other_switches_{scenario_name}.sh' 
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)
    else:
        CLI( net , script = stop_script_py)
        

    if LAUNCH_CONTROLLER_MODE == "First":
        print("START CONTROLLER NODE/S")
        print("#########################\n")

        os.system(start_controller_switch_sh)        

        print("FIRST START CONTROLLER/S")
        print("#########################\n")

        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


        print("START REST OF NODES/S")
        print("#########################\n")
        os.system(start_other_switches_sh)        

    else:
        print("START ALL NODES/S")
        print("#########################\n")
        if (os.path.exists(start_script_sh)):
            if (BFD_MIN_TX_ms != None):
                os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
            else:
                os.system(start_script_sh)        
        else:
            CLI( net , script = start_script_py)    

        print("LAUNCH CONTROLLER/S AT THE END")
        print("#########################\n")
        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 

    
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)        
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
    

# --------------------------------
# scenario_experiment_kill_switch
# --------------------------------
def scenario_experiment_kill_switch(scenario_name, switch_to_kill, interfaces, UPTIME=25, UPTIME_AFTER_KILL=10, DOWNTIME=10, BFD_MIN_TX_ms=None):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)
    else:
        CLI( net , script = stop_script_py)
        

    print("START")

    if (os.path.exists(start_script_sh)):
        if (BFD_MIN_TX_ms != None):
            os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
        else:
            os.system(start_script_sh)        
    else:
        CLI( net , script = start_script_py)    


    for c in controller_nodes:
        # Launch controller in node c
        launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
    print(str(time.time()) + " : " +" Launched controller(s)")

    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 


    # kill switch
    os.system(f'scripts/kill-switch.sh {switch_to_kill} {interfaces}')
    with open("/tmp/kill_time.txt", "w") as f:
        print ("kill_time: " + str(time.time()))
        print (time.time(), file=f)

    time.sleep (UPTIME_AFTER_KILL) 

    
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)        
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
    
        

# --------------------------------
# scenario_experiment_2
# --------------------------------
def scenario_experiment_2(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)
        

    print("START")

    if (os.path.exists(start_script_sh)):
        if (BFD_MIN_TX_ms != None):
            os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
        else:
            os.system(start_script_sh)        
    else:
        CLI( net , script = start_script_py)    


    print("Launch controller(s)")
    for c in controller_nodes:
        # Launch controller in node c
        launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')


        
    time.sleep(25)

    print("launch tcpdump en s1")    
    s1 = net.getNodeByName("s1")
    s1.cmd("rm /tmp/k.pcap")
    s1.cmd(f'timeout 25 tcpdump dst host 10.0.0.1 and udp port 5001 -i s1-eth0 -w /tmp/k.pcap &')

    print("launch iperf server en s0")
    s0 = net.getNodeByName("s0")
    s0.cmd(f'timeout 20 iperf -u -i 1 -s &')

    time.sleep(2)
    
    print("launch iperf client en s7")
    s7 = net.getNodeByName("s7")
    s7.cmd(f'iperf -u -c 10.0.0.1 -l 1200 -b 200M &')

    time.sleep (3)    
    os.system("scripts/kill-switch.sh s6 2")


    print("network up during ", UPTIME, "s")
    time.sleep (20)
    
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)        
    else:
        CLI( net , script = stop_script_py)
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
    

# --------------------------------
# scenario_experiment_9
# --------------------------------
def scenario_experiment_9(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)
        

    print("START")

    if (os.path.exists(start_script_sh)):
        if (BFD_MIN_TX_ms != None):
            os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
        else:
            os.system(start_script_sh)        
    else:
        CLI( net , script = start_script_py)    


    print("Launch controller(s)")
    for c in controller_nodes:
        # Launch controller in node c
        launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')


        
    time.sleep(25)

    print("launch tcpdump en s1")    
    s1 = net.getNodeByName("s1")
    s1.cmd("rm /tmp/k.pcap")
    s1.cmd(f'timeout 25 tcpdump dst host 10.0.0.1 and tcp port 5001 -i s1-eth0 -w /tmp/k.pcap &')


    
    print("launch iperf server en s0")
    s0 = net.getNodeByName("s0")
    s0.cmd(f'timeout 20 iperf -i 1 -s &')

    time.sleep(2)
    
    print("launch iperf client en s7")
    s7 = net.getNodeByName("s7")
    s7.cmd(f'iperf -c 10.0.0.1  -b 200M &')

    time.sleep (3)    
    os.system("scripts/kill-switch.sh s6 2")


    print("network up during ", UPTIME, "s")
    time.sleep (20)
    
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)        
    else:
        CLI( net , script = stop_script_py)
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
    
        
# --------------------------------
# scenario_experiment_5
# --------------------------------
def scenario_experiment_5(scenario_name, controllers_to_kill, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)
        

    print("START")

    if (os.path.exists(start_script_sh)):
        if (BFD_MIN_TX_ms != None):
            os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
        else:
            os.system(start_script_sh)        
    else:
        CLI( net , script = start_script_py)    


    print("Launch controller(s)")
    for c in controller_nodes:
        # Launch controller in node c
        launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')


        
    time.sleep(40)

    for c in controllers_to_kill:
        os.system(f'scripts/kill-switch.sh {c} 1')


    time.sleep (30)

    
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)        
    else:
        CLI( net , script = stop_script_py)
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
    

        
# --------------------------------
# scenario_experiment_4
# --------------------------------
def scenario_experiment_4(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)
        

    print("START")

    if (os.path.exists(start_script_sh)):
        if (BFD_MIN_TX_ms != None):
            os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
        else:
            os.system(start_script_sh)        
    else:
        CLI( net , script = start_script_py)    


    time.sleep(5)
        

    os.system("rm /tmp/*.pcap")
    for c in controller_nodes:
        print(f'launch tcpdump en {c}')    
#        c = net.getNodeByName(controller_name)
#    c.cmd(f'timeout 15 tcpdump (dst 10.0.0.101) or (src 10.0.0.101) -i s1-eth0 -w /tmp/k.pcap &')
        c.cmd(f'timeout 25 tcpdump -i {c}-eth0 -w /tmp/{c}.pcap &')    
        
    
    print("Launch controller(s)")
    for c in controller_nodes:
        # Launch controller in node c
        launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')


    time.sleep(25)

    
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)        
    else:
        CLI( net , script = stop_script_py)
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")


        
# --------------------------------
# kill_node_test
# --------------------------------
def kill_node_test(scenario_name, node_to_kill, UPTIME=5, KILLTIME=5):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    
    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}.py'
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}.py'
    print(stop_script)

    
    print(node_to_kill)
    
    net, controllers  = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    CLI( net , script = stop_script)


    # Launch controller in s0
    launch_controller(controllers[0], f'{SCENARIO_DIR}/params-controller-s0.conf', "/tmp/salida-s0")


    print("START")

    print(f'Test kill {node_to_kill}')
    CLI( net , script = start_script)    
    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 
    

    print (f'kill {node_to_kill}')
    killString = ""
    os.system(f'sudo kill -9 `cat /tmp/mininet-{node_to_kill}/*pid`')
    print(f'{node_to_kill} down during {KILLTIME} s')
    time.sleep (KILLTIME)
    
    UPTIME=40 # leave time to recover to all in cascade
    CLI( net , script = start_script)    
    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 
    
    CLI( net , script = stop_script)
    print("network down")

        
    print("FINISH")
    
    net.stop()
    

# --------------------------------
# Class ScenariosTestCase
# --------------------------------
class ScenariosTestCase(TestCase):

    def do_test(self, result, expected):
        self.maxDiff = None
        
        l = result.split("\n")
        l=set(l)
        l=list(l)
        l.sort()
        l=l[1:]
        result = "\n".join(l)
        result += "\n"
        
        self.assertEqual(expected,result)

    # ------------------------
    # test_scenario_one_switch
    # ------------------------
    def test_scenario_one_switch(self, UPTIME=10):
        scenario_test("one_switch")

        expected="switch [10.0.0.101] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)

        

    # ------------------------
    # test_scenario_arbol
    # ------------------------
    def test_scenario_arbol(self):
        scenario_test("arbol")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)

        
    # ------------------------
    # test_scenario_bucle1
    # ------------------------
    def test_scenario_bucle1(self):
        scenario_test("bucle1")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)

    
    # ------------------------
    # test_scenario_bucle2
    # ------------------------
    def test_scenario_bucle2(self):
        scenario_test("bucle2")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)
        
        

    # -------------------------------
    # test_kill_node_scenario_bucle2
    # -------------------------------
    def test_kill_node_scenario_bucle2(self):
        kill_node_test("bucle2", "s2")

        expected="switch [10.0.0.102] IS NOT MANAGED\nswitch [10.0.0.102] IS MANAGED\n"

        proc = subprocess.Popen(["egrep MANAGED /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.maxDiff = None
        
        l = result.split("\n")
        l=l[1:]
        result = "\n".join(l)
        result += "\n"

        print(expected)
        print(result)
        self.assertTrue(expected in result)


    # -------------------------------
    # test_scenario_linea15_bfd
    # -------------------------------
    def test_scenario_linea15_bfd(self):
        scenario_test("linea15_bfd", UPTIME=35, LAUNCH_CONTROLLER_MODE="End")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)

    # -------------------------------
    # test_scenario_linea15_2c_bfd
    # -------------------------------
    def test_scenario_linea15_2c_bfd(self):
        scenario_test("linea15_2c_bfd", UPTIME=35, LAUNCH_CONTROLLER_MODE="End")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True)
        (result_c0, err) = proc.communicate()
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True)
        (result_c1, err) = proc.communicate()


        result = result_c0.decode() + result_c1.decode()

        self.do_test(result,expected)


 

    # -------------------------------
    # test_scenario_linea15_nobfd
    # -------------------------------
    def test_scenario_linea15_nobfd(self):
        scenario_test("linea15_bfd", UPTIME=35)

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)


    # -------------------------------
    # test_scenario_linea15_bucle
    # -------------------------------
    def test_scenario_linea15_bucle(self):
        scenario_test("linea15_bucle", UPTIME=35)

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)

    # -------------------------------
    # test_scenario_clos_bfd
    # -------------------------------
    def test_scenario_clos_bfd(self):
        scenario_test("clos_bfd", UPTIME=20, BFD_MIN_TX_ms=100, LAUNCH_CONTROLLER_MODE="End")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)


    # -------------------------------
    # test_scenario_clos_old_bfd
    # -------------------------------
    def test_scenario_clos_old_bfd(self):
        scenario_test("clos_old_bfd", UPTIME=45, BFD_MIN_TX_ms=100)

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)


    # -------------------------------
    # test_scenario_att_bfd
    # -------------------------------
        
    def test_scenario_att_bfd(self):
        scenario_test("att_bfd", UPTIME=120, BFD_MIN_TX_ms=100, LAUNCH_CONTROLLER_MODE="First")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\nswitch [10.0.0.121] IS MANAGED\nswitch [10.0.0.122] IS MANAGED\nswitch [10.0.0.123] IS MANAGED\nswitch [10.0.0.124] IS MANAGED\nswitch [10.0.0.125] IS MANAGED\nswitch [10.0.0.126] IS MANAGED\nswitch [10.0.0.127] IS MANAGED\nswitch [10.0.0.128] IS MANAGED\nswitch [10.0.0.129] IS MANAGED\nswitch [10.0.0.130] IS MANAGED\nswitch [10.0.0.131] IS MANAGED\nswitch [10.0.0.132] IS MANAGED\nswitch [10.0.0.133] IS MANAGED\nswitch [10.0.0.134] IS MANAGED\nswitch [10.0.0.135] IS MANAGED\nswitch [10.0.0.136] IS MANAGED\nswitch [10.0.0.137] IS MANAGED\nswitch [10.0.0.138] IS MANAGED\nswitch [10.0.0.139] IS MANAGED\nswitch [10.0.0.140] IS MANAGED\nswitch [10.0.0.141] IS MANAGED\nswitch [10.0.0.142] IS MANAGED\nswitch [10.0.0.143] IS MANAGED\nswitch [10.0.0.144] IS MANAGED\nswitch [10.0.0.145] IS MANAGED\nswitch [10.0.0.146] IS MANAGED\nswitch [10.0.0.147] IS MANAGED\nswitch [10.0.0.148] IS MANAGED\nswitch [10.0.0.149] IS MANAGED\nswitch [10.0.0.150] IS MANAGED\nswitch [10.0.0.151] IS MANAGED\nswitch [10.0.0.152] IS MANAGED\nswitch [10.0.0.153] IS MANAGED\nswitch [10.0.0.154] IS MANAGED\nswitch [10.0.0.155] IS MANAGED\nswitch [10.0.0.156] IS MANAGED\nswitch [10.0.0.157] IS MANAGED\nswitch [10.0.0.158] IS MANAGED\nswitch [10.0.0.159] IS MANAGED\nswitch [10.0.0.160] IS MANAGED\nswitch [10.0.0.161] IS MANAGED\nswitch [10.0.0.162] IS MANAGED\nswitch [10.0.0.163] IS MANAGED\nswitch [10.0.0.164] IS MANAGED\nswitch [10.0.0.165] IS MANAGED\nswitch [10.0.0.166] IS MANAGED\nswitch [10.0.0.167] IS MANAGED\nswitch [10.0.0.168] IS MANAGED\nswitch [10.0.0.169] IS MANAGED\nswitch [10.0.0.170] IS MANAGED\nswitch [10.0.0.171] IS MANAGED\nswitch [10.0.0.172] IS MANAGED\nswitch [10.0.0.173] IS MANAGED\nswitch [10.0.0.174] IS MANAGED\nswitch [10.0.0.175] IS MANAGED\nswitch [10.0.0.176] IS MANAGED\nswitch [10.0.0.177] IS MANAGED\nswitch [10.0.0.178] IS MANAGED\nswitch [10.0.0.179] IS MANAGED\nswitch [10.0.0.180] IS MANAGED\nswitch [10.0.0.181] IS MANAGED\nswitch [10.0.0.182] IS MANAGED\nswitch [10.0.0.183] IS MANAGED\nswitch [10.0.0.184] IS MANAGED\nswitch [10.0.0.185] IS MANAGED\nswitch [10.0.0.186] IS MANAGED\nswitch [10.0.0.187] IS MANAGED\nswitch [10.0.0.188] IS MANAGED\nswitch [10.0.0.189] IS MANAGED\nswitch [10.0.0.190] IS MANAGED\nswitch [10.0.0.191] IS MANAGED\nswitch [10.0.0.192] IS MANAGED\nswitch [10.0.0.193] IS MANAGED\nswitch [10.0.0.194] IS MANAGED\nswitch [10.0.0.195] IS MANAGED\nswitch [10.0.0.196] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c0, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c1, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c2, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c3, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c4 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c4, err) = proc.communicate() 


        result = result_c0.decode() + result_c1.decode() + result_c2.decode() + result_c3.decode() + result_c4.decode()

        self.do_test(result,expected)
        

    # -------------------------------
    # test_scenario_b4_bfd
    # -------------------------------

    def test_scenario_b4_bfd(self):
        # Mejores tiempos con este arranque, primero el controlador, después switches
        scenario_test("b4_bfd", UPTIME=45, BFD_MIN_TX_ms=100, LAUNCH_CONTROLLER_MODE="First")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)
        
        
    # -------------------------------
    # test_scenario_bucle3_nobfd
    # -------------------------------
    def test_scenario_bucle3_nobfd(self):
        scenario_test("bucle3_nobfd")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)


    # -------------------------------
    # test_scenario_bucle3_bfd
    # -------------------------------
    def test_scenario_bucle3_bfd(self):
        scenario_test("bucle3_bfd", BFD_MIN_TX_ms=100)
        
        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)


    # -------------------------------
    # test_scenario_bucle4_nobfd
    # -------------------------------
    def test_scenario_bucle4_nobfd(self, LAUNCH_CONTROLLER_MODE="End"):
        scenario_test("bucle4_nobfd", LAUNCH_CONTROLLER_MODE=LAUNCH_CONTROLLER_MODE)

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)


    # -----------------------------------
    # test_scenario_2controllers (bucle4)
    # -----------------------------------
    def test_scenario_2controllers(self):
        scenario_test("2controllers", LAUNCH_CONTROLLER_MODE="End")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c0, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c1, err) = proc.communicate() 


        result = result_c0.decode() + result_c1.decode() 

        self.do_test(result,expected)



    # -----------------------------------
    # test_scenario_bucle4_bfd 
    # -----------------------------------

    def test_scenario_bucle4_bfd(self, LAUNCH_CONTROLLER_MODE="End"):
        scenario_test("bucle4_bfd", BFD_MIN_TX_ms=100, LAUNCH_CONTROLLER_MODE=LAUNCH_CONTROLLER_MODE)
        
        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\n"
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)


    # -----------------------------------
    # experiment_2_100ms_bfd_scenario
    # -----------------------------------

    def experiment_2_100ms_bfd_scenario(self):
        scenario_experiment_2("bucle4_bfd", BFD_MIN_TX_ms=100)


    # -----------------------------------
    # experiment_2_10ms_bfd_scenario
    # -----------------------------------

    def experiment_2_10ms_bfd_scenario(self):
        scenario_experiment_2("bucle4_bfd", BFD_MIN_TX_ms=10)


    # -----------------------------------
    # experiment_2_nobfd_scenario
    # -----------------------------------

    def experiment_2_nobfd_scenario(self):
        scenario_experiment_2("bucle4_nobfd")


    # -----------------------------------
    # experiment_9_100ms_bfd_scenario
    # -----------------------------------

    def experiment_9_100ms_bfd_scenario(self):
        scenario_experiment_9("bucle4_bfd", BFD_MIN_TX_ms=100)


    # -----------------------------------
    # experiment_9_10ms_bfd_scenario
    # -----------------------------------

    def experiment_9_10ms_bfd_scenario(self):
        scenario_experiment_9("bucle4_bfd", BFD_MIN_TX_ms=10)


    # -----------------------------------
    # experiment_9_nobfd_scenario
    # -----------------------------------

    def experiment_9_nobfd_scenario(self):
        scenario_experiment_9("bucle4_nobfd")

        
    # -----------------------------------
    # experiment_4_one_switch
    # -----------------------------------
        
    def experiment_4_one_switch(self):
        scenario_experiment_4("one_switch")
        

    # -----------------------------------
    # experiment_4_bucle4_bfd
    # -----------------------------------

    def experiment_4_bucle4_bfd(self):
        scenario_experiment_4("bucle4_bfd", UPTIME=45)


    # -----------------------------------
    # experiment_4_2controllers
    # -----------------------------------
    def experiment_4_2controllers(self):
        scenario_experiment_4("2controllers", UPTIME=45)


    # -----------------------------------
    # experiment_4_mesh
    # -----------------------------------

    def experiment_4_mesh(self):
        scenario_experiment_4("mesh", UPTIME=60)


    # -----------------------------------
    # experiment_4_mesh_4controllers
    # -----------------------------------
    def experiment_4_mesh_4controllers(self):
        scenario_experiment_4("mesh_4controllers", UPTIME=60)
        
        
    # -----------------------------------
    # experiment_4_1_one_switch
    # -----------------------------------

    def experiment_4_1_one_switch(self):
        scenario_experiment_4("one_switch")


    # -----------------------------------
    # experiment_4_1_bucle4_bfd
    # -----------------------------------

    def experiment_4_1_bucle4_bfd(self):
        scenario_experiment_4("bucle4_bfd", UPTIME=25)


    # -----------------------------------
    # experiment_4_1_bucle4_bfd
    # -----------------------------------
    def experiment_4_1_bucle4_nobfd(self):
        scenario_experiment_4("bucle4_nobfd", UPTIME=25)


    # -----------------------------------
    # experiment_4_1_linea15_nobfd
    # -----------------------------------

    def experiment_4_1_linea15_nobfd(self):
        scenario_experiment_4("linea15_nobfd", UPTIME=25)


    # -----------------------------------
    # experiment_5_1
    # -----------------------------------

    def experiment_5_1(self):
        scenario_experiment_5("mesh_4controllers", UPTIME=120, controllers_to_kill=["c3"])


    # -----------------------------------
    # experiment_5_2
    # -----------------------------------

    def experiment_5_2(self):
        scenario_experiment_5("mesh_4controllers", UPTIME=120, controllers_to_kill=["c3","c2"])


    # -----------------------------------
    # experiment_5_3
    # -----------------------------------

    def experiment_5_3(self):
        scenario_experiment_5("mesh_4controllers", UPTIME=120, controllers_to_kill=["c3","c2","c1"])


    # -----------------------------------
    # experiment_7_kill_c0
    # -----------------------------------

    def experiment_7_kill_c0(self):
        scenario_experiment_kill_switch("mesh_4controllers", UPTIME=60, UPTIME_AFTER_KILL=60, switch_to_kill="c0", interfaces=1)



    # -----------------------------------
    # experiment_7_kill_c1
    # -----------------------------------
    def experiment_7_kill_c1(self):
        scenario_experiment_kill_switch("mesh_4controllers", UPTIME=60, UPTIME_AFTER_KILL=60, switch_to_kill="c1", interfaces=1)


    # -----------------------------------
    # experiment_7_kill_c2
    # -----------------------------------

    def experiment_7_kill_c2(self):
        scenario_experiment_kill_switch("mesh_4controllers", UPTIME=60, UPTIME_AFTER_KILL=60, switch_to_kill="c2", interfaces=1)
        

    # -----------------------------------
    # experiment_7_kill_c3
    # -----------------------------------

    def experiment_7_kill_c3(self):
        scenario_experiment_kill_switch("mesh_4controllers", UPTIME=60, UPTIME_AFTER_KILL=60, switch_to_kill="c3", interfaces=1)


    # -----------------------------------
    # experiment_8_kill_s2
    # -----------------------------------

    def experiment_8_kill_s2(self):
        scenario_experiment_kill_switch("bucle4_bfd", UPTIME=30, UPTIME_AFTER_KILL=10, switch_to_kill="s2", interfaces=2)
        
        

    # -----------------------------------
    # test_scenario_mesh
    # -----------------------------------
        
    def test_scenario_mesh(self):
        scenario_test("mesh", UPTIME=30, LAUNCH_CONTROLLER_MODE="First")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.121] IS MANAGED\nswitch [10.0.0.122] IS MANAGED\nswitch [10.0.0.123] IS MANAGED\nswitch [10.0.0.124] IS MANAGED\nswitch [10.0.0.131] IS MANAGED\nswitch [10.0.0.132] IS MANAGED\nswitch [10.0.0.133] IS MANAGED\nswitch [10.0.0.134] IS MANAGED\nswitch [10.0.0.141] IS MANAGED\nswitch [10.0.0.142] IS MANAGED\nswitch [10.0.0.143] IS MANAGED\nswitch [10.0.0.144] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result, err) = proc.communicate() 

        result = result.decode()

        self.do_test(result,expected)
        

    # -----------------------------------
    # test_scenario_mesh_4controllers
    # -----------------------------------
        
    def test_scenario_mesh_4controllers(self):
        scenario_test("mesh_4controllers", UPTIME=60, LAUNCH_CONTROLLER_MODE="First")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.121] IS MANAGED\nswitch [10.0.0.122] IS MANAGED\nswitch [10.0.0.123] IS MANAGED\nswitch [10.0.0.124] IS MANAGED\nswitch [10.0.0.131] IS MANAGED\nswitch [10.0.0.132] IS MANAGED\nswitch [10.0.0.133] IS MANAGED\nswitch [10.0.0.134] IS MANAGED\nswitch [10.0.0.141] IS MANAGED\nswitch [10.0.0.142] IS MANAGED\nswitch [10.0.0.143] IS MANAGED\nswitch [10.0.0.144] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c0, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c1, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c2, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c3, err) = proc.communicate() 


        result = result_c0.decode() + result_c1.decode() + result_c2.decode() + result_c3.decode()

        self.do_test(result,expected)
        

    # -----------------------------------
    # experiment_scenario_mesh_4controllers
    # -----------------------------------

    def experiment_scenario_mesh_4controllers(self):
        scenario_test("mesh_4controllers", UPTIME=80)

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.121] IS MANAGED\nswitch [10.0.0.122] IS MANAGED\nswitch [10.0.0.123] IS MANAGED\nswitch [10.0.0.124] IS MANAGED\nswitch [10.0.0.131] IS MANAGED\nswitch [10.0.0.132] IS MANAGED\nswitch [10.0.0.133] IS MANAGED\nswitch [10.0.0.134] IS MANAGED\nswitch [10.0.0.141] IS MANAGED\nswitch [10.0.0.142] IS MANAGED\nswitch [10.0.0.143] IS MANAGED\nswitch [10.0.0.144] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c0, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c1, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c2, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c3, err) = proc.communicate() 


        result = result_c0.decode() + result_c1.decode() + result_c2.decode() + result_c3.decode()

        self.do_test(result,expected)


    # -----------------------------------
    # test_scenario_clos_2c_bfd
    # -----------------------------------

    def test_scenario_clos_2c_bfd(self):
        scenario_test("clos_2c_bfd", UPTIME=40, LAUNCH_CONTROLLER_MODE="First")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c1, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c3, err) = proc.communicate() 


        result = result_c1.decode() + result_c3.decode()

        self.do_test(result,expected)
        

    # -----------------------------------
    # experiment_scenario_clos_2c_bfd
    # -----------------------------------

    def experiment_scenario_clos_2c_bfd(self):
        scenario_test("clos_2c_bfd", UPTIME=60)

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c1, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c3, err) = proc.communicate() 


        result = result_c1.decode() + result_c3.decode()

        self.do_test(result,expected)
        

    # -----------------------------------
    # test_scenario_clos_4c_bfd
    # -----------------------------------

    def test_scenario_clos_4c_bfd(self):
        scenario_test("clos_4c_bfd", UPTIME=40, LAUNCH_CONTROLLER_MODE="First")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c1, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c2, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c3, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c4 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c4, err) = proc.communicate() 


        result = result_c1.decode() + result_c2.decode() + result_c3.decode() + result_c4.decode()

        self.do_test(result,expected)
        

    # -----------------------------------
    # experiment_scenario_clos_4c_bfd
    # -----------------------------------

    def experiment_scenario_clos_4c_bfd(self):
        scenario_test("clos_4c_bfd", UPTIME=60, LAUNCH_CONTROLLER_MODE="End")

        expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c1, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c2, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c3, err) = proc.communicate() 
        proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c4 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
        (result_c4, err) = proc.communicate() 


        result = result_c1.decode() + result_c2.decode() + result_c3.decode() + result_c4.decode()

        self.do_test(result,expected)
        
        



        
if __name__ == '__main__':
    setLogLevel( 'info' )
    main()
