import unittest
import pathlib
import sys
import os

import time
import pyshark


TEST_NAME       =     sys.argv[1]
EXPERIMENT_NAME =     sys.argv[2]
OUTPUT_FILE     =     sys.argv[3]
MAX_EXPERIMENTS = int(sys.argv[4])
# Position in args of the first controller log
ARGS_POSITION_OF_FIRST_CONTROLLER_LOG = 5


TEMP_FILE = "/tmp/salida"

OUTPUT_FILE_NAME=f'experiments_data/{EXPERIMENT_NAME}/{OUTPUT_FILE}'



def time_of (fileName, pattern, position):
    """
    	Returns timestamp of position line in fileName containing pattern:
	Assumes lines of fileName begin with "timestamp:"
    """
    results = []
    with open(fileName, 'r') as f:
        for line in f.readlines():
            if pattern in line:
                results.append(line)

    return results[position].split(":")[0] 



def sort (fileName):
    """ 
    returns file f'{fileName}.sorted' with contents of fileName sorted by 
    timestamp
    """

    with open(fileName, "r") as f, open(f'{fileName}.sorted', "w") as fSorted:
        lines = f.readlines()

        for line in sorted(lines, key=lambda line: line.split(":")[0]):
            print (line, file=fSorted)


            
def do_tests(test_name):
    '''
    returns True if test sucessful
    '''

    # remove temp files
    try:
        os.remove(TEMP_FILE)
        os.remove(f'{TEMP_FILE}.sorted')            
    except OSError as e:
        # no files to delete, ok
        pass

    # remove old controller log files
    for cf in range(ARGS_POSITION_OF_FIRST_CONTROLLER_LOG, len(sys.argv)):
        controller_log_file = sys.argv[cf]
        try:
            os.remove(controller_log_file)
        except OSError as e:
            # no files to delete, ok
            pass


    # run tests
    test_suite = unittest.TestLoader().loadTestsFromName(test_name)
    test_result = unittest.TextTestRunner().run(test_suite)

    return test_result.wasSuccessful()



def get_controller_log_file(first_controller_log):
    if (len(sys.argv) > (first_controller_log + 1)):
        # Si hay múltiples ficheros de log de controladores tenemos que
        # concatenarlos y ordenarlos

        # concatenar ficheros de log de controladores
        for controller_file_name in range(first_controller_log, len(sys.argv)):
            with open(sys.argv[controller_file_name], "r") as controller_file, open(TEMP_FILE, "a") as temp_file: 
                lines = controller_file.readlines()
                for line in lines:
                    print(line, file=temp_file)

        # ordenar fichero por marcas de tiempo
        sort(TEMP_FILE)
        return f'{TEMP_FILE}.sorted'
    else:
        return sys.argv[first_controller_log]        

    
        
def concat_controllers(first_controller_log):
    # concatenar ficheros de log de controladores
    for controller_file_name in range(first_controller_log, len(sys.argv)):
        with open(sys.argv[controller_file_name], "r") as controller_file, open(TEMP_FILE, "a") as temp_file: 
            lines = controller_file.readlines()
            for line in lines:
                print(line, file=temp_file)

    # ordenar fichero por marcas de tiempo
    sort(TEMP_FILE)
    controller_log_file = f'{TEMP_FILE}.sorted'

def Experiment_1_init(output_file_name, controller_log_file):
    os.remove(output_file_name)
    
def Experiment_1(output_file_name, controller_log_file):
    """Writes experiment results logged in controller_log_file to output_file

    Experiment: Network Bootstrap time (1-n controllers)

    Qué: (a) tiempo desde que el primer switch se pone en contacto con su
    controlador hasta que lo hace el último. 

    Cómo: se saca del log directamente

    """

    time_first_syn = float(time_of(controller_log_file, "SYN received", 0))
    time_last_managed = float(time_of(controller_log_file, "IS MANAGED", -1))

    with open(output_file_name, "a") as output_file:
        output_file.write(str(time_last_managed - float(time_first_syn)) + "\n" )


    
def Experiment_1_init(output_file_name, controller_log_file):
    os.remove(output_file_name)

def Experiment_2(output_file, PERIOD_S):
    current_tick = 0.0
    bytes = 0


    capture = pyshark.FileCapture("/tmp/k.pcap")

    print (output_file)

    packet_time = 0.0
    for packet in capture:
        packet_time += float(packet.frame_info.time_delta)

        if (packet_time >= current_tick + PERIOD_S) :
            print (str(current_tick)+ ", " +  str(bytes), file=output_file)
            current_tick += PERIOD_S
            bytes = 0

        try:
            bytes += int(packet.frame_info.len)
        except AttributeError:
            pass

    # last tick
    print (str(current_tick)+ ", " +  str(bytes), output_file)


    

def Experiment_2_bin_100ms(output_file, controller_log_file):
    """Writes experiment results logged in controller_log_file to output_file

    Experiment: 

    Qué: 

    Cómo: 

    """

    with open(output_file_name, "a") as output_file:
        Experiment_2(output_file, PERIOD_S = 0.1)

def Experiment_1_init(output_file_name, controller_log_file):
    for i in range(1,3):
        os.remove(f'output_file_name_{i}')

def Experiment_3(output_file_name, controller_log_file):
    """Writes experiment results logged in controller_log_file to output_file

    Experiment: Switch Bootstrap time (a distancia 1-n del controlador)

    Qué: For each added switch, we measure and average the difference
    between the instant i) when a switch is provided with the control
    flow rules; ii) when the switch was first observed by a controller

    Cómo: se saca del log directamente

    """

    for i in range(1,3):
        time_first_syn = float(time_of(controller_log_file, f'SYN received from 10.0.0.10{i}', 0))
        time_last_managed = float(time_of(controller_log_file, f'0{i}] IS MANAGED', -1))

        with open(f'output_file_name_{i}', "a") as output_file:
            output_file.write(str(time_last_managed - float(time_first_syn)) + "\n" )



    
def main():

    # create dir for experiment output if it does not exist
    print("Creating dir " + EXPERIMENT_NAME)
    pathlib.Path(f'experiments_data/{EXPERIMENT_NAME}').mkdir(parents=True, exist_ok=True)



    # Name of the initialisation method of the requested
    # EXPERIMENT_NAME
    experiment_method_init = globals()[f'{EXPERIMENT_NAME}_init'] 
    experiment_method_init(OUTPUT_FILE_NAME, controller_log_file)
            
    # repeat successfully MAX_EXPERIMENTS times the requested
    # EXPERIMENT_NAME method
    intentos_exitosos = 1
    while (intentos_exitosos <= MAX_EXPERIMENTS):
        if (do_tests(TEST_NAME)):
            print ("intentos_exitosos: " + str(intentos_exitosos))
            intentos_exitosos += 1 

            # If multiple controller log files, concat and sort them,
            # else just get its name from args
            controller_log_file = get_controller_log_file(ARGS_POSITION_OF_FIRST_CONTROLLER_LOG)

            # Name of the method of the requested EXPERIMENT_NAME
            experiment_method = globals()[EXPERIMENT_NAME] 
            experiment_method(OUTPUT_FILE_NAME, controller_log_file)



                            
if __name__ == "__main__":
    main()
