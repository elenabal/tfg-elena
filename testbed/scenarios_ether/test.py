#!/usr/bin/python

import sys
import time
import os

from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info


def doTest():

    if len(sys.argv) != 3:
        print("Usage: sudo python3 test.py [scenario node_to_kill]")
        print("scenarios available: bucle bucle_2 grande mesh")
        sys.exit(-1)
    

    UPTIME   = 10
    DOWNTIME = 10


    scenario_name = sys.argv[1]
    scenario_module = __import__(f'escenario_{scenario_name}')
    
    start_script    = f'start_scenario_{scenario_name}.py'
    print(start_script)
    stop_script     = f'stop_scenario_{scenario_name}.py'
    print(stop_script)
    node_to_kill    = sys.argv[2]
    print(node_to_kill)
    
    net, s0  = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    CLI( net , script = stop_script)

    # Launch controller in s0
    s0.cmd("source ../../venv/bin/activate")
    s0.cmd("ryu-manager --verbose   InBandController.py   >/tmp/salida 2>&1 &")
    
    

    print("START")
    for i in range(1, 3):
        CLI( net , script = start_script)    
        print("round ", i,  ": network up during ", UPTIME, "s")
        time.sleep (UPTIME) 

        CLI( net , script = stop_script)
        print("round ", i, ":  network down during ", DOWNTIME, "s")
        time.sleep (DOWNTIME) 

    print("FINISH")


    print(f'Test kill {node_to_kill}')
    CLI( net , script = start_script)    
    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 

    KILLTIME = 15
    print (f'kill {node_to_kill}')
    killString = ""
    os.system(f'sudo kill -9 `cat /tmp/mininet-{node_to_kill}/*pid`')
    time.sleep (KILLTIME)
    print(f'{node_to_kill} down during {KILLTIME} s')
        
    CLI( net , script = start_script)    
    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 

    CLI( net , script = stop_script)
    print("network down during ", DOWNTIME, "s")
    time.sleep (DOWNTIME) 

        
    net.stop()

    
if __name__ == '__main__':
    setLogLevel( 'info' )
    doTest()
