import unittest
import pathlib
import sys
import os

import time
import pyshark


TEST_NAME       =     sys.argv[1]
EXPERIMENT_NAME =     sys.argv[2]
OUTPUT_FILE     =     sys.argv[3]
MAX_EXPERIMENTS = int(sys.argv[4])
# Position in args of the first controller log
ARGS_POSITION_OF_FIRST_CONTROLLER_LOG = 5


TEMP_FILE = "/tmp/salida"

OUTPUT_FILE_NAME=f'experiments_data/{EXPERIMENT_NAME}/{OUTPUT_FILE}'



def time_of (fileName, pattern, position):
    """
    	Returns timestamp of position line in fileName containing pattern:
	Assumes lines of fileName begin with "timestamp:"
    """
    results = []
    with open(fileName, 'r') as f:
        for line in f.readlines():
            if pattern in line:
                results.append(line)

    return results[position].split(":")[0] 


def time_of_connected (fileName, pattern, position):
    """
        Returns timestamp of position line in fileName containing pattern:
        Not assume lines begin with "timestamp:", then search for next line with timestamp
    """
    results = []
    pos = 0
    with open(fileName, 'r') as f:
        l = f.readlines()
        for i in range(1,len(l)):
            if pattern in l[i]: 
                if (pos==position): 
                    res = ""
                    j = i+1
                    while (res=="" and j<len(l)):
                       if " : " in l[j]:
                           res = l[j].split(":")[0]
                       j = j+1
                    return res
                else:
                   pos = pos + 1
                
def connected_switches (fileName):
    """
        Returns timestamp of position line in fileName containing pattern:
        Not assume lines begin with "timestamp:", then search for next line with timestamp
    """
    results = []
    pattern = "address:"
    switches_str=""
    with open(fileName, 'r') as f:
        l = f.readlines()
        for i in range(1,len(l)):
            if pattern in l[i]: 
                el = l[i].split('(')[1]
                ip_addr = el.split(',')[0]
                switches_str = switches_str + ip_addr + " "
    return switches_str 

def sort (fileName):
    """ 
    returns file f'{fileName}.sorted' with contents of fileName sorted by 
    timestamp
    """

    with open(fileName, "r") as f, open(f'{fileName}.sorted', "w") as fSorted:
        lines = f.readlines()

        for line in sorted(lines, key=lambda line: line.split(":")[0]):
            print (line, file=fSorted)


def remove_temp_files():
    # remove temp files
    try:
        os.remove(TEMP_FILE)
        os.remove(f'{TEMP_FILE}.sorted')            
    except OSError as e:
        # no files to delete, ok
        pass

    
def do_tests(test_name):
    '''
    returns True if test sucessful
    '''

    remove_temp_files()
    

    # remove old controller log files
    for cf in range(ARGS_POSITION_OF_FIRST_CONTROLLER_LOG, len(sys.argv)):
        controller_log_file = sys.argv[cf]
        try:
            os.remove(controller_log_file)
        except OSError as e:
            # no files to delete, ok
            pass


    # run tests
    test_suite = unittest.TestLoader().loadTestsFromName(test_name)
    test_result = unittest.TextTestRunner().run(test_suite)

    return test_result.wasSuccessful()



def get_controller_log_file(first_controller_log):
    if (len(sys.argv) > (first_controller_log + 1)):
        # Si hay múltiples ficheros de log de controladores tenemos que
        # concatenarlos y ordenarlos

        # concatenar ficheros de log de controladores
        for controller_file_name in range(first_controller_log, len(sys.argv)):
            with open(sys.argv[controller_file_name], "r") as controller_file, open(TEMP_FILE, "a") as temp_file: 
                lines = controller_file.readlines()
                for line in lines:
                    print(line, file=temp_file)

        # ordenar fichero por marcas de tiempo
        sort(TEMP_FILE)
        return f'{TEMP_FILE}.sorted'
    else:
        return sys.argv[first_controller_log]        

def get_unique_controller_log_file(controller_log_file_list):
    if len(controller_log_file_list) == 1:
        return controller_log_file_list[0]

    for controller_log_file in controller_log_file_list:
        # Si hay múltiples ficheros de log de controladores tenemos que
        # concatenarlos y ordenarlos
        with open(controller_log_file, "r") as controller_file, open(TEMP_FILE, "a") as temp_file: 
            lines = controller_file.readlines()
            for line in lines:
                print(line, file=temp_file)

    # ordenar fichero por marcas de tiempo
    sort(TEMP_FILE)
    return f'{TEMP_FILE}.sorted'




def get_list_controller_log_file(first_controller_log):
    controller_log_list = []
    if (len(sys.argv) > (first_controller_log + 1)):
        # Si hay múltiples ficheros de log de controladores tenemos que
        # tener todos los nombres

        # concatenar ficheros de log de controladores
        for index in range(first_controller_log, len(sys.argv)):
             controller_log_list.append(sys.argv[index])

    else:
        controller_log_list.append(sys.argv[first_controller_log])        
    
    print("Controller_log_list=", controller_log_list)

    return controller_log_list

    
        
def concat_controllers(first_controller_log):
    # concatenar ficheros de log de controladores
    for controller_file_name in range(first_controller_log, len(sys.argv)):
        with open(sys.argv[controller_file_name], "r") as controller_file, open(TEMP_FILE, "a") as temp_file: 
            lines = controller_file.readlines()
            for line in lines:
                print(line, file=temp_file)

    # ordenar fichero por marcas de tiempo
    sort(TEMP_FILE)
    controller_log_file = f'{TEMP_FILE}.sorted'



# -----------------------------
# Experiment_1
# -----------------------------

def Experiment_1_init(output_file_name):
    # Borra los ficheros de salida, normalmente /tmp/salida-xn
    if os.path.exists(output_file_name):
        os.remove(output_file_name)
    
def Experiment_1(output_file_name, controller_log_file_list):
    """Writes experiment results logged in controller_log_file to output_file

    Experiment: Network Bootstrap time (1-n controllers)

    Qué: (a) tiempo desde que el primer switch se pone en contacto con su
    controlador hasta que lo hace el último. 

    Cómo: se saca del log directamente

    """
     
    # solo debería haber un controlador en la lista
    controller_log_file = controller_log_file_list[0]

    # Time of second switch connected to controller, first switch is controller switch.
    time_first_connected = float(time_of_connected(controller_log_file, "address:", 1))
    time_last_managed = float(time_of(controller_log_file, "IS MANAGED", -1))

    with open(output_file_name, "a") as output_file:
        output_file.write(str(time_last_managed - float(time_first_connected)) + "\n" )


# -----------------------------
# Experiment_1_multi_controller
# -----------------------------

def Experiment_1_multi_controller_init(output_file_name):
    # Borra los ficheros de salida, normalmente /tmp/salida-xn
    
    file_list = [output_file_name + "_max_for_controller", 
                 output_file_name + "_switches_for_controller", 
                 output_file_name + "_from_first_to_last"] 

    for file in file_list:       
        if os.path.exists(file):
            os.remove(file)


    
def Experiment_1_multi_controller(output_file_name, controller_log_file_list):
    """Writes experiment results logged in controller_log_file to output_file

    Experiment: Network Bootstrap time (1-n controllers)

    METHOD 1:
        Qué: para cada controlador se calcula el tiempo que tardan en 
        conectarse sus switches. Se toma el valor máximo de todos los controladores.

        Cómo: se saca de los log de cada controlador 

    METHOD 2:
        Qué: se calcula el tiempo desde que el primer switch se conecta hasta el
        último. 

        Cómo: se saca concatenando los log de cada controlador 

    """

    # ---------
    # METHOD 1
    # ---------
    output_file_method1 = output_file_name + "_max_for_controller"
    # Tiempo de conexión con cada controlador
    connection_time_list = []
    time_str=""
    for controller_log_file in controller_log_file_list:
        try:
            time_first_connected = float(time_of_connected(controller_log_file, "address:", 1))
            time_last_managed = float(time_of(controller_log_file, "IS MANAGED", -1))
            connection_time=time_last_managed - time_first_connected
            connection_time_list.append(connection_time)
            time_str = time_str + " " + str(connection_time) 
        except:
            # ocurre si ningún switch se conecta a un determinado controlador
            pass

    # calculo el maximo
    print("Lista de tiempos=", connection_time_list)
    max = connection_time_list[0]
    for i in range(0, len(connection_time_list)):
        if (connection_time_list[i] > max):
            max = connection_time_list[i]

    with open(output_file_method1, "a") as output_file:
        output_file.write(time_str + "\n" )

    #
    # Obtain switches connected to controller
    #
    output_file_switches = output_file_name + "_switches_for_controller"
    sw_str=""
    for controller_log_file in controller_log_file_list:
        try:
            switches = connected_switches(controller_log_file)
            sw_str = sw_str + "(" + controller_log_file + ", " + switches + ") "
        except:
            # ocurre si ningún switch se conecta a un determinado controlador
            pass

    with open(output_file_switches, "a") as output_file:
        output_file.write(sw_str + "\n" )

    # ---------
    # METHOD 2
    # ---------

    output_file_method2 = output_file_name + "_from_first_to_last"

    # si hay varios controladores, concatena y ordena todos los logs en un fichero
    controller_log_file =get_unique_controller_log_file(controller_log_file_list)

    time_first_connected = float(time_of(controller_log_file, "IS MANAGED", 0))
    time_last_managed = float(time_of(controller_log_file, "IS MANAGED", -1))
    time_str=str(time_last_managed - time_first_connected)

    with open(output_file_method2, "a") as output_file:
        output_file.write(time_str + "\n" )



# -----------------------------
# Experiment_2
# -----------------------------
    
def Experiment_2_bin_100ms_init(output_file_name):
    if os.path.exists(output_file_name):
        os.remove(output_file_name)

    if os.path.exists(f'{output_file_name}-iat'):
        os.remove(f'{output_file_name}-iat')
        

def _Experiment_2(output_file, output_file_iat, PERIOD_S):
    """Expects tcpdump capture in /tmp/k.pcap

    Qué: Medimos con iperf entre 2 hosts separados al máximo. Elegimos
    un enlace en el camino principal que tenga un backup y lo hacemos
    fallar.

    Genera medidas de throughput en output_file y de Inter Arrival Time (iat) en f'{output_file}-iat

    """

    current_tick = 0.0
    bytes = 0
    sum_delta_time = 0.0

    capture = pyshark.FileCapture("/tmp/k.pcap")


    packet_time = 0.0
    packets = 0
    for packet in capture:
        packet_time += float(packet.frame_info.time_delta)
        packets += 1
        
        if (packet_time >= current_tick + PERIOD_S) :
            print (str(current_tick)+ ", " +  str(bytes), file=output_file)
            print (str(current_tick)+ ", " +  str(sum_delta_time / packets), file=output_file_iat)

            current_tick += PERIOD_S
            bytes = 0

            packets = 0
            sum_delta_time = 0

        try:
            bytes += int(packet.frame_info.len)
            sum_delta_time += float(packet.frame_info.time_delta)            
        except AttributeError:
            pass

    capture.close()

    # last tick
    print (str(current_tick)+ ", " +  str(bytes), output_file)
    if (packets != 0):
        print (str(current_tick)+ ", " +  str(sum_delta_time / packets), file=output_file_iat)

    

def Experiment_2_bin_100ms(output_file_name, controller_log_file_list):
    """Writes experiment results logged in controller_log_file to output_file

    Experiment: 

    Qué: 

    Cómo: 

    """

    # si hay varios controladores, concatena y ordena todos los logs en un fichero
    # no se usa en este experimento porque las medidas se realizan sobre las capturas
    controller_log_file =get_unique_controller_log_file(controller_log_file_list)

    with open(output_file_name, "a") as output_file:
        with open(f'{output_file_name}-iat', "a") as output_file_iat:
            _Experiment_2(output_file, output_file_iat, PERIOD_S = 0.1)


# -----------------------------
# Experiment_9
# -----------------------------
def Experiment_9_bin_100ms_init(output_file_name):
    if os.path.exists(output_file_name):
        os.remove(output_file_name)

        
def Experiment_9_bin_100ms(output_file_name, controller_log_file_list):
    """Expects tcpdump capture in /tmp/k.pcap

    Qué: Medimos con iperf TCP entre 2 hosts separados al máximo. Elegimos
    un enlace en el camino principal que tenga un backup y lo hacemos
    fallar.

    Genera medidas de segmentos repetidos en output_file_name

    """

    os.system(f'su sdnwifi -c "python3 scripts/experiment_9.py /tmp/k 0.1" ')

    os.system(f'mv /tmp/k {output_file_name}')

            

# -----------------------------
# Experiment_3
# -----------------------------
def Experiment_3_init(output_file_name):
    for i in range(1,16):
        if os.path.exists(f'{output_file_name}_{i}'):
            os.remove(f'{output_file_name}_{i}')


            
def Experiment_3(output_file_name, controller_log_file_list):
    """Writes experiment results logged in controller_log_file to output_file

    Experiment: Switch Bootstrap time (a distancia 1-n del controlador)

    Qué: For each added switch, we measure and average the difference
    between the instant i) when a switch is provided with the control
    flow rules; ii) when the switch was first observed by a controller

    Cómo: se saca del log directamente

    """

    # En este experimento sólo hay un controlador
    controller_log_file = controller_log_file_list[0]

    for i in range(1,10):
        ip_address_pattern = "address:('10.0.0.10" + str(i)
        time_connected_socket = float(time_of_connected(controller_log_file, ip_address_pattern, 0))
        #time_first_syn = float(time_of(controller_log_file, f'SYN received from 10.0.0.10{i}', 0))
        time_last_managed = float(time_of(controller_log_file, f'0{i}] IS MANAGED', 0))

        with open(f'{output_file_name}_{i}', "a") as output_file:
            output_file.write(str(time_last_managed - float(time_connected_socket)) + "\n" )

    for i in range(10,16):
        ip_address_pattern = "address:('10.0.0.1" + str(i)
        time_connected_socket = float(time_of_connected(controller_log_file, ip_address_pattern, 0))
        #time_first_syn = float(time_of(controller_log_file, f'SYN received from 10.0.0.1{i}', 0))
        time_last_managed = float(time_of(controller_log_file, f'{i}] IS MANAGED', -1))

        with open(f'{output_file_name}_{i}', "a") as output_file:
            output_file.write(str(time_last_managed - float(time_connected_socket)) + "\n" )
            

# -----------------------------
# Experiment_3_1
# -----------------------------

def Experiment_3_1_init(output_file_name):
    for i in range(1,16):
        if os.path.exists(f'{output_file_name}{i}'):
            os.remove(f'{output_file_name}{i}')


    
def Experiment_3_1(output_file_name, controller_log_file_list):
    """Writes experiment results logged in controller_log_file to output_file

    Experiment: Switch Bootstrap time (a distancia 1-n del controlador)

    Qué: For each added switch, we measure and average until switch n is MANAGED

    Cómo: se saca del log directamente

    """

    # En este experimento sólo hay un controlador
    controller_log_file = controller_log_file_list[0]

    time_connected_socket = float(time_of_connected(controller_log_file, "address:", 1))

    for i in range(1,10):
        time_last_managed = float(time_of(controller_log_file, f'0{i}] IS MANAGED', 0))

        with open(f'{output_file_name}{i}', "a") as output_file:
            output_file.write(str(time_last_managed - time_connected_socket) + "\n" )

    for i in range(10,16):
        time_last_managed = float(time_of(controller_log_file, f'{i}] IS MANAGED', -1))

        with open(f'{output_file_name}{i}', "a") as output_file:
            output_file.write(str(time_last_managed - time_connected_socket) + "\n" )

# -----------------------------
# Experiment_4
# -----------------------------

def Experiment_4_init(output_file_name):
    if os.path.exists(output_file_name):
        os.remove(output_file_name)

def Experiment_4(output_file_name, controller_log_file_list):
    """ Expects tcpdump capture in /tmp/*.pcap
    """

    """Writes experiment results logged in controller_log_file to output_file

    Experiment: 

    Qué: 

    Cómo: Counts packets before an special probe message sent from the
    controller with src port 1234 and dst port 1234 just to mark the
    time a switch finishes its bootstratp

    """

    packet_count = 0
            
    for file in os.listdir("/tmp"):
        if file.endswith(".pcap"):
            
            capture = pyshark.FileCapture(f'/tmp/{file}')

            for pkt in capture:
                if (hasattr(pkt, "udp") and pkt.udp.srcport == 4321):
                    break
                else:
                    packet_count += 1

            capture.close()
        
    with open(output_file_name, "a") as output_file:
        print (str(packet_count), file=output_file)


# -----------------------------
# Experiment_4_1
# -----------------------------

def Experiment_4_1_init(output_file_name):
    if os.path.exists(output_file_name):
        os.remove(output_file_name)

def Experiment_4_1(output_file_name, controller_log_file):
    """ Expects tcpdump capture in /tmp/*.pcap
    """

    """Writes experiment results logged in controller_log_file to output_file

    Experiment:

    Qué:

    Cómo: Counts bytes Ethernet frames before an special probe message sent from the
    controller with src port 1234 and dst port 1234 just to mark the
    time a switch finishes its bootstratp

    """

    bytes = 0

    for file in os.listdir("/tmp"):
        if file.endswith(".pcap"):

            capture = pyshark.FileCapture(f'/tmp/{file}')

            for pkt in capture:
                if (hasattr(pkt, "udp") and pkt.udp.srcport == 4321):
                    break
                elif (hasattr(pkt, "tcp")):
                    bytes += int(pkt.ip.len) + 18 # Cabecera Ethernet

            capture.close()

    with open(output_file_name, "a") as output_file:
        print (str(bytes), file=output_file)


# --------------------------------
# Experiment_5
# --------------------------------

    
def Experiment_5_init(output_file_name):
    if os.path.exists(output_file_name):
        os.remove(output_file_name)
    

def Experiment_5(output_file_name, controller_log_file_list):
    """Writes experiment results logged in controller_log_file to output_file

    Experiment: Controller recovery time (1-n)

    Qué: tiempo desde que se paran n controladores hasta que todos los
    switches están con otro controlador

    Cómo: arrancamos escenario mesh. Esperamos a que estén
    todos. Matamos el nodo de n controladores. En el log fusionado de
    todos los controladores, medimos tiempo desde el primer NOT
    MANAGED hasta el último MANAGED. Esto funciona porque a pesar de
    matar el nodo en el que está corriendo el controlador, el proceso
    del controlador sigue funcionando e imprime en su traza los NOT
    MANAGED.

    """

    # Obtener un único fichero de log a partir de los ficheros de cada controlador
    controller_log_file = get_unique_controller_log_file(controller_log_file_list)

    time_first_not_managed = float(time_of(controller_log_file, "IS NOT MANAGED", 0))
    time_last_managed = float(time_of(controller_log_file, "IS MANAGED", -1))

    with open(output_file_name, "a") as output_file:
        output_file.write(str(time_last_managed - float(time_first_not_managed)) + "\n" )
        

        
# --------------------------------
# Experiment_6
# --------------------------------

def get_log_file(controller_log_file_list, start_log_time):
    """Concatena las líneas devueltas por el script
    scripts/experiment_6.sh para cada controlador y luego las ordena

    """
    remove_temp_files()
    
    for log_file in controller_log_file_list:
        print (log_file)
        os.system(f'./scripts/experiment_6.sh {log_file} {TEMP_FILE} {start_log_time}')
        
    sort(TEMP_FILE)
    return f'{TEMP_FILE}.sorted'
    


        
def Experiment_6_init(output_file_name):
    if os.path.exists(output_file_name):
        os.remove(output_file_name)

    
    
def Experiment_6(output_file_name, controller_log_file_list):
    """Writes experiment results logged in controller_log_file to
    output_file 

    Qué: Tiempo desde que un controlador conoce a otro hasta
    que todos conocen a todos

    Cómo: egrep "informing of new" /tmp/salida-c0 | awk '{if
    (!IP[$NF]) IP[$NF]=$1;} END{for (var in IP) print IP[var] "\t"
    var}' Se llama a este script en ./scripts/experiment_6.sh, llamado
    desde get_log_file()

    """
    # Get first controller

    log_file = get_log_file(controller_log_file_list, "0")
    
    time_first_controller_known_by_any_other_controller = float(time_of(log_file, "11.0.0", 0))
    time_last_controller_known_by_any_other_controller  = float(time_of(log_file, "11.0.0", -1))

    with open(output_file_name, "a") as output_file:
        output_file.write(str(time_last_controller_known_by_any_other_controller - float(time_first_controller_known_by_any_other_controller)) + "\n" )




# --------------------------------
# Experiment_7
# --------------------------------

def Experiment_7_init(output_file_name):
    if os.path.exists(output_file_name):
        os.remove(output_file_name)

    
    
def Experiment_7(output_file_name, controller_log_file_list):
    """Writes experiment results logged in controller_log_file to
    output_file 

    Qué: Tiempo desde que falla un controlador en el camino entre 2
    controladores no adyacentes hasta que estos pueden seguir
    comunicándose tras haberse reconectado los switches del
    controlador que ha fallado con otros controladores

    Cómo En mesh_4controllers matamos uno u otro controlador: c0, c1,
    c2 ó c3

    """

    with open("/tmp/kill_time.txt", "r") as f:
        kill_time = f.read()
        log_file = get_log_file(controller_log_file_list, kill_time)
    
    time_first_controller_known_by_any_other_controller = float(time_of(log_file, "11.0.0", 0))
    time_last_controller_known_by_any_other_controller  = float(time_of(log_file, "11.0.0", -1))

    with open(output_file_name, "a") as output_file:
        output_file.write(str(time_last_controller_known_by_any_other_controller - float(time_first_controller_known_by_any_other_controller)) + "\n" )


    
# --------------------------------
# Experiment_8
# --------------------------------

def Experiment_8_init(output_file_name):
    if os.path.exists(output_file_name):
        os.remove(output_file_name)

    
def Experiment_8(output_file_name, controller_log_file_list):
    """Writes experiment results logged in controller_log_file to
    output_file 

    Qué: Tiempo desde que se mata un switch hasta que el 
    controlador lo detecta.

    Cómo En bucle4_bfd se mata s2 y se mide el tiempo desde que se
         mata el switch hasta que el controlador detecta NOT MANAGED

    """

    f = open("/tmp/kill_time.txt", "r") 
    kill_time = f.read()
    f.close()
    
    # escenario con un único controlador
    controller_log_file = controller_log_file_list[0]
    time_not_managed = float(time_of(controller_log_file, "switch [10.0.0.102] IS NOT MANAGED", 0))

    with open(output_file_name, "a") as output_file:
        output_file.write(str(time_not_managed - float(kill_time)) + "\n" )
        


# --------------------------------
# main
# --------------------------------
        
def main():

    # create dir for experiment output if it does not exist
    print("Creating dir " + EXPERIMENT_NAME)
    pathlib.Path(f'experiments_data/{EXPERIMENT_NAME}').mkdir(parents=True, exist_ok=True)



    # Call method that initializes the experiment, named
    # f'{EXPERIMENT_NAME}_init'
    experiment_method_init = globals()[f'{EXPERIMENT_NAME}_init'] 
    experiment_method_init(OUTPUT_FILE_NAME)
            
    # repeat successfully MAX_EXPERIMENTS times the requested
    # EXPERIMENT_NAME method
    intentos_exitosos = 1
    while (intentos_exitosos <= MAX_EXPERIMENTS):
        if (do_tests(TEST_NAME)):
            print ("intentos_exitosos: " + str(intentos_exitosos))
            intentos_exitosos += 1 

            # If multiple controller log files, obtain all controller log files
            # If one controller -> list with one element
            controller_log_file_list= get_list_controller_log_file(ARGS_POSITION_OF_FIRST_CONTROLLER_LOG)


            # Call method of the experiment, named EXPERIMENT_NAME
            experiment_method = globals()[EXPERIMENT_NAME] 
            experiment_method(OUTPUT_FILE_NAME, controller_log_file_list)



                            
if __name__ == "__main__":
    main()
