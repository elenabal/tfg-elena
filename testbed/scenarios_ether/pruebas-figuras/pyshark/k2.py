# en s1 ejecuto este script

import pyshark


capture = pyshark.LiveCapture(interface='s1-eth0', bpf_filter='dst host 10.0.0.1 and udp port 5001')
capture.sniff(packet_count=5)
for packet in capture:
    print(packet.frame_info.time_delta)
    print(packet.data.len)

PERIOD_MS = 100

next_tick = PERIOD_MS


