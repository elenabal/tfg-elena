import csv 

import matplotlib as mpl
# Use the pgf backend (must be done before import pyplot interface)
mpl.use('pgf')

import matplotlib.pyplot as plt
from scipy.stats import norm

import numpy as np
import seaborn as sns

from os import listdir
from os.path import isfile, join

####### CONFIG ######
DATA_PATH="data" # directorio en el que están los ficheros con datos a
                 # representar gráficamente

X_LABEL = "Topology name"
Y_LABEL = "Bootstrap time (s)"


# Use the seborn style
plt.style.use('seaborn')
# But with fonts from the document body
plt.rcParams.update({
    "font.family": "serif",  # use serif/main font for text elements
    "text.usetex": True,     # use inline math for ticks
    "pgf.rcfonts": False     # don't setup fonts from rc parameters
    })



def set_axis_style(ax, labels):
    print(labels)
    ax.yaxis.set_ticks_position('left')
    ax.set_xticks(np.arange(0, len(labels)), labels=labels)
    ax.set_xticklabels(labels)

    ax.set_xlabel(X_LABEL)
    ax.set_ylabel(Y_LABEL)    



    

def set_size(width_pt, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to sit nicely in our document.

    Parameters
    ----------
    width_pt: float
            Document width in points
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)


def Experiment_1(experiment_name):
    # Leemos los datos de los ficheros. Nombre de fichero es nombre de
    # etiqueta en eje X

    files = [f for f in listdir(DATA_PATH) if isfile(join(DATA_PATH, f))]
    files.sort()

    df = []
    labels= []
    for file_name in files:
        labels.append(file_name)
        y = []
        with open(join(DATA_PATH,file_name)) as f:
            lines = f.readlines()
            for row in lines:
                y.append(float(row))

        # eliminamos valores max y min de y
        y = [i for i in y if i > min(y) and i < max(y)]

        df.append(y)



    # ver cómo determinar figsize:
    # https://jwalton.info/Embed-Publication-Matplotlib-Latex/
    fig, ax = plt.subplots(1, 1, figsize=set_size(252.560885))
    sns.set(style="whitegrid")


    # violinplot o boxplot
    sns.violinplot(data=df, ax = ax, orient ='v')
    #sns.boxplot(data=df, ax = ax, orient ='v')

    set_axis_style(ax,labels)
    
    fig.tight_layout()
    fig.savefig(f'{experiment_name}.pgf', format='pgf')

    

if __name__ == "__main__":
    Experiment_1("Experiment_1")
