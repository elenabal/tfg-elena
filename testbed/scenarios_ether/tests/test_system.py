import subprocess
import sys
import time
import os

from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info

import pytest
import re


# -------------------------
# launch_controller
# -------------------------
def launch_controller(node, config_file, log_file):
    # Launch controller in node
    #node.cmd("source ../../../sdnenv/bin/activate")
    node.cmd("source /home/sdnwifi/sdnenv/bin/activate")
    ryu_command=f'ryu-manager --observe-links --verbose   ./InBandController.py  --config-file {config_file} >{log_file} 2>&1 &'
    print(ryu_command)
    node.cmd(ryu_command)


# -------------------------
# scenario_test
# -------------------------
def scenario_test(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None, LAUNCH_CONTROLLER_MODE="End"):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    start_controller_switch_sh = f'{SCENARIO_DIR}/start_controller_switch_{scenario_name}.sh' 
    start_other_switches_sh = f'{SCENARIO_DIR}/start_other_switches_{scenario_name}.sh' 
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)
    else:
        CLI( net , script = stop_script_py)
        

    if LAUNCH_CONTROLLER_MODE == "First":
        print("START CONTROLLER NODE/S")
        print("#########################\n")

        os.system(start_controller_switch_sh)        

        print("FIRST START CONTROLLER/S")
        print("#########################\n")

        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


        print("START REST OF NODES/S")
        print("#########################\n")
        os.system(start_other_switches_sh)        

    else:
        print("START ALL NODES/S")
        print("#########################\n")
        if (os.path.exists(start_script_sh)):
            if (BFD_MIN_TX_ms != None):
                os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
            else:
                os.system(start_script_sh)        
        else:
            CLI( net , script = start_script_py)    

        print("LAUNCH CONTROLLER/S AT THE END")
        print("#########################\n")
        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 

    
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)        
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")

# -------------------------
# scenario_test_tabla_openflow. Switch s1
# -------------------------
def scenario_test_tabla_openflow_s1(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None, LAUNCH_CONTROLLER_MODE="End"):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    start_controller_switch_sh = f'{SCENARIO_DIR}/start_controller_switch_{scenario_name}.sh' 
    start_other_switches_sh = f'{SCENARIO_DIR}/start_other_switches_{scenario_name}.sh' 
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)
    else:
        CLI( net , script = stop_script_py)
        

    if LAUNCH_CONTROLLER_MODE == "First":
        print("START CONTROLLER NODE/S")
        print("#########################\n")

        os.system(start_controller_switch_sh)        

        print("FIRST START CONTROLLER/S")
        print("#########################\n")

        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


        print("START REST OF NODES/S")
        print("#########################\n")
        os.system(start_other_switches_sh)        

    else:
        print("START ALL NODES/S")
        print("#########################\n")
        if (os.path.exists(start_script_sh)):
            if (BFD_MIN_TX_ms != None):
                os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
            else:
                os.system(start_script_sh)        
        else:
            CLI( net , script = start_script_py)    

        print("LAUNCH CONTROLLER/S AT THE END")
        print("#########################\n")
        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 

    #Launch ./dumpAllFlows switch
    print("launch ./dumpAllFlows en s1")    
    s1 = net.getNodeByName("s1")
    s1.cmd("rm /tmp/dumpflow-s1.txt")
    s1.cmd("./dumpAllFlows.sh s1 > /tmp/dumpflow-s1.txt")

    
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)        
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
  
# -------------------------
# scenario_test_tabla_openflow. Switch s21
# -------------------------
def scenario_test_tabla_openflow_s21(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None, LAUNCH_CONTROLLER_MODE="End"):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    start_controller_switch_sh = f'{SCENARIO_DIR}/start_controller_switch_{scenario_name}.sh' 
    start_other_switches_sh = f'{SCENARIO_DIR}/start_other_switches_{scenario_name}.sh' 
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)
    else:
        CLI( net , script = stop_script_py)
        

    if LAUNCH_CONTROLLER_MODE == "First":
        print("START CONTROLLER NODE/S")
        print("#########################\n")

        os.system(start_controller_switch_sh)        

        print("FIRST START CONTROLLER/S")
        print("#########################\n")

        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


        print("START REST OF NODES/S")
        print("#########################\n")
        os.system(start_other_switches_sh)        

    else:
        print("START ALL NODES/S")
        print("#########################\n")
        if (os.path.exists(start_script_sh)):
            if (BFD_MIN_TX_ms != None):
                os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
            else:
                os.system(start_script_sh)        
        else:
            CLI( net , script = start_script_py)    

        print("LAUNCH CONTROLLER/S AT THE END")
        print("#########################\n")
        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 

    #Launch ./dumpAllFlows switch
    print("launch ./dumpAllFlows en s21")    
    s21 = net.getNodeByName("s21")
    s21.cmd("rm /tmp/dumpflow-s21.txt")
    s21.cmd("./dumpAllFlows.sh s21 > /tmp/dumpflow-s21.txt")

    
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)        
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
              
# -------------------------
# scenario_test_tabla_openflow_. Switch 3.
# -------------------------
def scenario_test_tabla_openflow_s3(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None, LAUNCH_CONTROLLER_MODE="End"):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    start_controller_switch_sh = f'{SCENARIO_DIR}/start_controller_switch_{scenario_name}.sh' 
    start_other_switches_sh = f'{SCENARIO_DIR}/start_other_switches_{scenario_name}.sh' 
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)
    else:
        CLI( net , script = stop_script_py)
        

    if LAUNCH_CONTROLLER_MODE == "First":
        print("START CONTROLLER NODE/S")
        print("#########################\n")

        os.system(start_controller_switch_sh)        

        print("FIRST START CONTROLLER/S")
        print("#########################\n")

        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


        print("START REST OF NODES/S")
        print("#########################\n")
        os.system(start_other_switches_sh)        

    else:
        print("START ALL NODES/S")
        print("#########################\n")
        if (os.path.exists(start_script_sh)):
            if (BFD_MIN_TX_ms != None):
                os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
            else:
                os.system(start_script_sh)        
        else:
            CLI( net , script = start_script_py)    

        print("LAUNCH CONTROLLER/S AT THE END")
        print("#########################\n")
        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 

    #Launch ./dumpAllFlows switch
    print("launch ./dumpAllFlows en s3")    
    s3 = net.getNodeByName("s3")
    s3.cmd("rm /tmp/dumpflow-s3.txt")
    s3.cmd("./dumpAllFlows.sh s3 > /tmp/dumpflow-s3.txt")

    
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)        
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
        

# -------------------------
# scenario_test_tabla_openflow. Switch s4
# -------------------------
def scenario_test_tabla_openflow_s4(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None, LAUNCH_CONTROLLER_MODE="End"):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    start_controller_switch_sh = f'{SCENARIO_DIR}/start_controller_switch_{scenario_name}.sh' 
    start_other_switches_sh = f'{SCENARIO_DIR}/start_other_switches_{scenario_name}.sh' 
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)
    else:
        CLI( net , script = stop_script_py)
        

    if LAUNCH_CONTROLLER_MODE == "First":
        print("START CONTROLLER NODE/S")
        print("#########################\n")

        os.system(start_controller_switch_sh)        

        print("FIRST START CONTROLLER/S")
        print("#########################\n")

        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


        print("START REST OF NODES/S")
        print("#########################\n")
        os.system(start_other_switches_sh)        

    else:
        print("START ALL NODES/S")
        print("#########################\n")
        if (os.path.exists(start_script_sh)):
            if (BFD_MIN_TX_ms != None):
                os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
            else:
                os.system(start_script_sh)        
        else:
            CLI( net , script = start_script_py)    

        print("LAUNCH CONTROLLER/S AT THE END")
        print("#########################\n")
        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 

    #Launch ./dumpAllFlows switch
    print("launch ./dumpAllFlows en s4")    
    s4 = net.getNodeByName("s4")
    s4.cmd("rm /tmp/dumpflow-s4.txt")
    s4.cmd("./dumpAllFlows.sh s4 > /tmp/dumpflow-s4.txt")

    
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)        
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
        

# -------------------------
# scenario_test_tabla_openflow. Switch s5
# -------------------------
def scenario_test_tabla_openflow_s5(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None, LAUNCH_CONTROLLER_MODE="End"):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    start_controller_switch_sh = f'{SCENARIO_DIR}/start_controller_switch_{scenario_name}.sh' 
    start_other_switches_sh = f'{SCENARIO_DIR}/start_other_switches_{scenario_name}.sh' 
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)
    else:
        CLI( net , script = stop_script_py)
        

    if LAUNCH_CONTROLLER_MODE == "First":
        print("START CONTROLLER NODE/S")
        print("#########################\n")

        os.system(start_controller_switch_sh)        

        print("FIRST START CONTROLLER/S")
        print("#########################\n")

        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


        print("START REST OF NODES/S")
        print("#########################\n")
        os.system(start_other_switches_sh)        

    else:
        print("START ALL NODES/S")
        print("#########################\n")
        if (os.path.exists(start_script_sh)):
            if (BFD_MIN_TX_ms != None):
                os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
            else:
                os.system(start_script_sh)        
        else:
            CLI( net , script = start_script_py)    

        print("LAUNCH CONTROLLER/S AT THE END")
        print("#########################\n")
        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 

    #Launch ./dumpAllFlows switch
    print("launch ./dumpAllFlows en s5")    
    s5 = net.getNodeByName("s5")
    s5.cmd("rm /tmp/dumpflow-s5.txt")
    s5.cmd("./dumpAllFlows.sh s5 > /tmp/dumpflow-s5.txt")

    
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)        
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
        
# -------------------------
# scenario_test_tabla_openflow. Switch s15
# -------------------------
def scenario_test_tabla_openflow_s15(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None, LAUNCH_CONTROLLER_MODE="End"):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    start_controller_switch_sh = f'{SCENARIO_DIR}/start_controller_switch_{scenario_name}.sh' 
    start_other_switches_sh = f'{SCENARIO_DIR}/start_other_switches_{scenario_name}.sh' 
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)
    else:
        CLI( net , script = stop_script_py)
        

    if LAUNCH_CONTROLLER_MODE == "First":
        print("START CONTROLLER NODE/S")
        print("#########################\n")

        os.system(start_controller_switch_sh)        

        print("FIRST START CONTROLLER/S")
        print("#########################\n")

        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


        print("START REST OF NODES/S")
        print("#########################\n")
        os.system(start_other_switches_sh)        

    else:
        print("START ALL NODES/S")
        print("#########################\n")
        if (os.path.exists(start_script_sh)):
            if (BFD_MIN_TX_ms != None):
                os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
            else:
                os.system(start_script_sh)        
        else:
            CLI( net , script = start_script_py)    

        print("LAUNCH CONTROLLER/S AT THE END")
        print("#########################\n")
        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 

    #Launch ./dumpAllFlows switch
    print("launch ./dumpAllFlows en s15")    
    s15 = net.getNodeByName("s15")
    s15.cmd("rm /tmp/dumpflow-s15.txt")
    s15.cmd("./dumpAllFlows.sh s15 > /tmp/dumpflow-s15.txt")

    
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)        
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
        
# -------------------------
# scenario_test_tabla_openflow. Switch s16
# -------------------------
def scenario_test_tabla_openflow_s16(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None, LAUNCH_CONTROLLER_MODE="End"):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    start_controller_switch_sh = f'{SCENARIO_DIR}/start_controller_switch_{scenario_name}.sh' 
    start_other_switches_sh = f'{SCENARIO_DIR}/start_other_switches_{scenario_name}.sh' 
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)
    else:
        CLI( net , script = stop_script_py)
        

    if LAUNCH_CONTROLLER_MODE == "First":
        print("START CONTROLLER NODE/S")
        print("#########################\n")

        os.system(start_controller_switch_sh)        

        print("FIRST START CONTROLLER/S")
        print("#########################\n")

        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


        print("START REST OF NODES/S")
        print("#########################\n")
        os.system(start_other_switches_sh)        

    else:
        print("START ALL NODES/S")
        print("#########################\n")
        if (os.path.exists(start_script_sh)):
            if (BFD_MIN_TX_ms != None):
                os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
            else:
                os.system(start_script_sh)        
        else:
            CLI( net , script = start_script_py)    

        print("LAUNCH CONTROLLER/S AT THE END")
        print("#########################\n")
        for c in controller_nodes:
            # Launch controller in node c
            launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
        print(str(time.time()) + " : " +" Launched controller(s)")


    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 

    #Launch ./dumpAllFlows switch
    print("launch ./dumpAllFlows en s16")    
    s16 = net.getNodeByName("s15")
    s16.cmd("rm /tmp/dumpflow-s16.txt")
    s16.cmd("./dumpAllFlows.sh s16 > /tmp/dumpflow-s16.txt")

    
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)        
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
        
        
# --------------------------------
# kill_node_test
# --------------------------------
def kill_node_test(scenario_name, node_to_kill, UPTIME=5, KILLTIME=5):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    
    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}.py'
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}.py'
    print(stop_script)

    
    print(node_to_kill)
    
    net, controllers  = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    CLI( net , script = stop_script)


    # Launch controller in s0
    launch_controller(controllers[0], f'{SCENARIO_DIR}/params-controller-s0.conf', "/tmp/salida-s0")


    print("START")

    print(f'Test kill {node_to_kill}')
    CLI( net , script = start_script)    
    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 
    

    print (f'kill {node_to_kill}')
    killString = ""
    os.system(f'sudo kill -9 `cat /tmp/mininet-{node_to_kill}/*pid`')
    print(f'{node_to_kill} down during {KILLTIME} s')
    time.sleep (KILLTIME)
    
    UPTIME=40 # leave time to recover to all in cascade
    CLI( net , script = start_script)    
    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 
    
    CLI( net , script = stop_script)
    print("network down")

        
    print("FINISH")
    
    net.stop()


# --------------------------------
# scenario_experiment_2
# --------------------------------
def scenario_experiment_2(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)
        

    print("START")

    if (os.path.exists(start_script_sh)):
        if (BFD_MIN_TX_ms != None):
            os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
        else:
            os.system(start_script_sh)        
    else:
        CLI( net , script = start_script_py)    


    print("Launch controller(s)")
    for c in controller_nodes:
        # Launch controller in node c
        launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')


        
    time.sleep(25)

    print("launch tcpdump en s1")    
    s1 = net.getNodeByName("s1")
    s1.cmd("rm /tmp/k.pcap")
    s1.cmd(f'timeout 25 tcpdump dst host 10.0.0.1 and udp port 5001 -i s1-eth0 -w /tmp/k.pcap &')

    print("launch iperf server en s0")
    s0 = net.getNodeByName("s0")
    s0.cmd(f'timeout 20 iperf -u -i 1 -s &')

    time.sleep(2)
    
    print("launch iperf client en s7")
    s7 = net.getNodeByName("s7")
    s7.cmd(f'iperf -u -c 10.0.0.1 -l 1200 -b 200M &')

    time.sleep (3)    
    os.system("scripts/kill-switch.sh s6 2")


    print("network up during ", UPTIME, "s")
    time.sleep (20)
    
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)        
    else:
        CLI( net , script = stop_script_py)
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
        
       
# --------------------------------
# scenario_experiment_9
# --------------------------------
def scenario_experiment_9(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)
        

    print("START")

    if (os.path.exists(start_script_sh)):
        if (BFD_MIN_TX_ms != None):
            os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
        else:
            os.system(start_script_sh)        
    else:
        CLI( net , script = start_script_py)    


    print("Launch controller(s)")
    for c in controller_nodes:
        # Launch controller in node c
        launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')


        
    time.sleep(25)

    print("launch tcpdump en s1")    
    s1 = net.getNodeByName("s1")
    s1.cmd("rm /tmp/k.pcap")
    s1.cmd(f'timeout 25 tcpdump dst host 10.0.0.1 and tcp port 5001 -i s1-eth0 -w /tmp/k.pcap &')


    
    print("launch iperf server en s0")
    s0 = net.getNodeByName("s0")
    s0.cmd(f'timeout 20 iperf -i 1 -s &')

    time.sleep(2)
    
    print("launch iperf client en s7")
    s7 = net.getNodeByName("s7")
    s7.cmd(f'iperf -c 10.0.0.1  -b 200M &')

    time.sleep (3)    
    os.system("scripts/kill-switch.sh s6 2")


    print("network up during ", UPTIME, "s")
    time.sleep (20)
    
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)        
    else:
        CLI( net , script = stop_script_py)
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed") 
        
       
# --------------------------------
# scenario_experiment_4
# --------------------------------
def scenario_experiment_4(scenario_name, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)
        

    print("START")

    if (os.path.exists(start_script_sh)):
        if (BFD_MIN_TX_ms != None):
            os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
        else:
            os.system(start_script_sh)        
    else:
        CLI( net , script = start_script_py)    


    time.sleep(5)
        

    os.system("rm /tmp/*.pcap")
    for c in controller_nodes:
        print(f'launch tcpdump en {c}')    
#        c = net.getNodeByName(controller_name)
#    c.cmd(f'timeout 15 tcpdump (dst 10.0.0.101) or (src 10.0.0.101) -i s1-eth0 -w /tmp/k.pcap &')
        c.cmd(f'timeout 25 tcpdump -i {c}-eth0 -w /tmp/{c}.pcap &')    
        
    
    print("Launch controller(s)")
    for c in controller_nodes:
        # Launch controller in node c
        launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')


    time.sleep(25)

    
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)        
    else:
        CLI( net , script = stop_script_py)
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
  
  
# --------------------------------
# scenario_experiment_5
# --------------------------------
def scenario_experiment_5(scenario_name, controllers_to_kill, UPTIME=25, DOWNTIME=10, BFD_MIN_TX_ms=None):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)
        

    print("START")

    if (os.path.exists(start_script_sh)):
        if (BFD_MIN_TX_ms != None):
            os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
        else:
            os.system(start_script_sh)        
    else:
        CLI( net , script = start_script_py)    


    print("Launch controller(s)")
    for c in controller_nodes:
        # Launch controller in node c
        launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')


        
    time.sleep(40)

    for c in controllers_to_kill:
        os.system(f'scripts/kill-switch.sh {c} 1')


    time.sleep (30)

    
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)        
    else:
        CLI( net , script = stop_script_py)
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")  
        
        
# --------------------------------
# scenario_experiment_kill_switch
# --------------------------------
def scenario_experiment_kill_switch(scenario_name, switch_to_kill, interfaces, UPTIME=25, UPTIME_AFTER_KILL=10, DOWNTIME=10, BFD_MIN_TX_ms=None):
    SCENARIO_DIR = f'scenarios/{scenario_name}'

    sys.path.append(f'{SCENARIO_DIR}')
    
    scenario_module = __import__(f'scenario_{scenario_name}')

    start_script    = f'{SCENARIO_DIR}/start_scenario_{scenario_name}'    
    start_script_py = f'{start_script}.py'
    start_script_sh = f'{start_script}.sh'
    print(start_script)

    stop_script     = f'{SCENARIO_DIR}/stop_scenario_{scenario_name}'
    stop_script_py  = f'{stop_script}.py'
    stop_script_sh  = f'{stop_script}.sh'        
    print(stop_script)
    
    net, controller_nodes = scenario_module.ovsns(batch = True)

    net.start()
    
    # First stop everything
    if (os.path.exists(stop_script_sh)):
        os.system(stop_script_sh)
    else:
        CLI( net , script = stop_script_py)
        

    print("START")

    if (os.path.exists(start_script_sh)):
        if (BFD_MIN_TX_ms != None):
            os.system(f'{start_script_sh} {BFD_MIN_TX_ms}')
        else:
            os.system(start_script_sh)        
    else:
        CLI( net , script = start_script_py)    


    for c in controller_nodes:
        # Launch controller in node c
        launch_controller(c, f'{SCENARIO_DIR}/params-controller-{c}.conf', f'/tmp/salida-{c}')
    print(str(time.time()) + " : " +" Launched controller(s)")

    print("network up during ", UPTIME, "s")
    time.sleep (UPTIME) 


    # kill switch
    os.system(f'scripts/kill-switch.sh {switch_to_kill} {interfaces}')
    with open("/tmp/kill_time.txt", "w") as f:
        print ("kill_time: " + str(time.time()))
        print (time.time(), file=f)

    time.sleep (UPTIME_AFTER_KILL) 

    
    if (os.path.exists(stop_script_py)):
        CLI( net , script = stop_script_py)
    else:
        os.system(stop_script_sh)        
    # print("network down during ", DOWNTIME, "s")
    # time.sleep (DOWNTIME) 

    print("FINISH")

    net.stop()

    # kill controllers
    try:
        os.system("killall -9 ryu-manager")
    except OSError as e:
        print("No ryu-manager left to be killed")
        
             
def do_test(result, expected):
    l = result.split("\n")
    l=set(l)
    l=list(l)
    l.sort()
    l=l[1:]
    result = "\n".join(l)
    result += "\n"
    
    assert expected == result


# ------------------------
# test_scenario_one_switch
# ------------------------
def test_scenario_one_switch():
    scenario_test("one_switch")

    expected="switch [10.0.0.101] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 
    
    result = result.decode()
        
    do_test(result,expected)


  
# ------------------------
# test_scenario_arbol
# ------------------------
def test_scenario_arbol():
    scenario_test("arbol")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 
    
    result = result.decode()
    
    do_test(result,expected)
    
    
    
    
# ------------------------
# test_scenario_bucle1
# ------------------------
def test_scenario_bucle1():
    scenario_test("bucle1")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
# ------------------------
# test_scenario_bucle2
# ------------------------
def test_scenario_bucle2():
    scenario_test("bucle2")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
    
# -------------------------------
# test_scenario_linea15_bfd
# -------------------------------
def test_scenario_linea15_bfd():
    scenario_test("linea15_bfd", UPTIME=35, LAUNCH_CONTROLLER_MODE="End")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
    
# -------------------------------
# test_scenario_linea15_2c_bfd
# -------------------------------
def test_scenario_linea15_2c_bfd():
    scenario_test("linea15_2c_bfd", UPTIME=35, LAUNCH_CONTROLLER_MODE="End")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True)
    (result_c0, err) = proc.communicate()
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True)
    (result_c1, err) = proc.communicate()

    result = result_c0.decode() + result_c1.decode()

    do_test(result,expected)
    
    
# -------------------------------
# test_scenario_linea15_nobfd
# -------------------------------
def test_scenario_linea15_nobfd():
    scenario_test("linea15_bfd", UPTIME=35)

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
# -------------------------------
# test_scenario_linea15_bucle
# -------------------------------
def test_scenario_linea15_bucle():
    scenario_test("linea15_bucle", UPTIME=35)

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
    
# -------------------------------
# test_scenario_clos_bfd
# -------------------------------
def test_scenario_clos_bfd():
    scenario_test("clos_bfd", UPTIME=20, BFD_MIN_TX_ms=100, LAUNCH_CONTROLLER_MODE="End")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
 
# -------------------------------
# test_scenario_att_bfd
# -------------------------------
        
def test_scenario_att_bfd():
    scenario_test("att_bfd", UPTIME=120, BFD_MIN_TX_ms=100, LAUNCH_CONTROLLER_MODE="First")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\nswitch [10.0.0.121] IS MANAGED\nswitch [10.0.0.122] IS MANAGED\nswitch [10.0.0.123] IS MANAGED\nswitch [10.0.0.124] IS MANAGED\nswitch [10.0.0.125] IS MANAGED\nswitch [10.0.0.126] IS MANAGED\nswitch [10.0.0.127] IS MANAGED\nswitch [10.0.0.128] IS MANAGED\nswitch [10.0.0.129] IS MANAGED\nswitch [10.0.0.130] IS MANAGED\nswitch [10.0.0.131] IS MANAGED\nswitch [10.0.0.132] IS MANAGED\nswitch [10.0.0.133] IS MANAGED\nswitch [10.0.0.134] IS MANAGED\nswitch [10.0.0.135] IS MANAGED\nswitch [10.0.0.136] IS MANAGED\nswitch [10.0.0.137] IS MANAGED\nswitch [10.0.0.138] IS MANAGED\nswitch [10.0.0.139] IS MANAGED\nswitch [10.0.0.140] IS MANAGED\nswitch [10.0.0.141] IS MANAGED\nswitch [10.0.0.142] IS MANAGED\nswitch [10.0.0.143] IS MANAGED\nswitch [10.0.0.144] IS MANAGED\nswitch [10.0.0.145] IS MANAGED\nswitch [10.0.0.146] IS MANAGED\nswitch [10.0.0.147] IS MANAGED\nswitch [10.0.0.148] IS MANAGED\nswitch [10.0.0.149] IS MANAGED\nswitch [10.0.0.150] IS MANAGED\nswitch [10.0.0.151] IS MANAGED\nswitch [10.0.0.152] IS MANAGED\nswitch [10.0.0.153] IS MANAGED\nswitch [10.0.0.154] IS MANAGED\nswitch [10.0.0.155] IS MANAGED\nswitch [10.0.0.156] IS MANAGED\nswitch [10.0.0.157] IS MANAGED\nswitch [10.0.0.158] IS MANAGED\nswitch [10.0.0.159] IS MANAGED\nswitch [10.0.0.160] IS MANAGED\nswitch [10.0.0.161] IS MANAGED\nswitch [10.0.0.162] IS MANAGED\nswitch [10.0.0.163] IS MANAGED\nswitch [10.0.0.164] IS MANAGED\nswitch [10.0.0.165] IS MANAGED\nswitch [10.0.0.166] IS MANAGED\nswitch [10.0.0.167] IS MANAGED\nswitch [10.0.0.168] IS MANAGED\nswitch [10.0.0.169] IS MANAGED\nswitch [10.0.0.170] IS MANAGED\nswitch [10.0.0.171] IS MANAGED\nswitch [10.0.0.172] IS MANAGED\nswitch [10.0.0.173] IS MANAGED\nswitch [10.0.0.174] IS MANAGED\nswitch [10.0.0.175] IS MANAGED\nswitch [10.0.0.176] IS MANAGED\nswitch [10.0.0.177] IS MANAGED\nswitch [10.0.0.178] IS MANAGED\nswitch [10.0.0.179] IS MANAGED\nswitch [10.0.0.180] IS MANAGED\nswitch [10.0.0.181] IS MANAGED\nswitch [10.0.0.182] IS MANAGED\nswitch [10.0.0.183] IS MANAGED\nswitch [10.0.0.184] IS MANAGED\nswitch [10.0.0.185] IS MANAGED\nswitch [10.0.0.186] IS MANAGED\nswitch [10.0.0.187] IS MANAGED\nswitch [10.0.0.188] IS MANAGED\nswitch [10.0.0.189] IS MANAGED\nswitch [10.0.0.190] IS MANAGED\nswitch [10.0.0.191] IS MANAGED\nswitch [10.0.0.192] IS MANAGED\nswitch [10.0.0.193] IS MANAGED\nswitch [10.0.0.194] IS MANAGED\nswitch [10.0.0.195] IS MANAGED\nswitch [10.0.0.196] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c0, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c2, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c3, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c4 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c4, err) = proc.communicate() 


    result = result_c0.decode() + result_c1.decode() + result_c2.decode() + result_c3.decode() + result_c4.decode()

    print(result)
    do_test(result,expected)



# -------------------------------
# test_scenario_b4_bfd
# -------------------------------

def test_scenario_b4_bfd():
   # Mejores tiempos con este arranque, primero el controlador, después switches
    scenario_test("b4_bfd", UPTIME=45, BFD_MIN_TX_ms=100, LAUNCH_CONTROLLER_MODE="First")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected) 
          


# -------------------------------
# test_scenario_bucle3_nobfd
# -------------------------------
def test_scenario_bucle3_nobfd():
    scenario_test("bucle3_nobfd")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
    
# -------------------------------
# test_scenario_bucle3_bfd
# -------------------------------
def test_scenario_bucle3_bfd():
    scenario_test("bucle3_bfd", BFD_MIN_TX_ms=100)
        
    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
    
    
# -------------------------------
# test_scenario_bucle4_nobfd
# -------------------------------
def test_scenario_bucle4_nobfd(LAUNCH_CONTROLLER_MODE="End"):
    scenario_test("bucle4_nobfd", LAUNCH_CONTROLLER_MODE=LAUNCH_CONTROLLER_MODE)

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
    
    
# -----------------------------------
# test_scenario_2controllers (bucle4)
# -----------------------------------
def test_scenario_2controllers():
    scenario_test("2controllers", LAUNCH_CONTROLLER_MODE="End")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c0, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 


    result = result_c0.decode() + result_c1.decode() 

    do_test(result,expected)
    
    
# -----------------------------------
# test_scenario_bucle4_bfd 
# -----------------------------------   
def test_scenario_bucle4_bfd(LAUNCH_CONTROLLER_MODE="End"):
    scenario_test("bucle4_bfd", BFD_MIN_TX_ms=100, LAUNCH_CONTROLLER_MODE=LAUNCH_CONTROLLER_MODE)
        
    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\n"
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
 
    
# -----------------------------------
# test_scenario_mesh
# -----------------------------------
        
def test_scenario_mesh():
    scenario_test("mesh", UPTIME=30, LAUNCH_CONTROLLER_MODE="First")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.121] IS MANAGED\nswitch [10.0.0.122] IS MANAGED\nswitch [10.0.0.123] IS MANAGED\nswitch [10.0.0.124] IS MANAGED\nswitch [10.0.0.131] IS MANAGED\nswitch [10.0.0.132] IS MANAGED\nswitch [10.0.0.133] IS MANAGED\nswitch [10.0.0.134] IS MANAGED\nswitch [10.0.0.141] IS MANAGED\nswitch [10.0.0.142] IS MANAGED\nswitch [10.0.0.143] IS MANAGED\nswitch [10.0.0.144] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    

# -----------------------------------
# test_scenario_mesh_4controllers
# -----------------------------------    

def test_scenario_mesh_4controllers():
    scenario_test("mesh_4controllers", UPTIME=60, LAUNCH_CONTROLLER_MODE="First")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.121] IS MANAGED\nswitch [10.0.0.122] IS MANAGED\nswitch [10.0.0.123] IS MANAGED\nswitch [10.0.0.124] IS MANAGED\nswitch [10.0.0.131] IS MANAGED\nswitch [10.0.0.132] IS MANAGED\nswitch [10.0.0.133] IS MANAGED\nswitch [10.0.0.134] IS MANAGED\nswitch [10.0.0.141] IS MANAGED\nswitch [10.0.0.142] IS MANAGED\nswitch [10.0.0.143] IS MANAGED\nswitch [10.0.0.144] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c0, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c2, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c3, err) = proc.communicate() 


    result = result_c0.decode() + result_c1.decode() + result_c2.decode() + result_c3.decode()

    do_test(result,expected)
    
    
  
# -----------------------------------
# experiment_scenario_mesh_4controllers
# -----------------------------------

def test_experiment_scenario_mesh_4controllers():
    scenario_test("mesh_4controllers", UPTIME=80)

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.121] IS MANAGED\nswitch [10.0.0.122] IS MANAGED\nswitch [10.0.0.123] IS MANAGED\nswitch [10.0.0.124] IS MANAGED\nswitch [10.0.0.131] IS MANAGED\nswitch [10.0.0.132] IS MANAGED\nswitch [10.0.0.133] IS MANAGED\nswitch [10.0.0.134] IS MANAGED\nswitch [10.0.0.141] IS MANAGED\nswitch [10.0.0.142] IS MANAGED\nswitch [10.0.0.143] IS MANAGED\nswitch [10.0.0.144] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c0, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c2, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c3, err) = proc.communicate() 


    result = result_c0.decode() + result_c1.decode() + result_c2.decode() + result_c3.decode()

    do_test(result,expected)



# -----------------------------------
# test_scenario_clos_2c_bfd
# -----------------------------------

def test_scenario_clos_2c_bfd():
    scenario_test("clos_2c_bfd", UPTIME=40, LAUNCH_CONTROLLER_MODE="First")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c3, err) = proc.communicate() 


    result = result_c1.decode() + result_c3.decode()

    do_test(result,expected)
        

# -----------------------------------
# experiment_scenario_clos_2c_bfd
# -----------------------------------

def test_experiment_scenario_clos_2c_bfd():
    scenario_test("clos_2c_bfd", UPTIME=60)

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c3, err) = proc.communicate() 

    result = result_c1.decode() + result_c3.decode()

    do_test(result,expected)
        

# -----------------------------------
# test_scenario_clos_4c_bfd
# -----------------------------------

def test_scenario_clos_4c_bfd():
    scenario_test("clos_4c_bfd", UPTIME=40, LAUNCH_CONTROLLER_MODE="First")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c2, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c3, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c4 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c4, err) = proc.communicate() 


    result = result_c1.decode() + result_c2.decode() + result_c3.decode() + result_c4.decode()

    do_test(result,expected)
        

# -----------------------------------
# experiment_scenario_clos_4c_bfd
# -----------------------------------

def test_experiment_scenario_clos_4c_bfd():
    scenario_test("clos_4c_bfd", UPTIME=60, LAUNCH_CONTROLLER_MODE="End")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c2, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c3, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c4 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c4, err) = proc.communicate() 

    result = result_c1.decode() + result_c2.decode() + result_c3.decode() + result_c4.decode()

    do_test(result,expected)



# -------------------------------
# test_kill_node_scenario_bucle2
# -------------------------------
def test_kill_node_scenario_bucle2():
    kill_node_test("bucle2", "s2")

    expected="switch [10.0.0.102] IS NOT MANAGED\nswitch [10.0.0.102] IS MANAGED\n"

    proc = subprocess.Popen(["egrep MANAGED /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    maxDiff = None
        
    l = result.split("\n")
    l=l[1:]
    result = "\n".join(l)
    result += "\n"

    print(expected)
    print(result)
    assert expected in result
    


# -----------------------------------
# experiment_2_100ms_bfd_scenario
# -----------------------------------

def test_experiment_2_100ms_bfd_scenario():
    scenario_experiment_2("bucle4_bfd", BFD_MIN_TX_ms=100)
        
# -----------------------------------
# experiment_2_10ms_bfd_scenario
# -----------------------------------

def test_experiment_2_10ms_bfd_scenario():
    scenario_experiment_2("bucle4_bfd", BFD_MIN_TX_ms=10)


# -----------------------------------
# experiment_2_nobfd_scenario
# -----------------------------------

def test_experiment_2_nobfd_scenario():
    scenario_experiment_2("bucle4_nobfd")
    



# -----------------------------------
# experiment_9_100ms_bfd_scenario
# -----------------------------------

def test_experiment_9_100ms_bfd_scenario():
    scenario_experiment_9("bucle4_bfd", BFD_MIN_TX_ms=100)


# -----------------------------------
# experiment_9_10ms_bfd_scenario
# -----------------------------------

def test_experiment_9_10ms_bfd_scenario():
    scenario_experiment_9("bucle4_bfd", BFD_MIN_TX_ms=10)


# -----------------------------------
# experiment_9_nobfd_scenario
# -----------------------------------

def test_experiment_9_nobfd_scenario():
    scenario_experiment_9("bucle4_nobfd")
    


# -----------------------------------
# experiment_4_one_switch
# -----------------------------------
        
def test_experiment_4_one_switch():
    scenario_experiment_4("one_switch")
        

# -----------------------------------
# experiment_4_bucle4_bfd
# -----------------------------------

def test_experiment_4_bucle4_bfd():
    scenario_experiment_4("bucle4_bfd", UPTIME=45)


# -----------------------------------
# experiment_4_2controllers
# -----------------------------------
def test_experiment_4_2controllers():
    scenario_experiment_4("2controllers", UPTIME=45)


# -----------------------------------
# experiment_4_mesh
# -----------------------------------

def test_experiment_4_mesh():
    scenario_experiment_4("mesh", UPTIME=60)


# -----------------------------------
# experiment_4_mesh_4controllers
# -----------------------------------
def test_experiment_4_mesh_4controllers():
    scenario_experiment_4("mesh_4controllers", UPTIME=60)
        
        
# -----------------------------------
# experiment_4_1_one_switch
# -----------------------------------

def test_experiment_4_1_one_switch():
    scenario_experiment_4("one_switch")


# -----------------------------------
# experiment_4_1_bucle4_bfd
# -----------------------------------

def test_experiment_4_1_bucle4_bfd():
    scenario_experiment_4("bucle4_bfd", UPTIME=25)


# -----------------------------------
# experiment_4_1_bucle4_bfd
# -----------------------------------
def test_experiment_4_1_bucle4_nobfd():
    scenario_experiment_4("bucle4_nobfd", UPTIME=25)


# -----------------------------------
# experiment_4_1_linea15_nobfd
# -----------------------------------

def test_experiment_4_1_linea15_nobfd():
    scenario_experiment_4("linea15_nobfd", UPTIME=25)




# -----------------------------------
# experiment_5_1
# -----------------------------------

def test_experiment_5_1():
    scenario_experiment_5("mesh_4controllers", UPTIME=120, controllers_to_kill=["c3"])


# -----------------------------------
# experiment_5_2
# -----------------------------------

def test_experiment_5_2():
    scenario_experiment_5("mesh_4controllers", UPTIME=120, controllers_to_kill=["c3","c2"])


# -----------------------------------
# experiment_5_3
# -----------------------------------

def test_experiment_5_3():
    scenario_experiment_5("mesh_4controllers", UPTIME=120, controllers_to_kill=["c3","c2","c1"])
    
    
# -----------------------------------
# experiment_7_kill_c0
# -----------------------------------

def test_experiment_7_kill_c0():
    scenario_experiment_kill_switch("mesh_4controllers", UPTIME=60, UPTIME_AFTER_KILL=60, switch_to_kill="c0", interfaces=1)



# -----------------------------------
# experiment_7_kill_c1
# -----------------------------------
def test_experiment_7_kill_c1():
    scenario_experiment_kill_switch("mesh_4controllers", UPTIME=60, UPTIME_AFTER_KILL=60, switch_to_kill="c1", interfaces=1)


# -----------------------------------
# experiment_7_kill_c2
# -----------------------------------

def test_experiment_7_kill_c2():
    scenario_experiment_kill_switch("mesh_4controllers", UPTIME=60, UPTIME_AFTER_KILL=60, switch_to_kill="c2", interfaces=1)
        

# -----------------------------------
# experiment_7_kill_c3
# -----------------------------------

def test_experiment_7_kill_c3():
    scenario_experiment_kill_switch("mesh_4controllers", UPTIME=60, UPTIME_AFTER_KILL=60, switch_to_kill="c3", interfaces=1)


# -----------------------------------
# experiment_8_kill_s2
# -----------------------------------

def test_experiment_8_kill_s2():
    scenario_experiment_kill_switch("bucle4_bfd", UPTIME=30, UPTIME_AFTER_KILL=10, switch_to_kill="s2", interfaces=2)
      


############################# TEST SISTEMA TABLAS OPENFLOW #############################

#A continuacion, se visualizan los test de sistemas relacionados con tablas openflow. En dichos test, verificamos como al considerarse un switch 'MANAGED' en el controlador, se inserta el grafo en el switch X para encaminar hacia el controlado, se visualizara el camino principal y rutas alternativas que deberia haber instalado el controlador en ese swich.
#para poder encaminar desde los switchs a el controlador.
# ------------------------
# test_scenario_one_switch_tabla_openflow
# ------------------------
#Test que verifica como al considerarse el switch s1 MANAGED, entonces en su tabla dumpflow aparecera la regla que inserta el grafo para poder encaminar el switch con el controlador.
def test_scenario_one_switch_tabla_openflow():
    scenario_test_tabla_openflow_s1("one_switch")

    expected="switch [10.0.0.101] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 
    
    result = result.decode()
    
    proc1 = subprocess.Popen(["grep -w nw_dst=10.0.0.1 /tmp/dumpflow-s1.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
    
    do_test(result,expected)
  
    #Busqueda en el fichero /dumpflow la ruta proporcionada por el controlador al switch s1
    assert re.search("set_field:0xf1000000",result1) != None   
  


# ------------------------
# test_scenario_arbol_tabla_openflow_s3
# ------------------------
#Test que verifica como al considerarse el switch s3 MANAGED, entonces en su tabla dumpflow aparecera la regla que inserta el grafo para poder encaminar el switch con el controlador.
def test_scenario_arbol_tabla_openflow():
    scenario_test_tabla_openflow_s3("arbol")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 
    result = result.decode()
    
    proc1 = subprocess.Popen(["grep -w nw_dst=11.0.0.1 /tmp/dumpflow-s3.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
    
    do_test(result,expected)
    
    #Busqueda en el fichero /dumpflow la ruta proporcionada por el controlador al switch s3
    assert re.search("set_field:0x11f10000",result1) != None
    	
   
   
# ------------------------
# test_scenario_bucle1_tabla_openflow_s3
# ------------------------
def test_scenario_bucle1_tabla_openflow():
    scenario_test_tabla_openflow_s3("bucle1")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    proc1 = subprocess.Popen(["grep -w nw_dst=10.0.0.1 /tmp/dumpflow-s3.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
    
    do_test(result,expected) 	
    #Busqueda en el fichero /dumpflow la ruta proporcionada por el controlador al switch s3
  
    assert re.search("set_field:0x19f10000",result1) != None #Ruta princial. El 9 significa que existe una ruta alternativa aparte de la principal.
    assert re.search("set_field:0x31f10000",result1) != None #Ruta alternativa.
    

# ------------------------
# test_scenario_bucle2_tabla_openflow
# ------------------------
def test_scenario_bucle2_tabla_openflow():
    scenario_test_tabla_openflow_s4("bucle2")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    proc1 = subprocess.Popen(["grep -w nw_dst=10.0.0.1 /tmp/dumpflow-s4.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
    
    do_test(result,expected)
    
    print(result1)
    #Busqueda en el fichero /dumpflow la ruta proporcionada por el controlador al switch s4
    assert re.search("set_field:0x99f10000",result1) != None #Ruta princial, los dos 9's significan que existen dos rutas alternativas.
    assert re.search("set_field:0x311f1000",result1) != None #Ruta alternativa
    assert re.search("set_field:0x2311f100",result1) != None #Ruta alternativa


# -------------------------------
# test_scenario_linea15_bfd_tabla_openflow
# -------------------------------
def test_scenario_linea15_bfd_tabla_openflow():
    scenario_test_tabla_openflow_s15("linea15_bfd", UPTIME=35, LAUNCH_CONTROLLER_MODE="End")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    proc1 = subprocess.Popen(["grep -w nw_dst=10.0.0.1 /tmp/dumpflow-s15.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
    
    do_test(result,expected)
    

    #Busqueda en el fichero /dumpflow la ruta proporcionada por el controlador al switch s15
    assert re.search("set_field:0x111111f1",result1) != None 
 
 
# -------------------------------
# test_scenario_linea15_2c_bfd_tabla_openflow
# -------------------------------
def test_scenario_linea15_2c_bfd_tabla_openflow():
    scenario_test_tabla_openflow_s4("linea15_2c_bfd", UPTIME=35, LAUNCH_CONTROLLER_MODE="End")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True)
    (result_c0, err) = proc.communicate()
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True)
    (result_c1, err) = proc.communicate()

    result = result_c0.decode() + result_c1.decode()

    do_test(result,expected)
    
    proc1 = subprocess.Popen(["grep -w nw_dst=10.0.0.1 /tmp/dumpflow-s4.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 
    #Busqueda en el fichero /dumpflow la ruta proporcionada por el controlador al switch s4
    assert re.search("set_field:0x111f1000",result1) != None #Ruta principal para ir al controlador1.
  

# -------------------------------
# test_scenario_linea15_nobfd_tabla_openflow
# -------------------------------
def test_scenario_linea15_nobfd_tabla_openflow():
    scenario_test_tabla_openflow_s15("linea15_bfd", UPTIME=35)

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
      
    proc1 = subprocess.Popen(["grep -w nw_dst=10.0.0.1 /tmp/dumpflow-s15.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 
 
    #Busqueda en el fichero /dumpflow la ruta proporcionada por el controlador al switch s15
    assert re.search("set_field:0x111111f1",result1) != None 
  
 
# -------------------------------
# test_scenario_linea15_bucle_tabla_openflow
# -------------------------------
def test_scenario_linea15_bucle_tabla_openflow():
    scenario_test_tabla_openflow_s16("linea15_bucle", UPTIME=35)

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0 /tmp/dumpflow-s16.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 
    #Busqueda en el fichero /dumpflow la ruta proporcionada por el controlador al switch s16
    assert re.search("set_field:0x1f100000",result1) != None 


# -------------------------------
# test_scenario_clos_bfd   
# -------------------------------
def test_scenario_clos_bfd_tabla_openflow():
    scenario_test_tabla_openflow_s15("clos_bfd", UPTIME=20, BFD_MIN_TX_ms=100, LAUNCH_CONTROLLER_MODE="End")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0 /tmp/dumpflow-s15.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 

     #Busqueda en el fichero /dumpflow de la ruta  proporcionada por el controlador al switch
    if (re.search("set_field:0x9f90000",result1) != None) or (re.search("set_field:0xa9f90000",result1) != None) or (re.search("actions=CONTROLLER:",result1) != None): #Ruta principal para llegar al controlador, pueden haber 2 rutas alternativas.
        print("Encontrada regla de encaminamiento")
        assert True
    else:
        raise ValueError ("No encontradas rutas")

# -------------------------------
# test_scenario_b4_bfd
# -------------------------------

def test_scenario_b4_bfd_tabla_openflow():
   # Mejores tiempos con este arranque, primero el controlador, después switches
    scenario_test_tabla_openflow_s4("b4_bfd", UPTIME=45, BFD_MIN_TX_ms=100, LAUNCH_CONTROLLER_MODE="First")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected) 
          
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0.1 /tmp/dumpflow-s4.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 

    #Busqueda en el fichero /dumpflow la ruta proporcionada por el controlador al switch s4
    assert re.search("set_field:0xf1000000",result1) != None 
    
    
 
# -------------------------------
# test_scenario_bucle3_nobfd    
# -------------------------------
def test_scenario_bucle3_nobfd_tabla_openflow():
    scenario_test_tabla_openflow_s4("bucle3_nobfd")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0.1 /tmp/dumpflow-s4.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 

    #Busqueda en el fichero /dumpflow de la ruta  proporcionada por el controlador al switch s4
    if (re.search("set_field:0x99f10000",result1) != None) or (re.search("set_field:0x311f1000",result1) != None) or (re.search("set_field:0x2311f100",result1) != None): #Ruta principal y alternativas para llegar al controlador
        print("Encontrada regla de encaminamiento")
        assert True
    else:
        raise ValueError ("No encontradas rutas")

        
   
# -------------------------------
# test_scenario_bucle3_bfd
# -------------------------------
def test_scenario_bucle3_bfd_tabla_openflow():
    scenario_test_tabla_openflow_s3("bucle3_bfd", BFD_MIN_TX_ms=100)
        
    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0.1 /tmp/dumpflow-s3.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 
    #Busqueda en el fichero /dumpflow la ruta proporcionada por el controlador al switch s3
    assert re.search("set_field:0x99f10000",result1) != None  #Ruta principal
    assert re.search("set_field:0x2311f100",result1) != None  #Ruta alternativa
    assert re.search("set_field:0x311f1000",result1) != None  #Ruta alternativa
    
# -----------------------------------
# test_scenario_bucle4_bfd 
# -----------------------------------   
def test_scenario_bucle4_bfd_tabla_openflow(LAUNCH_CONTROLLER_MODE="End"):
    scenario_test_tabla_openflow_s3("bucle4_bfd", BFD_MIN_TX_ms=100, LAUNCH_CONTROLLER_MODE=LAUNCH_CONTROLLER_MODE)
        
    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\n"
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0.1 /tmp/dumpflow-s3.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 
    #print(result1)
    #Busqueda en el fichero /dumpflow la ruta proporcionada por el controlador al switch s3
    assert re.search("set_field:0x99f10000",result1) != None  #Ruta principal
    assert re.search("set_field:0x23211f10",result1) != None  #Ruta alternativa
    assert re.search("set_field:0x3211f100",result1) != None  #Ruta alternativa
 
  
    
# -----------------------------------
# test_scenario_mesh
# -----------------------------------
        
def test_scenario_mesh_tabla_openflow():
    scenario_test_tabla_openflow_s4("mesh", UPTIME=30, LAUNCH_CONTROLLER_MODE="First")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.121] IS MANAGED\nswitch [10.0.0.122] IS MANAGED\nswitch [10.0.0.123] IS MANAGED\nswitch [10.0.0.124] IS MANAGED\nswitch [10.0.0.131] IS MANAGED\nswitch [10.0.0.132] IS MANAGED\nswitch [10.0.0.133] IS MANAGED\nswitch [10.0.0.134] IS MANAGED\nswitch [10.0.0.141] IS MANAGED\nswitch [10.0.0.142] IS MANAGED\nswitch [10.0.0.143] IS MANAGED\nswitch [10.0.0.144] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)
    
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0.1 /tmp/dumpflow-s4.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 

    #Busqueda en el fichero /dumpflow la ruta principal proporcionada por el controlador al switch s4
    if (re.search("set_field:0x99f10000",result1) != None) or (re.search("set_field:0xa9f10000",result1) != None):
        print("Encontrada regla encaminamiento princiapl")
        assert True
    else:
        raise ValueError ("No entrada ruta principal")
        
    #Busqueda en el fichero /dumpflow las rutas alternativas desde el switch hacia el controlador.  
    if (re.search("set_field:0x221f1000",result1) != None) or (re.search("set_field:0x21f10000",result1) != None) or (re.search("set_field:0x211f1000",result1) != None) or (re.search("set_field:0x11f10000",result1) != None):
        print("Encontrada reglas rutas alternativas")
        assert True
    else:
        raise ValueError ("No entrada ruta principal")
    
# -----------------------------------
# test_scenario_2controllers (bucle4)
# -----------------------------------
def test_scenario_2controllers_tabla_openflow():
    scenario_test_tabla_openflow_s3("2controllers", LAUNCH_CONTROLLER_MODE="End")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c0, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 


    result = result_c0.decode() + result_c1.decode() 

    do_test(result,expected)
    
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0 /tmp/dumpflow-s3.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 
  
    #Busqueda en el fichero /dumpflow la ruta proporcionada por el controlador al switch s3
    assert re.search("set_field:0x11f10000",result1) != None  #Ruta para ir al controlador c1   
  
# -----------------------------------
# test_scenario_2controllers (bucle4)
# -----------------------------------
def test_scenario_2controllers_tabla_openflow():
    scenario_test_tabla_openflow_s4("2controllers", LAUNCH_CONTROLLER_MODE="End")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c0, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 


    result = result_c0.decode() + result_c1.decode() 

    do_test(result,expected)
    
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0 /tmp/dumpflow-s4.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 
    #Busqueda en el fichero /dumpflow la ruta principal proporcionada por el controlador al switch s4
    if (re.search("set_field:0x1142f300",result1) != None) or (re.search("set_field:0x4f300000",result1) != None): #Ruta para ir al controlador c1
        print("Encontrada regla encaminamiento princiapl")
        assert True
    else:
        raise ValueError ("No entrada ruta principal")


# -------------------------------
# test_scenario_bucle4_nobfd
# -------------------------------
def test_scenario_bucle4_nobfd_tabla_openflow(LAUNCH_CONTROLLER_MODE="End"):
    scenario_test_tabla_openflow_s4("bucle4_nobfd", LAUNCH_CONTROLLER_MODE=LAUNCH_CONTROLLER_MODE)

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-s0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result, err) = proc.communicate() 

    result = result.decode()

    do_test(result,expected)

    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0.1 /tmp/dumpflow-s4.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 
    
    #Busqueda en el fichero /dumpflow las rutas alternativas proporcionada por el controlador al switch s4
    if (re.search("set_field:0x2411f100",result1) != None) or (re.search("set_field:0x411f1000",result1) != None): #Rutas alternativas
        print("Encontrada reglas encaminamiento alternativo")
        assert True
    else:
        raise ValueError ("No encontradas rutas alternativas")
        
    #Busqueda en el fichero /dumpflow la ruta principal proporcionada por el controlador al switch s4
    if (re.search("set_field:0x99f10000",result1) != None) or (re.search("set_field:0xa9f10000",result1) != None): #Ruta principal
        print("Encontrada regla encaminamiento princiapl")
        assert True
    else:
        raise ValueError ("No entrada ruta principal")
        
      
# -----------------------------------
# test_scenario_mesh_4controllers
# -----------------------------------    

def test_scenario_mesh_4controllers_tabla_openflow_c0_c2():
    scenario_test_tabla_openflow_s3("mesh_4controllers", UPTIME=60, LAUNCH_CONTROLLER_MODE="First")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.121] IS MANAGED\nswitch [10.0.0.122] IS MANAGED\nswitch [10.0.0.123] IS MANAGED\nswitch [10.0.0.124] IS MANAGED\nswitch [10.0.0.131] IS MANAGED\nswitch [10.0.0.132] IS MANAGED\nswitch [10.0.0.133] IS MANAGED\nswitch [10.0.0.134] IS MANAGED\nswitch [10.0.0.141] IS MANAGED\nswitch [10.0.0.142] IS MANAGED\nswitch [10.0.0.143] IS MANAGED\nswitch [10.0.0.144] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c0, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c2, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c3, err) = proc.communicate() 


    result = result_c0.decode() + result_c1.decode() + result_c2.decode() + result_c3.decode()

    do_test(result,expected)
    
    
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0.1 /tmp/dumpflow-s3.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 
   
    #Busqueda en el fichero /dumpflow las rutas alternativas proporcionada por el controlador al switch s3
    if (re.search("set_field:0x222f3000",result1) != None) or (re.search("set_field:0x322f3000",result1) != None) or (re.search("set_field:0x333f3000",result1) != None) or (re.search("set_field:0x1f100000",result1) != None) or (re.search("actions=CONTROLLER",result1) != None): #Rutas alternativas para llegar al controlador c2 o c0
        print("Encontrada reglas encaminamiento alternativo")
        assert True
    else:
        raise ValueError ("No encontradas rutas alternativas")

        
            
# -----------------------------------
# test_scenario_mesh_4controllers
# -----------------------------------    

def test_scenario_mesh_4controllers_tabla_openflow_c0_c3():
    scenario_test_tabla_openflow_s4("mesh_4controllers", UPTIME=60, LAUNCH_CONTROLLER_MODE="First")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.121] IS MANAGED\nswitch [10.0.0.122] IS MANAGED\nswitch [10.0.0.123] IS MANAGED\nswitch [10.0.0.124] IS MANAGED\nswitch [10.0.0.131] IS MANAGED\nswitch [10.0.0.132] IS MANAGED\nswitch [10.0.0.133] IS MANAGED\nswitch [10.0.0.134] IS MANAGED\nswitch [10.0.0.141] IS MANAGED\nswitch [10.0.0.142] IS MANAGED\nswitch [10.0.0.143] IS MANAGED\nswitch [10.0.0.144] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c0, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c2, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c3, err) = proc.communicate() 


    result = result_c0.decode() + result_c1.decode() + result_c2.decode() + result_c3.decode()

    do_test(result,expected)
    
    
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0 /tmp/dumpflow-s4.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 
    #print(result1)
    #Busqueda en el fichero /dumpflow las rutas alternativas proporcionada por el controlador c3 al switch s4
    if (re.search("set_field:0x122f3000",result1) != None) or (re.search("set_field:0x22f30000",result1) != None) or (re.search("set_field:0x221f1000",result1) != None) or (re.search("set_field:0x21f10000",result1) != None) or (re.search("actions=CONTROLLER",result1) != None): #Rutas alternativas para llegar al controlador c3 o c2.
        print("Encontrada reglas encaminamiento alternativo")
        assert True
    else:
        raise ValueError ("No encontradas rutas alternativas")
        

     
# -----------------------------------
# test_scenario_mesh_4controllers
# -----------------------------------    

def test_scenario_mesh_4controllers_tabla_openflow_c0_c1():
    scenario_test_tabla_openflow_s21("mesh_4controllers", UPTIME=60, LAUNCH_CONTROLLER_MODE="First")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.121] IS MANAGED\nswitch [10.0.0.122] IS MANAGED\nswitch [10.0.0.123] IS MANAGED\nswitch [10.0.0.124] IS MANAGED\nswitch [10.0.0.131] IS MANAGED\nswitch [10.0.0.132] IS MANAGED\nswitch [10.0.0.133] IS MANAGED\nswitch [10.0.0.134] IS MANAGED\nswitch [10.0.0.141] IS MANAGED\nswitch [10.0.0.142] IS MANAGED\nswitch [10.0.0.143] IS MANAGED\nswitch [10.0.0.144] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c0 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c0, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c2, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c3, err) = proc.communicate() 


    result = result_c0.decode() + result_c1.decode() + result_c2.decode() + result_c3.decode()

    do_test(result,expected)
    
    
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0 /tmp/dumpflow-s21.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 
    print(result1)

    #Busqueda en el fichero /dumpflow las rutas alternativas proporcionada por el controlador c1 al switch s4
    if (re.search("set_field:0x133f3000",result1) != None) or (re.search("set_field:33f30000",result1) != None) or (re.search("set_field:22f30000",result1) != None): #Rutas alternativas para llegar al controlador c1 o c0
        print("Encontrada reglas encaminamiento alternativo")
        assert True
    else:
        raise ValueError ("No encontradas rutas alternativas")

      
# -----------------------------------
# test_scenario_clos_2c_bfd
# -----------------------------------

def test_scenario_clos_2c_bfd_tabla_openflow():
    scenario_test_tabla_openflow_s15("clos_2c_bfd", UPTIME=40, LAUNCH_CONTROLLER_MODE="First")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c3, err) = proc.communicate() 


    result = result_c1.decode() + result_c3.decode()

    do_test(result,expected)
    
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0 /tmp/dumpflow-s15.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 
  
    #Busqueda en el fichero /dumpflow la accion cuando el destino es el controlador.
    assert re.search("actions=CONTROLLER:",result1) != None  
    
# -----------------------------------
# test_scenario_clos_4c_bfd
# -----------------------------------

def test_scenario_clos_4c_bfd_tabla_openflow():
    scenario_test_tabla_openflow_s5("clos_4c_bfd", UPTIME=40, LAUNCH_CONTROLLER_MODE="First")

    expected="switch [10.0.0.101] IS MANAGED\nswitch [10.0.0.102] IS MANAGED\nswitch [10.0.0.103] IS MANAGED\nswitch [10.0.0.104] IS MANAGED\nswitch [10.0.0.105] IS MANAGED\nswitch [10.0.0.106] IS MANAGED\nswitch [10.0.0.107] IS MANAGED\nswitch [10.0.0.108] IS MANAGED\nswitch [10.0.0.109] IS MANAGED\nswitch [10.0.0.110] IS MANAGED\nswitch [10.0.0.111] IS MANAGED\nswitch [10.0.0.112] IS MANAGED\nswitch [10.0.0.113] IS MANAGED\nswitch [10.0.0.114] IS MANAGED\nswitch [10.0.0.115] IS MANAGED\nswitch [10.0.0.116] IS MANAGED\nswitch [10.0.0.117] IS MANAGED\nswitch [10.0.0.118] IS MANAGED\nswitch [10.0.0.119] IS MANAGED\nswitch [10.0.0.120] IS MANAGED\n"

    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c1 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c1, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c2 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c2, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c3 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c3, err) = proc.communicate() 
    proc = subprocess.Popen(["egrep 'IS MANAGED' /tmp/salida-c4 | sed 's/.*: //'"], stdout=subprocess.PIPE, shell=True) 
    (result_c4, err) = proc.communicate() 


    result = result_c1.decode() + result_c2.decode() + result_c3.decode() + result_c4.decode()

    do_test(result,expected)
    proc1 = subprocess.Popen(["grep  nw_dst=10.0.0 /tmp/dumpflow-s5.txt"], stdout=subprocess.PIPE, shell=True)
    (result1, err1) = proc1.communicate() 
    result1 = result1.decode()
 

    #Busqueda en el fichero /dumpflow de las rutas  proporcionadas por el controlador al switch
    if (re.search("set_field:0x2f100000",result1) != None) or (re.search("set_field:0x132f1000",result1) != None) or (re.search("actions=CONTROLLER",result1) != None) : #Rutas para llegar el controlador
        print("Encontrada regla de encaminamiento")
        assert True
    else:
        raise ValueError ("No encontradas rutas")
        

