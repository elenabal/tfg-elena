
# --------------------------------------------------------------
# Experiment_1
#
#   Con 1 controlador se realizan medidas de tiempo de conexión
#   de todos los switches al controlador
# --------------------------------------------------------------
# Tiempo desde que se conecta el primer switch al controlador hasta que se conectan todos los switches
# Se obtiene de las marcas de tiempo del fichero de log (cuando todos están MANAGED)


# BUCLE4_NOBFD
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_bucle4_nobfd      Experiment_1 bucle4_nobfd      10 /tmp/salida-s0

# BUCLE4_BFD
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_bucle4_bfd        Experiment_1 bucle4_bfd        10 /tmp/salida-s0 

# LINEA15_NOBFD
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_linea15_nobfd     Experiment_1 linea15_nobfd    10 /tmp/salida-s0 

# LINEA15_BFD
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_linea15_bfd     Experiment_1 linea15_bfd     10 /tmp/salida-s0 

# CLOS_BFD
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_clos_bfd          Experiment_1 clos_bfd        10 /tmp/salida-c0 

# B4_BFD
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_b4_bfd            Experiment_1 b4_bfd          10 /tmp/salida-c0 

# MESH_BFD
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_mesh              Experiment_1 mesh            10 /tmp/salida-s0 


# -----------------------------------------------------------------------------
# Experiment_1_multi_controller 
#
#    Tiempo de conexión de todos los switches a algún controlador.
#    Genera varios ficheros:
#     - scenario_max_for_controller: muestra el tiempo que ha tardado 
#              el último switch de cada controlador
#     - scenario_switches_for_controller: muestra los switches que
#              se han conectado a cada controlador
#     - scenario_from_first_to_last: concatena todos los logs y 
#              busca el tiempo desde que se conectó el primero hasta el último
# ------------------------------------------------------------------------------
# BUCLE4_BFD 2 CONTROLLERS (no hay diferencia, las oleadas de conexiones son parecidas a 1 controlador
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_2controllers      Experiment_1_multi_controller 2controllers      5 /tmp/salida-c0 /tmp/salida-c1

# MESH 4 CONTROLLERS (tampoco hay muchas diferencias porque hay controladores que se quedan con 6 sws)
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_mesh_4controllers Experiment_1_multi_controller mesh_4controllers 5 /tmp/salida-c0 /tmp/salida-c1 /tmp/salida-c2 /tmp/salida-c3

# ATT_BFD 5 CONTROLLERS
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_att_bfd           Experiment_1_multi_controller att_bfd           5 /tmp/salida-c0 /tmp/salida-c1 /tmp/salida-c2 /tmp/salida-c3 /tmp/salida-c4

# CLOS_BFD 2 CONTROLLERS
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_clos_2c_bfd      Experiment_1_multi_controller clos_2c_bfd      5 /tmp/salida-c1 /tmp/salida-c3 

# CLOS_BFD 4 CONTROLLERS
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_clos_4c_bfd      Experiment_1_multi_controller clos_4c_bfd      5 /tmp/salida-c1 /tmp/salida-c2 /tmp/salida-c3 /tmp/salida-c4

# LINEA15_BFD 2 CONTROLLERS
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_linea15_2c_bfd     Experiment_1_multi_controller linea15_2c_bfd     10 /tmp/salida-c0 /tmp/salida-c1


# --------------------------------------------------------------------------
# Experiment_2
# 
#     Tráfico perdido por conmutar de camino principal a alternativo.
#     En el escenario bucle4, se arranca iperf servidor UDP (s0) y 
#     cliente UDP (s7) enviando 200M durante 10 segundos 
#     Se mata uno de los switches (s6) para que se conmute de camino 
#     principal a camino alternativo. 
#     Se está capturando el tráfico UDP entre s0 y s7 en la interfaz 
#     s1-eth0. Se observa medidas de throughput y de inter-arrival time.
#     Se puede ver el corte de tráfico.
#     Pruebas con BFD a 100ms, BFD a 10ms y NOBFD
#     Genera 2 ficheros:
#         - scenario: medidas de throughput de tráfico recibido en controlador
#         - scenario-iat:  Inter Arrival Time 
# ---------------------------------------------------------------------------

sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_2_100ms_bfd_scenario  Experiment_2_bin_100ms 100ms_bfd_min_tx 1 /tmp/salida-s0
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_2_10ms_bfd_scenario   Experiment_2_bin_100ms 10ms_bfd_min_tx  1 /tmp/salida-s0 
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_2_nobfd_scenario      Experiment_2_bin_100ms nobfd            1 /tmp/salida-s0 


# ---------------------------------------------------------------------------------------------
# Experiment_3
#
#     Tiempo desde que el controlador recibe el SYN de un switch hasta que 
#     el switch está MANAGED. Se obtiene de las marcas de tiempo del log
#     Se hace para cada switch del escenario y está programado sólo para
#     el escenario linea15. Para todos es lo mismo, entre 0.01 y 0.02 s
# ---------------------------------------------------------------------------------------------
# LINEA15_BFD
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_linea15_bfd      Experiment_3 tiempo           5 /tmp/salida-s0

# ---------------------------------------------------------------------------------------------
# Experiment_3_1
#     Tiempo que tarda cada switch en estar conectado tomando como referencia de partida desde
#     que el primer switch se conectó. El último switch mostrará el tiempo de conexión
#     de todos los switches para ese escenario.
#     Se hace para cada switch del escenario y está programado sólo para
#     el escenario linea15 ya que depende de los switches que haya.
# ----------------------------------------------------------------------------------------------
# LINEA15_BFD
sudo python3 stats.py  test_scenarios.ScenariosTestCase.test_scenario_linea15_bfd      Experiment_3_1 s           5 /tmp/salida-s0




# -----------------------------------------------------------------------------------------------
# Experiment_4
#
# Número de paquetes hasta que se manda el PROBE DE UDP a un switch para averiguar su puerto
# Se hace la captura en la interfaz eth0 del controlador/es. Esta medida no parece
# muy interesante porque el número de paquetes no te da su longitud.
# ------------------------------------------------------------------------------------------------

sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_4_one_switch         Experiment_4 one_switch           5 /tmp/salida-s0
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_4_bucle4_bfd         Experiment_4 bucle4_bfd           5 /tmp/salida-s0
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_4_2controllers       Experiment_4 2controllers         5 /tmp/salida-c0 /tmp/salida-c1
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_4_mesh               Experiment_4 mesh                 5 /tmp/salida-s0
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_4_mesh_4controllers  Experiment_4 mesh_4controllers    5 /tmp/salida-c0 /tmp/salida-c1 /tmp/salida-c2 /tmp/salida-c3



# ------------------------------------------------------------------------------------------------
# Experiment_4_1
# 
# Número de bytes en el nivel Ethernet hasta que se manda el PROBE DE UDP a un switch para averiguar su puerto
# Se hace la captura en la interfaz eth0 del controlador/es
# Sólo hace la cuenta para el primer switch, es más interesante para cada switch, ya que
# los switches lejanos llevarán más información de grafos y gastará más tráfico de control (?)
# ------------------------------------------------------------------------------------------------

sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_4_1_one_switch         Experiment_4_1 one_switch           5 /tmp/salida-s0
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_4_1_bucle4_bfd         Experiment_4_1 bucle4_bfd           5 /tmp/salida-s0
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_4_1_bucle4_nobfd         Experiment_4_1 bucle4_nobfd           5 /tmp/salida-s0
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_4_1_linea15_nobfd      Experiment_4_1 linea15_nobfd        5 /tmp/salida-s0



# ------------------------------------------------------------------------------------------------
# Experiment_5
# 
# Tiempo desde que se paran n controladores hasta que todos los switches están en otro controlador
# Se usa el escenario mesh_4controllers
# Prueba 5_1: se mata s40
# Prueba 5_2: se mata s40, s30
# Prueba 5_3: se mata s40, s30, s20
# Se obtiene del log desde que aparece el primer NOT_MANAGED hasta que aparece el último MANAGED
# ------------------------------------------------------------------------------------------------
# MESH_4CONTROLLERS
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_5_1                   Experiment_5 1                    5 /tmp/salida-c0 /tmp/salida-c1 /tmp/salida-c2 /tmp/salida-c3
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_5_2                   Experiment_5 2                    5 /tmp/salida-c0 /tmp/salida-c1 /tmp/salida-c2 /tmp/salida-c3
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_5_3                   Experiment_5 3                    5 /tmp/salida-c0 /tmp/salida-c1 /tmp/salida-c2 /tmp/salida-c3


# ------------------------------------------------------------------------------------------------
# Experiment_6
#
#    Tiempo desde que un controlador conoce a otro hasta que todos se conocen
#    En cada log se buscan las trazas de detección de un nuevo controlador. Se guardan esas trazas
#    de todos los controladores en un fichero y se ordenan. Se resta la última marca de 
#    tiempo de la primera.
# ------------------------------------------------------------------------------------------------

# BLUCLE4_2CONTROLLERS
sudo python3 stats.py test_scenarios.ScenariosTestCase.test_scenario_2controllers      Experiment_6 2controllers      5 /tmp/salida-c0 /tmp/salida-c1

# ATT_BFD (5 controladores)
sudo python3 stats.py test_scenarios.ScenariosTestCase.test_scenario_mesh_4controllers Experiment_6 mesh_4controllers 5 /tmp/salida-c0 /tmp/salida-c1 /tmp/salida-c2 /tmp/salida-3

# BLUCLE4_2CONTROLLERS
sudo python3 stats.py test_scenarios.ScenariosTestCase.test_scenario_att_bfd           Experiment_6 att_bfd           5 /tmp/salida-c0 /tmp/salida-c1 /tmp/salida-c2 /tmp/salida-c3 /tmp/salida-c4


# ------------------------------------------------------------------------------------------------
# Experiment_7
# 
#    Tiempo desde que falla un controlador que une a dos controladores no adyacentes, 
#    hasta que dichos controladores se pueden seguir comunicando porque los switches 
#    se han reconectado a otros controladores.
#    Escenario: mesh_4controllers
#    (OJO: No se comprueba que el controlador que se mata une a otros 2 controladores no
#    adyacentes)
# ------------------------------------------------------------------------------------------------
# MESH_4CONTROLLERS
sudo python3 stats.py test_scenarios.ScenariosTestCase.experiment_7_kill_c0       Experiment_7 mesh_4controllers_kill_c0  5 /tmp/salida-c0 /tmp/salida-c1 /tmp/salida-c2 /tmp/salida-c3
sudo python3 stats.py test_scenarios.ScenariosTestCase.experiment_7_kill_c1      Experiment_7 mesh_4controllers_kill_c1 5 /tmp/salida-c0 /tmp/salida-c1 /tmp/salida-c2 /tmp/salida-c3
sudo python3 stats.py test_scenarios.ScenariosTestCase.experiment_7_kill_c2      Experiment_7 mesh_4controllers_kill_c2 5 /tmp/salida-c0 /tmp/salida-c1 /tmp/salida-c2 /tmp/salida-c3
sudo python3 stats.py test_scenarios.ScenariosTestCase.experiment_7_kill_c3      Experiment_7 mesh_4controllers_kill_c3 5 /tmp/salida-c0 /tmp/salida-c1 /tmp/salida-c2 /tmp/salida-c3


# ------------------------------------------------------------------------------------------------
# Experiment_8
#
#   Tiempo desde que se mata un switch hasta que el controlador lo detecta. 
#   Usando el scenario de bucle4_bfd se mata el switch s2 y se obtiene la hora
#   de su muerte, despué en las trazas del controlador se obtiene cuando éste
#   lo ha detectado (NOT MANAGED) 
# ------------------------------------------------------------------------------------------------
# BUCLE4_BFD
sudo python3 stats.py test_scenarios.ScenariosTestCase.experiment_8_kill_s2       Experiment_8 s2-killed         5 /tmp/salida-s0




# ------------------------------------------------------------------------------------------------
# Experiment_9
# 
#    Segmentos TCP repetidos debido a la conmutación de camino principal a camino alternativo
#    En el escenario bucle4, se arranca iperf servidor TCP (s0) y cliente TCP (s7) enviando 200M durante 10 segundos 
#    y se tira uno de los switches (s6) para que se conmute de camino principal a camino alternativo. 
#    Se está capturando el tráfico TCP entre s0 y s7 en la interfaz s1-eth0. Se observa número de paquetes duplicados 
#    retransmisiones.
#    Pruebas con BFD a 100ms, BFD a 10ms y NOBFD
# ------------------------------------------------------------------------------------------------
# BUCLE4_BFD 100ms
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_9_100ms_bfd_scenario  Experiment_9_bin_100ms 100ms_bfd_min_tx 1 /tmp/salida-s0

# BUCLE4_BFD 10ms
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_9_10ms_bfd_scenario   Experiment_9_bin_100ms 10ms_bfd_min_tx  1 /tmp/salida-s0 

# BUCLE4_NOBFD 
sudo python3 stats.py  test_scenarios.ScenariosTestCase.experiment_9_nobfd_scenario      Experiment_9_bin_100ms nobfd            1 /tmp/salida-s0 
