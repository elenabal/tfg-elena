import unittest

from graphs import *
import networkx as nx


class TestGraphMethods(unittest.TestCase):

    def setUp(self):
        self.node_r1 = "R1"
        self.node_r2 = "R2"
        self.node_r3 = "R3"
        self.node_r4 = "R4"
        self.node_r5 = "R5"
        
        self.DG = nx.DiGraph()
    
        self.DG.add_edges_from([(self.node_r1, self.node_r2)], weight=1, port=0)
        self.DG.add_edges_from([(self.node_r1, self.node_r3)], weight=1, port=1)
        self.DG.add_edges_from([(self.node_r1, self.node_r4)], weight=1, port=2)

        self.DG.add_edges_from([(self.node_r2, self.node_r1)], weight=1, port=0)
        self.DG.add_edges_from([(self.node_r2, self.node_r5)], weight=1, port=1)
        self.DG.add_edges_from([(self.node_r2, self.node_r4)], weight=1, port=2)
        self.DG.add_edges_from([(self.node_r2, self.node_r3)], weight=1, port=3)

        self.DG.add_edges_from([(self.node_r3, self.node_r1)], weight=1, port=0)
        self.DG.add_edges_from([(self.node_r3, self.node_r2)], weight=1, port=1)
        self.DG.add_edges_from([(self.node_r3, self.node_r4)], weight=1, port=2)

        self.DG.add_edges_from([(self.node_r4, self.node_r2)], weight=1, port=0)
        self.DG.add_edges_from([(self.node_r4, self.node_r3)], weight=1, port=1)
        self.DG.add_edges_from([(self.node_r4, self.node_r1)], weight=1, port=2)
        self.DG.add_edges_from([(self.node_r4, self.node_r5)], weight=1, port=3)

        self.DG.add_edges_from([(self.node_r5, self.node_r2)], weight=1, port=0)
        self.DG.add_edges_from([(self.node_r5, self.node_r4)], weight=1, port=1)


    def test_attributes(self):
        g = graph(self.DG, self.node_r1, self.node_r5)
        self.assertEqual(attributes(self.DG, g[0], "port"), [0, 1])
        self.assertEqual(attributes(self.DG, g[1], "port"), [2, 3])
        self.assertEqual(attributes(self.DG, g[2], "port"), [2, 3])        

        g = graph(self.DG, self.node_r5, self.node_r1)
        self.assertEqual(attributes(self.DG, g[0], "port"), [0, 0])
        self.assertEqual(attributes(self.DG, g[1], "port"), [1, 2])
        self.assertEqual(attributes(self.DG, g[2], "port"), [2, 2])        

        g = graph(self.DG, self.node_r3, self.node_r4)
        self.assertEqual(attributes(self.DG, g[0], "port"), [2])
        self.assertEqual(attributes(self.DG, g[1], "port"), [0, 2])

        g = graph(self.DG, self.node_r5, self.node_r3)
        self.assertEqual(attributes(self.DG, g[0], "port"), [0, 3])
        self.assertEqual(attributes(self.DG, g[1], "port"), [1, 1])
        self.assertEqual(attributes(self.DG, g[2], "port"), [0, 1])        

        g = graph(self.DG, self.node_r4, self.node_r3)
        self.assertEqual(attributes(self.DG, g[0], "port"), [1])
        self.assertEqual(attributes(self.DG, g[1], "port"), [0, 3])

        

    def test_graph(self):
        g = graph(self.DG, self.node_r1, self.node_r5)
        self.assertEqual(g, [['R1', 'R2', 'R5'], ['R1', 'R4', 'R5'], ['R2', 'R4', 'R5']])

        g = graph(self.DG, self.node_r5, self.node_r1)
        self.assertEqual(g, [['R5', 'R2', 'R1'], ['R5', 'R4', 'R1'], ['R2', 'R4', 'R1']])


        g = graph(self.DG, self.node_r3, self.node_r4)
        self.assertEqual(g, [['R3', 'R4'], ['R3', 'R1', 'R4']])


        g = graph(self.DG, self.node_r5, self.node_r3)
        self.assertEqual(g, [['R5', 'R2', 'R3'], ['R5', 'R4', 'R3'], ['R2', 'R1', 'R3']])
        

    def test_toBitarray(self):
        self.assertEqual(toBitarray(6,4), bitarray('0110'))
        
        
    def test_encode_path(self):
        ep = encode_path([1,2,3,7], 5)
        self.assertEqual(ep, bitarray('00001000100001100111'))

        ep = encode_path([1,2,3,7], 3)
        self.assertEqual(ep, bitarray('001010011111'))
        
    def test_encode_graph(self):

        g = graph(self.DG, self.node_r1, self.node_r5)
        eg = encode_graph(self.DG, g, 3, 4)
        self.assertEqual(eg, bitarray('00000100100110010010010011'))

        g = graph(self.DG, self.node_r1, self.node_r5)
        eg = encode_graph(self.DG, g, 3, 2)
        self.assertEqual(eg, bitarray('0001001001100110010011'))
        
        g = graph(self.DG, self.node_r5, self.node_r1)
        eg = encode_graph(self.DG, g, 4, 3)
        self.assertEqual(eg, bitarray('000001000010010000001000100010'))


                                       

        

if __name__ == '__main__':
    unittest.main()

