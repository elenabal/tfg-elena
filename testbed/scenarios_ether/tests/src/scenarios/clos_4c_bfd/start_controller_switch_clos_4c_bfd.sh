#!/bin/bash

declare -A ifaces

# ifaces array que indica el número de interfaces en cada switch.
# s1, s2, s3, s4 -> 5 interfaces
# s5, s6, s9, s10, s13, s14, s17, s18 -> 4 interfaces
# s7, s8, s11, s12, s15, s16, s19, s20 -> 2 interfaces

ifaces[c1]=1
ifaces[c2]=1
ifaces[c3]=1
ifaces[c4]=1

echo "Creando switch ci interfaces=${ifaces[c$i]}"
for i in {1..4}
do
   echo ./scripts/start-controller-switch-n-ifaces.sh c$i ${ifaces[c$i]} 10.0.0.1 11.0.0.$i bfd
   ./scripts/start-controller-switch-n-ifaces.sh c$i ${ifaces[c$i]} 10.0.0.1 11.0.0.$i bfd
   
done

