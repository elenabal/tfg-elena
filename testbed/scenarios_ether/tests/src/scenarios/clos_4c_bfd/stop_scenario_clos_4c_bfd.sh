#!/bin/bash

declare -A ifaces

# ifaces array que indica el número de interfaces en cada switch.
# s1, s2, s3, s4 -> 5 interfaces
# s5, s6, s9, s10, s13, s14, s17, s18 -> 4 interfaces
# s7, s8, s11, s12, s15, s16, s19, s20 -> 2 interfaces
ifaces[c1]=1
ifaces[c2]=1
ifaces[c3]=1
ifaces[c4]=1

ifaces[s1]=5
ifaces[s2]=5
ifaces[s3]=5
ifaces[s4]=5

for i in 5 6 9 10 13 14 17 18
do
   ifaces[s$i]=4
done
for i in 7 8 11 12 15 16 19 20
do
   ifaces[s$i]=2
done

for i in {1..4}
do
    echo "Eliminando Switch c$i interfaces=${ifaces[c$i]}"
    ./scripts/kill-switch.sh c$i ${ifaces[c$i]}
done

for i in {1..20}
do
    num=`expr 100 + $i`
    echo "Eliminando Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/kill-switch.sh s$i ${ifaces[s$i]} 
done



