#!/bin/bash

declare -A ifaces

 
ifaces[c0]=1
ifaces[c1]=1

for i in {1..15}
do
   ifaces[s$i]=2
done


echo "Creando switch c0 interfaces=${ifaces[c0]}"
./scripts/start-controller-switch-n-ifaces.sh c0 ${ifaces[c0]} 10.0.0.1 11.0.0.1 bfd

echo "Creando switch c1 interfaces=${ifaces[c1]}"
./scripts/start-controller-switch-n-ifaces.sh c1 ${ifaces[c1]} 10.0.0.1 11.0.0.2 bfd

echo "Creando switch s1 interfaces=${ifaces[s1]} vecino controlador"
./scripts/start-switch.sh s1 ${ifaces[s1]} 10.0.0.101 100

echo "Creando switch s15 interfaces=${ifaces[s15]} vecino controlador"
./scripts/start-switch.sh s15 ${ifaces[s15]} 10.0.0.115 100

# Iniciar el resto de los switches 
for i in 2 14 3 13 4 12 5 11 6 10 7 9 8
do
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100
done


