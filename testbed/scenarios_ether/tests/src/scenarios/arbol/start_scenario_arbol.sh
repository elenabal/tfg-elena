#!/bin/bash

declare -A ifaces


ifaces[s0]=1

ifaces[s1]=3
ifaces[s2]=2
ifaces[s3]=2
ifaces[s4]=2


echo "Creando switch s0 interfaces=${ifaces[s0]}"
./scripts/start-controller-switch-n-ifaces.sh s0 ${ifaces[s0]} 10.0.0.1 11.0.0.1


# Iniciar el resto de los switches en paralelo
for i in {1..4}
do
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num
done



