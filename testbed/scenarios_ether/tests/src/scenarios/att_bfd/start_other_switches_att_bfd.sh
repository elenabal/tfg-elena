#!/bin/bash


function inicia_switches(){
    for (( i=$1; i<=$2; i++ ))
    do
        num=`expr 100 + $i`
        echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
        ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100
    done
}

declare -A ifaces

# ifaces array que indica el número de interfaces en cada switch.
# s1 -> 5 interfaces
# s2, s3, s4, s5, s6, s9, s10, s13, s14, s17, s18 -> 4 interfaces
# s7, s8, s11, s12, s15, s16, s19, s20 -> 2 interfaces

for i in 2 3 13 15 18 19 21 23 24 35 37 38 39 40 42 43 44 45 46 49 51 53 57 59 61 64 68 69 72 73 75 76 77 79 80 85 86 88 91 93 95 96
do
   ifaces[s$i]=1
done

for i in 5 11 12 16 27 34 48 50 60 63 66 71 78 84 89 92 94
do
   ifaces[s$i]=2
done

for i in 4 7 9 10 17 22 25 26 29 30 31 33 52 56 62 65 70 81 90
do
   ifaces[s$i]=3
done

for i in 6 8 14 28 55 87
do
   ifaces[s$i]=4
done

for i in 1 32 47 54 74 82
do
   ifaces[s$i]=5
done


for i in 20 36 41 58 67 83
do
   ifaces[s$i]=6
done

# 60 SWITCHES

#---------------------------------------------------
# Inicia switches vecinos a controlador
#---------------------------------------------------
for i in 1 20 50 62 81
do 
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100
done

#---------------------------------------------------
# Inicia switches distancia 2  a controlador
#---------------------------------------------------
for i in 47 2 3 4 10 22 23 25 26 28 29 34 58 82 94
do 
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100
done

#---------------------------------------------------
# Inicia algunos switches distancia 3 a controlador
#---------------------------------------------------
for i in 21 14
do 
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100
done

 
# Inicia el resto
for i in 5 6 7 8 9 11 12 13 15 16 17 18 19 24 27 30 31 32 33 35 36 37 38 39 40 41 42 43 44 45 46 48 49 51 52 53 54 55 56 57 59 60 61 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 83 84 85 86 87 88 89 90 91 92 93 95 96
do
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100 &
done
 
