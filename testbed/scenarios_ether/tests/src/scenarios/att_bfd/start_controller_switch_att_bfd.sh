#!/bin/bash
declare -A ifaces

# ifaces array que indica el número de interfaces en cada switch.
# s1 -> 5 interfaces
# s2, s3, s4, s5, s6, s9, s10, s13, s14, s17, s18 -> 4 interfaces
# s7, s8, s11, s12, s15, s16, s19, s20 -> 2 interfaces

ifaces[c0]=1
ifaces[c1]=1
ifaces[c2]=1
ifaces[c3]=1
ifaces[c4]=1


echo "Creando switch c0 interfaces=${ifaces[c0]}"
./scripts/start-controller-switch.sh c0 ${ifaces[c0]} 10.0.0.1 11.0.0.1

echo "Creando switch c1 interfaces=${ifaces[c1]}"
./scripts/start-controller-switch.sh c1 ${ifaces[c1]} 10.0.0.1 11.0.0.2

echo "Creando switch c2 interfaces=${ifaces[c2]}"
./scripts/start-controller-switch.sh c2 ${ifaces[c2]} 10.0.0.1 11.0.0.3

echo "Creando switch c3 interfaces=${ifaces[c3]}"
./scripts/start-controller-switch.sh c3 ${ifaces[c3]} 10.0.0.1 11.0.0.4

echo "Creando switch c4 interfaces=${ifaces[c4]}"
./scripts/start-controller-switch.sh c4 ${ifaces[c4]} 10.0.0.1 11.0.0.5

