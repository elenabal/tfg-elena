#!/bin/bash

declare -A ifaces


ifaces[s1]=3
ifaces[s2]=4
ifaces[s3]=4
ifaces[s4]=3

ifaces[s21]=3
ifaces[s22]=2
ifaces[s23]=3
ifaces[s24]=2

ifaces[s31]=3
ifaces[s32]=2
ifaces[s33]=3
ifaces[s34]=2

ifaces[s41]=3
ifaces[s42]=2
ifaces[s43]=2
ifaces[s44]=2


echo "Creando switch s1 interfaces=${ifaces[s1]} vecino controlador"
./scripts/start-switch.sh s1 ${ifaces[s1]} 10.0.0.101

# Iniciar el resto de los switches 
for i in 2 3 21 23 4 31 33 22 24 32 34 41 42 43 44
do
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 
done


