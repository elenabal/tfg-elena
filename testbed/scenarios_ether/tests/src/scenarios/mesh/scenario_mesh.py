#!/usr/bin/python


from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info


# Topologia
#                                      +--+
#                                      |s0|
#                                      +--+
#                                       1|
#                                        |
#                                       1|
#                                        |
#                                       +--+
#                                 +-----|s1|----+
#                                 / 2   +--+ 3   \
#                                /                \
#                           3   /1                1\   
#                             +--+                 +--+ 3
#               +-------------|s2|                 |s3|-----------+
#              /              +--+                 +--+            \
#             /             4 / \2                2/ \4             \
#          1 /               /   \                /   \              \1
#          +---+            /     \              /     \              +---+
#          |s21|           /       \            /       \             |s31|
#          +---+          /         \          /         \            +---+
#         2/   \3        /           \        /           \          3/   \ 2
#        1/     \2   1  /             \      /             \    1   2/     \1
#     +---+     +---+  /               \    /               \    +---+     +---+
#     |s22|     |s23|--+               \   /                 +---|s33|     |s32|
#     +---+     +---+                   \ /                      +---+     +---+
#        2\     /3                     1\ /2                        3\     /2
#         1\   /2                       +--+                          2\   /1
#          +---+                        |s4|                           +---+ 
#          |s24|                        +--+                           |s34|
#          +---+                        3 |                            +---+
#                                       1 |
#                                       +---+
#                                       |s41|
#                                       +---+
#                                      2/   \3
#                                      /     \
#                                    1/       \1
#                                 +---+       +---+
#                                 |s42|       |s43|
#                                 +---+       +---+
#                                    2\       /2
#                                      \     /
#                                      1\   /2
#                                       +---+
#                                       |s44|
#                                       +---+

def ovsns(batch = False):

    "Create an empty network and add nodes to it."

    net = Mininet( topo=None, build=False)

    s0 = net.addHost( 's0', ip='0.0.0.0' )
    s1 = net.addHost( 's1', ip='0.0.0.0' )
    s2 = net.addHost( 's2', ip='0.0.0.0' )
    s3 = net.addHost( 's3', ip='0.0.0.0' )
    s4 = net.addHost( 's4', ip='0.0.0.0' )

    s21 = net.addHost( 's21', ip='0.0.0.0' )
    s22 = net.addHost( 's22', ip='0.0.0.0' )
    s23 = net.addHost( 's23', ip='0.0.0.0' )
    s24 = net.addHost( 's24', ip='0.0.0.0' )

    s31 = net.addHost( 's31', ip='0.0.0.0' )
    s32 = net.addHost( 's32', ip='0.0.0.0' )
    s33 = net.addHost( 's33', ip='0.0.0.0' )
    s34 = net.addHost( 's34', ip='0.0.0.0' )

    s41 = net.addHost( 's41', ip='0.0.0.0' )
    s42 = net.addHost( 's42', ip='0.0.0.0' )
    s43 = net.addHost( 's43', ip='0.0.0.0' )
    s44 = net.addHost( 's44', ip='0.0.0.0' )

    sw_dict={0:s0, 1:s1, 2:s2, 3:s3, 4:s4, 21:s21, 22:s22, 23:s23, 24:s24, 
            31:s31, 32:s32, 33:s33, 34:s34, 41:s41, 42:s42, 43:s43, 44:s44}

    net.addLink( s0, s1, 0, 0 )
    net.addLink( s1, s2, 1, 0 )
    net.addLink( s1, s3, 2, 0 )
    net.addLink( s2, s4, 1, 0 )
    net.addLink( s3, s4, 1, 1 )

    net.addLink( s2, s21, 2, 0 )
    net.addLink( s2, s23, 3, 0 )
    net.addLink( s21, s22, 1, 0 )
    net.addLink( s21, s23, 2, 1 )
    net.addLink( s22, s24, 1, 0 )
    net.addLink( s23, s24, 2, 1 )

    net.addLink( s3, s31, 2, 0 )
    net.addLink( s3, s33, 3, 0 )
    net.addLink( s31, s32, 1, 0 )
    net.addLink( s31, s33, 2, 1 )
    net.addLink( s32, s34, 1, 0 )
    net.addLink( s33, s34, 2, 1 )

    net.addLink( s4, s41, 2, 0 )
    net.addLink( s41, s42, 1, 0 )
    net.addLink( s41, s43, 2, 0 )
    net.addLink( s42, s44, 1, 0 )
    net.addLink( s43, s44, 1, 1 )

    
    for i in sw_dict.keys():
       print (sw_dict[i])
       if i == 0:
           s0.cmd("ip link set s0-eth0 address 00:00:00:00:00:01")
           s0.cmd("ip link set dev s0-eth0  mtu 1400")
       else: 
           n_ifaces=0
           if i in [22, 24, 32, 34, 42, 43, 44]:
               n_ifaces=2
           elif i in [1, 4, 21, 23, 31, 33, 41]:
               n_ifaces=3
           elif i in [2, 3]:
               n_ifaces=4

           for iface in range(n_ifaces):
               if i in [1, 2, 3, 4]:
                   sw_dict[i].cmd("ip link set s"+ str(i) +"-eth"+ str(iface) + " address 00:00:00:00:0"+str(i)+":0"+str(iface))
                   sw_dict[i].cmd("ip link set dev s"+ str(i) +"-eth"+ str(iface) + " mtu 1400")
               else:
                   sw_dict[i].cmd("ip link set s"+ str(i) +"-eth"+ str(iface) + " address 00:00:00:00:"+str(i)+":0"+str(iface))
                   sw_dict[i].cmd("ip link set dev s"+ str(i) +"-eth"+ str(iface) + " mtu 1400")

    if not batch:
        net.start()
        CLI( net )        
        net.stop()
    else: # batch
        return net, [s0]


if __name__ == '__main__':
    setLogLevel( 'info' )
    ovsns()
