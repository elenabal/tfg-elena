#!/bin/bash

declare -A ifaces

 

for i in {1..15}
do
   ifaces[s$i]=2
done

echo "Creando switch s1 interfaces=${ifaces[s1]} vecino controlador"
./scripts/start-switch.sh s1 ${ifaces[s1]} 10.0.0.101 100

# Iniciar el resto de los switches 
for i in {2..15}
do
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100
done


