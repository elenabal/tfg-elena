#!/bin/bash

declare -A ifaces

ifaces[s0]=1

for i in {1..15}
do
   ifaces[s$i]=2
done

echo "Eliminando Switch s0 interfaces=${ifaces[s0]}"
./scripts/kill-switch.sh s0 ${ifaces[s0]}

for i in {1..15}
do
    num=`expr 100 + $i`
    echo "Eliminando Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/kill-switch.sh s$i ${ifaces[s$i]} 
done

