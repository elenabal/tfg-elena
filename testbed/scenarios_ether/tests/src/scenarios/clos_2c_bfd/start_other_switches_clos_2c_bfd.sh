#!/bin/bash

declare -A ifaces

# ifaces array que indica el número de interfaces en cada switch.
# s2, s4 -> 4 interfaces
# s1, s3 -> 5 interfaces
# s5, s6, s9, s10, s13, s14, s17, s18 -> 4 interfaces
# s7, s8, s11, s12, s15, s16, s19, s20 -> 2 interfaces

ifaces[s1]=5
ifaces[s2]=4
ifaces[s3]=5
ifaces[s4]=4

for i in 5 6 9 10 13 14 17 18
do
   ifaces[s$i]=4
done
for i in 7 8 11 12 15 16 19 20
do
   ifaces[s$i]=2
done


# Switches de los controladores
echo "Switch s1 IP=10.0.0.101 interfaces=${ifaces[s1]}"
./scripts/start-switch.sh s1 ${ifaces[s1]} 10.0.0.101 100 

echo "Switch s3 IP=10.0.0.103 interfaces=${ifaces[s3]}"
./scripts/start-switch.sh s3 ${ifaces[s3]} 10.0.0.103 100 


# Resto de switches
echo "Switch s2 IP=10.0.0.102 interfaces=${ifaces[s2]}"
./scripts/start-switch.sh s2 ${ifaces[s2]} 10.0.0.102 100 &

echo "Switch s4 IP=10.0.0.104 interfaces=${ifaces[s4]}"
./scripts/start-switch.sh s4 ${ifaces[s4]} 10.0.0.104 100 &

for i in {5..20}
do
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100 &
done

