#!/usr/bin/python



from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf

# Topologia
#
#                             c1                                         c3                           
#                              |                                          |                          
#                              |                                          |                          
#                             s1                     s2                   s3                        s4
#                              |                     |                    |                         |
#                          ____|_____________________|_____            ___|_________________________|______
#                         |    |      |           |        |          |   |      |              |          |
#                       __|____|______|___________|______  |        __|___|______|______________|_______   |
#                      |  |        |  |       |   |     |  |       |  |       |  |          |   |       |  |
#                       s5          s9         s13       s17        s6         s10           s14         s18
#                       |            |          |         |         |          |              |           |
#                       |            |          |         |_________|__________|______________|________   |
#                       |            |          |                   |          |              |        |  |
#                       |            |          |___________________|__________|________      |        |  |
#                       |            |                              |          |       |      |        |  |
#                    ___|____________|______________________________|          |       |      |        |  |
#                   |   |     |     _|________________________________________ |     __|______|_     __|__|_____ 
#                 __|___|___  |   _|_|__   |                                       _|__|____    |   |   __|___  |
#                |  |       | |  | |    |  |                                      | |       |   |   |  |      | |
#                 s7         s8  s11    s12                                       s15        s16     s19      s20
#


# Lanzaremos 4 controladores 

def ovsns(batch = False):

    "Create an empty network and add nodes to it."

    net = Mininet( topo=None,
                   build=False)

    c1 = net.addHost( 'c1', ip='0.0.0.0.', mac='00:00:00:00:00:01' )
    c3 = net.addHost( 'c3', ip='0.0.0.0.', mac='00:00:00:00:00:03' )

    s1 = net.addHost( 's1', ip='0.0.0.0',  mac='00:00:00:00:01:00' )
    s2 = net.addHost( 's2', ip='0.0.0.0',  mac='00:00:00:00:02:00' )
    s3 = net.addHost( 's3', ip='0.0.0.0',  mac='00:00:00:00:03:00' )
    s4 = net.addHost( 's4', ip='0.0.0.0',  mac='00:00:00:00:04:00' )
    s5 = net.addHost( 's5', ip='0.0.0.0',  mac='00:00:00:00:05:00' )
    s6 = net.addHost( 's6', ip='0.0.0.0',  mac='00:00:00:00:06:00' )
    s7 = net.addHost( 's7', ip='0.0.0.0',  mac='00:00:00:00:07:00' )
    s8 = net.addHost( 's8', ip='0.0.0.0',  mac='00:00:00:00:08:00' )
    s9 = net.addHost( 's9', ip='0.0.0.0',  mac='00:00:00:00:09:00' )
    s10 = net.addHost( 's10', ip='0.0.0.0',  mac='00:00:00:00:10:00' )
    s11 = net.addHost( 's11', ip='0.0.0.0',  mac='00:00:00:00:11:00' )
    s12 = net.addHost( 's12', ip='0.0.0.0',  mac='00:00:00:00:12:00' )
    s13 = net.addHost( 's13', ip='0.0.0.0',  mac='00:00:00:00:13:00' )
    s14 = net.addHost( 's14', ip='0.0.0.0',  mac='00:00:00:00:14:00' )
    s15 = net.addHost( 's15', ip='0.0.0.0',  mac='00:00:00:00:15:00' )
    s16 = net.addHost( 's16', ip='0.0.0.0',  mac='00:00:00:00:16:00' )
    s17 = net.addHost( 's17', ip='0.0.0.0',  mac='00:00:00:00:17:00' )
    s18 = net.addHost( 's18', ip='0.0.0.0',  mac='00:00:00:00:18:00' )
    s19 = net.addHost( 's19', ip='0.0.0.0',  mac='00:00:00:00:19:00' )
    s20 = net.addHost( 's20', ip='0.0.0.0',  mac='00:00:00:00:20:00' )

    # Add in link options for different experiments
    linkopts = dict(bw=1000)
    linkopts2 = dict(delay='1ms', bw=1000);

    net.addLink(c1, s1, 0, 0, cls=TCLink,**linkopts)
    net.addLink(c3, s3, 0, 0, cls=TCLink,**linkopts)

    net.addLink(s1, s5, 1, 0, cls=TCLink,**linkopts)
    net.addLink(s1, s9, 2, 0, cls=TCLink,**linkopts)
    net.addLink(s1, s13, 3, 0, cls=TCLink,**linkopts)
    net.addLink(s1, s17, 4, 0, cls=TCLink,**linkopts)
    net.addLink(s2, s5, 0, 1, cls=TCLink,**linkopts)
    net.addLink(s2, s9, 1, 1, cls=TCLink,**linkopts)
    net.addLink(s2, s13, 2, 1, cls=TCLink,**linkopts)
    net.addLink(s2, s17, 3, 1, cls=TCLink,**linkopts)
    net.addLink(s3, s6, 1, 0, cls=TCLink,**linkopts)
    net.addLink(s3, s10, 2, 0, cls=TCLink,**linkopts)
    net.addLink(s3, s14, 3, 0, cls=TCLink,**linkopts)
    net.addLink(s3, s18, 4, 0, cls=TCLink,**linkopts)
    net.addLink(s4, s6, 0, 1, cls=TCLink,**linkopts)
    net.addLink(s4, s10, 1, 1, cls=TCLink,**linkopts)
    net.addLink(s4, s14, 2, 1, cls=TCLink,**linkopts)
    net.addLink(s4, s18, 3, 1, cls=TCLink,**linkopts)

    net.addLink(s5, s7, 2, 0, cls=TCLink,**linkopts)
    net.addLink(s5, s8, 3, 0, cls=TCLink,**linkopts)
    net.addLink(s6, s7, 2, 1, cls=TCLink,**linkopts)
    net.addLink(s6, s8, 3, 1, cls=TCLink,**linkopts)
    net.addLink(s9, s11, 2, 0, cls=TCLink,**linkopts)
    net.addLink(s9, s12, 3, 0, cls=TCLink,**linkopts)
    net.addLink(s10, s11, 2, 1, cls=TCLink,**linkopts)
    net.addLink(s10, s12, 3, 1, cls=TCLink,**linkopts)
    net.addLink(s13, s15, 2, 0, cls=TCLink,**linkopts)
    net.addLink(s13, s16, 3, 0, cls=TCLink,**linkopts)
    net.addLink(s14, s15, 2, 1, cls=TCLink,**linkopts)
    net.addLink(s14, s16, 3, 1, cls=TCLink,**linkopts)
    net.addLink(s17, s19, 2, 0, cls=TCLink,**linkopts)
    net.addLink(s17, s20, 3, 0, cls=TCLink,**linkopts)
    net.addLink(s18, s19, 2, 1,  cls=TCLink,**linkopts)
    net.addLink(s18, s20, 3, 1, cls=TCLink,**linkopts)




    # s1 5 interfaces: c1, s5, s9, s13, s17
    s1.cmd("ip link set s1-eth1 address 00:00:00:00:01:01")
    s1.cmd("ip link set s1-eth2 address 00:00:00:00:01:02")
    s1.cmd("ip link set s1-eth3 address 00:00:00:00:01:03")
    s1.cmd("ip link set s1-eth4 address 00:00:00:00:01:04")

    # s2 4 interfaces: s5, s9, s13, s17
    s2.cmd("ip link set s2-eth1 address 00:00:00:00:02:01")
    s2.cmd("ip link set s2-eth2 address 00:00:00:00:02:02")
    s2.cmd("ip link set s2-eth3 address 00:00:00:00:02:03")

    # s3 5 interfaces: c3, s6, s10, s14, s18
    s3.cmd("ip link set s3-eth1 address 00:00:00:00:03:01")
    s3.cmd("ip link set s3-eth2 address 00:00:00:00:03:02")
    s3.cmd("ip link set s3-eth3 address 00:00:00:00:03:03")
    s3.cmd("ip link set s3-eth4 address 00:00:00:00:03:04")

    # s4 4 interfaces: s6, s10, s14, s18
    s4.cmd("ip link set s4-eth1 address 00:00:00:00:04:01")
    s4.cmd("ip link set s4-eth2 address 00:00:00:00:04:02")
    s4.cmd("ip link set s4-eth3 address 00:00:00:00:04:03")

    # s5 4 interfaces: s1, s2, s7, s8
    s5.cmd("ip link set s5-eth1 address 00:00:00:00:05:01")
    s5.cmd("ip link set s5-eth2 address 00:00:00:00:05:02")
    s5.cmd("ip link set s5-eth3 address 00:00:00:00:05:03")

    # s6 4 interfaces: s3, s4, s7, s8
    s6.cmd("ip link set s6-eth1 address 00:00:00:00:06:01")
    s6.cmd("ip link set s6-eth2 address 00:00:00:00:06:02")
    s6.cmd("ip link set s6-eth3 address 00:00:00:00:06:03")

    # s7 2 interfaces: s5, s6
    s7.cmd("ip link set s7-eth1 address 00:00:00:00:07:01")

    # s8 2 interfaces: s5, s6
    s8.cmd("ip link set s8-eth1 address 00:00:00:00:08:01")

    # s9 4 interfaces: s1, s2, s11, s12
    s9.cmd("ip link set s9-eth1 address 00:00:00:00:09:01")
    s9.cmd("ip link set s9-eth2 address 00:00:00:00:09:02")
    s9.cmd("ip link set s9-eth3 address 00:00:00:00:09:03")

    # s10 4 interfaces: s3, s4, s11, s12
    s10.cmd("ip link set s10-eth1 address 00:00:00:00:10:01")
    s10.cmd("ip link set s10-eth2 address 00:00:00:00:10:02")
    s10.cmd("ip link set s10-eth3 address 00:00:00:00:10:03")

    # s11 2 interfaces: s9, s10
    s11.cmd("ip link set s11-eth1 address 00:00:00:00:11:01")

    # s12 2 interfaces: s9, s10
    s12.cmd("ip link set s12-eth1 address 00:00:00:00:12:01")

    # s13 4 interfaces: s1, s2, s15, s16
    s13.cmd("ip link set s13-eth1 address 00:00:00:00:13:01")
    s13.cmd("ip link set s13-eth2 address 00:00:00:00:13:02")
    s13.cmd("ip link set s13-eth3 address 00:00:00:00:13:03")

    # s14 4 interfaces: s3, s4, s15, s16
    s14.cmd("ip link set s14-eth1 address 00:00:00:00:14:01")
    s14.cmd("ip link set s14-eth2 address 00:00:00:00:14:02")
    s14.cmd("ip link set s14-eth3 address 00:00:00:00:14:03")

    # s15 2 interfaces: s13, s14
    s15.cmd("ip link set s15-eth1 address 00:00:00:00:15:01")

    # s16 2 interfaces: s13, s14
    s16.cmd("ip link set s16-eth1 address 00:00:00:00:16:01")

    # s17 4 interfaces: s1, s2, s19, s20
    s17.cmd("ip link set s17-eth1 address 00:00:00:00:17:01")
    s17.cmd("ip link set s17-eth2 address 00:00:00:00:17:02")
    s17.cmd("ip link set s17-eth3 address 00:00:00:00:17:03")

    # s18 4 interfaces: s3, s4, s19, s20
    s18.cmd("ip link set s18-eth1 address 00:00:00:00:18:01")
    s18.cmd("ip link set s18-eth2 address 00:00:00:00:18:02")
    s18.cmd("ip link set s18-eth3 address 00:00:00:00:18:03")

    # s19 2 interfaces: s17, s18
    s19.cmd("ip link set s19-eth1 address 00:00:00:00:19:01")

    # s20 2 interfaces: s17, s18
    s20.cmd("ip link set s20-eth1 address 00:00:00:00:20:01")

    if not batch:
        net.start()
        CLI( net )        
        net.stop()
    else: # batch
        return net, [c1, c3]
        
if __name__ == '__main__':
    setLogLevel( 'info' )
    ovsns()
