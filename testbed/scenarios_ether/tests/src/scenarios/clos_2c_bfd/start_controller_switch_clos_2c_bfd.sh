#!/bin/bash

declare -A ifaces

# ifaces array que indica el número de interfaces en cada switch.
ifaces[c1]=1
ifaces[c3]=1



echo ./scripts/start-controller-switch-n-ifaces.sh c1 ${ifaces[c1]} 10.0.0.1 11.0.0.1 bfd
./scripts/start-controller-switch-n-ifaces.sh c1 ${ifaces[c1]} 10.0.0.1 11.0.0.1 bfd

echo ./scripts/start-controller-switch-n-ifaces.sh c3 ${ifaces[c3]} 10.0.0.1 11.0.0.3 bfd
./scripts/start-controller-switch-n-ifaces.sh c3 ${ifaces[c3]} 10.0.0.1 11.0.0.3 bfd

