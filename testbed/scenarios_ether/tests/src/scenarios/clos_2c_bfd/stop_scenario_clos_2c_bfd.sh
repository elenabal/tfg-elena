#!/bin/bash

declare -A ifaces

# ifaces array que indica el número de interfaces en cada switch.
# s2, s4 -> 4 interfaces
# s1, s3 -> 5 interfaces
# s5, s6, s9, s10, s13, s14, s17, s18 -> 4 interfaces
# s7, s8, s11, s12, s15, s16, s19, s20 -> 2 interfaces
ifaces[c1]=1
ifaces[c3]=1

ifaces[s1]=5
ifaces[s2]=4
ifaces[s3]=5
ifaces[s4]=4

for i in 5 6 9 10 13 14 17 18
do
   ifaces[s$i]=4
done
for i in 7 8 11 12 15 16 19 20
do
   ifaces[s$i]=2
done

echo "Eliminando Switch c1 interfaces=${ifaces[c1]}"
./scripts/kill-switch.sh c1 ${ifaces[c1]}
echo "Eliminando Switch c3 interfaces=${ifaces[c3]}"
./scripts/kill-switch.sh c3 ${ifaces[c3]}

for i in {1..20}
do
    num=`expr 100 + $i`
    echo "Eliminando Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/kill-switch.sh s$i ${ifaces[s$i]} 
done



