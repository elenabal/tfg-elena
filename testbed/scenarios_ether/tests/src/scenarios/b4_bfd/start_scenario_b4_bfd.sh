#!/bin/bash

declare -A ifaces


ifaces[c0]=1

ifaces[s1]=2
ifaces[s2]=2
ifaces[s3]=3
ifaces[s4]=5
ifaces[s5]=3
ifaces[s6]=4
ifaces[s7]=4
ifaces[s8]=4
ifaces[s9]=2
ifaces[s10]=4
ifaces[s11]=4
ifaces[s12]=2


echo "Creando switch c0 interfaces=${ifaces[c0]}"
./scripts/start-controller-switch-n-ifaces.sh c0 ${ifaces[c0]} 10.0.0.1 11.0.0.1 bfd


echo "Creando switch s1 interfaces=${ifaces[s1]} vecino controlador"
./scripts/start-switch.sh s1 ${ifaces[s1]} 10.0.0.101 100

# Iniciar el resto de los switches 
for i in 2 5 3 4 7 8 6 10 9 11 12
do
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100
done


