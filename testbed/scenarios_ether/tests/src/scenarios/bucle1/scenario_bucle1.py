#!/usr/bin/python

import sys

from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info


# Topologia
#                                                              
#       1         1    2              1   2            1    2          1
#    s0 ----------- s1 --------------- s2 -------------- s3 ------------ h2
# 10.0.0.1           \ 3            3 /                               10.0.0.2
#                     \              /
#                      \          3 /           1
#                       -------- s4 ------------- h3
#                              1    2            10.0.0.3
#
# Lanzaremos un controlador independiente en s0

def ovsns(batch = False):


    "Create an empty network and add nodes to it."

    net = Mininet( topo=None,
                   build=False)

    s0 = net.addHost( 's0', ip='0.0.0.0.', mac='00:00:00:00:00:01' )
    s1 = net.addHost( 's1', ip='0.0.0.0',  mac='00:00:00:00:11:10' )
    s2 = net.addHost( 's2', ip='0.0.0.0',  mac='00:00:00:00:22:20' )
    s3 = net.addHost( 's3', ip='0.0.0.0',  mac='00:00:00:00:33:30' )
    s4 = net.addHost( 's4', ip='0.0.0.0',  mac='00:00:00:00:44:40' )
    h2 = net.addHost( 'h2', ip='10.0.0.2', mac='00:00:00:00:00:02' )
    h3 = net.addHost( 'h3', ip='10.0.0.3', mac='00:00:00:00:00:03' )

    net.addLink( s0, s1, 0, 0 )
    net.addLink( s1, s2, 1, 0 )
    net.addLink( s1, s4, 2, 0 )
    net.addLink( s2, s3, 1, 0 )
    net.addLink( s3, h2, 1, 0 )
    net.addLink( s4, h3, 1, 0 )
    net.addLink( s2, s4, 2, 2 )

    s1.cmd("ip link set s1-eth1 address 00:00:00:00:11:11")
    s1.cmd("ip link set s1-eth2 address 00:00:00:00:11:12")
    s2.cmd("ip link set s2-eth1 address 00:00:00:00:22:21")
    s2.cmd("ip link set s2-eth2 address 00:00:00:00:22:22")
    s3.cmd("ip link set s3-eth1 address 00:00:00:00:33:31")
    s4.cmd("ip link set s4-eth1 address 00:00:00:00:44:41")
    s4.cmd("ip link set s4-eth2 address 00:00:00:00:44:42")

#    s1.cmd("ifconfig s1-eth1 mtu 1000 up")
#    s1.cmd("ifconfig s1-eth2 mtu 1000 up")
#    s2.cmd("ifconfig s2-eth1 mtu 1000 up")
#    s2.cmd("ifconfig s2-eth2 mtu 1000 up")
#    s3.cmd("ifconfig s3-eth1 mtu 1000 up")
#    s4.cmd("ifconfig s4-eth1 mtu 1000 up")
#    s4.cmd("ifconfig s4-eth2 mtu 1000 up")


    #s0.cmd("ip link set dev s0-eth0 mtu 1300")
#
#    s1.cmd("ip link set dev s1-eth0 mtu 1300")
#    s1.cmd("ip link set dev s1-eth1 mtu 1300")
#    s1.cmd("ip link set dev s1-eth2 mtu 1300")
#
#    s2.cmd("ip link set dev s2-eth0 mtu 1300")
#    s2.cmd("ip link set dev s2-eth1 mtu 1300")
#    s2.cmd("ip link set dev s2-eth2 mtu 1300")
#
#    s3.cmd("ip link set dev s3-eth0 mtu 1300")
#    s3.cmd("ip link set dev s3-eth1 mtu 1300")
#
#    s4.cmd("ip link set dev s4-eth0 mtu 1300")
#    s4.cmd("ip link set dev s4-eth1 mtu 1300")
#    s4.cmd("ip link set dev s4-eth2 mtu 1300")



    if not batch:
        net.start()
        CLI( net )        
        net.stop()
    else: # batch
        return net, [s0]



if __name__ == '__main__':
    setLogLevel( 'info' )
    ovsns()
