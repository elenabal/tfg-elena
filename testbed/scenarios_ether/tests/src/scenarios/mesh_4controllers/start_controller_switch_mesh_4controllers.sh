#!/bin/bash

declare -A ifaces


for i in 0 1 2 3
do
   ifaces[c$i]=1
done

for i in 1 4 21 23 31 33 41 24 34 44
do
   ifaces[s$i]=3
done

for i in 22 32 42 43
do
   ifaces[s$i]=2
done

for i in 2 3
do
   ifaces[s$i]=4
done



echo "Creando switches de controladores"

for i in 0 1 2 3
do
    num=`expr 1 + $i`
    echo "Switch del controlador c$i IP=10.0.0.1 interfaces=${ifaces[c$i]} 11.0.0.$num bfd"
    ./scripts/start-controller-switch-n-ifaces.sh c$i ${ifaces[c$i]} 10.0.0.1 11.0.0.$num bfd
done
   
# Iniciamos los switches que están conectados directamente con los 4c
for i in 24 34 44 1
do
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100
done

