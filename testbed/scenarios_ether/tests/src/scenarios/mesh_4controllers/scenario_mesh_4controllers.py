#!/usr/bin/python


from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf


# Topologia
#                                      +--+
#                                      |c0|
#                                      +--+
#                                       1|
#                                        |
#                                       1|
#                                        |
#                                       +--+
#                                 +-----|s1|----+
#                                 / 2   +--+ 3   \
#                                /                \
#                           3   /1                1\   
#                             +--+                 +--+ 3
#               +-------------|s2|                 |s3|-----------+
#              /              +--+                 +--+            \
#             /             4 / \2                2/ \4             \
#          1 /               /   \                /   \              \1
#          +---+            /     \              /     \              +---+
#          |s21|           /       \            /       \             |s31|
#          +---+          /         \          /         \            +---+
#         2/   \3        /           \        /           \          3/   \ 2
#        1/     \2   1  /             \      /             \    1   2/     \1
#     +---+     +---+  /               \    /               \    +---+     +---+
#     |s22|     |s23|--+               \   /                 +---|s33|     |s32|
#     +---+     +---+                   \ /                      +---+     +---+
#        2\     /3                     1\ /2                        3\     /2
#         1\   /2                       +--+                          2\   /1
#          +---+                        |s4|                           +---+ 
#          |s24|                        +--+                           |s34|
#          +---+                        3 |                            +---+
#           3|                          1 |                             3|
#            |                          +---+                            |
#            |                          |s41|                            |
#           1|                          +---+                           1|
#          +---+                       2/   \3                         +---+
#          |c1|                       /     \                         |c2 |
#          +---+                     1/       \1                       +---+
#                                 +---+       +---+
#                                 |s42|       |s43|
#                                 +---+       +---+
#                                    2\       /2
#                                      \     /
#                                      1\   /2
#                                       +---+
#                                       |s44|
#                                       +---+
#                                        3|
#                                         |
#                                         |
#                                        1|
#                                       +---+
#                                       |c3|
#                                       +---+
#
def ovsns(batch = False):

    "Create an empty network and add nodes to it."

    net = Mininet( topo=None, build=False)

    c0  = net.addHost( 'c0', ip='0.0.0.0', mac='00:00:00:00:00:01' )
    c1 = net.addHost( 'c1', ip='0.0.0.0', mac='00:00:00:00:00:02' )
    c2 = net.addHost( 'c2', ip='0.0.0.0', mac='00:00:00:00:00:03' )
    c3 = net.addHost( 'c3', ip='0.0.0.0', mac='00:00:00:00:00:04' )

    s1 = net.addHost( 's1', ip='0.0.0.0', mac='00:00:00:00:01:00')
    s2 = net.addHost( 's2', ip='0.0.0.0', mac='00:00:00:00:02:00')
    s3 = net.addHost( 's3', ip='0.0.0.0', mac='00:00:00:00:03:00')
    s4 = net.addHost( 's4', ip='0.0.0.0', mac='00:00:00:00:04:00')

    s21 = net.addHost( 's21', ip='0.0.0.0', mac='00:00:00:00:21:00')
    s22 = net.addHost( 's22', ip='0.0.0.0', mac='00:00:00:00:22:00')
    s23 = net.addHost( 's23', ip='0.0.0.0', mac='00:00:00:00:23:00')
    s24 = net.addHost( 's24', ip='0.0.0.0', mac='00:00:00:00:24:00')

    s31 = net.addHost( 's31', ip='0.0.0.0', mac='00:00:00:00:31:00')
    s32 = net.addHost( 's32', ip='0.0.0.0', mac='00:00:00:00:32:00')
    s33 = net.addHost( 's33', ip='0.0.0.0', mac='00:00:00:00:33:00')
    s34 = net.addHost( 's34', ip='0.0.0.0', mac='00:00:00:00:34:00')

    s41 = net.addHost( 's41', ip='0.0.0.0', mac='00:00:00:00:41:00')
    s42 = net.addHost( 's42', ip='0.0.0.0', mac='00:00:00:00:42:00')
    s43 = net.addHost( 's43', ip='0.0.0.0', mac='00:00:00:00:43:00')
    s44 = net.addHost( 's44', ip='0.0.0.0', mac='00:00:00:00:44:00')

   # Add in link options for different experiments
    linkopts = dict(bw=1000)
    linkopts2 = dict(delay='1ms', bw=1000);

    net.addLink( c0, s1, 0, 0, cls=TCLink, **linkopts)
    net.addLink( s1, s2, 1, 0, cls=TCLink, **linkopts )
    net.addLink( s1, s3, 2, 0, cls=TCLink, **linkopts )
    net.addLink( s2, s4, 1, 0, cls=TCLink, **linkopts )
    net.addLink( s3, s4, 1, 1, cls=TCLink, **linkopts )

    net.addLink( s2, s21, 2, 0, cls=TCLink, **linkopts )
    net.addLink( s2, s23, 3, 0, cls=TCLink, **linkopts )
    net.addLink( s21, s22, 1, 0, cls=TCLink, **linkopts )
    net.addLink( s21, s23, 2, 1, cls=TCLink, **linkopts )
    net.addLink( s22, s24, 1, 0, cls=TCLink, **linkopts )
    net.addLink( s23, s24, 2, 1, cls=TCLink, **linkopts )
    net.addLink( s24, c1, 2, 0, cls=TCLink, **linkopts )

    net.addLink( s3, s31, 2, 0, cls=TCLink, **linkopts )
    net.addLink( s3, s33, 3, 0, cls=TCLink, **linkopts )
    net.addLink( s31, s32, 1, 0, cls=TCLink, **linkopts )
    net.addLink( s31, s33, 2, 1, cls=TCLink, **linkopts )
    net.addLink( s32, s34, 1, 0, cls=TCLink, **linkopts )
    net.addLink( s33, s34, 2, 1, cls=TCLink, **linkopts )
    net.addLink( s34, c2, 2, 0, cls=TCLink, **linkopts )

    net.addLink( s4, s41, 2, 0, cls=TCLink, **linkopts )
    net.addLink( s41, s42, 1, 0, cls=TCLink, **linkopts )
    net.addLink( s41, s43, 2, 0, cls=TCLink, **linkopts )
    net.addLink( s42, s44, 1, 0, cls=TCLink, **linkopts )
    net.addLink( s43, s44, 1, 1, cls=TCLink, **linkopts )
    net.addLink( s44, c3, 2, 0, cls=TCLink, **linkopts )
 
    # s1 3 interfaces
    s1.cmd("ip link set s1-eth1 address 00:00:00:00:01:01")
    s1.cmd("ip link set s1-eth2 address 00:00:00:00:01:02")

    # s2 4 interfaces
    s2.cmd("ip link set s2-eth1 address 00:00:00:00:02:01")
    s2.cmd("ip link set s2-eth2 address 00:00:00:00:02:02")
    s2.cmd("ip link set s2-eth3 address 00:00:00:00:02:03")

    # s3 4 interfaces
    s3.cmd("ip link set s3-eth1 address 00:00:00:00:03:01")
    s3.cmd("ip link set s3-eth2 address 00:00:00:00:03:02")
    s3.cmd("ip link set s3-eth3 address 00:00:00:00:03:03")

    # s4 3 interfaces
    s4.cmd("ip link set s4-eth1 address 00:00:00:00:04:01")
    s4.cmd("ip link set s4-eth2 address 00:00:00:00:04:02")

    # s21 3 interfaces
    s21.cmd("ip link set s21-eth1 address 00:00:00:00:21:01")
    s21.cmd("ip link set s21-eth2 address 00:00:00:00:21:02")

    # s22 2 interfaces
    s22.cmd("ip link set s22-eth1 address 00:00:00:00:22:01")

    # s23 3 interfaces
    s23.cmd("ip link set s23-eth1 address 00:00:00:00:23:01")
    s23.cmd("ip link set s23-eth2 address 00:00:00:00:23:02")

    # s24 2 interfaces
    s24.cmd("ip link set s24-eth1 address 00:00:00:00:24:01")

    # s31 3 interfaces
    s31.cmd("ip link set s31-eth1 address 00:00:00:00:31:01")
    s31.cmd("ip link set s31-eth2 address 00:00:00:00:31:02")

    # s32 2 interfaces
    s32.cmd("ip link set s32-eth1 address 00:00:00:00:32:01")

    # s33 3 interfaces
    s33.cmd("ip link set s33-eth1 address 00:00:00:00:33:01")
    s33.cmd("ip link set s33-eth2 address 00:00:00:00:33:02")

    # s34 2 interfaces
    s34.cmd("ip link set s34-eth1 address 00:00:00:00:34:01")

    # s41 3 interfaces
    s41.cmd("ip link set s41-eth1 address 00:00:00:00:41:01")
    s41.cmd("ip link set s41-eth2 address 00:00:00:00:41:02")

    # s42 2 interfaces
    s42.cmd("ip link set s42-eth1 address 00:00:00:00:42:01")

    # s43 2 interfaces
    s43.cmd("ip link set s43-eth1 address 00:00:00:00:43:01")

    # s44 3 interfaces
    s44.cmd("ip link set s44-eth1 address 00:00:00:00:44:01")
    s44.cmd("ip link set s44-eth2 address 00:00:00:00:44:02")




    if not batch:
        net.start()
        CLI( net )        
        net.stop()
    else: # batch
        return net, [c0, c1, c2, c3]


if __name__ == '__main__':
    setLogLevel( 'info' )
    ovsns()
