#!/bin/bash

declare -A ifaces


ifaces[s1]=4
ifaces[s2]=2
ifaces[s3]=3
ifaces[s4]=4
ifaces[s5]=2
ifaces[s6]=2
ifaces[s7]=2
ifaces[s8]=2



# Switch vecino al controlador
echo "Switch s1 IP=10.0.0.101 interfaces=${ifaces[s1]}"
./scripts/start-switch.sh s1 ${ifaces[s1]} 10.0.0.101

sleep 0.5

# Iniciar el resto de los switches en paralelo
for i in 2 5 6 3 4 7 8
do
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num
done



