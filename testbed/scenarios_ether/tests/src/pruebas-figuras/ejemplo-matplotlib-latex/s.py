import csv 

import matplotlib as mpl
# Use the pgf backend (must be done before import pyplot interface)
mpl.use('pgf')

import matplotlib.pyplot as plt
from scipy.stats import norm




# Use the seborn style
plt.style.use('seaborn')
# But with fonts from the document body
plt.rcParams.update({
    "font.family": "serif",  # use serif/main font for text elements
    "text.usetex": True,     # use inline math for ticks
    "pgf.rcfonts": False     # don't setup fonts from rc parameters
    })






def set_size(width_pt, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to sit nicely in our document.

    Parameters
    ----------
    width_pt: float
            Document width in points
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)


x = []
y = []

with open('data.csv', 'r') as csvfile:
    lines = csv.reader(csvfile, delimiter=',')
    for row in lines:
        x.append(float(row[0]))
        y.append(float(row[1]))

fig, ax = plt.subplots(1, 1, figsize=set_size(345))
# ver cómo determinar figsize: https://jwalton.info/Embed-Publication-Matplotlib-Latex/
    
#    ax.scatter(x, y, marker='o', linestyle='-')
ax.plot(x, y, marker='.', linestyle='-')
ax.set_ylabel('Bandwidth (bps)')
ax.set_xlabel('Time (s)')

fig.tight_layout()
fig.savefig('norm.pgf', format='pgf')
