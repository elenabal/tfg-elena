import sys
import time
import pyshark

''' 
Every sys.argv[2] seconds it calculates bandwidth in sys.argv[1] pcap file

Outputs in stdout tick, bandwidth pairs for every tick of sys.argv[2] duration
'''
if (len(sys.argv) != 3):
    print("usage: ")
    print("        First  arg is the name of a .pcap file")
    print("        Second arg is a float with the # of seconds of bin.")
    sys.exit(1)

PERIOD_S = float(sys.argv[2])

current_tick = 0.0
bytes = 0

capture = pyshark.FileCapture(sys.argv[1])
 
packet_time = 0.0
for packet in capture:
    packet_time += float(packet.frame_info.time_delta)

    if (packet_time >= current_tick + PERIOD_S) :
        print (str(current_tick)+ ", " +  str(bytes))
        current_tick += PERIOD_S
        bytes = 0
        
    try:
        bytes += int(packet.frame_info.len)
    except AttributeError:
        pass

# last tick
print (str(current_tick)+ ", " +  str(bytes))
    





