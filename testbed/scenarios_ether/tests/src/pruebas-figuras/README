** Notas

Podemos caracterizar las topologías con diámetro, número de nodos,
grado medio de los nodos



==============================   PRUEBAS   ===================================



* Experiment_1 Network Bootstrap time (1-n controllers)

Qué: (a) tiempo desde que el primer switch se pone en contacto con su
controlador hasta que lo hace el último. Variante (b) desde que el
primero está managed hasta que lo está el último

Cómo: (b) se saca del log directamente. (a) también pero hay que buscar el SYN correspondiente


* Experiment_2 Throughput loss when failures + Interarrival time of packets 

Qué: Se mide con iperf entre 2 hosts separados al
máximo. Se elige un enlace en el camino principal que tenga un backup y
lo hacen fallar. Experimentos de 30s. Link failure tras 10s
Qué: The IAT will show an increase in its value after a failure due to the
recovery time required to overcome it.



* Experiment_3 Switch Bootstrap time (a distancia 1-n del controlador)

Qué: For each added switch, we measure and average the difference
between the instant i) when a switch is provided with the control flow
rules; ii) when the switch was first observed by a controller



* Experiment_4 Communication overhead (network con 1-n controladores)

Qué: número de mensajes máximo que se intercambian en toda la red hasta que todos son MANAGED

Cómo: bastaría con contar los mensajes capturados en s1-eth0 y los equivalentes para el resto de controladores

Para un único switch: nuevo escenario en el que arrancamos un único nodo s1 y contamos paquetes en captura de s1-eth0

Para varios switches y varios controladores:
Lo hacemos con bucle_4_bfd (1 controlador) y con 2controllers (2 controladores)
y
Lo hacemos con mesh (1 controlador) y mesh_4controllers (4 controladores)



Experiment_5  Controller recovery time (1-n)

Qué: tiempo desde que se paran n controladores hasta que todos los
switches están con otro controlador

Cómo: arrancamos escenario mesh. Esperamos a que estén todos. Matamos el nodo de n controladores. En el log fusionado de todos los controladores, medimos tiempo desde el primer NOT MANAGED hasta el último MANAGED. Esto funciona porque a pesar de matar el nodo en el que está corriendo el controlador, el proceso del controlador sigue funcionando e imprime en su traza los NOT MANAGED.




Experiment_6 Controller bootstrap communication convergence time (1-n)

Qué: Tiempo desde que un controlador conoce a otro hasta que todos conocen a todos

Cómo: egrep "informing of new"  /tmp/salida-c0  | awk '{if (!IP[$NF]) IP[$NF]=$1;} END{for (var in IP) print IP[var] "\t" var}' | sort -k 1



Experiment_7 Non-adjacent Controller communication resiliency after controller failure

    Qué: Tiempo desde que falla un controlador en el camino entre 2
    controladores no adyacentes hasta que estos pueden seguir
    comunicándose tras haberse reconectado los switches del
    controlador que ha fallado con otros controladores

    Cómo En mesh_4controllers matamos uno u otro controlador: s0, s20,
    s30 o s40




Experiment_8 Switch recovery time

Qué: tiempo desde que se para un switch hasta que su controlador lo da por fallido

Cómo: diferencia de tiempos desde que se mata switch hasta que en log
sale NOT MANAGED.

Habría que medir cuánto pasa hasta que las rutas que pasaban por ese
switch han sido reinstaladas en los switches / controladores
afectados. Entiendo que basta con un ciclo de instalación de reglas
pues los cambios de rutas se aplican periodicamente a todos los nodos
de la red



Experiment_9 duplicated tcp packets





* 8 Adjacent Controller communication resiliency after switch failure

Tiempo desde que falla un switch en el camino entre dos controladores
adyacentes hasta que estos vuelven a poderse comunicar

En realidad esto no mide nada nuevo 



* 9 Non-adjacent Controller communication resiliency after switch failure

Tiempo desde que falla un switch  en el camino entre dos controladores
no adyacentes hasta que estos vuelven a poderse comunicar





* 12 Link recovery time
Qué: tiempo desde que se tira un enlace hasta que su controlador lo da
por fallido








* 15 Retransmisiones when failures

Qué: Con wireshark ven retransmisiones tras tirar enlace. Miden
incremento en el porcentaje de paquetes retransmitidos



* 16 BAD TCP when failures

Qué: Con wireshark ven retransmisiones tras tirar enlace. Miden
incremento en el porcentaje de paquetes con flag BAD TCP según Wireshark



17* Flow table occupancy:

Qué: No. of active flow entries in a flow table after successful
bootstrapping procedure.

Cómo: wc de dump-flows


----------------------------  todo   -------------
Quizá un test para ver cuánto de bien se distribuye la carga entre múltiples controladores



==============================  REFERENCIAS  ===================================


** Pruebas que hacen los de Renaissance: A Self-Stabilizing,...

1. Tiempo de bootstrap

1.1 Bootstrap time. Tiempo hasta que todos los controladores alcanzan
estado legítimo (cada controlador puede comunicarse con cualquier otro
nodo de la red (pudiendo instalar reglas en cualquier switch)

3 controladores en topologías pequeñas, 7 en las grandes

1.2 Communication overhead. Communication cost per node. Número de
mensajes neceario para que un controlador alcance estado
legítimo. "Concretely, we measure the maximum number of controller
messages". No entiendo muy bien.


2. Recovery times

2.1 Recovery after the occurrence of one  controller's fail-stop failure
2.2 Recovery after the occurrence of multiple but not all fail-strop controller failures
2.3 Recovery after the occurrence of switch's fail-stop failure. Tiran
un switch aleatoriamente y ven cuánto tarda la red en recuperarse (the
time it takes the system to regain legitimacy (stability))
2.4 Recovery after the occurrence of permanent link-failures. Idem.


3. Overhead

4. Throughput during failures 

4.1 Throughput loss Miden con iperf entre 2 hosts separados al
máximo. Eligen un enlace en el camino principal que tenga un backup y
lo hacen fallar. Experimentos de 30s. Link failure tras 10s.

4.2 Con wireshark ven retransmisiones tras tirar enlace. Miden
incremento en el porcentaje de paquetes retransmitidos y de paquetes
con flag BAD TCP.



** Pruebas que hacen los de Automated Bootstrapping of a
   Fault-Resilient In-Band Control Plane (Sakic, Avdic, vanBemten,
   Kellerer)

1. Global bootstrapping convergence time (GBCT): difference between i)
the time instant at which all switches are provisioned with resilient
flow rules; and ii) the instant when the first switch was observed by
any controller

2. Time required to extend the network (TEXT): For each added switch, we
measure and average the difference between the instant i) when a
switch is provided with the control flow rules; ii) when the switch
was first observed by a controllerw

3. Flow table occupancy (FTO): No. of active flow entries in a flow
table after successful bootstrapping procedure.

Topologías: line, ring, star



** Pruebas que hacen en Amaru

The evaluation studies the three core features of Amaru:
1) automatic bootstrapping => convergence time
2) scalability             => packet comsumption
3) resiliency              => throughput during failure events

Se comparan con: ResilientFlow [6] (basado en OSPF) y Assadujjaman et al. [27]

4 topologías:
1) mesh-like topology of 10 nodes in figure 11
2) Barabasi and Bonabeau del Brite topology generator
3) Waxman del Brite topology generator
4) Finally, we also tested a real network topology adapted from Rogers
fiber backbone network [46], as shown in Fig. 1 and Fig. 16. This
topology encompasses 19 nodes connected by 24 links spanning
approximately 4,920 km, with a total fiber length of approximately
11,183 km [27]

Respecto a topologías 2) y 3): "Although these types of networks are
also synthetic, they claim to illustrate some of the most
representative topologies of the Internet. The experiments were
conducted configuring different average number of links per node
(3,5,7) and different network sizes, from 10 to 60 nodes using 10-node
steps. The link bandwidth is constant and the Waxman parameters to the
default values alfa=0.15 and beta=0.2. The minimum number of runs per
experiment was set up to 10 to calculate standard deviations.

Pruebas:

1) Convergence time. We can define the convergence time as the time
that the Amaru protocol requires to obtain the number of desired paths
according to the parameters N and L. This convergence time mainly
depends on the size of the network and the average node degree.

Comparan con RSTP en Fig. 13. Moreover, it is important to highlight
how Amaru clearly outperforms RSTP, as it converges 500 times faster
than RSTP, in average. 

2) Packet consumption.

We analyzed the total number of packets required to complete a full
exploration and address configuration procedure in Amaru. 

También comparan con RSTP.

In average, RSTP requires a number of packets 7,7% higher than Amaru.

Amaru always requires a small number of packets in larger topologies.

También comparan con OSPF en esta métrica. The reason for this is that
Amaru can obtain multiple paths per root node and RSTP not, while OSPF
obtains the full knowledge of a topology in a distributed way to later
calculate any desired k-shortest paths according to certain metric.

The number of packets in OSPF is one order of magniturde larger than
Amaru, more specifically OSPF requires around 30 times more packets
than Amaru in average. The main reason for this could be the same as
in the case of the convergence time: the exploration nature of Amaru,
which allows carrying the information at once through the network
without the need of multiple message excanges among neighbors, as RSTP
and OSPF require.

3) Recovery time

Diferencian entre controller => switch y switch => controller.

Para estas pruebas usan 2 topologies: the first topology (fig 11) is symmetric
to measure how fast similar paths are selected, while the second one
is a real scenario: the Rogers topology.

3.1) Throughput of in-band traffic

3.2) Inter-arrival time (IAT) between received in-band packets. The
IAT will show an increase in its value after a failure due to the
recovery time required to overcome it.


The test consists of evaluating the throughput from A to H (fig 11)
where the ARoot is connected (switch => controller). Node A has saved
three routes A-C-E-H, A-B-D-F-H and A-G-I-J-H (respectivamente puertos
1, 2 y 3). 

The first link failure happens at time 1s and the in-band traffic is
rerouted through the path through port 2. Finally, a second failure
occurs at time 2s and the traffic is redirected once again, this time
through port 3. 

En figura 17 muestran 3.2 en la banda superior, y 3.1, para C=>S y
S=>C en ambos casos (4 figuras de la izda en fig 17).

Luego repiten el experimento en la topología Rogers real de Canadá (4
figuras de la derecha). Con 4 fallos de enlace en 4 segundos, en lugar
de 2, "to check failures at different distances from the SDN
controller".

Prueban entre los nodos Toronto y Victoria. Fallos introducidos en
Victoria-Vancouver y los otros 3 están también marcados en figura
16. Resultados similares a los de la otra topología.

Esta disquisición sobre dónde introducen los fallos: Though
improbable, a corner case would be if all learned paths used at least
one of the failed links. In this case, none of the learned AMACs could
work as alternative routes and the Amaru discovery mechanism should be
relaunched, either locally or, in the worse case, globally.

Convendría por ellos que cuando introduzcamos fallos tengamos en
cuenta si los fallos provocan o no recálculo pues ello se reflejaría
en los tiempos medidos. Quizá las pruebas pueden comprobarlo
automáticamente y así descartar o etiquetar el tipo de fallos
introducidos. 


Los números son impresionantes incluso cuando es en el sentido menos
favorable, e incluso cuando no tienen caminos alternativos y tiene que
recalcular. Ver último párrafo en columna izda pp 123215


