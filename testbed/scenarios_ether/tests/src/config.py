import networkx as nx

from ryu import cfg


class Config():

    def __init__(self):
        self._DG = nx.DiGraph()

        CONF = cfg.CONF
        CONF.register_opts([
            cfg.BoolOpt('use_bfd_param', default=False,
                           help = ('Set to True if failure detection must be done using BFD protocol')),
            cfg.StrOpt('controller_switch_param', default="s0",
                           help = ('Set to the name of the switch co-located with the controller')),
            cfg.StrOpt('controller_ip_param',
                       help = ('Set to the ip address of the switch co-located with the controller'))])
        self._USE_BFD           = CONF.use_bfd_param
        self._CONTROLLER_SWITCH = CONF.controller_switch_param
        self._CONTROLLER_IP     = CONF.controller_ip_param        

        
    def DG(self):
        return self._DG

    def isIn(self, address):
        return address in self._DG.nodes.keys()

    def setProperty(self, address, property, value):
        self._DG.nodes[address][property] = value

    def getProperty(self, address, property):
        if address in self._DG.nodes() and property in self._DG.nodes[address].keys():
            return self._DG.nodes[address][property]
        else:
            return None

    def nodes(self):
        return list(self._DG.nodes.keys())


    def peer_controllers(self):
        nc  = {}
        nnc = {}
        
        for n in self.DG().nodes.keys():
            if "is_neighbor_controller" in self._DG.nodes[n].keys():
                nc[n]=self.DG().nodes[n]["learnt_from"]
            elif "is_non_neighbor_controller" in self._DG.nodes[n].keys():
                nnc[n]=self.DG().nodes[n]["learnt_from"]
        return nc, nnc
    
    def use_bfd(self):
        return self._USE_BFD

    def controller_switch(self):
        return self._CONTROLLER_SWITCH

    def controller_ip(self):
        return self._CONTROLLER_IP
    
