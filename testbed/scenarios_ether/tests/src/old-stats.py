import unittest
import sys
import os

TEST_NAME = sys.argv[1]
OUTPUT_FILE=sys.argv[2]
MAX_EXPERIMENTS = int(sys.argv[3])
ARGS_BEFORE_CONTROLLER_LOGS = 4
CONTROLLER_LOG_FILE=sys.argv[ARGS_BEFORE_CONTROLLER_LOGS]

TEMP_FILE = "/tmp/salida"


def time_of (fileName, pattern, position):
    """
    	Returns timestamp of position line in fileName containing pattern:wq
	Assumes lines of fileName begin with "timestamp:"
    """
    results = []
    with open(fileName, 'r') as f:
        for line in f.readlines():
            if pattern in line:
                results.append(line)

    return results[position].split(":")[0] 

def sort (fileName):
    """ 
    returns file f'{fileName}.sorted' with contents of fileName sorted by 
    timestamp
    """

    with open(fileName, "r") as f, open(f'{fileName}.sorted', "w") as fSorted:
        lines = f.readlines()

        for line in sorted(lines, key=lambda line: line.split(":")[0]):
            print (line, file=fSorted)
    

intentos_exitosos = 1
with open(OUTPUT_FILE, "w") as f:
    while (intentos_exitosos <= MAX_EXPERIMENTS):

        # remove temp files
        try:
            os.remove(TEMP_FILE)
            os.remove(f'{TEMP_FILE}.sorted')            
        except OSError as e:
            print("Error: %s : %s" % ("temporal files", e.strerror))

        # remove controller log files
        for cf in range(ARGS_BEFORE_CONTROLLER_LOGS, len(sys.argv) - 1):
            controller_log_file = sys.argv[cf]
            try:
                os.remove(controller_log_file)
            except OSError as e:
                print("Error: %s : %s" % (controller_log_file, e.strerror))

        # run tests
        test_suite = unittest.TestLoader().loadTestsFromName(TEST_NAME)
        test_result = unittest.TextTestRunner().run(test_suite)


        if (test_result.wasSuccessful()):
            if (len(sys.argv) > ARGS_BEFORE_CONTROLLER_LOGS + 1):
                print ("CONCATENAR")
                # concatenar ficheros de log de controladores
                for cf in range(ARGS_BEFORE_CONTROLLER_LOGS, len(sys.argv)):
                    with open(sys.argv[cf], "r") as lf, open(TEMP_FILE, "a") as tf: 
                        lines = lf.readlines()
                        for line in lines:
                            print(line, file=tf)
                        
                # ordenar fichero por marcas de tiempo
                sort(TEMP_FILE)
                CONTROLLER_LOG_FILE = f'{TEMP_FILE}.sorted'

            print ("intentos_exitosos: " + str(intentos_exitosos))
            intentos_exitosos += 1 
            time_first_syn = float(time_of(CONTROLLER_LOG_FILE, "SYN received", 0))
            time_last_managed = float(time_of(CONTROLLER_LOG_FILE, "IS MANAGED", -1))

            f.write(str(time_last_managed - float(time_first_syn)) + "\n" )


