#!/bin/bash

if [ ! $# -eq 1 ]
then
    echo "Usage error: $0 <switchName>"
    exit -1
fi

echo Stop OVSDB for $1

pid_ovsdb_server=`cat /tmp/mininet-$1/ovsdb-server.pid`

ovs-appctl --target=/var/run/openvswitch/ovsdb-server.${pid_ovsdb_server}.ctl exit
rm /tmp/mininet-$1/conf.db
