import sys
import pyshark

def main(output_file, PERIOD_S):
    current_tick = 0.0

    capture = pyshark.FileCapture("/tmp/k.pcap", display_filter="tcp.analysis.retransmission")

    
    packet_time = 0.0
    dup_tcp = 0


    for packet in capture:
        print(">> 2")
        
        packet_time += float(packet.frame_info.time_delta)
        
        if (packet_time >= current_tick + PERIOD_S) :
            print (str(current_tick)+ ", " +  str(dup_tcp), file=output_file)

            current_tick += PERIOD_S
            dup_tcp = 0

        try:
            dup_tcp += 1
            print(">> 3")
        except AttributeError:
            pass

    print(">> 4")
    capture.close()

    # last tick
    print (str(current_tick)+ ", " +  str(dup_tcp), file=output_file)

    print ("bye")

if __name__ == "__main__":
    with open(sys.argv[1], "w") as output_file:
        main(output_file, PERIOD_S = float(sys.argv[2]))
    
