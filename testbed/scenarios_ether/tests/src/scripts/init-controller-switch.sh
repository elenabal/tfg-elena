#!/bin/bash
if [ ! $# -eq 3 ]
then
    echo "Usage error: $0 <switchName> <CONTROLLER_IP> <CONTROLLER_EXT_IP>"
    exit -1
fi

SWITCH_NAME=$1
CONTROLLER_IP=$2
CONTROLLER_EXT_IP=$3

./scripts/startOvsDb.sh $SWITCH_NAME
./scripts/startOvs.sh $SWITCH_NAME
./scripts/createBrSdn.sh $SWITCH_NAME 1 $CONTROLLER_IP
ifconfig $SWITCH_NAME inet $CONTROLLER_IP
ifconfig $SWITCH_NAME:1 inet $CONTROLLER_EXT_IP

