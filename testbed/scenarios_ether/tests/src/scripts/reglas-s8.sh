
ovs-ofctl add-flow s8 "table=0,priority=0,actions=resubmit(,2)"

# la solicitud de ARP se la enviamos al controlador para que conozca a un nuevo switch
ovs-ofctl add-flow s8 "table=2,arp,dl_dst=ff:ff:ff:ff:ff:ff,arp_tpa=10.0.0.1,actions=CONTROLLER"

# gratuitous ARP, luego ya conocemos al switch que lo ha generado: que responda la pila TCP/IP
ovs-ofctl add-flow s8 "table=2,arp,arp_tpa=10.0.0.1,actions=LOCAL"

# respuesta de ARP la genera el controllador y se la manda a s8 como pkt_out, 
# s8 debe enviarla por su interfaz s8-eth0
ovs-ofctl add-flow s8 "table=2,arp,arp_spa=10.0.0.1,actions=s8-eth0"

#DHCP
ovs-ofctl add-flow s8 "table=2,ipv4,udp,tp_src=68,tp_dst=67,actions=LOCAL"
ovs-ofctl add-flow s8 "table=2,ipv4,udp,tp_src=67,tp_dst=68,actions=s8-eth0"

#Cuando envíe mensajes IPv6mcast_node_00 lo tira
ovs-ofctl add-flow s8 "table=2,dl_dst=33:33:00:00:00:00,actions=drop"
ovs-ofctl add-flow s8 "table=2,dl_dst=33:33:00:00:00:16,actions=drop"

# Paquetes TCP se entregan en la pila TCP/IP
ovs-ofctl add-flow s8 "table=2,ipv4,nw_dst=10.0.0.1,tcp,tp_dst=6633,actions=LOCAL"
ovs-ofctl add-flow s8 "table=2,ipv4,nw_dst=11.0.0.8,actions=LOCAL"


#ovs-ofctl add-flow s8 "table=2,ipv4,nw_dst=10.0.0.1,actions=LOCAL"

# Cuando nos llega ACK de s8-eth0 lo tiramos. El controlador instala otra de mayor prioridad con mismo match y actions=LOCAL
ovs-ofctl add-flow s8 "table=2,priority=65533,in_port=1,ipv4,nw_dst=10.0.0.1,tcp,tp_dst=6633,actions=drop"


# LLDP
ovs-ofctl add-flow s8  "table=2,dl_dst=01:80:c2:00:00:00/ff:ff:ff:00:00:00,actions=CONTROLLER"
