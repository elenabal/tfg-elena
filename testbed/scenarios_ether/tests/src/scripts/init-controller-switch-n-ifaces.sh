#!/bin/bash
if [ ! $# -eq 4 ]
then
    echo "Usage error: $0 <switchName> <N_IFACES> <CONTROLLER_IP> <CONTROLLER_EXT_IP>"
    exit -1
fi

SWITCH_NAME=$1
N_IFACES=$2
CONTROLLER_IP=$3
CONTROLLER_EXT_IP=$4

./scripts/startOvsDb.sh $SWITCH_NAME
./scripts/startOvs.sh $SWITCH_NAME
./scripts/createBrSdn.sh $SWITCH_NAME $N_IFACES $CONTROLLER_IP
ifconfig $SWITCH_NAME inet $CONTROLLER_IP
ifconfig $SWITCH_NAME:1 inet $CONTROLLER_EXT_IP

