#!/bin/bash

if [ ! $# -gt 1 ]
then
    echo "Usage error: $0 <switchName> <interfaces> [min_tx in ms]"
    exit -1
fi

SWITCH_NAME=$1
IFACE=$2



MIN_TX=100
if [ $# -eq 3 ]
then
    MIN_TX=$3
fi

echo Starting BFD for $SWITCH_NAME and interface $IFACE with MIN_TX $MIN_TX

ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock set interface $SWITCH_NAME-$IFACE bfd:enable=true bfd:min_rx=0 bfd:min_tx=$MIN_TX
