import pytest
import graphs
from bitarray import bitarray
from bitarray.util import ba2hex
import networkx as nx



######################### PRUEBAS FUNCIONALES ####################################

#Test que verifica el comportamiento correcto de la funcion ports. Devuelve los puertos que relaciona el camino para dichas direcciones IP
def test_ports():
	G = nx.DiGraph()
	G.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
	G.edges['10.0.0.1','10.0.0.2']['port'] = 1
	G.edges['10.0.0.2','10.0.0.3']['port'] = 4
	G.edges['10.0.0.3','10.0.0.4']['port'] = 6
	G.edges['10.0.0.4','10.0.0.1']['port'] = 9
	G.edges['10.0.0.2','10.0.0.4']['port'] = 3
	path = ['10.0.0.1','10.0.0.2','10.0.0.3','10.0.0.4']
	
	assert graphs.ports(G,path) == [1,4,6]
	
#Test que verifica el comportamiento correcto de la funcion ports. Dado solo un arco de conexion, devuelve el puerto que relaciona el camino para dichas direcciones IP
def test_ports_one_edge():
	G = nx.DiGraph()
	G.add_edges_from([('10.0.0.1','10.0.0.2')])
	G.edges['10.0.0.1','10.0.0.2']['port'] = 1
	path = ['10.0.0.1','10.0.0.2']
	assert graphs.ports(G,path) == [1]	
	
	
#Test que verifica el comportamiento correcto de la funcion ports. Dada la lista path vacias, devuelve una lista vacia de puertos.
def test_ports_path_empty():
	G = nx.DiGraph()
	G.add_edges_from([('10.0.0.1','10.0.0.2')])
	G.edges['10.0.0.1','10.0.0.2']['port'] = 1
	path = []
	assert graphs.ports(G,path) == []
	
	
#Test que verifica que se convierte correctamente a bitarray.
def test_toBitarray():
	assert graphs.toBitarray(5,6) == bitarray('000101')
	
	
#Test que verifica que dado un numero negativo, no se puede convertir en bitarray
def test_int_negative_toBitarray():
	with pytest.raises(ValueError):
		assert graphs.toBitarray(-5,6)


#Test que verifica que dado un un numero de bits negativo, no se puede convertir en bitarray
def test_number_bit_negative_toBitarray():
	with pytest.raises(ValueError):
		assert graphs.toBitarray(5,-6)
		
		
#Test que verifica que dado un tipo de parametro distinto a un numero, no se puede convertir en bitarray
def test_other_entry_toBitarray():
	with pytest.raises(ValueError):
		assert graphs.toBitarray('prueba',6)
		
		
#Test que verifica que se codifica correctamente dado una lista de numeros y el numero de bits que queremos representar
def test_encode_path():
	assert graphs.encode_path([1,2,3,4],6) == bitarray('00000100001000001100111100010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000')


#Verificar que si la entrada es un bit negativo, salta una excepcion
def test_encode_path_error_bit_negative():
	with pytest.raises(ValueError):
		assert graphs.encode_path([1,2,3,4],-6) 
	
	
#Test que verifica que si la entrada es una lista de numeros, el cual uno es negativo, salta una excepcion	
def test_encode_path_error_list_negative():
	with pytest.raises(ValueError):
		assert graphs.encode_path([1,-2,3,4],6) 
		
		
#Test que verifica que si la entrada es una lista vacia, rellena el bitarray mediante 0's-> REVISAR!!!!!!DUDA
def test_encode_path_error_list_empty():
	assert graphs.encode_path([],3) == bitarray('00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000')


#Test que verifica el calculo de un camino alternativo dado un grafo y sus nodos.
def test_alt_path():
	G = nx.Graph()
	G.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
	list = ['10.0.0.1','10.0.0.2','10.0.0.3','10.0.0.4']
	assert graphs.alt_path(G,list) == ['10.0.0.1','10.0.0.4']
	
	
#Test que verifica el comportamiento de la funcion nodo alternativo si solamente se tiene una arista. Devolvera lista vacia porque no existen mas caminos.
def test_not_alt_path():
	G = nx.Graph()
	G.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.1')])
	list = ['10.0.0.1','10.0.0.2']
	assert graphs.alt_path(G,list) == []
	
	
#Test que verifica el comportamiento de la funcion nodo alternativo si recibe un grafo vacio. 
def test_not_graph_alt_path():
	G = nx.Graph()
	G.add_edges_from([])
	list = ['10.0.0.1','10.0.0.2']
	with pytest.raises(ValueError):
		assert graphs.alt_path(G,list)
		
		
#Test que verifica el comportamiento de la funcion graphs. Dado un grafo dirigido, un origen, un destino y una ruta por donde debe pasar se comprueba que devuelve la ruta principal y posteriormente rutas alternativas a dicho grafo.
def test_create_graph():
	G = nx.DiGraph() #Creamos un grafo dirigido (con flechas)
	G.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
	assert graphs.graph(G,'10.0.0.2','10.0.0.4','10.0.0.3') == [['10.0.0.2', '10.0.0.3', '10.0.0.4'], [], []]
	
	
#Test que verifica el comportamiento de la funcion graphs. Dado un grafo dirigido, un origen, un destino y sin una ruta de preferencia por donde deberia pasar,devuelve la ruta principal y posteriormente rutas alternativas a dicho grafo.
def test_create_graph_without_through_ancho():
	G = nx.DiGraph()
	G.add_edges_from([('10.0.0.1','10.0.0.3'),('10.0.0.2','10.0.0.1'),('10.0.0.2','10.0.0.3'),('10.0.0.2','10.0.0.4'),('10.0.0.3','10.0.0.5'),('10.0.0.4','10.0.0.3'),('10.0.0.4','10.0.0.8'),('10.0.0.5','10.0.0.6'),('10.0.0.5','10.0.0.7'),('10.0.0.8','10.0.0.5')])
	assert graphs.graph(G,'10.0.0.2','10.0.0.6',None) == [['10.0.0.2', '10.0.0.3', '10.0.0.5', '10.0.0.6'], ['10.0.0.2', '10.0.0.1', '10.0.0.3', '10.0.0.5', '10.0.0.6'], [], []]



#Test que verifica el comportamiento de la funcion graphs. Dado un grafo dirigido, un origen, un destino y una ruta por donde debe pasar, devuelve la ruta principal y posteriormente rutas alternativas a dicho grafo. -> ¡¡OJO!! Error encontrado y solucionado en la funcion graph del modulo graph.py.El error se daba cuando through_anchor no era predecessor del destino.
@pytest.mark.parametrize(
	"input_x,input_y,input_z,expected",
	[
	('10.0.0.2','10.0.0.5','10.0.0.8',[['10.0.0.2', '10.0.0.4', '10.0.0.8', '10.0.0.5'], [], [], []]),
	('10.0.0.2','10.0.0.8','10.0.0.4',[['10.0.0.2', '10.0.0.4', '10.0.0.8'], [], []]),
	('10.0.0.2', '10.0.0.3', '10.0.0.1',[['10.0.0.2', '10.0.0.1', '10.0.0.3'], [], []]),
	('10.0.0.2', '10.0.0.6', '10.0.0.5',[['10.0.0.2', '10.0.0.3', '10.0.0.5', '10.0.0.6'], ['10.0.0.2', '10.0.0.1', '10.0.0.3', '10.0.0.5', '10.0.0.6'], [], []]),
	('10.0.0.2', '10.0.0.6', '10.0.0.8',[['10.0.0.2', '10.0.0.4', '10.0.0.8', '10.0.0.5', '10.0.0.6'], [], [], [], []]),
	('10.0.0.2', '10.0.0.6', '10.0.0.4',[['10.0.0.2', '10.0.0.4', '10.0.0.8', '10.0.0.5', '10.0.0.6'], [], [], [], []]),
	]
)
def test_create_graph_more_primary_path(input_x,input_y,input_z,expected):
	G = nx.DiGraph()
	G.add_edges_from([('10.0.0.1', '10.0.0.3'),('10.0.0.2', '10.0.0.1'),('10.0.0.2', '10.0.0.3'),('10.0.0.2','10.0.0.4'),('10.0.0.3','10.0.0.5'),('10.0.0.4','10.0.0.3'),('10.0.0.4','10.0.0.8'),('10.0.0.5','10.0.0.6'),('10.0.0.5','10.0.0.7'),('10.0.0.8','10.0.0.5')])
		
	assert expected == graphs.graph(G,input_x,input_y,input_z) #TEST->OK!
	
#El fallo consistia en que devolvia una la funcion [] debido a que saltaba una excepcion. Esto se daba porque through_anchor no era predecessor del destino y aun asi borraba dicho arco, por ello saltaba la excepcion. Se ha modificado el codigo para adaptarlo tambien a dicha situacion.

 

#Test que verifica el comportamiento de la funcion encoded_graph. Al pasarle una lista vacia, nos devuelve una lista vacia.
def test_list_empty_encoded_graph():
	G = nx.DiGraph()
	G.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
	assert graphs.encoded_graph(G,[],2,128) == []
	
	
#Test que verifica el comportamiento de la funcion encoded_graph. Nos devuelve el camino principal codificado y el camino alternativo.
def test_encoded_graph():
	G = nx.DiGraph()
	G.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),	('10.0.0.2','10.0.0.4')])
	G.edges['10.0.0.1','10.0.0.2']['port'] = 1
	G.edges['10.0.0.2','10.0.0.3']['port'] = 4
	G.edges['10.0.0.3','10.0.0.4']['port'] = 6
	G.edges['10.0.0.4','10.0.0.1']['port'] = 9
	G.edges['10.0.0.2','10.0.0.4']['port'] = 3
	path = [('10.0.0.1','10.0.0.2','10.0.0.4'),('10.0.0.1','10.0.0.2','10.0.0.3','10.0.0.4')]
	assert graphs.encoded_graph(G,path,2,128) == ['3f300000000000000000000000000000', '14f60000000000000000000000000000']


#Test que verifica el comportamiento de la funcion encoded_graph. Dado solamente un arco, nos devuelve su ruta principal.
def test_encoded_graph_one_edge():
	G = nx.DiGraph()
	G.add_edges_from([('10.0.0.1','10.0.0.101')])
	G.edges['10.0.0.1','10.0.0.101']['port'] = 1
	path = [('10.0.0.1','10.0.0.101')]
	assert graphs.encoded_graph(G,path,2,128) == ['f1000000000000000000000000000000']
	
def test_encoded_graph_one_main_path():
	G = nx.DiGraph()
	G.add_edges_from([('10.0.0.1','10.0.0.101'),('10.0.0.101','10.0.0.102')])
	G.edges['10.0.0.1','10.0.0.101']['port'] = 1
	G.edges['10.0.0.101','10.0.0.102']['port'] = 2
	path = [('10.0.0.1','10.0.0.101','10.0.0.102'),[],[]]
	assert graphs.encoded_graph(G,path,2,128) == ['1f200000000000000000000000000000']


#Test que verifica la funcion create_graph, forzandola para que salte la excepcion.
def test_create_graph_exception():
	G = nx.DiGraph() #Creamos un grafo dirigido (con flechas)
	G.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
	assert graphs.graph(G,'10.0.0.2','10.0.0.4','10.0.0.7') == []

		
		
	
