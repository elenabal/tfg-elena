import pytest
from InBandController import InBandController
import networkx as nx
import graphs
from bitarray import bitarray
from bitarray.util import ba2hex
import config

##### 	PRUEBAS FUNCIONALES ##################################
def test_is_empty(mocker):
    C = config.Config()
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    assert InBandController.is_empty(fake_instance_InbandController,[])
		
		
def test_is_not_empty(mocker):
    C = config.Config()
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    resultado = InBandController.is_empty(fake_instance_InbandController,["dato1","dato2"])
    assert resultado == False
    
    
def test_port_calculation(mocker):
    C = config.Config()
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    expected = 8080
    assert InBandController.port_calculation(fake_instance_InbandController,8088) == expected
    
    
    
def test_negativo_port_calculation(mocker):
    C = config.Config()
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    with pytest.raises(TypeError):
       InBandController.port_calculation(fake_instance_InbandController,[])



def test_state_change_handler_main_dispatcher(mocker):
   class ev:
   	datapath = ['10.0.0.1','10.0.0.2'],
   	state = "MAIN_DISPATCHER"
   	
   C = config.Config()
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   assert None == InBandController._state_change_handler(fake_instance_InbandController,ev)
   
 

   
######## PRUEBAS DE INTEGRACION ##########################

			
#Test que comprueba la funcion search_predecessors.	
def test_search_predecessors(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C

    assert ['10.0.0.3', '10.0.0.2'] == InBandController.search_predecessors(fake_instance_InbandController, [],'10.0.0.4','10.0.0.1')



#Test que verifica la funcion search_predecessors dado un grafo vacio.
def test_search_predecessors_graph_empty(mocker):
    C = config.Config()
    DG = C.DG()
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C

    assert [] == InBandController.search_predecessors(fake_instance_InbandController, [],'any_ip','any_ip')



#Test que verifica la funcion search_predecessors dado un grafo con un solo arco (La Ip no tiene predecesor)
def test_search_predecessors_graph_with_only_ip(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2')])
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C

    assert [] == InBandController.search_predecessors(fake_instance_InbandController, [],'10.0.0.1','any_ip')
    
   
#Test que comprueba la funcion not_edge. Si no existe un arco entre un par de IP's, se crea y se refresca la variable do_refresh	
def test_not_edge_refresh_one_edge(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    do_refresh = False

    assert True == InBandController.not_edge(fake_instance_InbandController, '10.0.0.1', '10.0.0.2', '3', '3', do_refresh)
    
    
    
#Test que comprueba la funcion not_edge. Al existir arcos en ambas direcciones entre dos direcciones IP's, no se refresca la variable do_refresh	
def test_not_edge_not_refresh(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.1'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    do_refresh = False
    
    assert False == InBandController.not_edge(fake_instance_InbandController, '10.0.0.1', '10.0.0.2', '3', '3', do_refresh)
    
    
#Test que comprueba la funcion not_edge. Si no existe ningun arco entre un par de IP's, los crea y se refresca la variable do_refresh	
def test_not_edge_any_edge(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    do_refresh = False

    assert True == InBandController.not_edge(fake_instance_InbandController, '10.0.0.1', '10.0.0.3', '3', '3', do_refresh)


#Test que comprueba la funcion is_neighbor_controller, en el caso de no tener el parametro is_neighbor_controller	
def test_is_neighbor_controller_not_config(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C

    assert False == InBandController._is_neighbor_controller(fake_instance_InbandController, '10.0.0.2')
    
    
#Test que comprueba la funcion is_neighbor_controller, en el caso de tener el valor is_neighbor_controller.	
def test_is_neighbor_controller(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    C.setProperty('10.0.0.2',"is_neighbor_controller",True)
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C

    assert True == InBandController._is_neighbor_controller(fake_instance_InbandController, '10.0.0.2')
    
#Test que comprueba la funcion is_non_neighbor_controller, en el caso de no tener el parametro is_no_neighbor_controller	
def test_is_non_neighbor_controller_not_config(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C

    assert False == InBandController._is_non_neighbor_controller(fake_instance_InbandController, '10.0.0.2')
    
    
#Test que comprueba la funcion is_neighbor_controller, en el caso de tener el valor is_non_neighbor_controller.	
def test_is_non_neighbor_controller(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    C.setProperty('10.0.0.2',"is_non_neighbor_controller",True)
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C

    assert True == InBandController._is_non_neighbor_controller(fake_instance_InbandController, '10.0.0.2')
    
    
#Test que comprueba la funcion remove_edge. En este caso, los parametros de entrada son neighbors y se elimina el arco que los une.
def test_remove_edge(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    InBandController.remove_edge(fake_instance_InbandController, '10.0.0.2', '10.0.0.3')
    
    assert [('10.0.0.1', '10.0.0.2'), ('10.0.0.2', '10.0.0.4'),('10.0.0.3', '10.0.0.4'), ('10.0.0.4', '10.0.0.1')] == list(DG.edges)
    
#Test que comprueba la funcion remove_edge. En este caso, los parametros de entrada son neighbors y se eliminan los arcos que los une en ambos sentidos.
def test_remove_edge_two_edges(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.2'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    
    InBandController.remove_edge(fake_instance_InbandController, '10.0.0.2', '10.0.0.3')
    assert [('10.0.0.1', '10.0.0.2'), ('10.0.0.2', '10.0.0.4'),('10.0.0.3', '10.0.0.4'), ('10.0.0.4', '10.0.0.1')] == list(DG.edges)
    
    
#Test que comprueba la funcion remove_edge. En este caso los parametros de entrada no son neighbors, se eleva la excepcion y segun el codigo debe continuar su funcionamiento con normalidad
def test_remove_edge_not_neighbor(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    
    assert None == InBandController.remove_edge(fake_instance_InbandController, '10.0.0.1', '10.0.0.3')
    


def test_get_through_anchor(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_nodes_from([('10.0.0.1','10.0.0.2',"anchors"),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    C.setProperty('10.0.0.2',"state","not_managed")
    C.setProperty('10.0.0.2',"anchors","anchors")
    C.setProperty('10.0.0.2', "anchors", {'10.0.0.3': 'anchors'})
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    
    assert '10.0.0.3' == InBandController.get_through_anchor(fake_instance_InbandController,None, '10.0.0.2')

    
#Test que comprueba la funcion get_through_anchor. En este caso, uno de los parametros en managed, por ello, se devuelve through_anchor = None 
def test_get_through_anchor_managed(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    C.setProperty('10.0.0.2',"state","managed")
    DG.nodes['10.0.0.2']["anchors"] = ["anchors"]
    DG.nodes['10.0.0.3']["anchors"] = ["anchors"]
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    
    assert None == InBandController.get_through_anchor(fake_instance_InbandController,None, '10.0.0.2')
    
    
#Test que verifica la funcion find_neighbor_by_port
def test_find_neighbor_by_port(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    C.DG()['10.0.0.1']["10.0.0.2"]["port"] = "8080"
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    
    assert '10.0.0.2' == InBandController.find_neighbor_by_port(fake_instance_InbandController,'10.0.0.1', '8080',None)

#Test que verifica la funcion find_neighbor_by_port cuando los puertos son distintos
def test_not_find_neighbor_by_port(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    C.DG()['10.0.0.1']["10.0.0.2"]["port"] = "4040"
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    
    assert None == InBandController.find_neighbor_by_port(fake_instance_InbandController,'10.0.0.1', '8080',None)
    


#Test que verifica la funcion config_period_controller_alive cuando ip_dst y neighbor_controller no tienen un arco que los une.
def test_config_period_controller_alive(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    ip_dst = '10.0.0.2'
    neighbor_controller = '10.0.0.3'
    physical_port_dst = '3'
    InBandController.config_period_controller_alive(fake_instance_InbandController, ip_dst, neighbor_controller, physical_port_dst)
    assert [('10.0.0.1', '10.0.0.2'), ('10.0.0.2', '10.0.0.4'), ('10.0.0.2', '10.0.0.3'), ('10.0.0.3', '10.0.0.4'), ('10.0.0.4', '10.0.0.1')] == list(DG.edges)



#Test que verifica la funcion config_period_controller_alive cuando ip_dst y neighbor_controller no tienen un arco que los une.
def test_config_period_controller_alive_has_edge(mocker):
    C = config.Config()
    DG = C.DG()
    DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
    fake_instance_InbandController = mocker.Mock()
    fake_instance_InbandController.config = C
    ip_dst = '10.0.0.2'
    neighbor_controller = '10.0.0.3'
    physical_port_dst = '3'
    assert None == InBandController.config_period_controller_alive(fake_instance_InbandController, ip_dst, neighbor_controller, physical_port_dst)
    



	
#Test que verifica la funcion send_syn_rcvd. Al ejecutarse correctamente la funcion, devolvera el valor 'enviado'
def test_send_syn_rcvd(mocker):
   class datapath:
   	address = ['10.0.0.1','10.0.0.2']
   class ipv4_pkt:
        src = '255.255.255.0'
   class pkt_eth:
        src = '10.0.0.1'
	
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   datapath = datapath()
   ipv4_pkt = ipv4_pkt()
   pkt_eth = pkt_eth()
   C.setProperty('10.0.0.1',"state","managed")

   assert 'enviado' == InBandController.send_syn_rcvd(fake_instance_InbandController, datapath, ipv4_pkt, pkt_eth, '8088', 'mensaje')


#Test que verifica la funcion send_syn_rcvd. Al faltar configuracion para realizarse las comprobaciones, no se envia el mensaje broadcast.
def test_not_send_syn_rcvd(mocker):
   class datapath:
   	address = ['10.0.0.5','10.0.0.2']
   class ipv4_pkt:
        src = '255.255.255.0'
   class pkt_eth:
        src = '10.0.0.1'
	
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   datapath = datapath()
   ipv4_pkt = ipv4_pkt()
   pkt_eth = pkt_eth()
   C.setProperty('10.0.0.1',"state","managed")

   assert 'no_enviado' == InBandController.send_syn_rcvd(fake_instance_InbandController, datapath, ipv4_pkt, pkt_eth, '8088', 'mensaje')
   
   
#Test que verifica la funcion send_bcastARP. Al cumplirse todas las condiciones, se ejecuta correctamente la funcion enviandose el mensaje bcast, devolvera el valor 'enviado'
def test_send_bcastARP(mocker):
   class datapath:
   	address = ['10.0.0.1','10.0.0.2']
	
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   datapath = datapath()
   C.setProperty('10.0.0.1',"state","managed")

   assert 'enviado' == InBandController.send_bcastARP(fake_instance_InbandController, datapath, '8080', '10.0.0.1', '255.255.255.0')
   
   
#Test que verifica la funcion send_bcastARP. Al faltar configuracion para realizarse las comprobaciones, no se envia el mensaje broadcast.
def test_not_send_bcastARP(mocker):
   class datapath:
   	address = ['10.0.0.1','10.0.0.2']

	
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   datapath = datapath()



   assert 'no_enviado' == InBandController.send_bcastARP(fake_instance_InbandController, datapath, '8080', '10.0.0.1', '255.255.255.0')



#Test que verifica la funcion non_neighbor_controller_in_peer_controllers cuando en la configuracion si hay un peer_controller como neighbor_controller, por lo tanto, configura peer_controller como False
def test_non_neighbor_controllers_in_peer_controllers(mocker):
   
   peer_controllers = {
   	"10.0.0.2": 'peer_controller_1'
   }

   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   C.setProperty('10.0.0.2',"is_neighbor_controller",True)
   neighbor_controller = '10.0.0.3'



   assert None == InBandController.non_neighbor_controllers_in_peer_controllers(fake_instance_InbandController, peer_controllers, '10.0.0.4',neighbor_controller, '7079', '255.255.255.0')
   

   

def test_config_managed_node_ip_neighbor_controller(mocker):
  
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   C.setProperty('10.0.0.2',"is_neighbor_controller",True)
   managed = "managed"


   assert managed == InBandController.config_managed_node_ip(fake_instance_InbandController, '10.0.0.2')
   
   
def test_config_managed_node_ip_non_neighbor_controller(mocker):
  
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   C.setProperty('10.0.0.2',"is_non_neighbor_controller",True)
   managed = "managed"


   assert managed == InBandController.config_managed_node_ip(fake_instance_InbandController, '10.0.0.2')
   
   
   
   
def test_config_managed_node_ip_controller_ip(mocker):
  
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   managed = "managed"


   assert managed == InBandController.config_managed_node_ip(fake_instance_InbandController, '10.0.0.1')
   
def test_config_managed_node_ip_not_managed(mocker):
  
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   managed = "managed"
   C.setProperty('10.0.0.2',"state","not_managed")


   assert managed == InBandController.config_managed_node_ip(fake_instance_InbandController, '10.0.0.2')
   
   
   
   
def test_deactivate_switch_as_managed(mocker):
  
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   managed = "managed"
   C.setProperty('10.0.0.2',"state","not_managed")

   InBandController.remove_node(fake_instance_InbandController, '10.0.0.1')
   assert  [('10.0.0.2', '10.0.0.3'), ('10.0.0.2', '10.0.0.4'), ('10.0.0.3', '10.0.0.4')] == list(DG.edges)
   
   

def test_deactivate_switch_anchors(mocker):
  
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   C.setProperty('10.0.0.2',"anchors",'10.0.0.1')
   C.setProperty('10.0.0.2', "anchors", {'10.0.0.1': 'anchors'})
   

   InBandController.remove_node(fake_instance_InbandController, '10.0.0.1')
   assert  [('10.0.0.2', '10.0.0.3'), ('10.0.0.2', '10.0.0.4'), ('10.0.0.3', '10.0.0.4')] == list(DG.edges)

 
def test_insert_edge_from_anchor_to_new_switch(mocker):
  
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C


   InBandController.insert_edge_from_anchor_to_new_switch(fake_instance_InbandController, '10.0.0.2','10.0.0.3','8088','255.255.255.0')
   assert [('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.4'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1')] == list(DG.edges)
   
   
def test_port_down_hasnt_node(mocker):
  
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C


   assert None == InBandController.port_down(fake_instance_InbandController, '10.0.0.5','8088') 


def test_port_down_hasnt_edge(mocker):
  
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   C.DG()['10.0.0.1']["10.0.0.2"]["port"] = "8080"
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
    
   assert None == InBandController.port_down(fake_instance_InbandController,'10.0.0.1', '8080')
 
 
#Test que verifica la funcion send_probe. Al faltar configuracion para realizarse las comprobaciones, no se envia el mensaje broadcast.
def test_send_probe_not_node(mocker):
   class datapath:
   	address = ['10.0.0.7']

	
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   datapath = datapath()



   assert None == InBandController._send_probe(fake_instance_InbandController, '10.0.0.2',datapath)

 

#Test que verifica la funcion send_bcastARP. Al faltar configuracion para realizarse las comprobaciones, no se envia el mensaje broadcast.
def test_do_send_probe(mocker):
   class datapath:
   	address = ['10.0.0.7']

	
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   datapath = datapath()



   assert None == InBandController._do_send_probe(fake_instance_InbandController, '10.0.0.2',datapath,'10.0.0.3','8080','9090')
   
   
def test_switch_features_handler(mocker):
   class ev:
   	msg = 'anystring'
   
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2'),('10.0.0.2','10.0.0.3'),('10.0.0.3','10.0.0.4'),('10.0.0.4','10.0.0.1'),('10.0.0.2','10.0.0.4')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   assert None == InBandController._switch_features_handler(fake_instance_InbandController,ev)
   
   
   
def test_init_controller_switch(mocker):
   
   C = config.Config()
   DG = C.DG()
   DG.add_edges_from([('10.0.0.1','10.0.0.2')])
   fake_instance_InbandController = mocker.Mock()
   fake_instance_InbandController.config = C
   assert None == InBandController.init_controller_switch(fake_instance_InbandController)
  
 

