#!/usr/bin/python


from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info


# Topologia
#                                                              
#       1         1    2              1   2            1    2    1    2
#    s0 ----------- s1 --------------- s2 -------------- s3 ------ s4 ------ s5     ...     s15 --- h2
# 10.0.0.1           \ 3            3 /                               
#                     \              /
#                      \          3 /           1
#                       ------ s16  ------------- h3
#                              1    2            10.0.0.3
#
# Lanzaremos un controlador independiente en s0

def ovsns(batch = False):

    "Create an empty network and add nodes to it."

    net = Mininet( topo=None,
                   build=False)

    s0 = net.addHost( 's0', ip='0.0.0.0', mac='00:00:00:00:00:01' )
    s1 = net.addHost( 's1', ip='0.0.0.0', mac='00:00:00:00:11:10' )
    s2 = net.addHost( 's2', ip='0.0.0.0', mac='00:00:00:00:22:20' )
    s3 = net.addHost( 's3', ip='0.0.0.0', mac='00:00:00:00:33:30' )
    s4 = net.addHost( 's4', ip='0.0.0.0', mac='00:00:00:00:44:40' )
    s5 = net.addHost( 's5', ip='0.0.0.0', mac='00:00:00:00:55:50' )
    s6 = net.addHost( 's6', ip='0.0.0.0', mac='00:00:00:00:66:60' )
    s7 = net.addHost( 's7', ip='0.0.0.0', mac='00:00:00:00:77:70' )
    s8 = net.addHost( 's8', ip='0.0.0.0', mac='00:00:00:00:88:80' )
    s9 = net.addHost( 's9', ip='0.0.0.0', mac='00:00:00:00:99:90' )
    s10 = net.addHost( 's10', ip='0.0.0.0', mac='00:00:00:00:aa:a0' )
    s11 = net.addHost( 's11', ip='0.0.0.0', mac='00:00:00:00:bb:b0' )
    s12 = net.addHost( 's12', ip='0.0.0.0', mac='00:00:00:00:cc:c0' )
    s13 = net.addHost( 's13', ip='0.0.0.0', mac='00:00:00:00:dd:d0' )
    s14 = net.addHost( 's14', ip='0.0.0.0', mac='00:00:00:00:ee:e0' )
    s15 = net.addHost( 's15', ip='0.0.0.0', mac='00:00:00:00:ff:f0' )
    s16 = net.addHost( 's16', ip='0.0.0.0', mac='00:00:00:11:00:00' )

    sw_list=[s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16]

    h2 = net.addHost( 'h2', ip='10.0.0.2', mac='00:00:00:00:00:02' )
    h3 = net.addHost( 'h3', ip='10.0.0.3', mac='00:00:00:00:00:03' )

    net.addLink( s0, s1, 0, 0 )
    net.addLink( s1, s2, 1, 0 )
    net.addLink( s1, s16, 2, 0 )
    net.addLink( s2, s3, 1, 0 )
    net.addLink( s2, s16, 2, 2 )
    net.addLink( s16, h3, 1, 0 )
    net.addLink( s3, s4, 1, 0 )
    net.addLink( s4, s5, 1, 0 )    
    net.addLink( s5, s6, 1, 0 )
    net.addLink( s6, s7, 1, 0 )
    net.addLink( s7, s8, 1, 0 )
    net.addLink( s8, s9, 1, 0 )
    net.addLink( s9, s10, 1, 0 )
    net.addLink( s10, s11, 1, 0 )
    net.addLink( s11, s12, 1, 0 )
    net.addLink( s12, s13, 1, 0 )
    net.addLink( s13, s14, 1, 0 )
    net.addLink( s14, s15, 1, 0 )
    net.addLink( s15, h2, 1, 0 )

    
    for i in range(17):
       n_ifaces=2
       if i == 0:
           n_ifaces=1
       elif i==1 or i==2 or i==16:
           n_ifaces=3

       for iface in range(n_ifaces):
           hex_switch=hex(i)
           hex_digit=hex_switch[2]
           if i==0:
               sw_list[i].cmd("ip link set s"+ str(i) +"-eth"+ str(iface) + " address 00:00:00:00:00:01")
           else:
               sw_list[i].cmd("ip link set s"+ str(i) +"-eth"+ str(iface) + " address 00:00:00:00:"+hex_digit+hex_digit+":"+hex_digit+str(iface))


    if not batch:
        net.start()
        CLI( net )        
        net.stop()
    else: # batch
        return net, [s0]


if __name__ == '__main__':
    setLogLevel( 'info' )
    ovsns()
