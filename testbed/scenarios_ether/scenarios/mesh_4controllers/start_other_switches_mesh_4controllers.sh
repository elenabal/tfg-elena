#!/bin/bash

declare -A ifaces



for i in 1 4 21 23 31 33 41 24 34 44
do
   ifaces[s$i]=3
done

for i in 22 32 42 43
do
   ifaces[s$i]=2
done

for i in 2 3
do
   ifaces[s$i]=4
done


# Iniciamos los switches que están conectados directamente con los 4c
#for i in 24 34 44 1
#do
#    num=`expr 100 + $i`
#    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
#    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100 
#done

# Iniciamos el resto de switches
for i in 33 42 23 2 3 22 32 43 21 31 41 4
do
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100 
done


