#!/bin/bash

declare -A ifaces

ifaces[c0]=1
ifaces[c1]=1
ifaces[c2]=1
ifaces[c3]=1

for i in 2 3
do
   ifaces[s$i]=4
done

for i in 1 4 21 23 31 32 41 24 34 44
do
   ifaces[s$i]=3
done

for i in 22 33 42 43
do
   ifaces[s$i]=2
done

for i in 0 1 2 3
do
   ./scripts/kill-switch.sh c$i ${ifaces[c$i]} 
done

for i in 1 2 3 4 21 22 23 24 31 32 33 34 41 42 43 44
do
    num=`expr 100 + $i`
    echo "Eliminando Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/kill-switch.sh s$i ${ifaces[s$i]} 
done



