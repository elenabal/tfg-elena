#!/usr/bin/python



from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf

# Topologia

# Lanzaremos un controlador independiente en c0

def ovsns(batch = False):

    "Create an empty network and add nodes to it."

    net = Mininet( topo=None,
                   build=False)

    s = [ ] # list of switches
    c0 = net.addHost( 'c0', ip='0.0.0.0.', mac='00:00:00:11:11:11' )
    s.append(c0)
    c1 = net.addHost( 'c1', ip='0.0.0.0.', mac='00:00:00:22:22:22' )
    c2 = net.addHost( 'c2', ip='0.0.0.0.', mac='00:00:00:33:33:33' )
    c3 = net.addHost( 'c3', ip='0.0.0.0.', mac='00:00:00:44:44:44' )
    c4 = net.addHost( 'c4', ip='0.0.0.0.', mac='00:00:00:55:55:55' )

    for i in range(1, 10):
        s.append(net.addHost( 's' + str(i) , ip='0.0.0.0',  mac='00:00:00:00:0' + str(i) + ':00' ))

    for i in range(10, 97):
        s.append(net.addHost( 's' + str(i) , ip='0.0.0.0',  mac='00:00:00:00:' + str(i) + ':00' ))

    # Add in link options for different experiments
    linkopts = dict(bw=1000)
    linkopts2 = dict(delay='1ms', bw=1000);


    net.addLink(s[1], s[3], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[1], s[2], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[1], s[4], 3, 0, cls=TCLink,**linkopts)
    net.addLink(s[4], s[5], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[5], s[6], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[6], s[7], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[7], s[4], 1, 2, cls=TCLink,**linkopts)
    net.addLink(s[7], s[8], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[6], s[9], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[9], s[8], 1, 1, cls=TCLink,**linkopts)
    net.addLink(s[8], s[11], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[9], s[10], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[8], s[12], 3, 0, cls=TCLink,**linkopts)


    net.addLink(s[11], s[13], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[12], s[14], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[14], s[15], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[6],  s[16], 3, 0, cls=TCLink,**linkopts)
    net.addLink(s[16], s[17], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[17], s[18], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[14], s[19], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[14], s[21], 3, 0, cls=TCLink,**linkopts)
    net.addLink(s[10], s[20], 1, 0, cls=TCLink,**linkopts)

    net.addLink(s[20], s[25], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[20], s[26], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[26], s[25], 1, 1, cls=TCLink,**linkopts)
    net.addLink(s[25], s[24], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[26], s[27], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[20], s[23], 3, 0, cls=TCLink,**linkopts)
    net.addLink(s[20], s[22], 4, 0, cls=TCLink,**linkopts)
    #net.addLink(s[20], s[28], 5, 0, cls=TCLink,**linkopts)
    net.addLink(s[22], s[28], 1, 0, cls=TCLink,**linkopts)

    net.addLink(s[28], s[29], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[29], s[30], 1, 0, cls=TCLink,**linkopts)

    net.addLink(s[30], s[31], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[31], s[32], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[30], s[32], 2, 1, cls=TCLink,**linkopts)
    net.addLink(s[32], s[33], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[32], s[34], 3, 0, cls=TCLink,**linkopts)
    net.addLink(s[31], s[35], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[32], s[36], 4, 0, cls=TCLink,**linkopts)
#    # net.addLink(s[32], s[37], 6, 0, cls=TCLink,**linkopts)
    net.addLink(s[36], s[37], 1, 0, cls=TCLink,**linkopts)

#   #net.addLink(s[32], s[38], 7, 0, cls=TCLink,**linkopts)
    net.addLink(s[36], s[38], 2, 0, cls=TCLink,**linkopts)

#   #net.addLink(s[32], s[40], 8, 0, cls=TCLink,**linkopts)
    net.addLink(s[36], s[40], 3, 0, cls=TCLink,**linkopts)
    

#   #net.addLink(s[32], s[39], 9, 0, cls=TCLink,**linkopts)
    net.addLink(s[36], s[39], 4, 0, cls=TCLink,**linkopts)

#   #net.addLink(s[32], s[41], 10, 0, cls=TCLink,**linkopts)
    net.addLink(s[36], s[41], 5, 0, cls=TCLink,**linkopts)

    net.addLink(s[28], s[41], 2, 1, cls=TCLink,**linkopts)

    net.addLink(s[28], s[42], 3, 0, cls=TCLink,**linkopts)
    net.addLink(s[41], s[43], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[41], s[44], 3, 0, cls=TCLink,**linkopts)
    net.addLink(s[41], s[45], 4, 0, cls=TCLink,**linkopts)
    net.addLink(s[41], s[46], 5, 0, cls=TCLink,**linkopts)

    net.addLink(s[27], s[47], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[47], s[48], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[48], s[52], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[52], s[49], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[52], s[51], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[47], s[50], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[47], s[53], 3, 0, cls=TCLink,**linkopts)
    net.addLink(s[47], s[54], 4, 0, cls=TCLink,**linkopts)


    net.addLink(s[54], s[56], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[54], s[55], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[54], s[57], 3, 0, cls=TCLink,**linkopts)
#    #net.addLink(s[20], s[54], 6, 4, cls=TCLink,**linkopts)
    net.addLink(s[22], s[54], 2, 4, cls=TCLink,**linkopts)

    net.addLink(s[55], s[60], 1, 0, cls=TCLink,**linkopts)

    net.addLink(s[60], s[58], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[55], s[59], 2, 0, cls=TCLink,**linkopts)

    net.addLink(s[58], s[63], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[58], s[65], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[55], s[58], 3, 3, cls=TCLink,**linkopts)
    net.addLink(s[58], s[62], 4, 0, cls=TCLink,**linkopts)
    net.addLink(s[58], s[61], 5, 0, ls=TCLink,**linkopts)
#    #net.addLink(s[32], s[66], 11, 0, cls=TCLink,**linkopts)
    net.addLink(s[33], s[66], 1, 0, cls=TCLink,**linkopts)


#    #net.addLink(s[32], s[67], 12, 0, cls=TCLink,**linkopts)
    net.addLink(s[33], s[67], 2, 0, cls=TCLink,**linkopts)

    net.addLink(s[63], s[64], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[66], s[67], 1, 1, cls=TCLink,**linkopts)
    net.addLink(s[67], s[68], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[67], s[69], 3, 0, cls=TCLink,**linkopts)
    net.addLink(s[67], s[70], 4, 0, cls=TCLink,**linkopts)

    net.addLink(s[67], s[71], 5, 0, cls=TCLink,**linkopts)
    #net.addLink(s[67], s[72], 6, 0, cls=TCLink,**linkopts)
    net.addLink(s[71], s[72], 1, 0, cls=TCLink,**linkopts)

    net.addLink(s[74], s[75], 0, 0, cls=TCLink,**linkopts)
    net.addLink(s[74], s[73], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[74], s[76], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[74], s[77], 3, 0, cls=TCLink,**linkopts)
    #net.addLink(s[74], s[82], 4, 0, cls=TCLink,**linkopts)
    net.addLink(s[74], s[78], 4, 0, cls=TCLink,**linkopts)
    net.addLink(s[78], s[82], 1, 0, cls=TCLink,**linkopts)

    net.addLink(s[82], s[81], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[82], s[80], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[82], s[79], 3, 0, cls=TCLink,**linkopts)
    net.addLink(s[82], s[83], 4, 0, cls=TCLink,**linkopts)
    net.addLink(s[83], s[92], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[83], s[93], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[83], s[94], 3, 0, cls=TCLink,**linkopts)
    net.addLink(s[94], s[81], 1, 1, cls=TCLink,**linkopts)
    net.addLink(s[83], s[89], 4, 0, cls=TCLink,**linkopts)
    #net.addLink(s[83], s[88], 5, 0, cls=TCLink,**linkopts)
    net.addLink(s[89], s[88], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[83], s[87], 5, 0, cls=TCLink,**linkopts)

#    #net.addLink(s[83], s[86], 7, 0, cls=TCLink,**linkopts)
    net.addLink(s[87], s[86], 1, 0, cls=TCLink,**linkopts)
#    #net.addLink(s[83], s[84], 8, 0, cls=TCLink,**linkopts)
    net.addLink(s[92], s[84], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[84], s[90], 1, 0, cls=TCLink,**linkopts)

    net.addLink(s[90], s[85], 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[90], s[91], 2, 0, cls=TCLink,**linkopts)
#   # net.addLink(s[58], s[83], 6, 9, cls=TCLink,**linkopts)
    net.addLink(s[87], s[65], 2, 1, cls=TCLink,**linkopts)
#   #net.addLink(s[67], s[58], 7, 7, cls=TCLink,**linkopts)
    net.addLink(s[70], s[65], 1, 2, cls=TCLink,**linkopts)
#   #net.addLink(s[67], s[83], 8, 10, cls=TCLink,**linkopts)
    net.addLink(s[70], s[87], 2, 3, cls=TCLink,**linkopts)

#   #net.addLink(s[1], s[32], 4, 13, cls=TCLink,**linkopts)
    net.addLink(s[1], s[34], 4, 1, cls=TCLink,**linkopts)
#    #net.addLink(s[17], s[20], 2, 7, cls=TCLink,**linkopts)
    net.addLink(s[17], s[10], 2, 2, cls=TCLink,**linkopts)

#   #net.addLink(s[54], s[96], 5, 0, cls=TCLink,**linkopts)
    net.addLink(s[56], s[96], 1, 0, cls=TCLink,**linkopts)
#   #net.addLink(s[54], s[95], 6, 0, cls=TCLink,**linkopts)
    net.addLink(s[56], s[95], 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[29], s[62], 2, 1, cls=TCLink,**linkopts)



    net.addLink(s[1], c0, 0, 0, cls=TCLink,**linkopts)
    net.addLink(s[50], c1, 1, 0, cls=TCLink,**linkopts)
    net.addLink(s[81], c2, 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[62], c3, 2, 0, cls=TCLink,**linkopts)
    net.addLink(s[20], c4, 5, 0, cls=TCLink,**linkopts)

    if not batch:
        net.start()
        CLI( net )        
        net.stop()
    else: # batch
        return net, [c0, c1, c2, c3, c4]
        
if __name__ == '__main__':
    setLogLevel( 'info' )
    ovsns()
