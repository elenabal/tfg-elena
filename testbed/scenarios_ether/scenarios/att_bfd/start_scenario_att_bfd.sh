#!/bin/bash


function inicia_switches(){
    for (( i=$1; i<=$2; i++ ))
    do
        num=`expr 100 + $i`
        echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
        ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100
    done
}

declare -A ifaces

# ifaces array que indica el número de interfaces en cada switch.
# s1 -> 5 interfaces
# s2, s3, s4, s5, s6, s9, s10, s13, s14, s17, s18 -> 4 interfaces
# s7, s8, s11, s12, s15, s16, s19, s20 -> 2 interfaces
ifaces[c0]=1
ifaces[c1]=1
ifaces[c2]=1
ifaces[c3]=1
ifaces[c4]=1

for i in 2 3 13 15 18 19 21 23 24 35 37 38 39 40 42 43 44 45 46 49 51 53 57 59 61 64 68 69 72 73 75 76 77 79 80 85 86 88 91 93 95 96
do
   ifaces[s$i]=1
done

for i in 5 11 12 16 27 34 48 50 60 63 66 71 78 84 89 92 94
do
   ifaces[s$i]=2
done

for i in 4 7 9 10 17 22 25 26 29 30 31 33 52 56 62 65 70 81 90
do
   ifaces[s$i]=3
done

for i in 6 8 14 28 55 87
do
   ifaces[s$i]=4
done

for i in 1 32 47 54 74 82
do
   ifaces[s$i]=5
done


for i in 20 36 41 58 67 83
do
   ifaces[s$i]=6
done


echo "Creando switch c0 interfaces=${ifaces[c0]}"
./scripts/start-controller-switch.sh c0 ${ifaces[c0]} 10.0.0.1 11.0.0.1

echo "Creando switch c1 interfaces=${ifaces[c1]}"
./scripts/start-controller-switch.sh c1 ${ifaces[c1]} 10.0.0.1 11.0.0.2

echo "Creando switch c2 interfaces=${ifaces[c2]}"
./scripts/start-controller-switch.sh c2 ${ifaces[c2]} 10.0.0.1 11.0.0.3

echo "Creando switch c3 interfaces=${ifaces[c3]}"
./scripts/start-controller-switch.sh c3 ${ifaces[c3]} 10.0.0.1 11.0.0.4

echo "Creando switch c4 interfaces=${ifaces[c4]}"
./scripts/start-controller-switch.sh c4 ${ifaces[c4]} 10.0.0.1 11.0.0.5

# 60 SWITCHES

inicia_switches 1 9
inicia_switches 20 29
inicia_switches 45 54
inicia_switches 55 64
inicia_switches 80 89
inicia_switches 10 19
inicia_switches 70 79
inicia_switches 30 39
inicia_switches 90 96
inicia_switches 40 44
inicia_switches 65 69

