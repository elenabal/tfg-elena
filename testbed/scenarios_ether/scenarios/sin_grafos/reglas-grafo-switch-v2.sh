#!/bin/bash

if [ ! $# -eq 1 ]
then
    echo "Usage error: $0 <switchName>"
    exit -1
fi

SWITCH_NAME=$1

# insertamos cabeceras nsh con grafo en la tabla 0 y resubmit a la 1
# Sólo le ponemos nsh_ttl (ttl es igual al número de caminos alternativos del grafo) en la cabecera nsh más externa. En las cabeceras nsh de los alternativos no tiene sentido, porque los alternativos no tienen alternativos.
# Si el mensaje se entrega en puerto local, último puerto=0xff, entonces el último nodo no tiene camino alternativo.
# Si el mensaje se entrega en un puerto diferente del local, último puerto=0xf? (? es el puerto, ?!=f), entonces el último nodo sí tiene camino alternativo.


#ya viene con grafo
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=0,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,actions=resubmit(,1)"

##
## -------------------------------------
## Tabla 1: reglas de forwarding en $SWITCH_NAME
## -------------------------------------
##


# al enviar por un puerto n hay que usar "actions=output:n". Si n=in_port no lo envía. Por eso,
# siempre asignamos a in_port un número de puerto que no se use (0xffff), para que no se cumpla n=in_port.

###########
# PUERTO 1
###########
# puerto 1:  estas reglas hacen match en el puerto (nsh_c1[28..31]=0x1 o nsh_c1[28..31]=0x9), el puerto 0x1 está activo

# Los cuatro primeros bits=0x1 (0001 en binario) y ttl=0
# OJO: No puede darse bits=0x9 (1001 en binario) y ttl=0
# No hay alternativo para este salto (por ser 0x1) 
# Va por camino alternativo o va por el principal y no hay alternativos, solo elimino 4 bits más significativos
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57997,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x1,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->xxreg1[100..127],move:nsh_c2[0..31]->xxreg1[68..99],move:nsh_c3[0..31]->xxreg1[36..67],move:nsh_c4[0..31]->xxreg1[4..35],load:0x0->xxreg1[0..3],move:xxreg1[96..127]->nsh_c1[0..31],move:xxreg1[64..95]->nsh_c2[0..31],move:xxreg1[32..63]->nsh_c3[0..31],move:xxreg1[0..31]->nsh_c4[0..31],encap(ethernet),pop:dl_dst,pop:dl_src,output:1"


# Los cuatro primeros bits=0x9 (1001 en binario) y ttl=1
# Al decrementar el ttl se hace cero y el mensaje no se envía. No hay que decrementar, sino asignar ttl=0.
# Hay alternativo para este salto (por ser 0x9)
# quito la cabecera del alternativo y creo una nueva cabecera moviendo bits a la parte más significativa y ttl=0
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x9,nsh_ttl=1,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->xxreg1[100..127],move:nsh_c2[0..31]->xxreg1[68..99],move:nsh_c3[0..31]->xxreg1[36..67],move:nsh_c4[0..31]->xxreg1[4..35],load:0x0->xxreg1[0..3],decap(),decap(),encap(nsh(md_type=1)),set_field:0x1234->nsh_spi,move:xxreg1[96..127]->nsh_c1[0..31],move:xxreg1[64..95]->nsh_c2[0..31],move:xxreg1[32..63]->nsh_c3[0..31],move:xxreg1[0..31]->nsh_c4[0..31],load:0x0->nsh_ttl,encap(ethernet),pop:dl_dst,pop:dl_src,output:1"

# los cuatro primeros bits =0x9 (1001 en binario) y ttl>1
# Hay alternativo para este salto (por ser 0x9), quito la cabecera del alternativo y creo una cabecera moviendo bits a la parte más significativa, decremento nsh_ttl
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57998,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x9,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->xxreg1[100..127],move:nsh_c2[0..31]->xxreg1[68..99],move:nsh_c3[0..31]->xxreg1[36..67],move:nsh_c4[0..31]->xxreg1[4..35],load:0x0->xxreg1[0..3],move:nsh_ttl->reg1[0..7],decap(),decap(),encap(nsh(md_type=1)),set_field:0x1234->nsh_spi,move:xxreg1[96..127]->nsh_c1[0..31],move:xxreg1[64..95]->nsh_c2[0..31],move:xxreg1[32..63]->nsh_c3[0..31],move:xxreg1[0..31]->nsh_c4[0..31],move:reg1[0..7]->nsh_ttl,dec_nsh_ttl,encap(ethernet),pop:dl_dst,pop:dl_src,output:1"


####################
# PUERTO 0xf1 o 0xf9
####################
#puerto 0xf1: último salto, el puerto hacia el destino final es 1. 
# si ttl=0 es camino alternativo, o no existe camino alternativo. Desencapsulamos y enviamos al destino final por el puerto 1
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[24..31]=0xf1,nsh_ttl=0,actions=push:dl_src,push:dl_dst,decap(),decap(),encap(ethernet),pop:dl_dst,pop:dl_src,output:1"

# si ttl=1 hay alternativo para este salto  
# desencapsulo aquí, quitando NSH del alternativo, en vez de enviarlo a tablas 2 y 3 para desencapsular
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57998,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[24..31]=0xf9,actions=push:dl_src,push:dl_dst,decap(),decap(),decap(),encap(ethernet),pop:dl_dst,pop:dl_src,output:1"




###########
# PUERTO 2
###########
# puerto 2:  estas reglas hacen match en el puerto (nsh_c1[28..31]=0x2 o nsh_c1[28..31]=0xa), el puerto 0x2 está activo

# Los cuatro primeros bits=0x2 (0010 en binario) y ttl=0
# OJO: No puede darse bits=0xa (1010 en binario) y ttl=0
# No hay alternativo para este salto (por ser 0x2) 
# Va por camino alternativo o va por el principal y no hay alternativos, solo elimino 4 bits más significativos
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57997,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x2,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->xxreg1[100..127],move:nsh_c2[0..31]->xxreg1[68..99],move:nsh_c3[0..31]->xxreg1[36..67],move:nsh_c4[0..31]->xxreg1[4..35],load:0x0->xxreg1[0..3],move:xxreg1[96..127]->nsh_c1[0..31],move:xxreg1[64..95]->nsh_c2[0..31],move:xxreg1[32..63]->nsh_c3[0..31],move:xxreg1[0..31]->nsh_c4[0..31],encap(ethernet),pop:dl_dst,pop:dl_src,output:2"


# Los cuatro primeros bits=0xa (1010 en binario) y ttl=1
# Al decrementar el ttl se hace cero y el mensaje no se envía. No hay que decrementar, sino asignar ttl=0.
# Hay alternativo para este salto (por ser 0xa)
# quito la cabecera del alternativo y creo una nueva cabecera moviendo bits a la parte más significativa y ttl=0
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0xa,nsh_ttl=1,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->xxreg1[100..127],move:nsh_c2[0..31]->xxreg1[68..99],move:nsh_c3[0..31]->xxreg1[36..67],move:nsh_c4[0..31]->xxreg1[4..35],load:0x0->xxreg1[0..3],decap(),decap(),encap(nsh(md_type=1)),set_field:0x1234->nsh_spi,move:xxreg1[96..127]->nsh_c1[0..31],move:xxreg1[64..95]->nsh_c2[0..31],move:xxreg1[32..63]->nsh_c3[0..31],move:xxreg1[0..31]->nsh_c4[0..31],load:0x0->nsh_ttl,encap(ethernet),pop:dl_dst,pop:dl_src,output:2"

# los cuatro primeros bits =0xa (1010 en binario) y ttl>1
# Hay alternativo para este salto (por ser 0xa), quito la cabecera del alternativo y creo una cabecera moviendo bits a la parte más significativa, decremento nsh_ttl
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57998,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0xa,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->xxreg1[100..127],move:nsh_c2[0..31]->xxreg1[68..99],move:nsh_c3[0..31]->xxreg1[36..67],move:nsh_c4[0..31]->xxreg1[4..35],load:0x0->xxreg1[0..3],move:nsh_ttl->reg1[0..7],decap(),decap(),encap(nsh(md_type=1)),set_field:0x1234->nsh_spi,move:xxreg1[96..127]->nsh_c1[0..31],move:xxreg1[64..95]->nsh_c2[0..31],move:xxreg1[32..63]->nsh_c3[0..31],move:xxreg1[0..31]->nsh_c4[0..31],move:reg1[0..7]->nsh_ttl,dec_nsh_ttl,encap(ethernet),pop:dl_dst,pop:dl_src,output:2"


####################
# PUERTO 0xf2 o 0xfa
####################
#puerto 0xf2: último salto, el puerto hacia el destino final es 2. 
# si ttl=0 es camino alternativo, o no existe camino alternativo. Desencapsulamos y enviamos al destino final por el puerto 2
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[24..31]=0xf2,nsh_ttl=0,actions=push:dl_src,push:dl_dst,decap(),decap(),encap(ethernet),pop:dl_dst,pop:dl_src,output:2"

# si ttl=1 hay alternativo para este salto  
# desencapsulo aquí, quitando NSH del alternativo, en vez de enviarlo a tablas 2 y 3 para desencapsular
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57998,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[24..31]=0xfa,actions=push:dl_src,push:dl_dst,decap(),decap(),decap(),encap(ethernet),pop:dl_dst,pop:dl_src,output:2"





###########
# PUERTO 3
###########
# puerto 3:  estas reglas hacen match en el puerto (nsh_c1[28..31]=0x3 o nsh_c1[28..31]=0xb), el puerto 0x3 está activo

# Los cuatro primeros bits=0x3 (0011 en binario) y ttl=0
# OJO: No puede darse bits=0xb (1011 en binario) y ttl=0
# No hay alternativo para este salto (por ser 0x3) 
# Va por camino alternativo o va por el principal y no hay alternativos, solo elimino 4 bits más significativos
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57997,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x3,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->xxreg1[100..127],move:nsh_c2[0..31]->xxreg1[68..99],move:nsh_c3[0..31]->xxreg1[36..67],move:nsh_c4[0..31]->xxreg1[4..35],load:0x0->xxreg1[0..3],move:xxreg1[96..127]->nsh_c1[0..31],move:xxreg1[64..95]->nsh_c2[0..31],move:xxreg1[32..63]->nsh_c3[0..31],move:xxreg1[0..31]->nsh_c4[0..31],encap(ethernet),pop:dl_dst,pop:dl_src,output:3"


# Los cuatro primeros bits=0xb (1011 en binario) y ttl=1
# Al decrementar el ttl se hace cero y el mensaje no se envía. No hay que decrementar, sino asignar ttl=0.
# Hay alternativo para este salto (por ser 0xb)
# quito la cabecera del alternativo y creo una nueva cabecera moviendo bits a la parte más significativa y ttl=0
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0xb,nsh_ttl=1,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->xxreg1[100..127],move:nsh_c2[0..31]->xxreg1[68..99],move:nsh_c3[0..31]->xxreg1[36..67],move:nsh_c4[0..31]->xxreg1[4..35],load:0x0->xxreg1[0..3],decap(),decap(),encap(nsh(md_type=1)),set_field:0x1234->nsh_spi,move:xxreg1[96..127]->nsh_c1[0..31],move:xxreg1[64..95]->nsh_c2[0..31],move:xxreg1[32..63]->nsh_c3[0..31],move:xxreg1[0..31]->nsh_c4[0..31],load:0x0->nsh_ttl,encap(ethernet),pop:dl_dst,pop:dl_src,output:3"

# los cuatro primeros bits =0xb (1011 en binario) y ttl>1
# Hay alternativo para este salto (por ser 0xb), quito la cabecera del alternativo y creo una cabecera moviendo bits a la parte más significativa, decremento nsh_ttl
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57998,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0xb,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->xxreg1[100..127],move:nsh_c2[0..31]->xxreg1[68..99],move:nsh_c3[0..31]->xxreg1[36..67],move:nsh_c4[0..31]->xxreg1[4..35],load:0x0->xxreg1[0..3],move:nsh_ttl->reg1[0..7],decap(),decap(),encap(nsh(md_type=1)),set_field:0x1234->nsh_spi,move:xxreg1[96..127]->nsh_c1[0..31],move:xxreg1[64..95]->nsh_c2[0..31],move:xxreg1[32..63]->nsh_c3[0..31],move:xxreg1[0..31]->nsh_c4[0..31],move:reg1[0..7]->nsh_ttl,dec_nsh_ttl,encap(ethernet),pop:dl_dst,pop:dl_src,output:3"


#####################
# PUERTO 0xf3 o 0xfb
####################
#puerto 0xf3: último salto, el puerto hacia el destino final es 3. 
# si ttl=0 es camino alternativo, o no existe camino alternativo. Desencapsulamos y enviamos al destino final por el puerto 3
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[24..31]=0xf3,nsh_ttl=0,actions=push:dl_src,push:dl_dst,decap(),decap(),encap(ethernet),pop:dl_dst,pop:dl_src,output:3"

# si ttl=1 hay alternativo para este salto  
# desencapsulo aquí, quitando NSH del alternativo, en vez de enviarlo a tablas 2 y 3 para desencapsular
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57998,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[24..31]=0xfb,actions=push:dl_src,push:dl_dst,decap(),decap(),decap(),encap(ethernet),pop:dl_dst,pop:dl_src,output:2"




#####################
# NO HAY PUERTO 
#####################
# si no ha hecho match antes es porque su puerto no está activo
# si TTL==0 no hay camino alternativo
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=40000, dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_ttl=0,actions=drop"

# si TTL>0 y de los 4 bits del puerto el más significativo es 0, no hay camino alternativo para este salto, lo hay para otros
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=39999, dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[31..31]=0,actions=drop"

# si TTL>0 y estoy en último salto nsh_c1[28..31]=0xf -> el bit más significativo del puerto DEBE SER 1
# hay camino alternativo para este salto, ponerlo como principal y quitar todos los demás alternativos, para lo cual hacemos resubmit a tabla 2,
# guardando en reg1 el TTL.
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=39999, dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0xf,nsh_c1[27..27]=1,actions=push:dl_src,push:dl_dst,decap(),move:nsh_ttl->reg1[0..7],decap(),encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,2)"

# si TTL>0 
# hay camino alternativo para este salto, ponerlo como principal y quitar todos los demás alternativos, para lo cual hacemos resubmit a tabla 2,
# guardando en reg1 el TTL.
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=39998, dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[31..31]=1,actions=push:dl_src,push:dl_dst,decap(),move:nsh_ttl->reg1[0..7],decap(),encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,2)"


##
## -------------------------------------------------------------------------------------
## tabla 2 comprobar si el alternativo es NULL y si no, almacenar alternativo en reg0
## -------------------------------------------------------------------------------------
##

# Dejo el camino alternativo en xxreg1, ya tenía almacenado en reg1 el TTL y lo mando a la tabla 3 para que quite todos los nsh alternativos
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME  "table=2,priority=60000,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,actions=push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..31]->xxreg1[0..31],move:nsh_c2[0..31]->xxreg1[32..63],move:nsh_c3[0..31]->xxreg1[64..95],move:nsh_c4[0..31]->xxreg1[96..127],encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,3)"


## -------------------------------------------------------------------------------------
## tabla 3 de desencapsulamiento de paths alternativos
## -------------------------------------------------------------------------------------
# si nsh_ttl = reg1 = 1 desencapsulo, quedando sin ningún nsh, así que recuperamos el alternativo de xxreg1 y le ponemos ttl=0. Finalmente, resubmit a tabla 1 para forward
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME  "table=3,priority=60000, dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,reg1=1,actions=push:dl_src,push:dl_dst,decap(),decap(),encap(nsh(md_type=1)),load:0x1234->nsh_spi,load:0->nsh_ttl,move:xxreg1[0..31]->nsh_c1[0..31],move:xxreg1[32..63]->nsh_c2[0..31],move:xxreg1[64..95]->nsh_c3[0..31],move:xxreg1[96..127]->nsh_c4[0..31],encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,1)"


# si nsh_ttl > 1 (si fuera 1, la regla anterior de mayor prioridad se habría cumplido nsh_ttl=1) recupero reg1->nsh_ttl, nsh_ttl_dec y lo vuelvo a dejar en reg1, decap y resubmit para seguir desencapsulando
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME  "table=3, priority=59999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,actions=push:dl_src,push:dl_dst,decap(),move:reg1[0..7]->nsh_ttl,dec_nsh_ttl,move:nsh_ttl->reg1[0..7],decap(),encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,3)"

