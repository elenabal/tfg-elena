#!/bin/bash

declare -A ifaces

ifaces[s0]=1

ifaces[s1]=4
ifaces[s2]=2
ifaces[s3]=3
ifaces[s4]=4
ifaces[s5]=2
ifaces[s6]=2
ifaces[s7]=2
ifaces[s8]=2


echo "Eliminando Switch s0 interfaces=${ifaces[s0]}"
./scripts/kill-switch.sh s0 ${ifaces[s0]}

for i in {1..8}
do
    num=`expr 100 + $i`
    echo "Eliminando Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/kill-switch.sh s$i ${ifaces[s$i]} 
done



