#!/bin/bash

declare -A ifaces


ifaces[s0]=1

ifaces[s1]=3
ifaces[s2]=4
ifaces[s3]=4
ifaces[s4]=3

ifaces[s21]=3
ifaces[s22]=2
ifaces[s23]=3
ifaces[s24]=2

ifaces[s31]=3
ifaces[s32]=2
ifaces[s33]=3
ifaces[s34]=2

ifaces[s41]=3
ifaces[s42]=2
ifaces[s43]=2
ifaces[s44]=2



echo "Eliminando Switch s0 interfaces=${ifaces[s0]}"
./scripts/kill-switch.sh s0 ${ifaces[s0]}

for i in 1 2 3 4 21 22 23 24 31 32 33 34 41 42 43 44
do
    num=`expr 100 + $i`
    echo "Eliminando Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/kill-switch.sh s$i ${ifaces[s$i]} 
done



