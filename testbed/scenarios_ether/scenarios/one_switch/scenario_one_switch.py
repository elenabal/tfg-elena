#!/usr/bin/python

from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info


# Topologia
#                                                              
#       1         1   
#    s0 ----------- s1 
# 10.0.0.1           
#                    
#                    
# Lanzaremos un controlador independiente en s0

def ovsns(batch = False):

    "Create an empty network and add nodes to it."

    net = Mininet( topo=None,
                   build=False)

    s0 = net.addHost( 's0', ip='0.0.0.0.', mac='00:00:00:00:00:01' )
    s1 = net.addHost( 's1', ip='0.0.0.0',  mac='00:00:00:00:11:10' )

    net.addLink( s0, s1, 0, 0 )


    s1.cmd("ip link set s1-eth1 address 00:00:00:00:11:11")


    if not batch:
        net.start()
        CLI( net )        
        net.stop()
    else: # batch
        return net, [s0]


if __name__ == '__main__':
    setLogLevel( 'info' )
    ovsns()
