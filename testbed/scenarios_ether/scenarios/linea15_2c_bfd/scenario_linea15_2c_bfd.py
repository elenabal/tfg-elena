#!/usr/bin/python


from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info


# Topologia
#                                                              
#       1         1    2              1   2            1    2    1    2
#    c0 ----------- s1 --------------- s2 -------------- s3 ------ s4 ------ s5     ...     s15 --- c1
# 10.0.0.1                                                                                         10.0.0.2
#
# Lanzaremos dos controladores independiente en c0 y  c1

def ovsns(batch = False):

    "Create an empty network and add nodes to it."

    net = Mininet( topo=None,
                   build=False)

    c0 = net.addHost( 'c0', ip='0.0.0.0', mac='00:00:00:00:00:01' )
    c1 = net.addHost( 'c1', ip='0.0.0.0', mac='00:00:00:00:00:02' )
    s1 = net.addHost( 's1', ip='0.0.0.0', mac='00:00:00:00:11:10' )
    s2 = net.addHost( 's2', ip='0.0.0.0', mac='00:00:00:00:22:20' )
    s3 = net.addHost( 's3', ip='0.0.0.0', mac='00:00:00:00:33:30' )
    s4 = net.addHost( 's4', ip='0.0.0.0', mac='00:00:00:00:44:40' )
    s5 = net.addHost( 's5', ip='0.0.0.0', mac='00:00:00:00:55:50' )
    s6 = net.addHost( 's6', ip='0.0.0.0', mac='00:00:00:00:66:60' )
    s7 = net.addHost( 's7', ip='0.0.0.0', mac='00:00:00:00:77:70' )
    s8 = net.addHost( 's8', ip='0.0.0.0', mac='00:00:00:00:88:80' )
    s9 = net.addHost( 's9', ip='0.0.0.0', mac='00:00:00:00:99:90' )
    s10 = net.addHost( 's10', ip='0.0.0.0', mac='00:00:00:00:aa:a0' )
    s11 = net.addHost( 's11', ip='0.0.0.0', mac='00:00:00:00:bb:b0' )
    s12 = net.addHost( 's12', ip='0.0.0.0', mac='00:00:00:00:cc:c0' )
    s13 = net.addHost( 's13', ip='0.0.0.0', mac='00:00:00:00:dd:d0' )
    s14 = net.addHost( 's14', ip='0.0.0.0', mac='00:00:00:00:ee:e0' )
    s15 = net.addHost( 's15', ip='0.0.0.0', mac='00:00:00:00:ff:f0' )

    sw_list=[s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15]

    net.addLink( c0, s1, 0, 0 )
    net.addLink( s1, s2, 1, 0 )
    net.addLink( s2, s3, 1, 0 )
    net.addLink( s3, s4, 1, 0 )
    net.addLink( s4, s5, 1, 0 )    
    net.addLink( s5, s6, 1, 0 )
    net.addLink( s6, s7, 1, 0 )
    net.addLink( s7, s8, 1, 0 )
    net.addLink( s8, s9, 1, 0 )
    net.addLink( s9, s10, 1, 0 )
    net.addLink( s10, s11, 1, 0 )
    net.addLink( s11, s12, 1, 0 )
    net.addLink( s12, s13, 1, 0 )
    net.addLink( s13, s14, 1, 0 )
    net.addLink( s14, s15, 1, 0 )
    net.addLink( s15, c1, 1, 0 )

    c0.cmd("ip link set c0-eth0 address 00:00:00:00:00:01")
    c1.cmd("ip link set c1-eth0 address 00:00:00:00:00:02")
    
    for i in range(15):
       n_ifaces=2

       for iface in range(n_ifaces):
           hex_switch=hex(i)
           hex_digit=hex_switch[2]
           sw_list[i].cmd("ip link set s"+ str(i) +"-eth"+ str(iface) + " address 00:00:00:00:"+hex_digit+hex_digit+":"+hex_digit+str(iface))


    if not batch:
        net.start()
        CLI( net )        
        net.stop()
    else: # batch
        return net, [c0, c1]


if __name__ == '__main__':
    setLogLevel( 'info' )
    ovsns()
