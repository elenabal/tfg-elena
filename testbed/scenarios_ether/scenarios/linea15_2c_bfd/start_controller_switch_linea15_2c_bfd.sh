#!/bin/bash

declare -A ifaces

 
ifaces[c0]=1
ifaces[c1]=1




echo "Creando switch c0 interfaces=${ifaces[c0]}"
./scripts/start-controller-switch-n-ifaces.sh c0 ${ifaces[c0]} 10.0.0.1 11.0.0.1 bfd

echo "Creando switch c1 interfaces=${ifaces[c1]}"
./scripts/start-controller-switch-n-ifaces.sh c1 ${ifaces[c1]} 10.0.0.1 11.0.0.2 bfd


