#!/bin/bash

declare -A ifaces

ifaces[c0]=1
ifaces[c1]=1

for i in {1..15}
do
   ifaces[s$i]=2
done

echo "Eliminando Switch c0 interfaces=${ifaces[c0]}"
./scripts/kill-switch.sh c0 ${ifaces[c0]}

echo "Eliminando Switch c1 interfaces=${ifaces[c1]}"
./scripts/kill-switch.sh c1 ${ifaces[c1]}

for i in {1..15}
do
    num=`expr 100 + $i`
    echo "Eliminando Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/kill-switch.sh s$i ${ifaces[s$i]} 
done

