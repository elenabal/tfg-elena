#!/usr/bin/python



from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info


# Topologia
#                                                              
#       1         1    2              1   2            1    2          1
#    s0 ----------- s1 --------------- s2 -------------- s3 ------------ h2
# 10.0.0.1         4|\ 3                                /3              10.0.0.2
#                   | \                              1 /
#                   |  \                              s8
#                   |   \                          2 /
#                   |    \                          /
#                   |     \ 1  2               1  3/  2         1
#                   |      s5 ------------------- s4 ---------- h3
#                   |                             4|        10.0.0.3
#                    \                             |
#                   1 \   2                     1  | 2
#                      s6 ----------------------- s7
#
# Lanzaremos un controlador independiente en s0

def ovsns(batch = False):

    "Create an empty network and add nodes to it."

    net = Mininet( topo=None,
                   build=False)

    s0 = net.addHost( 's0', ip='0.0.0.0.', mac='00:00:00:00:00:01' )
    s1 = net.addHost( 's1', ip='0.0.0.0',  mac='00:00:00:00:11:10' )
    s2 = net.addHost( 's2', ip='0.0.0.0',  mac='00:00:00:00:22:20' )
    s3 = net.addHost( 's3', ip='0.0.0.0',  mac='00:00:00:00:33:30' )
    s4 = net.addHost( 's4', ip='0.0.0.0',  mac='00:00:00:00:44:40' )
    s5 = net.addHost( 's5', ip='0.0.0.0',  mac='00:00:00:00:55:50' )
    s6 = net.addHost( 's6', ip='0.0.0.0',  mac='00:00:00:00:66:60' )
    s7 = net.addHost( 's7', ip='0.0.0.0',  mac='00:00:00:00:77:70' )
    s8 = net.addHost( 's8', ip='0.0.0.0',  mac='00:00:00:00:88:80' )
    h2 = net.addHost( 'h2', ip='10.0.0.2', mac='00:00:00:00:00:02' )
    h3 = net.addHost( 'h3', ip='10.0.0.3', mac='00:00:00:00:00:03' )

    net.addLink( s0, s1, 0, 0 )
    net.addLink( s1, s2, 1, 0 )
    net.addLink( s1, s5, 2, 0 )
    net.addLink( s1, s6, 3, 0 )
    net.addLink( s6, s7, 1, 0 )
    net.addLink( s5, s4, 1, 0 )
    net.addLink( s2, s3, 1, 0 )
    net.addLink( s3, h2, 1, 0 )
    net.addLink( s4, h3, 1, 0 )
    net.addLink( s3, s8, 2, 0 )
    net.addLink( s8, s4, 1, 2 )
    net.addLink( s4, s7, 3, 1 )

    s1.cmd("ip link set s1-eth1 address 00:00:00:00:11:11")
    s1.cmd("ip link set s1-eth2 address 00:00:00:00:11:12")
    s1.cmd("ip link set s1-eth2 address 00:00:00:00:11:13")
    s2.cmd("ip link set s2-eth1 address 00:00:00:00:22:21")
    s3.cmd("ip link set s3-eth1 address 00:00:00:00:33:31")
    s3.cmd("ip link set s3-eth2 address 00:00:00:00:33:32")
    s4.cmd("ip link set s4-eth1 address 00:00:00:00:44:41")
    s4.cmd("ip link set s4-eth2 address 00:00:00:00:44:42")
    s4.cmd("ip link set s4-eth3 address 00:00:00:00:44:43")
    s5.cmd("ip link set s5-eth1 address 00:00:00:00:55:51")
    s6.cmd("ip link set s6-eth1 address 00:00:00:00:66:61")
    s7.cmd("ip link set s7-eth1 address 00:00:00:00:77:71")
    s8.cmd("ip link set s8-eth1 address 00:00:00:00:88:81")


    if not batch:
        net.start()
        CLI( net )        
        net.stop()
    else: # batch
        return net, [s0]
        
if __name__ == '__main__':
    setLogLevel( 'info' )
    ovsns()
