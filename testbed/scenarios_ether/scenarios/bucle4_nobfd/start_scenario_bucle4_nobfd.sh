#!/bin/bash

declare -A ifaces


ifaces[s0]=1

ifaces[s1]=4
ifaces[s2]=2
ifaces[s3]=3
ifaces[s4]=4
ifaces[s5]=2
ifaces[s6]=2
ifaces[s7]=2
ifaces[s8]=2


echo "Creando switch s0 interfaces=${ifaces[s0]}"
./scripts/start-controller-switch-n-ifaces.sh s0 ${ifaces[s0]} 10.0.0.1 11.0.0.1


echo "Creando switch s1 interfaces=${ifaces[s1]} vecino controlador"
./scripts/start-switch.sh s1 ${ifaces[s1]} 10.0.0.101

# Iniciar el resto de los switches 
for i in 2 5 6 3 4 7 8
do
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 
done


