#!/bin/bash

declare -A ifaces

# ifaces array que indica el número de interfaces en cada switch.
ifaces[c0]=4


echo "Creando switch c0 interfaces=${ifaces[c0]}"
./scripts/start-controller-switch-n-ifaces.sh c0 ${ifaces[c0]} 10.0.0.1 11.0.0.1 bfd
