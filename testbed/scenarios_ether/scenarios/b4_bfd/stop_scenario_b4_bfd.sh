#!/bin/bash

declare -A ifaces

ifaces[c0]=1

ifaces[s1]=2
ifaces[s2]=2
ifaces[s3]=3
ifaces[s4]=5
ifaces[s5]=3
ifaces[s6]=4
ifaces[s7]=4
ifaces[s8]=4
ifaces[s9]=2
ifaces[s10]=4
ifaces[s11]=4
ifaces[s12]=2


echo "Eliminando Switch c0 interfaces=${ifaces[c0]}"
./scripts/kill-switch.sh c0 ${ifaces[c0]}

for i in {1..12}
do
    num=`expr 100 + $i`
    echo "Eliminando Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/kill-switch.sh s$i ${ifaces[s$i]} 
done


