#!/usr/bin/python



from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf

# Topologia
#
# c0 y 12 switches
#
#


# Lanzaremos un controlador independiente en s0

def ovsns(batch = False):

    "Create an empty network and add nodes to it."

    net = Mininet( topo=None,
                   build=False)

    c0 = net.addHost( 'c0', ip='0.0.0.0.', mac='00:00:00:00:00:01' )
    s1 = net.addHost( 's1', ip='0.0.0.0',  mac='00:00:00:00:11:10' )
    s2 = net.addHost( 's2', ip='0.0.0.0',  mac='00:00:00:00:22:20' )
    s3 = net.addHost( 's3', ip='0.0.0.0',  mac='00:00:00:00:33:30' )
    s4 = net.addHost( 's4', ip='0.0.0.0',  mac='00:00:00:00:44:40' )
    s5 = net.addHost( 's5', ip='0.0.0.0',  mac='00:00:00:00:55:50' )
    s6 = net.addHost( 's6', ip='0.0.0.0',  mac='00:00:00:00:66:60' )
    s7 = net.addHost( 's7', ip='0.0.0.0',  mac='00:00:00:00:77:70' )
    s8 = net.addHost( 's8', ip='0.0.0.0',  mac='00:00:00:00:88:80' )
    s9 = net.addHost( 's9', ip='0.0.0.0',  mac='00:00:00:00:99:90' )
    s10 = net.addHost( 's10', ip='0.0.0.0',  mac='00:00:00:00:aa:a0' )
    s11 = net.addHost( 's11', ip='0.0.0.0',  mac='00:00:00:00:bb:b0' )
    s12 = net.addHost( 's12', ip='0.0.0.0',  mac='00:00:00:00:cc:c0' )

    # Add in link options for different experiments
    linkopts = dict(bw=1000)
    linkopts2 = dict(delay='1ms', bw=1000);

    net.addLink(c0, s4, 0, 0, cls=TCLink,**linkopts)
    net.addLink(s1, s2, 0, 0, cls=TCLink,**linkopts) # l1
    net.addLink(s1, s3, 1, 0, cls=TCLink,**linkopts) # l2
    net.addLink(s3, s4, 1, 1, cls=TCLink,**linkopts) # l3 
    net.addLink(s2, s5, 1, 0, ls=TCLink,**linkopts) # l4
    net.addLink(s3, s6, 2, 0, cls=TCLink,**linkopts) # l5
    net.addLink(s6, s8, 1, 0, cls=TCLink,**linkopts) # l6
    net.addLink(s6, s7, 2, 0, cls=TCLink,**linkopts) # l7
    net.addLink(s7, s11, 1, 0, cls=TCLink,**linkopts) # l8
    net.addLink(s9, s11, 0, 1, cls=TCLink,**linkopts) # l9
    net.addLink(s8, s10, 1, 0, cls=TCLink,**linkopts) # l10
    net.addLink(s11, s12, 2, 0, cls=TCLink,**linkopts) # l11
    net.addLink(s4, s5, 2, 1, cls=TCLink,**linkopts) # l12
    net.addLink(s5, s6, 2, 3, cls=TCLink,**linkopts) # l13
    net.addLink(s10, s12, 1, 1, cls=TCLink,**linkopts) # l14
    net.addLink(s4, s8, 3, 2, cls=TCLink,**linkopts) # l15
    net.addLink(s4, s7, 4, 2, cls=TCLink,**linkopts) # l16
    net.addLink(s7, s8, 3, 3, cls=TCLink,**linkopts) # l17
    net.addLink(s9, s10, 1, 2, cls=TCLink,**linkopts) # l18
    net.addLink(s10, s11, 3, 3, cls=TCLink,**linkopts) # l19


    # switch(n_interfaces)
    # s1(2), s2(2), s3(3), s4(5), s5(3), s6(4), s7(4)
    # s8(4), s9(2), s10(4), s11(4), s12(2)

    s1.cmd("ip link set s1-eth1 address 00:00:00:00:11:11")
    s1.cmd("ip link set s1-eth2 address 00:00:00:00:11:12")

    s2.cmd("ip link set s2-eth1 address 00:00:00:00:22:21")

    s3.cmd("ip link set s3-eth1 address 00:00:00:00:33:31")
    s3.cmd("ip link set s3-eth2 address 00:00:00:00:33:32")

    s4.cmd("ip link set s4-eth1 address 00:00:00:00:44:41")
    s4.cmd("ip link set s4-eth2 address 00:00:00:00:44:42")
    s4.cmd("ip link set s4-eth3 address 00:00:00:00:44:43")

    s5.cmd("ip link set s5-eth1 address 00:00:00:00:55:51")
    s5.cmd("ip link set s5-eth2 address 00:00:00:00:55:52")

    s6.cmd("ip link set s6-eth1 address 00:00:00:00:66:61")
    s6.cmd("ip link set s6-eth2 address 00:00:00:00:66:62")
    s6.cmd("ip link set s6-eth3 address 00:00:00:00:66:63")

    s7.cmd("ip link set s7-eth1 address 00:00:00:00:77:71")
    s7.cmd("ip link set s7-eth2 address 00:00:00:00:77:72")
    s7.cmd("ip link set s7-eth3 address 00:00:00:00:77:73")

    s8.cmd("ip link set s8-eth1 address 00:00:00:00:88:81")
    s8.cmd("ip link set s8-eth2 address 00:00:00:00:88:82")
    s8.cmd("ip link set s8-eth3 address 00:00:00:00:88:83")

    s9.cmd("ip link set s9-eth1 address 00:00:00:00:99:91")

    s10.cmd("ip link set s10-eth1 address 00:00:00:00:aa:a1")
    s10.cmd("ip link set s10-eth2 address 00:00:00:00:aa:a2")
    s10.cmd("ip link set s10-eth3 address 00:00:00:00:aa:a3")

    s11.cmd("ip link set s11-eth1 address 00:00:00:00:bb:b1")
    s11.cmd("ip link set s11-eth2 address 00:00:00:00:bb:b2")
    s11.cmd("ip link set s11-eth3 address 00:00:00:00:bb:b3")

    s12.cmd("ip link set s12-eth1 address 00:00:00:00:cc:c1")

    if not batch:
        net.start()
        CLI( net )        
        net.stop()
    else: # batch
        return net, [c0]
        
if __name__ == '__main__':
    setLogLevel( 'info' )
    ovsns()
