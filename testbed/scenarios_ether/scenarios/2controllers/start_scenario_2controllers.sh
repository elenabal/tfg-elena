#!/bin/bash

declare -A ifaces

# ifaces array que indica el número de interfaces en cada switch.

ifaces[c0]=1
ifaces[c1]=1

ifaces[s1]=4
ifaces[s2]=2
ifaces[s3]=3
ifaces[s4]=4
ifaces[s5]=2
ifaces[s6]=2
ifaces[s7]=3
ifaces[s8]=2


echo ./scripts/start-controller-switch-n-ifaces.sh c0 ${ifaces[c0]} 10.0.0.1 11.0.0.1 bfd
./scripts/start-controller-switch-n-ifaces.sh c0 ${ifaces[c0]} 10.0.0.1 11.0.0.1 bfd
   
echo ./scripts/start-controller-switch-n-ifaces.sh c1 ${ifaces[c1]} 10.0.0.1 11.0.0.2 bfd
./scripts/start-controller-switch-n-ifaces.sh c1 ${ifaces[c1]} 10.0.0.1 11.0.0.2 bfd

# Switches de los controladores
echo "Switch s7 IP=10.0.0.107 interfaces=${ifaces[s7]}"
./scripts/start-switch.sh s7 ${ifaces[s7]} 10.0.0.107 100 

echo "Switch s1 IP=10.0.0.101 interfaces=${ifaces[s1]}"
./scripts/start-switch.sh s1 ${ifaces[s1]} 10.0.0.101 100



# Resto de switches

for i in 2 4 5 6 3 8
do
    num=`expr 100 + $i`
    echo "Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/start-switch.sh s$i ${ifaces[s$i]} 10.0.0.$num 100 
done

