#!/bin/bash

declare -A ifaces

# ifaces array que indica el número de interfaces en cada switch.

ifaces[c0]=1
ifaces[c1]=1

echo "---------------------
echo "Creando switch c0"
echo "---------------------
echo ./scripts/start-controller-switch-n-ifaces.sh c0 ${ifaces[c0]} 10.0.0.1 11.0.0.1 bfd
./scripts/start-controller-switch-n-ifaces.sh c0 ${ifaces[c0]} 10.0.0.1 11.0.0.1 bfd
   
echo "---------------------
echo "Creando switch c1"
echo "---------------------
echo ./scripts/start-controller-switch-n-ifaces.sh c1 ${ifaces[c1]} 10.0.0.1 11.0.0.2 bfd
./scripts/start-controller-switch-n-ifaces.sh c1 ${ifaces[c1]} 10.0.0.1 11.0.0.2 bfd

sleep 2


