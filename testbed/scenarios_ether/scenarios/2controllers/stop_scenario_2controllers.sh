#!/bin/bash

declare -A ifaces

# ifaces array que indica el número de interfaces en cada switch.

ifaces[c0]=1
ifaces[c1]=1

ifaces[s1]=4
ifaces[s2]=2
ifaces[s3]=3
ifaces[s4]=4
ifaces[s5]=2
ifaces[s6]=2
ifaces[s7]=3
ifaces[s8]=2

echo "Eliminando Switch c0 interfaces=${ifaces[c0]}"
./scripts/kill-switch.sh c0 ${ifaces[c0]}
echo "Eliminando Switch c1 interfaces=${ifaces[c1]}"
./scripts/kill-switch.sh c1 ${ifaces[c1]}

for i in {1..8}
do
    num=`expr 100 + $i`
    echo "Eliminando Switch s$i IP=10.0.0.$num interfaces=${ifaces[s$i]}"
    ./scripts/kill-switch.sh s$i ${ifaces[s$i]} 
done



