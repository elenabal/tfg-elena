""" Graph library
"""

import networkx as nx
import matplotlib.pyplot as plt 
from bitarray import bitarray
from bitarray.util import ba2hex

import logging

logger = logging.getLogger('graphs')
logger.setLevel(logging.DEBUG)


def ports(DG, path):
    """
    :param DG:             A directed graph
    :param path:           List of nodes in DG
    """
    edge_attribute = "port"
    size = len(path) - 1

    ports = [-1]*size
    for i in range(0, size):
        ports[i]=DG[path[i]][path[i+1]][edge_attribute]

    return ports


def alt_path(DG, dp):
    """
    :param DG: A directed graph
    :param dp: A path (list of nodes of DG)
    :return:   Alternate path to dp after removing first edge dp[0]-dp[1]
    """
    edges = list(DG.edges())

    l=[]

    try:
        edges.remove((dp[0],dp[1]))
        dg = DG.edge_subgraph(edges)
        l=list(nx.dijkstra_path(dg, dp[0], dp[-1]))
    except (nx.exception.NodeNotFound, nx.exception.NetworkXNoPath):
        l=[]
        
    return l


def graph (DG, src, dst, through_anchor=None):
    """
    :param DG: A directed graph
    :param src: node in DG
    :param dst: node in DG
    :param through_anchor: paths must pass through through_anchor node before reaching dst
    :return:  [] if not primary path, else, a list of paths. First element is primary_path from node src to node dst
    The rest are alternate paths for each node in primary_path[0] .. primary_path[-1]
    A path is a list of nodes in DG
    """


    if through_anchor:
        logger.debug("graph with through_anchor %s, from %s to %s", through_anchor, src, dst)

        edges = list(DG.edges())

        predecessors = DG.predecessors(dst)
        for n in predecessors:
            if n != through_anchor:
                edges.remove((n, dst))
        dg = DG.edge_subgraph(edges)
    else:
        logger.debug("graph without through_anchor")        
        dg = DG

    
    try:
        primary_path = list(nx.dijkstra_path(dg, src, dst))
    except nx.exception.NetworkXNoPath:
        return []
        

    paths=[[]] * len(primary_path)

    paths[0] = primary_path
    for i in range(0, len(primary_path)-1):
        paths[i+1] = alt_path(dg, primary_path[i:])

    return paths


def toBitarray(i,nbits):
    """
    :param i: An integer
    :param nbits: Number of bits to be used in the binary representation of i
    :return: A bitarray with i converted to bin using nbits bits left-padded with 0s 
    """
    return bitarray(format(i,"0"+str(nbits)+"b"))


def encode_path(p, nbits, total=128):
    """
    :param p:     A list of ints
    :param nbits: Number of bits to represent each int in p
    :param total: total number of bits for encoding of a path
    :return:      A bitarray with each int in p converted bot bin using nbits bits for each
    """
    ba = total * bitarray('0')

    start = 0
    for i in range(0, len(p)):
        pb = toBitarray(p[i], nbits)
        if i == len(p) - 1:
            ba[start:start+nbits] = toBitarray(0xf,nbits)
            start += nbits
            ba[start:start+nbits] = pb
            start += nbits
        else:
            ba[start:start+nbits] = pb
            start += nbits
        
    return ba



def encoded_graph(DG, g, nbits, total=128):

    logger.debug("\n>>> g:%s", g)

    if g == []:
        return []
    
    encoded_graph=[]
    
    # Port list of main path
    main_path=ports(DG,g[0])
    logger.debug("main path before: %s", main_path)

    # get ports list for each alt path in graph g
    for i in range(1,len(g)):
        if len(g[i]) != 0:
            # activate flag in port corresonding to this alt path,
            # indicating there is alt path
            logger.debug("alt path %s : %s", i,ports(DG,g[i]))
            main_path[i-1] |= 2**(nbits-1)  

            
            # encode ports in path and append to graph
            b=encode_path(ports(DG, g[i]), 4, total)
            encoded_graph.append(ba2hex(b))

    encoded_main_path=ba2hex(encode_path(main_path,4,total))
    logger.debug("ENCODED_GRAPH: %s", [encoded_main_path]+encoded_graph)
    return [encoded_main_path]+encoded_graph



    



        
