#!/bin/bash

if [ ! $# -eq 1 ]
then
    echo "Usage error: $0 <switchName>"
    exit -1
fi

echo Stop vswitchd for $1

pid_vswitchd=`cat /tmp/mininet-$1/ovs-vswitchd.pid`

ovs-appctl --target=/var/run/openvswitch/ovs-vswitchd.${pid_vswitchd}.ctl exit --cleanup
