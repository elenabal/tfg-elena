#!/bin/bash

if [ ! $# -eq 3 ]
then
    echo "Usage error: $0 <switchName> <IPAddress> <controllerIP>"
    exit -1
fi

SWITCH_NAME=$1
IP_ADDRESS=$2
CONTROLLER_IP=$3

# ------
# TABLA 0
# ------

# SYN de local resetea el reg9: se establece nueva conexión, máxima prioridad para que envíe por ALL
#ovs-ofctl add-flow $SWITCH_NAME "table=0,priority=65535,in_port=LOCAL,ipv4,nw_src=$IP_ADDRESS,nw_dst=$CONTROLLER_IP,tcp,tcp_flags=syn,tp_dst=6633,actions=learn(table=1,priority=24999,dl_type=0x800,nw_proto=6,nw_src=$CONTROLLER_IP,tp_src=6633,nw_dst=$IP_ADDRESS,load:0x0->NXM_NX_REG9[0..31]),ALL"
ovs-ofctl add-flow $SWITCH_NAME "table=0,priority=65535,in_port=LOCAL,ipv4,nw_src=$IP_ADDRESS,nw_dst=$CONTROLLER_IP,tcp,tcp_flags=syn,tp_dst=6633,actions=learn(table=1,priority=24999,dl_type=0x800,nw_proto=6,nw_src=$CONTROLLER_IP,tp_src=6633,nw_dst=$IP_ADDRESS,load:0x0->NXM_NX_REG9[0..31]),mod_dl_dst=ff:ff:ff:ff:ff:ff,ALL"

# SYN+ACK 
ovs-ofctl add-flow $SWITCH_NAME "table=0,priority=65534,ipv4,nw_src=$CONTROLLER_IP,nw_dst=$IP_ADDRESS,tcp,tcp_flags=+syn+ack,tp_src=6633,actions=resubmit(,1),resubmit(,2)"


# Resto de tráfico TCP
ovs-ofctl add-flow $SWITCH_NAME "table=0,priority=0,actions=resubmit(,1),resubmit(,2)"




# ------
# TABLA 2
# ------
# Primer SYN+ACK (i.e. reg9==0x0), genera regla para hacer que reg9 <- in_port y entrega en local
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=65535,NXM_NX_REG9[]=0,ipv4,nw_src=$CONTROLLER_IP,nw_dst=$IP_ADDRESS,tcp,tcp_flags=+syn+ack,tp_src=6633,actions=learn(table=1,priority=24999,dl_type=0x800,nw_proto=6,nw_src=$CONTROLLER_IP,tp_src=6633,nw_dst=$IP_ADDRESS,load:NXM_OF_IN_PORT->NXM_NX_REG9[0..15]),learn(table=1,priority=24999,dl_type=0x800,nw_proto=6,nw_dst=$CONTROLLER_IP,tp_dst=6633,nw_src=$IP_ADDRESS,load:NXM_OF_IN_PORT->NXM_NX_REG9[0..15]),LOCAL"

# Tras haber recibido SYN+ACK de un controlador (match reg9 != 0), sólo entregamos en LOCAL si in_port == reg9 (que es el puerto con el controlador)
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=65534,in_port=1,reg9=1,ipv4,nw_src=$CONTROLLER_IP,nw_dst=$IP_ADDRESS,tcp,tp_src=6633,actions=LOCAL"
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=65534,in_port=2,reg9=2,ipv4,nw_src=$CONTROLLER_IP,nw_dst=$IP_ADDRESS,tcp,tp_src=6633,actions=LOCAL"
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=65534,in_port=3,reg9=3,ipv4,nw_src=$CONTROLLER_IP,nw_dst=$IP_ADDRESS,tcp,tp_src=6633,actions=LOCAL"
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=65534,in_port=4,reg9=4,ipv4,nw_src=$CONTROLLER_IP,nw_dst=$IP_ADDRESS,tcp,tp_src=6633,actions=LOCAL"


# Tras haber recibido SYN+ACK de un controlador (match reg9 != 0), hacemos drop de tráfico TCP que no venga por puerto del controlador (reg9)
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=65533,ipv4,nw_src=$CONTROLLER_IP,nw_dst=$IP_ADDRESS,tcp,tp_src=6633,actions=drop"

# Tras haber recibido SYN+ACK de un controlador (match reg9 != 0), tráfico enviado a controlador sólo se manda por reg9 (puerto hacia controlador)
# Tráfico TCP LOCAL dirigido al controlador con el que quiero conectar : enviar por el puerto por el que hemos establecido la conexión.
# Cuando esté managed el controlador instala regla con prio 40000 que tapa a esta para insertar el grafo
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=39999,in_port=LOCAL,ipv4,nw_dst=$CONTROLLER_IP,tcp,tp_dst=6633,actions=output:NXM_NX_REG9[0..15]"




#ARP
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=65000,in_port=LOCAL,arp,dl_dst=ff:ff:ff:ff:ff:ff,actions=output:ALL"
#ovs-ofctl add-flow $SWITCH_NAME "table=2,arp,dl_dst=$ETH_ADDRESS,actions=LOCAL"
ovs-ofctl add-flow $SWITCH_NAME "table=2,arp,arp_tpa=$IP_ADDRESS,actions=LOCAL"

#DHCP
ovs-ofctl add-flow $SWITCH_NAME "table=2,priority=65000,in_port=LOCAL,ipv4,udp,tp_src=68,tp_dst=67,dl_dst=ff:ff:ff:ff:ff:ff,actions=output:ALL"

#Cuando envíe mensajes IPv6mcast_node_04 lo tira
ovs-ofctl add-flow $SWITCH_NAME "table=2,dl_dst=33:33:00:00:00:04,actions=drop"
ovs-ofctl add-flow $SWITCH_NAME "table=2,dl_dst=33:33:00:00:00:16,actions=drop"


#LLDP
ovs-ofctl add-flow $SWITCH_NAME "table=2,dl_dst=01:80:c2:00:00:00/ff:ff:ff:00:00:00,actions=CONTROLLER"

#Probe Topo
ovs-ofctl add-flow $SWITCH_NAME "table=2,udp,tp_src=3333,tp_dst=4444,dl_dst=ff:ff:ff:ff:ff:ff,actions=CONTROLLER"


