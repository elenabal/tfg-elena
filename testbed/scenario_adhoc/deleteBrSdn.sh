#!/bin/bash

if [ ! $# -eq 3 ]
then
    echo "Usage error: $0 <switchName> <numberInterfaces> <type_of_interfaces:wlan/eth>"
    exit -1
fi

SWITCH_NAME=$1
N_IFACES=$2
IFACE_TYPE=$3

echo Delete OVS bridge for $SWITCH_NAME
val=`expr $N_IFACES - 1`

for i in $(seq 0 $val);
do
   ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock del-port $SWITCH_NAME $SWITCH_NAME-$IFACE_TYPE$i
done
ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock del-br $SWITCH_NAME


