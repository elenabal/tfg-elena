# Requiere:
git clone https://github.com/yurekten/ryu
cd ryu
pip install .



# Maqueta de bootstrap con controlador sin grafos

Topología definida en el fichero escenario-bucle.py

 10.0.0.1      10.0.0.101      10.0.0.102         10.0.0.103       10.0.0.2
    s0 ----------- s1 -------------- s2 -------------- s3 ------------ h2
                    \
                     \
                      \
                       --------------s4 ------------- h3
                               10.0.0.104          10.0.0.3


Los switches se arrancan en modo secure, out-of-band i
Nosotros queremos un plano de control in-band, pero en ovs out-of-band significa sin entradas en tablas de flujos. El modo in-band de ovs implica que mete entradas de las tablas para poder alcanzar al controlador. Nosotros metemos las nuetras por lo que queremos el modo out-of-band

## Modo de arranque

- En un terminal arrancar mininet con la topología
  sudo python3 escenario.py

- En xterm de s0  arrancar controlador:
  s0~:> sudo ryu-manager --verbose ./miControlador.py 

- En el CLI de mininet:
  mininet > source ./start-scenario.py

## Interrumpir ejecución
- Interrumpir el controlador
- En el CLI de mininet:
  mininet > source ./stop-scenario.py


## up-switch-port.sh y down-switch-port.sh levantan/tiran puertos
- En el terminal del switch donde se desea deshabilitar un puerto, por ejemplo en s1 desactivar el puerto 2:
  s1~:> ./down-switch-port.sh s1 2
- En el terminal del switch donde se desea habilitar un puerto, por ejemplo en s1 activar el puerto 2:
  s1~:> ./up-switch-port.sh s1 2
   

## Consultar estado

Para consultar la información de un switch, por ejemplo s1, ejecutamos en su terminal:
   s1:~> /dumpAllFlows.sh s1



## Ejemplo de ejecución
 Topologia
                                                              
       1         1    2              1   2            1    2          1
    s0 ----------- s1 --------------- s2 -------------- s3 ------------ h2
 10.0.0.1           \ 3            3 /                               10.0.0.2
                     \              /
                     \          3 /           1
                       -------- s4 ------------- h3
                              1    2            10.0.0.3

- En un terminal arrancar mininet con la topología
  sudo python3 escenario-bucle.py

- En xterm de s0  arrancar controlador:
  mininet> xterm s0
  s0~:> source ../../venv/bin/activate
  s0~:> sudo ryu-manager --verbose ./miControlador.py 

- En el CLI de mininet:
  mininet > source ./start-scenario-bucle.py

- Prueba de camino alternativo
  mininet> s2 wireshark&                  # capturamos en s2-eth0
  mininet> s2 wireshark&                  # capturamos en s2-eth2

  Vemos que todo el tráfico va por el camino principal (s2-eth0)
  Ahora tiramos el enlace s1-s2 en ambos sentidos:

  mininet> s2 ./down-switch-port.sh s2 1
  mininet> s1 ./down-switch-port.sh s1 2
  Vemos que todo el tráfivo va por el camino alternativo (s2-eth2)

- Para interrumpir la ejecución: interrumpir el controlador e interrumpir el escenario
  mininet > source ./stop-scenario-bucle.py


## up-switch-port.sh y down-switch-port.sh levantan/tiran puertos
- En el terminal del switch donde se desea deshabilitar un puerto, por ejemplo en s1 desactivar el puerto 2:
  s1~:> ./down-switch-port.sh s1 2
- En el terminal del switch donde se desea habilitar un puerto, por ejemplo en s1 activar el puerto 2:
  s1~:> ./up-switch-port.sh s1 2
   

## Ejemplo de ejecución
 Topologia
                                                              
       1         1    2              1   2      1    2    1   2     1  2     1  2         1   2
    s0 ----------- s1 --------------- s2 -------- s3 ------s4 ------ s5 ----- s6 -- ... -- s16 ------ h2
 10.0.0.1           \ 3            3 /                                                             10.0.0.2
                     \              /
                     \          3 /           1
                       -------- s4 ------------- h3
                              1    2            10.0.0.3

- En un terminal arrancar mininet con la topología
  sudo python3 escenario-grande.py

- En xterm de s0  arrancar controlador:
  mininet> xterm s0
  s0~:> source ../../venv/bin/activate
  s0~:> sudo ryu-manager --verbose ./miControlador.py 

- En el CLI de mininet:
  mininet > source ./start-scenario-grande.py

- Para interrumpir la ejecución: interrumpir el controlador e interrumpir el escenario
  mininet > source ./stop-scenario-grande.py


## up-switch-port.sh y down-switch-port.sh levantan/tiran puertos
- En el terminal del switch donde se desea deshabilitar un puerto, por ejemplo, en s1 desactivar el puerto 2:
  s1~:> ./down-switch-port.sh s1 2
- En el terminal del switch donde se desea habilitar un puerto, por ejemplo en s1 activar el puerto 2:
  s1~:> ./up-switch-port.sh s1 2
   

# Topology Visualizer
No funciona con firefox

Para que funcione, suponiendo que ryu está en $RYU:
1) copiar topology-visualizer/d3.v3.min.js a $RYU/ryu/app/gui_topology/html
2) copiar topology-visualizer/ibc.html a $RYU/ryu/app/gui_topology/html
3) ryu-manager  --observe-links --app-lists ryu.app.ofctl
_rest ryu.app.gui_topology.gui_topology InBandController.py
4) instalación de brave-browser: wget  https://brave.com/linux/
5) arrancar brave-browser en s0 sin ser root, y cargar http://localhost:8080/ibc.html 

API REST docs: https://github.com/faucetsdn/ryu/blob/master/ryu/app/rest_topology.py


# Testing
Example: 
 sudo python3 -m unittest test_scenarios.ScenariosTestCase.test_kill_node_scenario_bucle


# Activate BFD
https://ryu-devel.narkive.com/5xenac6s/enabling-bfd-from-the-controller

Activate in file params.conf setting True in param use_bfd_param:
---
[DEFAULT]
use_bfd_param = True
--- 
Then launch controller passing the params.conf file as parameter to the --config-file option:

sudo ryu-manager --verbose InBandController.py --config-file params.conf > /tmp/salida 2>&1

LLDP is required to rediscover lost links, so you must launch with --observe-links:
sudo ryu-manager --observe-links --verbose InBandController.py --config-file params.conf > /tmp/salida 2>&1

