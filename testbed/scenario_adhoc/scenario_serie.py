#!/usr/bin/python


import sys

from mininet.log import setLogLevel, info
from mn_wifi.link import wmediumd, adhoc
from mn_wifi.cli import CLI
from mn_wifi.net import Mininet_wifi
from mn_wifi.wmediumdConnector import interference


def topology(autoTxPower):
    "Create a network."
    net = Mininet_wifi(link=wmediumd, wmediumd_mode=interference)

    info("*** Creating nodes\n")
    sta0 = net.addStation('sta0', position='130,10,0', ip="10.0.0.1/24"  , mac="00:00:00:00:00:01" )

    sta3 = net.addStation('sta3', position='10,10,0',  ip="10.0.0.103/24", mac="00:00:00:00:00:33" )
    sta2 = net.addStation('sta2', position='50,10,0',  ip="10.0.0.102/24", mac="00:00:00:00:00:22" )
    sta1 = net.addStation('sta1', position='90,10,0',  ip="10.0.0.101/24", mac="00:00:00:00:00:11" )
    sta4 = net.addStation('sta4', position='170,10,0', ip="10.0.0.104/24", mac="00:00:00:00:00:44" )
    sta5 = net.addStation('sta5', position='210,10,0', ip="10.0.0.105/24", mac="00:00:00:00:00:55" )
    sta6 = net.addStation('sta6', position='250,10,0', ip="10.0.0.106/24", mac="00:00:00:00:00:66" )
    sta7 = net.addStation('sta7', position='290,10,0', ip="10.0.0.107/24", mac="00:00:00:00:00:77" ) 
        
    net.setPropagationModel(model="logDistance", exp=4)

    info("*** Configuring wifi nodes\n")
    net.configureWifiNodes()

    info("*** Creating links\n")
    net.addLink(sta1, intf='sta1-wlan0', cls=adhoc, ssid='adhocNet',
                mode='g', channel=5)
    net.addLink(sta0, intf='sta0-wlan0', cls=adhoc, ssid='adhocNet',
                mode='g', channel=5)
    net.addLink(sta2, intf='sta2-wlan0', cls=adhoc, ssid='adhocNet',
                mode='g', channel=5)
    net.addLink(sta3, intf='sta3-wlan0', cls=adhoc, ssid='adhocNet',
                mode='g', channel=5)
    net.addLink(sta4, intf='sta4-wlan0', cls=adhoc, ssid='adhocNet',
                mode='g', channel=5)
    net.addLink(sta5, intf='sta5-wlan0', cls=adhoc, ssid='adhocNet',
                mode='g', channel=5)
    net.addLink(sta6, intf='sta6-wlan0', cls=adhoc, ssid='adhocNet',
                mode='g', channel=5)
    net.addLink(sta7, intf='sta7-wlan0', cls=adhoc, ssid='adhocNet',
                mode='g', channel=5)
    
    net.plotGraph(max_x=300, max_y=200)
    
    info("*** Starting network\n")
    net.build()

    info("*** Running CLI\n")
    CLI(net)

    info("*** Stopping network\n")
    net.stop()


if __name__ == '__main__':
    setLogLevel('info')
    autoTxPower = True if '-a' in sys.argv else False
    topology(autoTxPower)
