# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import re
import config

from ryu.lib.ovs import vsctl as ovs_vsctl

from ryu.topology.switches import LLDPPacket
from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.ofproto import ofproto_v1_4
from ryu.ofproto import ofproto_v1_4_parser
# from ryu.ofproto import ofproto_v1_5
# from ryu.ofproto import ofproto_v1_5_parser
from ryu.ofproto import nicira_ext
from ryu.ofproto import oxm_fields

from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ipv4
from ryu.lib.packet import tcp
from ryu.lib.packet import ether_types
from ryu.lib.packet import arp
from ryu.ofproto import ether
from ryu.ofproto import inet
from ryu.lib.packet import udp

from ryu.services.protocols.vrrp import api as vrrp_api
from ryu.services.protocols.vrrp import event as vrrp_event
from ryu.services.protocols.vrrp import utils

from ryu.lib import hub

from ryu.topology import event, switches
from ryu.topology.api import get_switch, get_link

import networkx as nx
import matplotlib.pyplot as plt 
import graphs
import time
from multiprocessing import Process,Queue
import sys
import os
import logging

def _keyboard(q):
    sys.stdin=open(0)
    while True:
        i=input()
        q.put("g")



class InBandController(app_manager.RyuApp):
    # OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION, ofproto_v1_4.OFP_VERSION, ofproto_v1_5.OFP_VERSION]
    OFP_VERSIONS = [ofproto_v1_4.OFP_VERSION]


    MAX_PORTS = 7
    
    # for each ip_address stores the list of virtual ports. Each neighbor is
    # assigned a new unused virtual port
    _port_factory = {}
    
    # our ip addr. TBD: ask for it, don't hardwire it
    CONTROLLER_IP = '10.0.0.1'
    TCP_CONTROLLER_PORT = 6633

    # used to reply to ARP requests of non managed switches
    CONTROLLER_MAC=None
    
    SRC_PORT_PROBE = 11111
    DST_PORT_PROBE = 22222

    TOPO_SRC_PORT_PROBE = 3333
    TOPO_DST_PORT_PROBE = 4444        

        # nsh_spi: identificador en la cabecera nsh para todos los mensajes que lleven grafo
    NSH_SPI=0x1234
    NSH_MD_TYPE=1

    # period for activating a switch as MANAGED
    PERIOD_CONVERT_TO_MANAGED = 3

    PERIOD_MONITOR_SWITCH = 1

    MAX_MONITOR_UNANSWERED = 5    



    # hard_timeout for rules that activate as anchor a switch
    TIMEOUT_INSTALL_FLOWS_AS_MANAGED_SWITCH = MAX_MONITOR_UNANSWERED * PERIOD_MONITOR_SWITCH


    LEARNT_ACTION_TIMEOUT = 2
#    LEARNT_ACTION_TIMEOUT = 0.5

    PERIOD_SEND_PROBE = 5    
    MAX_TIMES_SEND_PROBE = 3

    LLDP_MAX_AGE = 2


    # ovs registers used
    # reg0: used as a temporary variable to initializa in_port to fff
    # reg1: used for managing nsh ttl
    # reg2: used for remembering the destination port in nsh packets because learnt actions can't match on nsh por
    # reg3: used to know if a destination port in nsh traffic is active in table of learnt flows
    # xxreg1 (can't use xxreg0 because it collides with reg0,reg1,reg2,reg3)
    #        used to copy c1,c2,c3,c4 nsh fields and process graph contained in them

    
    LEARNING_FLOWS_TABLE = 0 # CONTAINS LEARN ACTIONS 
    ACTIVE_PORTS_TABLE   = 1 # CONTAINS LEARNT FLOWS
    
    INITIAL_TABLE        = 2 
    TABLE_ROUTING        = 3 
    TABLE_DECAP          = 4 
    SECOND_TABLE_DECAP   = 5 
    

    # cookie used in all flowmods by this controller except the ones
    # for ARP installed in _switch_features_handler(self, ev): ALL
    # those with cookie 2 are deleted in a switch that could have old
    # entries from a controller
    COOKIE=2 


    def __init__(self, *args, **kwargs):

        super(InBandController, self).__init__(*args, **kwargs)

        self.logger.setLevel(logging.DEBUG)
#        self.logger.setLevel(logging.INFO)


        # Create graph
        self.config = config.Config()
        
        l = os.popen("ifconfig " + self.config.controller_switch()).read()
        self.CONTROLLER_MAC=str(re.findall(r'ether (\S+)', str(l))[0])


        self.dpidToIP={} # maps dpid => node in graph
        
        # needed for the --observe-links option for topology discovery
        self.topology_api_app = self

        # hub.spawn(self._draw)
        
        hub.spawn(self._refresh_as_managed)
        hub.spawn(self._monitor)        


        
    @set_ev_cls(ofp_event.EventOFPFlowRemoved, MAIN_DISPATCHER)
    def flow_removed_handler(self, ev):
        msg = ev.msg
        dp = msg.datapath
        ofp = dp.ofproto

        if msg.reason == ofp.OFPRR_HARD_TIMEOUT and msg.table_id == 1:
            self.logger.debug('OFPFlowRemoved received from %s: '
                              'table_id=%d reason=%s priority=%d '
                              'idle_timeout=%d hard_timeout=%d cookie=%d '
                              'match=%s',
                              self.dpidToIP[dp.id],
                              msg.table_id, msg.reason, msg.priority,
                              msg.idle_timeout, msg.hard_timeout, msg.cookie,
                              msg.match)

            port = re.findall(r'reg2\': (\d+)', str(msg.match))[0]
            port = int(port) - 8
        
            self.logger.debug(">>>> port: %d", port)
        
#            self.port_down(self.dpidToIP[dp.id], port)
        
            
            
    @set_ev_cls(ofp_event.EventOFPEchoReply, [MAIN_DISPATCHER])
    def _i_am_alive(self, ev):
        if ev.msg.datapath.address[0] in self.config.nodes():
            self.config.setProperty(ev.msg.datapath.address[0], "hello_counter", 0)
            self.config.setProperty(ev.msg.datapath.address[0], "state", "managed")
            


    def check_ports(self,node_ip, p):
        """ Deactivate port p of node_ip if it is found dead by lldp
        First check if port is inactive
        We only deactivate if:
        a) neighbor through p is in main path of node_ip to/from controller and we have an alternate path
        b) no neighbor through p
        """

        if self.config.DG().nodes[node_ip]["ports"][p]["lldp_age"] < self.LLDP_MAX_AGE:
            self.config.DG().nodes[node_ip]["ports"][p]["lldp_age"] += 1
            return

        # find neighbor of node_ip through port p
        neighbor = None
        for n in self.config.DG().neighbors(node_ip):
            if self.config.DG()[node_ip][n]["port"] == p:
                neighbor = n

        self.logger.debug("main path from controller %s",self.config.DG().nodes[node_ip]["graph_from_controller"][0])
        self.logger.debug("main path to controller %s",self.config.DG().nodes[node_ip]["graph_to_controller"][0])

        if not neighbor:
            # it is a port that does not connect with a neighbor
            self.logger.info("PORT IS DOWN: %s from switch %s", p, node_ip)
        elif neighbor in self.config.DG().nodes[node_ip]["graph_from_controller"][0]:
            # the port connects to the closest neighbor in the main
            # path from us towards controller
            if self.config.DG().nodes[node_ip]["graph_to_controller"][1]:
                # there is an alternate path, so deactivate ports in main path
                self.deactivate_port_flows_table_1(node_ip, p)
                self.logger.info("PORT IS DOWN: %s from switch %s", p, node_ip)
                reverse_port =  self.config.DG()[neighbor][node_ip]["port"]
                self.deactivate_port_flows_table_1(neighbor, reverse_port)
                self.logger.info("PORT IS DOWN: %s from switch %s", reverse_port, neighbor)

                # Add rules to controller's switch to add graph towards the switch
                self.add_graph_towards_new_switch(node_ip)
                
        elif node_ip in self.config.DG().nodes[neighbor]["graph_from_controller"][0]:
            # the port connects to the next neighbor in the main path
            # from controller to neighbor
            i = self.config.DG().nodes[neighbor]["graph_from_controller"][0].index(node_ip)
            if self.config.DG().nodes[neighbor]["graph_from_controller"][i+1] != []:
                self.logger.info("CAMINO ALTERNATIVO EN %s",i+1)
                # there is an alternate path, so deactivate ports in main path
                self.logger.info("PORT IS DOWN: %s from switch %s", p, node_ip)
                self.deactivate_port_flows_table_1(node_ip, p)
                reverse_port =  self.config.DG()[neighbor][node_ip]["port"]
                self.deactivate_port_flows_table_1(neighbor, reverse_port)            
                self.logger.info("PORT IS DOWN: %s from switch %s", reverse_port, neighbor)                      

                # Add rules to controller's switch to add graph towards the neighbor
                self.add_graph_towards_new_switch(neighbor)
            

    def _monitor(self):
        while True:
            self.logger.info("\n#### _monitor\n")

            unmanaged_nodes = []

            for node_ip in self.config.nodes():                
                if node_ip == self.CONTROLLER_IP or node_ip == self.CONTROLLER_IP+"CONTROLLER" or\
                   "state" not in self.config.DG().nodes[node_ip].keys() or not\
                   self.config.getProperty(node_ip, "state") == "managed":                    
                    continue

                self.logger.debug("hello_counter for %s = %s", node_ip, self.config.getProperty(node_ip,"hello_counter"))
                # send Echo_Request or tag as down for deactivation if excesive echos not replied
                if self.config.getProperty(node_ip, "hello_counter") < self.MAX_MONITOR_UNANSWERED:
                    self.config.setProperty(node_ip, "hello_counter", 1 + self.config.getProperty(node_ip, "hello_counter"))
                    datapath = self.config.getProperty(node_ip, "datapath")
                    ofp_parser = datapath.ofproto_parser
                    req = ofp_parser.OFPEchoRequest(datapath, None)
                    datapath.send_msg(req)
                else:
                    unmanaged_nodes.append(node_ip)

                # # check ports of node
                # for p in self.config.DG().nodes[node_ip]["ports"].keys():
                #     if p == self.config.DG().nodes[node_ip]["datapath"].ofproto.OFPP_LOCAL:
                #         continue
                #     else:
                #         self.check_ports(node_ip, p)
                            
                
            # deactivate switches tagged as down
            for node_ip in unmanaged_nodes:
                self.logger.info("switch [%s] IS NOT MANAGED", node_ip)
                if self.config.getProperty(node_ip, "state") == "managed":
                    self.deactivate_switch_as_managed(node_ip)
                    
            hub.sleep(self.PERIOD_MONITOR_SWITCH)

    def init_controller_switch(self):
        self.logger.info("\n#### init_controller_switch\n")

        # refresh controller switch
        datapath = self.config.getProperty(self.CONTROLLER_IP, "datapath")

        if not datapath:
            return

        parser = datapath.ofproto_parser

        # rule 1
        flow_match = parser.OFPMatch(in_port=1,
                                     eth_type=ether_types.ETH_TYPE_IP,
                                     ip_proto=inet.IPPROTO_TCP,
                                     ipv4_dst=self.CONTROLLER_IP,
                                     tcp_dst=self.TCP_CONTROLLER_PORT
                                     )
        flow_actions = [parser.OFPActionOutput(datapath.ofproto.OFPP_LOCAL)]

        self.add_flow(datapath, 65534, flow_match, flow_actions, hard_timeout=self.TIMEOUT_INSTALL_FLOWS_AS_MANAGED_SWITCH, table_id=self.INITIAL_TABLE)

        
        # rule 2
        flow_match = parser.OFPMatch(in_port=1,
                                     eth_type=ether_types.ETH_TYPE_IP,
                                     ip_proto=inet.IPPROTO_TCP,
                                     ipv4_dst=self.CONTROLLER_IP,
                                     tcp_dst=self.TCP_CONTROLLER_PORT,
                                     tcp_flags=tcp.TCP_SYN
                                     )
        flow_actions = [parser.OFPActionOutput(datapath.ofproto.OFPP_CONTROLLER)]
        self.add_flow(datapath, 65535, flow_match, flow_actions, hard_timeout=self.TIMEOUT_INSTALL_FLOWS_AS_MANAGED_SWITCH, table_id=self.INITIAL_TABLE)



        datapath.send_barrier()
            

    def refresh_nodes(self):
        self.logger.info("\n#### refresh_nodes\n")


        # refresh controller switch
        self.init_controller_switch()
        
        # refresh rest of switches
        nodes = self.config.nodes()
        for n in nodes:
            self.logger.debug('#### _refresh_as_managed for: %s????', n)

            # check switches distint of controller's switch
            if "datapath" in self.config.DG().nodes[n].keys() and n != self.CONTROLLER_IP and "port_to_controller" in self.config.DG().nodes[n].keys() and self.config.getProperty(n, "port_to_controller") != None:
                self.logger.debug('################ yes, _refresh_as_managed for: %s, eth_addr: %s', n, self.get_eth(n, self.config.getProperty(n, "port_to_controller")))
                self.install_flows_as_managed_switch(self.config.getProperty(n, "datapath"), n, eth_addr = self.get_eth(n, self.config.getProperty(n, "port_to_controller")))

                # Add rules to controller's switch to add graph towards the new switch
                self.add_graph_towards_new_switch(ipv4_src = n)

                # Probe topo (our substitute for lldp)
                self._send_probe_topo(n, self.get_eth(n, self.config.getProperty(n, "port_to_controller")), self.config.getProperty(n, "datapath"))

        # Probe topo (our substitute for lldp) for controller switch
        if self.config.getProperty(self.CONTROLLER_IP, "datapath"):
            self._send_probe_topo(self.CONTROLLER_IP, self.CONTROLLER_MAC, self.config.getProperty(self.CONTROLLER_IP, "datapath"))



        
    def _refresh_as_managed(self):
        while True:
            self.logger.info("\n#### _refresh_as_managed\n")

            self.refresh_nodes()

            time.sleep(self.PERIOD_CONVERT_TO_MANAGED)

            
    
    def _draw(self):
        q = Queue()
        p = Process(target=_keyboard, args=(q,))
        p.start()

        while True:
            try:
                draw = q.get_nowait()
                pos = nx.spring_layout(self.config.DG())
                nx.draw(self.config.DG(),pos,with_labels=True, node_size=1000, node_shape='h', arrowstyle='-')
                edge_labels = nx.get_edge_attributes(self.config.DG(), 'port')
                nx.draw_networkx_edge_labels(self.config.DG(), pos, edge_labels, label_pos=0.7)
                plt.plot()
                plt.show()
            except:
                time.sleep(2)

                


    @set_ev_cls(ofp_event.EventOFPStateChange,
                [MAIN_DISPATCHER, DEAD_DISPATCHER])
    def _state_change_handler(self, ev):
        datapath = ev.datapath
        if ev.state == MAIN_DISPATCHER:
            self.logger.info('REGISTERED switch: %s', datapath.address[0])
        elif ev.state == DEAD_DISPATCHER:
            self.logger.info('UNREGISTERED switch: %s', datapath.address[0])

        



            
                
    # requires running controller with --observe-links option
    @set_ev_cls(event.EventSwitchEnter)
    def get_topology_data(self, ev):
        switch_list = get_switch(self.topology_api_app, None)
        switches=[switch.dp.id for switch in switch_list]
        links_list = get_link(self.topology_api_app, None)
        links=[(link.src.dpid,link.dst.dpid,{'port':link.src.port_no}) for link in links_list]

        self.logger.debug('#### event.EventSwitchEnter links   : %s', links)
        self.logger.debug('#### event.EventSwitchEnter switches: %s', switches)
        


        
    @set_ev_cls(ofp_event.EventOFPPortDescStatsReply, CONFIG_DISPATCHER)
    def _port_desc_stats_reply_handler(self, ev):

        datapath = ev.msg.datapath

        
        self.logger.info("_port_desc_stats_reply_handler %s", datapath.address[0])

        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # first of all we delete all previous flows
        self.del_all_flows(datapath)

        ports = {}
        ports_description = []
        for p in ev.msg.body:
            ports_description.append('port_no=%d hw_addr=%s name=%s config=0x%08x '
                                     'state=0x%08x properties=%s' %
                                     (p.port_no, p.hw_addr,
                                      p.name, p.config, p.state, repr(p.properties)))
            ports[p.port_no]={"lldp_age"  : 0, # number of cycles without lldp message received for this port
                              "hw_addr"   : p.hw_addr,
                              "name"      : p.name,
                              "config"    : p.config,
                              "state"     : p.state,
                              "properties": repr(p.properties)}
            
            

            
        self.logger.debug('#### OFPPortDescStatsReply received: %s', ports_description)


        if (datapath.address[0] == self.CONTROLLER_IP):
            self.logger.info("switch sc [%s] connected", datapath.address[0])
            # This is the switch of the controller. We add it to switch_info here. The rest
            # of switches are added when their anchor sends us a PacketIn with their SYN
            self.logger.debug("-----------SWITCH [%s]:: out_port=%d TO CONTROLLER", self.CONTROLLER_IP, datapath.ofproto.OFPP_LOCAL)

            # Reset ports for the switch of the controller
            self.delete_node_from_port_factory(self.CONTROLLER_IP)

            ### Insert controller and sc intro graph
            # add edge from sc (CONTROLLER_IP) to controller (CONTROLLER_IP+"CONTROLLER")
            port = self.port_factory(self.CONTROLLER_IP)
            self.config.DG().add_edges_from([(self.CONTROLLER_IP, self.CONTROLLER_IP+"CONTROLLER")], physical_port=datapath.ofproto.OFPP_LOCAL, eth_address=self.CONTROLLER_MAC, port=port)
            self.logger.debug("Added new port %s to controller switch for communication with controller", port)
            
            self.config.setProperty(self.CONTROLLER_IP, "datapath", datapath)
            self.dpidToIP[datapath.id]=self.CONTROLLER_IP
            self.config.setProperty(self.CONTROLLER_IP, "state", "managed")

            self.config.setProperty(self.CONTROLLER_IP, "graphs_from_controller", {})
            
            self.init_controller_switch()
            
        else:
            self.logger.info("switch [%s] connected", datapath.address[0])
            parser = datapath.ofproto_parser
            self.logger.debug("-----------          ADD FLOW: each UDP PROBE to CONTROLLER generate PACKET_IN ") 
            flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP,
                                         ip_proto=inet.IPPROTO_UDP, udp_src=self.SRC_PORT_PROBE, udp_dst=self.DST_PORT_PROBE
            )
            
            flow_actions = [parser.OFPActionOutput(datapath.ofproto.OFPP_CONTROLLER)]
            self.add_flow(datapath, 60000, flow_match, flow_actions, table_id=self.INITIAL_TABLE)
            datapath.send_barrier()

            # A switch different from the switch co-located with the
            # controller => Add datapath to node of new switch. The
            # switch co-located with controller is added in the
            # features handler
            self.config.setProperty(ev.msg.datapath.address[0], "datapath", ev.msg.datapath)
            self.dpidToIP[ev.msg.datapath.id]=ev.msg.datapath.address[0]


            
            # This is a new switch, distint from the switch of the controller:
            # send probe to know its port to controller
            for anchor_address in self.config.DG().nodes[datapath.address[0]]["anchors"].keys():
                self.config.DG().nodes[datapath.address[0]]["anchors"][anchor_address]=False
                self.logger.info("Sending probe to anchor %s", anchor_address)
                hub.spawn(self._send_probe, anchor_address, self.config.DG()[anchor_address][datapath.address[0]]["eth_address"], datapath, self.config.DG()[datapath.address[0]][anchor_address]["eth_address"])


        ip_addr = datapath.address[0]
        # Add ports info to node 
        self.config.setProperty(ip_addr, "ports", ports)
        
        # install flow entries
        self.add_flows_for_forwarding_graph(ip_addr)

        
    def get_eth(self, ip_addr, physical_port):
        ports = self.config.getProperty(ip_addr, "ports")
        return ports[physical_port]["hw_addr"]

    def get_physical_port(self, ip_addr, eth_addr):
        ports = self.config.getProperty(ip_addr, "ports")
        for p in ports.keys():
            if ports[p]["hw_addr"] == eth_addr:
                return p
    
        
    
        
    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _switch_features_handler(self, ev):
        pass
        # datapath = ev.msg.datapath
        # ofproto = datapath.ofproto
        # parser = datapath.ofproto_parser

        # if datapath.address[0] == self.CONTROLLER_IP:
            
        # if (ev.msg.datapath.address[0] != self.CONTROLLER_IP):
            


        
    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        pkt_eth = pkt.get_protocol(ethernet.ethernet)


        # LDDP
        if pkt_eth.ethertype == ether_types.ETH_TYPE_LLDP and self.config.DG().nodes[datapath.address[0]]["state"]=="managed":
            dst_dpid = msg.datapath.id
            src_dpid, src_port_no = LLDPPacket.lldp_parse(msg.data)  

            
            node_a = self.dpidToIP[src_dpid]
            port_a = src_port_no
            node_b = msg.datapath.address[0]
            port_b = in_port
            self.logger.debug("LLDP received informing of %s(%s) <=> %s(%s)", node_a, port_a,  node_b, port_b)

            do_refresh = False
            if not self.config.DG().has_edge(node_a, node_b):
                self.config.DG().add_edges_from([(node_a, node_b)],physical_port=port_a, eth_address=self.get_eth(node_a, port_a), port = self.port_factory(node_a))
                do_refresh = True
            if not self.config.DG().has_edge(node_b, node_a):
                self.config.DG().add_edges_from([(node_b, node_a)],physical_port=port_b, eth_address=self.get_eth(node_b, port_b), port = self.port_factory(node_b))
                do_refresh = True

            if do_refresh:
                self.refresh_nodes()
                
            

        #     if self.config.DG().nodes[self.dpidToIP[src_dpid]]["ports"][src_port_no]["lldp_age"] == self.LLDP_MAX_AGE:
        #         self.activate_port_flows_table_1(self.dpidToIP[src_dpid], src_port_no)
        #         # Add rules to controller's switch to add graph towards the new switch
        #         self.add_graph_towards_new_switch(self.dpidToIP[src_dpid])
        #     self.config.DG().nodes[self.dpidToIP[src_dpid]]["ports"][src_port_no]["lldp_age"]=0
            
        #     if self.config.DG().nodes[msg.datapath.address[0]]["ports"][in_port]["lldp_age"] == self.LLDP_MAX_AGE:
        #         self.activate_port_flows_table_1(msg.datapath.address[0], in_port)
        #         # Add rules to controller's switch to add graph towards the new switch
        #         self.add_graph_towards_new_switch(msg.datapath.address[0])
        #     self.config.DG().nodes[msg.datapath.address[0]]["ports"][in_port]["lldp_age"]=0            
        #     return

        self.logger.debug("----- NEW PACKET_IN:: eth_src=%s eth_dst=%s in_port=%s", 
                         pkt_eth.src, pkt_eth.dst, in_port)


        # ARP
        pkt_arp = pkt.get_protocol(arp.arp)
        if pkt_arp and pkt_arp.opcode == arp.ARP_REQUEST and pkt_arp.dst_ip == self.CONTROLLER_IP:
            #Packet-in containing an ARP from a new node can't be received from unamanaged node
            if datapath.address[0] in self.config.nodes() and self.config.getProperty(datapath.address[0], "state") =="managed":
                self.logger.debug("   ----- PACKET_IN:: ARP REQUEST")

                # Send back ARP answer
                self.bcastARP(dpath=datapath, in_port=in_port, eth=pkt_eth, arp_req=pkt_arp)

            return

        
        # IPV4
        pkt_ipv4 = pkt.get_protocol(ipv4.ipv4)
        pkt_tcp = pkt.get_protocol(tcp.tcp)
        pkt_udp = pkt.get_protocol(udp.udp)


        # TCP SYN
        if pkt_ipv4 and pkt_ipv4.dst == self.CONTROLLER_IP and pkt_tcp and pkt_tcp.dst_port == self.TCP_CONTROLLER_PORT and pkt_tcp.has_flags(tcp.TCP_SYN):
            # Packet-in containing a TCP SYN from a new switch can't be received from unamanaged node
            if datapath.address[0] in self.config.nodes() and self.config.getProperty(datapath.address[0], "state") == "managed":
                self.logger.debug("   ----- PACKET_IN:: SYN       from=%s", pkt_ipv4.src)
                self.syn_rcvd(anchor_address=datapath.address[0], eth_src=pkt_eth.src, ip_src=pkt_ipv4.src, in_port=in_port, pkt=pkt)

            return


        # UDP PROBE from switch. Received fron unmanaged node
        if pkt_ipv4 and pkt_udp and pkt_udp.src_port == self.SRC_PORT_PROBE and pkt_udp.dst_port == self.DST_PORT_PROBE:
            self.logger.debug("   ----- PACKET_IN:: PROBE from %s anchor %s through port %s", datapath.address[0], pkt_ipv4.src, in_port)
            
            self.activate_switch_as_managed(datapath=datapath, anchor_address=pkt_ipv4.src, in_port=in_port)

            return


        # TOPO PROBE from switch
        if pkt_ipv4 and pkt_udp and pkt_udp.src_port == self.TOPO_SRC_PORT_PROBE and pkt_udp.dst_port == self.TOPO_DST_PORT_PROBE:
            ip_src  = pkt_ipv4.src
            eth_src = pkt_eth.src
            ip_dst  = datapath.address[0]
            physical_port_dst = in_port
            self.logger.debug("   ----- PACKET_IN:: TOPO PROBE informing of edge from node %s with eth %s to node %s with physical port %s ",  ip_src, eth_src, ip_dst, physical_port_dst)
            if not self.config.DG().has_edge(ip_src, ip_dst):
                self.config.DG().add_edges_from([(ip_src, ip_dst)], port=self.port_factory(ip_src), physical_port=self.get_physical_port(ip_src, eth_src), eth_address=eth_src)                
            
            return

                                      
                                      
    def _send_probe_topo(self, anchor_address, anchor_eth_address, anchor_dpath):

        bcast_address = "ff:ff:ff:ff:ff:ff"
        bcast_ip = "255.255.255.255"        

        
        answer_e = ethernet.ethernet(dst=bcast_address,
                                     src=anchor_eth_address, ethertype=ether.ETH_TYPE_IP)
        answer_i = ipv4.ipv4(proto=inet.IPPROTO_UDP, src=anchor_address, dst=bcast_ip)
        answer_u = udp.udp(src_port=self.TOPO_SRC_PORT_PROBE, dst_port=self.TOPO_DST_PORT_PROBE)
        
        answer_pkt = packet.Packet()
        answer_pkt.add_protocol(answer_e)
        answer_pkt.add_protocol(answer_i)
        answer_pkt.add_protocol(answer_u)
        answer_pkt.serialize()

        
        answer_parser  = anchor_dpath.ofproto_parser
        answer_ofproto = anchor_dpath.ofproto
        answer_in_port = answer_ofproto.OFPP_CONTROLLER
        
        for answer_out_port in self.config.DG().nodes[anchor_address]["ports"].keys():
            if answer_out_port != anchor_dpath.ofproto.OFPP_LOCAL:
                self.logger.debug("----------- SENDING PROBE TOPO to anchor %s outport=%d", anchor_address, answer_out_port)
                actions = [answer_parser.OFPActionOutput(port=answer_out_port)]
                out = answer_parser.OFPPacketOut(datapath=anchor_dpath, buffer_id=answer_ofproto.OFP_NO_BUFFER,
                                                 in_port=answer_in_port, actions=actions, data=answer_pkt)                                             
                anchor_dpath.send_msg(out)
                


            
    def _send_probe(self, anchor_address, anchor_eth_address, datapath, datapath_eth_address):
        dst = datapath.address[0]
        if not dst in self.config.nodes():
            return

        times = 0

        while (times < self.MAX_TIMES_SEND_PROBE and \
               dst in self.config.nodes() and \
               anchor_address in self.config.DG().nodes[dst]["anchors"].keys() and not\
               self.config.DG().nodes[dst]["anchors"][anchor_address]):

            answer_pkt = packet.Packet()

            if "datapath" in self.config.DG().nodes[anchor_address].keys():
                anchor_dpath=self.config.DG().nodes[anchor_address]["datapath"]
            else:
                break

            self.logger.debug("   ----- _send_probe to: %s through anchor %s", dst, anchor_address)
            

            # addr. Could use the src addr of the datapath
            answer_e = ethernet.ethernet(dst=datapath_eth_address,
                                         src=anchor_eth_address, ethertype=ether.ETH_TYPE_IP)
            # src address of probe is the address of anchor
            answer_i = ipv4.ipv4(proto=inet.IPPROTO_UDP, src=anchor_address, dst=dst)
            answer_u = udp.udp(src_port=self.SRC_PORT_PROBE, dst_port=self.DST_PORT_PROBE)

            answer_pkt.add_protocol(answer_e)
            answer_pkt.add_protocol(answer_i)
            answer_pkt.add_protocol(answer_u)
            answer_pkt.serialize()

            answer_out_port = self.config.DG()[anchor_address][dst]["physical_port"]
            
            answer_parser= anchor_dpath.ofproto_parser
            answer_ofproto = anchor_dpath.ofproto
            answer_in_port = answer_ofproto.OFPP_CONTROLLER
#            answer_match = ofproto_v1_5_parser.OFPMatch(in_port=answer_in_port)
            answer_match = ofproto_v1_4_parser.OFPMatch(in_port=answer_in_port)
            actions = [answer_parser.OFPActionOutput(port=answer_out_port)]
            self.logger.debug("----------- SENDING PROBE anchor=%s outport=%d", anchor_dpath.address[0], answer_out_port)
            out = answer_parser.OFPPacketOut(datapath=anchor_dpath, buffer_id=answer_ofproto.OFP_NO_BUFFER,
                                             in_port=answer_in_port, actions=actions, data=answer_pkt)                                             
#                                         match=answer_match, actions=actions, data=answer_pkt)
            anchor_dpath.send_msg(out)

            hub.sleep(self.PERIOD_SEND_PROBE)


    def del_all_flows(self, datapath, cookie=COOKIE):
        self.logger.debug("----------- del_all_flows for %s", datapath.address[0])

        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        mod = parser.OFPFlowMod(
            cookie=cookie,
            cookie_mask=0xFFFFFFFFFFFFFFFF,
            datapath=datapath,
            table_id=ofproto.OFPTT_ALL,
            command=ofproto.OFPFC_DELETE,
            out_port=ofproto.OFPP_ANY,
            out_group=ofproto.OFPG_ANY,
        )

        datapath.send_msg(mod)
        datapath.send_barrier()

        

    def add_flow(self, datapath, priority, match, actions, table_id, buffer_id=None, hard_timeout=0, cookie=COOKIE, command="add"):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        command_type = ofproto.OFPFC_ADD;
        if command == "modify":
            command_type = ofproto.OFPFC_MODIFY;


        
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(table_id=table_id, datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match, hard_timeout=hard_timeout,
                                    instructions=inst, cookie=cookie, command=command_type)
        else:
            mod = parser.OFPFlowMod(table_id=table_id, datapath=datapath, priority=priority,
                                    match=match, hard_timeout=hard_timeout, instructions=inst, cookie=cookie, command=command_type)

        datapath.send_msg(mod)

        
    def bcastARP(self, dpath, in_port, eth, arp_req):
        self.logger.debug("-----------bcastARP:: datapath %s received ARP in in_port=%s", dpath.address[0], in_port)
        ofproto = dpath.ofproto
        parser = dpath.ofproto_parser

        answer_dst_mac = eth.src
        answer_src_mac = self.CONTROLLER_MAC


        
        answer_ether_proto = ether.ETH_TYPE_ARP

        answer_hwtype = 1
        answer_arp_proto = ether.ETH_TYPE_IP
        answer_hlen = 6
        answer_plen = 4
        answer_dst_mac = arp_req.src_mac
        answer_dst_ip = arp_req.src_ip
        answer_src_ip = arp_req.dst_ip
        answer_output_port = in_port
        answer_in_port = ofproto.OFPP_CONTROLLER
        
        answer_arp_code = arp.ARP_REPLY

        answer_pkt = packet.Packet()

        # Include as source address of Ethernet frame and as src eth
        # in arp answer the one of the anchor, not the one of the
        # controller Necessary for adhoc mode of 802.11
        self.logger.debug("-----------bcastARP:: dir eth origen %s ", self.get_eth(dpath.address[0],answer_output_port))
        answer_e = ethernet.ethernet(answer_dst_mac, self.get_eth(dpath.address[0],answer_output_port), answer_ether_proto)
        answer_a = arp.arp(answer_hwtype, answer_arp_proto, answer_hlen, answer_plen, answer_arp_code,
                           self.get_eth(dpath.address[0],answer_output_port), answer_src_ip, answer_dst_mac, answer_dst_ip)

        answer_pkt.add_protocol(answer_e)
        answer_pkt.add_protocol(answer_a)

        answer_pkt.serialize()
            
        answer_in_port = ofproto.OFPP_CONTROLLER
#        match = ofproto_v1_5_parser.OFPMatch(in_port=answer_in_port)
        match = ofproto_v1_4_parser.OFPMatch(in_port=answer_in_port)
        actions = [parser.OFPActionOutput(port=answer_output_port)]
        self.logger.debug("-----------ARP_HANDLER:: build packet anchor=%s", dpath.address[0])

        out = parser.OFPPacketOut(datapath=dpath, buffer_id=ofproto.OFP_NO_BUFFER,
                                  in_port=answer_in_port, actions=actions, data=answer_pkt)
#                                  match=match, actions=actions, data=answer_pkt)

        self.logger.debug("-----------ARP_HANDLER:: send packet")
        dpath.send_msg(out)

        # anade en la cache de ARP para que l gratuitous ARP que haga el controlador no se envie
        self.logger.info(">>>>>>>>>>>>>>>>> ARP INSERTED FOR %s", answer_dst_ip)
        os.system('arp -s ' + answer_dst_ip + ' ' + answer_dst_mac)
 
    
    def port_factory(self, address):
        if address in self._port_factory.keys():
            for p in range(1, self.MAX_PORTS + 1):
                if p not in self._port_factory[address]:
                    self._port_factory[address].append(p)
                    self.logger.debug("Assigned port % s to node %s in port_factory", p, address)
                    return p
        else:
            self._port_factory[address] = [1]
            return 1

    def return_port_to_port_factory(self, address, port):
        if port in self._port_factory[address]:
            self._port_factory[address].remove(port)
            self.logger.debug("Returned port %s to port_factory for node %s", port, address)

    def delete_node_from_port_factory(self, address):
        if address in self._port_factory.keys():
            del self._port_factory[address] 
            self.logger.debug("Deleted node %s from port_factory", address)        

    def syn_rcvd(self, anchor_address, eth_src, ip_src, in_port, pkt):
        if not ip_src in self.config.nodes():
            self.logger.debug("SYN received from NEW %s eth_addr=%s through anchor %s", ip_src, eth_src, anchor_address)


            ##### insert edge from anchor to new switch 
            port = self.port_factory(anchor_address)
            physical_port = in_port
            self.config.DG().add_edges_from([(anchor_address, ip_src)], physical_port=physical_port, eth_address=self.get_eth(anchor_address, physical_port), port=port)
            ##### insert edge from new switch to anchor
            # apuntamos eth_src en arco. El puerto físico y el virtual los crearemos cuando conteste al probe
            self.config.DG().add_edges_from([(ip_src, anchor_address)], eth_address=eth_src)            

            self.config.setProperty(ip_src, "state", "syn_rcvd")
            
            # A new neighbor of anchor_address => update forwarding graph rules of anchor
            self.add_flows_table_routing(ipv4_addr=anchor_address, port_no=port, physical_port=physical_port, eth_src=self.get_eth(anchor_address, physical_port), eth_dst=eth_src, table_routing = self.TABLE_ROUTING)
                
            
            self.logger.debug("Added link from %s to new node %s with port %s and physical_port %s", anchor_address, ip_src, port, physical_port)
            
            # it's a switch, so it MUST have a datapath attribute
            # We'll fill it with its datapath once the connection is established
            self.config.setProperty(ip_src, "datapath", None) # the datapath of the switch

            self.config.setProperty(ip_src, "hello_counter", 0)

            # We still don't know its port_to_controller. Probe will get it
            self.config.setProperty(ip_src, "port_to_controller", None)

            
        
            # dictionary of {anchor_address -> received_probe(boolean)}
            self.config.setProperty(ip_src, "anchors", {anchor_address: False})


            p = self.config.DG()[anchor_address][ip_src]["port"]
            physical_port= self.config.DG()[anchor_address][ip_src]["physical_port"]
            eth_address = self.config.DG()[ip_src][anchor_address]["eth_address"]
            self.add_learning_flow_for_port (anchor_address, p, physical_port, eth_address, self.LEARNING_FLOWS_TABLE, self.ACTIVE_PORTS_TABLE, self.INITIAL_TABLE)            


        else: # we already know this switch
            self.logger.debug("SYN received from ALREADY KNOWN %s eth_addr=%s through anchor %s", ip_src, eth_src, anchor_address)

            if "anchors" in self.config.DG().nodes[ip_src].keys() and not anchor_address in self.config.DG().nodes[ip_src]["anchors"].keys():
                # It's a switch we already know, but trying to connect from a new anchor
                
                ##### insert edge from anchor to new switch 
                port = self.port_factory(anchor_address)
                physical_port = in_port
                anchor_eth_address = self.get_eth(anchor_address, physical_port)
                self.config.DG().add_edges_from([(anchor_address, ip_src)], physical_port=physical_port, eth_address=anchor_eth_address, port=port)
                self.logger.debug("Added link from %s to new node %s with port %s and physical_port %s", anchor_address, ip_src, port, physical_port)
                
                ##### insert edge from new switch to anchor
                # apuntamos eth_src en arco. El puerto físico y el virtual los crearemos cuando conteste al probe
                self.config.DG().add_edges_from([(ip_src, anchor_address)], eth_address=eth_src)            
                # annotate anchor of new switch: will turn true when probe is received
                self.config.DG().nodes[ip_src]["anchors"][anchor_address] = False

                p = self.config.DG()[anchor_address][ip_src]["port"]
                physical_port= self.config.DG()[anchor_address][ip_src]["physical_port"]
                eth_address = self.config.DG()[ip_src][anchor_address]["eth_address"]
                self.add_learning_flow_for_port (anchor_address, p, physical_port, eth_address, self.LEARNING_FLOWS_TABLE, self.ACTIVE_PORTS_TABLE, self.INITIAL_TABLE)
        
        # A new neighbor of anchor_address => update forwarding graph rules of anchor
        if self.config.DG().has_edge(anchor_address, ip_src) and "port" in self.config.DG()[anchor_address][ip_src].keys():
            port = self.config.DG()[anchor_address][ip_src]["port"]
            physical_port = self.config.DG()[anchor_address][ip_src]["physical_port"]
            self.add_flows_table_routing(ipv4_addr=anchor_address, port_no=port, physical_port=physical_port, eth_src=self.get_eth(anchor_address, self.config.DG()[anchor_address][ip_src]["physical_port"]), eth_dst=eth_src, table_routing = self.TABLE_ROUTING)

        


        # Add rules to controller's switch to add graph towards the new switch
        self.add_graph_towards_new_switch(ipv4_src=ip_src)


        
        
        # anade en la cache de ARP para que l gratuitous ARP que haga el controlador no se envie
        pkt_eth = pkt.get_protocol(ethernet.ethernet)
        self.logger.info(">>>>>>>>>>>>>>>>> ARP INSERTED FOR %s", ip_src)
        os.system('arp -s ' + ip_src + ' ' + pkt_eth.src)
        
        # Finally inject SYN in local TCP/IP stack SYN+ACK will
        # already be routed using the graph installed in previous
        # sentence
        pkt_eth = pkt.get_protocol(ethernet.ethernet)
        # change destination ether address to the one of the controller
        new_pkt_eth = ethernet.ethernet(self.CONTROLLER_MAC, pkt_eth.src, pkt_eth.ethertype)

        pkt_ip  = pkt.get_protocol(ipv4.ipv4)
        pkt_tcp = pkt.get_protocol(tcp.tcp)
        
        syn_pkt = packet.Packet()

        syn_pkt.add_protocol(new_pkt_eth)
        syn_pkt.add_protocol(pkt_ip)
        syn_pkt.add_protocol(pkt_tcp)

        syn_pkt.serialize()
        self.send_pkt_out_to_controller_tcpip(pkt=syn_pkt)

        

    def add_flow_actions_push_ethernet_header_to_stack(self, flow_actions, parser):
        if not flow_actions:
            flow_actions = [parser.NXActionStackPush(field="eth_src", start=0, end=48)]
        else:     
            flow_actions += [parser.NXActionStackPush(field="eth_src", start=0, end=48)]
        flow_actions += [parser.NXActionStackPush(field="eth_dst", start=0, end=48)]
        flow_actions += [parser.NXActionDecap()]
        return flow_actions

    def add_flow_actions_pop_ethernet_header_from_stack(self, flow_actions, parser):
        flow_actions += [parser.NXActionEncapEther()]
        flow_actions += [parser.NXActionStackPop(field="eth_dst", start=0, end=48)]
        flow_actions += [parser.NXActionStackPop(field="eth_src", start=0, end=48)]            
        return flow_actions


    def add_flow_actions_encap_nsh(self, flow_actions, parser, nsh_c1, nsh_c2, nsh_c3, nsh_c4, nsh_spi=NSH_SPI, nsh_ttl=0):
        flow_actions += [parser.NXActionEncapNsh()]
        flow_actions += [parser.OFPActionSetField(nsh_spi=nsh_spi)]
        flow_actions += [parser.OFPActionSetField(nsh_c1=nsh_c1)]
        flow_actions += [parser.OFPActionSetField(nsh_c2=nsh_c2)]
        flow_actions += [parser.OFPActionSetField(nsh_c3=nsh_c3)]
        flow_actions += [parser.OFPActionSetField(nsh_c4=nsh_c4)]                    
        flow_actions += [parser.OFPActionSetField(nsh_ttl=nsh_ttl)]            
        return flow_actions

    def add_flow_actions_move_path_from_nsh_cn_to_xxreg1(self, flow_actions, parser):
        # mueve los bits de nsh_cn a xxreg1, eliminando los primeros 4 bits.
        if not flow_actions:
            flow_actions = [parser.NXActionRegMove(src_field="nsh_c1", dst_field="xxreg1", n_bits=28, src_ofs=0, dst_ofs=100)]
        else:
            flow_actions += [parser.NXActionRegMove(src_field="nsh_c1", dst_field="xxreg1", n_bits=28, src_ofs=0, dst_ofs=100)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c2", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=68)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c3", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=36)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c4", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=4)]
        flow_actions += [parser.NXActionRegLoad(ofs_nbits=nicira_ext.ofs_nbits(0,3), dst="xxreg1", value=0x0)]
        return flow_actions


    def add_flow_actions_move_path_from_xxreg1_to_nsh_cn(self, flow_actions, parser):
        # mueve los 128 bits de xxreg1 a nsh_cn
        if not flow_actions:
           flow_actions = [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c1", n_bits=32, src_ofs=96, dst_ofs=0)]
        else:
           flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c1", n_bits=32, src_ofs=96, dst_ofs=0)]

        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c2", n_bits=32, src_ofs=64, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c3", n_bits=32, src_ofs=32, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c4", n_bits=32, src_ofs=0, dst_ofs=0)]
        return flow_actions

        
    def add_graph_towards_new_switch(self, ipv4_src):
        answer_datapath = self.config.getProperty(self.CONTROLLER_IP, "datapath")


        flow_actions= None
        parser = answer_datapath.ofproto_parser
        self.logger.debug(" add_graph_towards_new_switch %s", ipv4_src)

        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP, ipv4_dst=ipv4_src)
        flow_actions = self.add_flow_actions_push_ethernet_header_to_stack(flow_actions=flow_actions, parser=parser)

        g = graphs.graph(self.config.DG(), self.CONTROLLER_IP, ipv4_src)
        # store graph in controller's switch
        self.config.DG().nodes[self.CONTROLLER_IP]["graphs_from_controller"][ipv4_src]=g
        # store graph in switch
        self.config.setProperty(ipv4_src, "graph_from_controller", g)

        if g == []:
            self.logger.debug("NO ROUTE TOWARDS %s", ipv4_src)
            return

        g = graphs.encoded_graph(self.config.DG(), g, 4)

        # alternate paths from last to first
        for i in reversed(range(1,len(g))):
            flow_actions = self.add_flow_actions_encap_nsh(flow_actions=flow_actions, parser=parser, nsh_c1=int(g[i][0:8],16), nsh_c2=int(g[i][8:16],16), nsh_c3=int(g[i][16:24],16), nsh_c4=int(g[i][24:32],16))        

        # main path in g[0] with ttl == leng(g)-1 == number of alternate paths
        flow_actions = self.add_flow_actions_encap_nsh(flow_actions=flow_actions, parser=parser, nsh_c1=int(g[0][0:8],16), nsh_c2=int(g[0][8:16],16), nsh_c3=int(g[0][16:24],16), nsh_c4=int(g[0][24:32],16),nsh_ttl=len(g)-1)

        flow_actions = self.add_flow_actions_pop_ethernet_header_from_stack(flow_actions, parser=parser)

        p= g[0][0:2]
        port_number=int(p[0], 16)
        if p[0]=='f':
            port_number=int(p[1], 16)
            
        self.logger.debug("------------------------------------------PORT NUMBER=%s",port_number) 

        flow_actions += [parser.NXActionRegLoad(ofs_nbits=nicira_ext.ofs_nbits(0,3), dst="reg2", value=port_number)]

        flow_actions += [parser.NXActionResubmitTable(table_id=self.ACTIVE_PORTS_TABLE)]

        flow_actions += [parser.NXActionResubmitTable(table_id=self.TABLE_ROUTING)]

        self.logger.debug("-----------          ADD GRAPH en switch=%s to IPnode=%s", answer_datapath.address[0], ipv4_src) 
        self.add_flow(answer_datapath, 40000, flow_match, flow_actions, table_id=self.INITIAL_TABLE)
        answer_datapath.send_barrier()




        
    def install_flows_as_managed_switch(self, datapath, ipv4_src, eth_addr):
        self.logger.debug(" install_flows_as_managed_switch at %s, eth_addr: %s", ipv4_src, eth_addr)
        
        flow_actions= None
        answer_datapath = datapath
        parser = answer_datapath.ofproto_parser
        
        
        # now install graph
        flow_actions= None
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP, ipv4_dst=self.CONTROLLER_IP)
        flow_actions = self.add_flow_actions_push_ethernet_header_to_stack(flow_actions=flow_actions, parser=parser)



        g = graphs.graph(self.config.DG(), ipv4_src, self.CONTROLLER_IP)
        self.config.setProperty(ipv4_src, "graph_to_controller", g)
        if g == []:
            self.logger.debug("NO ROUTE TOWARDS CONTROLLER FROM %s", ipv4_src)
            return
            
        g = graphs.encoded_graph(self.config.DG(), g, 4)
        # alternate paths from last to first
        for i in reversed(range(1,len(g))):
            flow_actions = self.add_flow_actions_encap_nsh(flow_actions=flow_actions, parser=parser, nsh_c1=int(g[i][0:8],16), nsh_c2=int(g[i][8:16],16), nsh_c3=int(g[i][16:24],16), nsh_c4=int(g[i][24:32],16))        

        # main path in g[0] with ttl == leng(g)-1 == number of alternate paths
        flow_actions = self.add_flow_actions_encap_nsh(flow_actions=flow_actions, parser=parser, nsh_c1=int(g[0][0:8],16), nsh_c2=int(g[0][8:16],16), nsh_c3=int(g[0][16:24],16), nsh_c4=int(g[0][24:32],16),nsh_ttl=len(g)-1)
        flow_actions = self.add_flow_actions_pop_ethernet_header_from_stack(flow_actions, parser=parser)
        flow_actions += [parser.NXActionResubmitTable(table_id=self.LEARNING_FLOWS_TABLE)]


        self.logger.debug("-----------          ADD GRAPH en switch=%s to CONTROLLER", ipv4_src) 
        self.add_flow(answer_datapath, 40000, flow_match, flow_actions,
                      hard_timeout=self.TIMEOUT_INSTALL_FLOWS_AS_MANAGED_SWITCH,
                      table_id=self.INITIAL_TABLE)
        answer_datapath.send_barrier()




        ###########################
        self.logger.debug("-----------          ADD FLOW: each SYN to CONTROLLER generate PACKET_IN ") 

        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP,
                                     ip_proto=inet.IPPROTO_TCP,
                                     ipv4_dst=self.CONTROLLER_IP,
                                     tcp_dst=self.TCP_CONTROLLER_PORT,
                                     tcp_flags=tcp.TCP_SYN)
 
        flow_actions = [parser.OFPActionOutput(answer_datapath.ofproto.OFPP_CONTROLLER)]
        self.add_flow(answer_datapath, 60000, flow_match, flow_actions,
                      hard_timeout=self.TIMEOUT_INSTALL_FLOWS_AS_MANAGED_SWITCH,
                      table_id=self.INITIAL_TABLE)
        answer_datapath.send_barrier()


        ###########################
        self.logger.debug("-----------          ADD FLOW: each ARP to CONTROLLER generate PACKET_IN ") 
        flow_match = parser.OFPMatch(eth_type=0x806,
                                     arp_tpa=self.CONTROLLER_IP)
 
        flow_actions = [parser.OFPActionOutput(answer_datapath.ofproto.OFPP_CONTROLLER)]
        self.add_flow(answer_datapath, 60000, flow_match, flow_actions,
                      hard_timeout=self.TIMEOUT_INSTALL_FLOWS_AS_MANAGED_SWITCH,
                      table_id=self.INITIAL_TABLE)
        answer_datapath.send_barrier()


        
        

    # TBD: Do we need to compose a new pkt? Can't inject pkt directly?
    def send_pkt_out_to_controller_tcpip(self, pkt):
        dpath = self.config.getProperty(self.CONTROLLER_IP, "datapath")
        ofproto = dpath.ofproto
        parser = dpath.ofproto_parser

        recv_eth = pkt.get_protocol(ethernet.ethernet)
        recv_ipv4 = pkt.get_protocol(ipv4.ipv4)
        recv_tcp = pkt.get_protocol(tcp.tcp)

        answer_pkt = packet.Packet()
        answer_pkt.add_protocol(recv_eth)
        answer_pkt.add_protocol(recv_ipv4)
        answer_pkt.add_protocol(recv_tcp)
        answer_pkt.serialize()
            
        #Cualquier packet IN de TCP SYN/ TCP ACK  envio pkt out al switch LOCAL
        answer_in_port = ofproto.OFPP_CONTROLLER
        answer_output_port = dpath.ofproto.OFPP_LOCAL

        actions = [parser.OFPActionOutput(port=answer_output_port)]

        answer_match = parser.OFPMatch(in_port=answer_in_port)
        self.logger.debug("----------- send_pkt_out_to_tcpip: in_port=%d", answer_in_port)
        out = parser.OFPPacketOut(datapath=dpath, buffer_id=ofproto.OFP_NO_BUFFER,
                                  in_port=answer_in_port, actions=actions, data=answer_pkt)                                  
#                                  match=answer_match, actions=actions, data=answer_pkt)
        self.logger.debug("----------- send_pkt_out_to_tcpip: send SYN to TCP/IP stack of Controller")
        dpath.send_msg(out)
 


    def port_down(self, node_ip, port):
        self.logger.info("port %s down in node %s", port, node_ip)

        if not self.config.DG().has_node(node_ip):
            return
        
        # find neighbor of node_ip through port port
        neighbor_ip = None
        neighbor_port = None        
        for n in self.config.DG().neighbors(node_ip):
            if "port" in self.config.DG()[node_ip][n] and self.config.DG()[node_ip][n]["port"] == port:
                neighbor_ip = n
                if self.config.DG().has_edge(neighbor_ip,node_ip) and "port" in self.config.DG()[neighbor_ip][node_ip].keys():
                    neighbor_port = self.config.DG()[neighbor_ip][node_ip]["port"]
                    break
                else:
                    break

        self.logger.info("    neighbor of %s through port %s is neighbor %s with port %s", node_ip, port, neighbor_ip, neighbor_port)        

        self.return_port_to_port_factory(node_ip, port)

        # Delete edges between node_ip and neighbor_ip
        if neighbor_ip != None:
            self.return_port_to_port_factory(node_ip, port)
            
            # Delete edge from node_ip to neighbor_ip 
            if self.config.DG().has_edge(node_ip, neighbor_ip):            
                self.config.DG().remove_edge(node_ip, neighbor_ip)

            
            # Delete edge from neighbor_ip to node_ip
            if self.config.DG().has_edge(neighbor_ip, node_ip):
                self.config.DG().remove_edge(neighbor_ip,node_ip)

        if neighbor_ip != None and neighbor_port != None:
            self.return_port_to_port_factory(neighbor_ip, neighbor_port)

            
        self.refresh_nodes()

        
        
    def deactivate_switch_as_managed(self, node_ip):
        # deactivated node
        self.logger.info("deactivate_switch %s", node_ip)

        neighbors = []
        for neighbor_ip in self.config.DG().neighbors(node_ip):
            # in neighbor, delete node as its anchor
            if "anchors" in self.config.DG().nodes[neighbor_ip].keys() and \
               node_ip in self.config.DG().nodes[neighbor_ip]["anchors"].keys():
                del self.config.DG().nodes[neighbor_ip]["anchors"][node_ip]
            neighbors.append(neighbor_ip)
        

        for neighbor_ip in neighbors:
            try:
                self.config.DG().remove_edge(node_ip, neighbor_ip)
            except nx.exception.NetworkXError:
                pass

            # delete port from neighbor to this node
            if "port" in self.config.DG()[neighbor_ip][node_ip].keys():
                port = self.config.DG()[neighbor_ip][node_ip]["port"]
                self.return_port_to_port_factory(neighbor_ip, port)

            try:
                self.config.DG().remove_edge(neighbor_ip,node_ip)
            except nx.exception.NetworkXError:
                pass

        # remove node from graph
        self.config.DG().remove_node(node_ip)

        # delete all ports of this node from factory
        self.delete_node_from_port_factory(node_ip)

        # in CONTROLLER's switch, delete flow towards this switch
        datapath = self.config.getProperty(self.CONTROLLER_IP, "datapath")
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP, ipv4_dst=node_ip)
        self.delete_flow(datapath, match)
        self.logger.info("Deleted flow in sc towards %s", node_ip)

        

    def activate_switch_as_managed(self, datapath, anchor_address, in_port):
        ##### add port and physical port to edge from new switch to anchor
        port          = self.port_factory(datapath.address[0])
        physical_port = in_port
        self.config.DG()[datapath.address[0]][anchor_address]["port"] = port
        self.config.DG()[datapath.address[0]][anchor_address]["physical_port"] = physical_port
        
        self.logger.debug("Added link from %s to %s", datapath.address[0], anchor_address)
        
        # Register that we have received the probe from this anchor for this datapath
        self.config.DG().nodes[datapath.address[0]]["anchors"][anchor_address]=True
                             
        # Register port to controller
        self.config.setProperty(datapath.address[0], "port_to_controller", in_port)

        self.add_flows_table_routing(ipv4_addr=datapath.address[0], port_no=port, physical_port=physical_port, eth_src=self.get_eth(datapath.address[0], in_port), eth_dst=self.config.DG()[anchor_address][datapath.address[0]]["eth_address"], table_routing = self.TABLE_ROUTING)
        
        self.install_flows_as_managed_switch(datapath, datapath.address[0], eth_addr = self.get_eth(datapath.address[0], in_port))

        
        self.config.setProperty(datapath.address[0], "hello_counter", 0)

        if not self.config.getProperty(datapath.address[0], "state") == "managed":
            self.config.setProperty(datapath.address[0], "state", "managed")

        self.logger.info("switch [%s] IS MANAGED", datapath.address[0])        


        p = self.config.DG()[datapath.address[0]][anchor_address]["port"]
        physical_port= self.config.DG()[datapath.address[0]][anchor_address]["physical_port"]
        eth_address= self.config.DG()[anchor_address][datapath.address[0]]["eth_address"]
        self.add_learning_flow_for_port (datapath.address[0], p, physical_port, eth_address, self.LEARNING_FLOWS_TABLE, self.ACTIVE_PORTS_TABLE, self.INITIAL_TABLE)        

                

        parser = datapath.ofproto_parser
        ofproto = datapath.ofproto
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP,
                                     ip_proto=inet.IPPROTO_TCP,
                                     ipv4_dst=self.CONTROLLER_IP,
                                     tcp_dst=self.TCP_CONTROLLER_PORT)
        
        #self.delete_flow(datapath, flow_match, table_id=self.ACTIVE_PORTS_TABLE)
        mod = parser.OFPFlowMod(datapath=datapath, match=flow_match, table_id=self.ACTIVE_PORTS_TABLE, out_port=ofproto.OFPP_ANY, out_group=ofproto.OFPG_ANY, command=ofproto.OFPFC_DELETE)
        datapath.send_msg(mod)

        datapath.send_barrier()
        

                

    def delete_flow(self, datapath, match, table_id=None):
        ofproto = datapath.ofproto
        if table_id == None:
            table_id=ofproto.OFPTT_ALL
            
        parser = datapath.ofproto_parser
        mod = parser.OFPFlowMod(datapath=datapath, match=match, cookie=self.COOKIE,cookie_mask=0xFFFFFFFFFFFFFFFF,table_id=table_id,out_port=ofproto.OFPP_ANY, out_group=ofproto.OFPG_ANY, command=ofproto.OFPFC_DELETE)
        datapath.send_msg(mod)

        datapath.send_barrier()
        

        
    def deactivate_port_flows_table_1(self,ipv4_addr, port_no):
        """
        Deletes flows for port_no in table 1 of ipv4_addr switch 
        """        
        self.logger.debug("-----------   deactivate_port_flows_table_1 to: %s, port: %s", ipv4_addr, port_no)
        
        datapath = self.config.getProperty(ipv4_addr, "datapath")
        parser = datapath.ofproto_parser
        
        # En tabla 1: 
        # Para puerto 1, los cuatro bits del camino =0x1 (no hay camino alternativo)
        dst_port=port_no<<28 
        mask=0xf0000000

        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_c1=(dst_port, mask))
        self.delete_flow(datapath, flow_match)

        # Para el puerto 0x9: puerto 1 con alternativo, y  ttl=1:
        # desencapsular)
        dst_port=(8+port_no)<<28
        mask=0xf0000000
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_c1=(dst_port, mask),
                                     nsh_ttl=1)
        self.delete_flow(datapath, flow_match)

        # Para el puerto 0x9: puerto 1 con alternativo y ttl>1: desencapsular, quitar
        # alternativo y reencapsular
        dst_port=(8+port_no)<<28
        mask=0xf0000000
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_c1=(dst_port, mask))
        self.delete_flow(datapath, flow_match)
        
        # Para el puerto 0xf1: puerto 1 es el último salto y no hay alternativo => nsh_ttl
        # debe ser 0
        dst_port=(0xf0+port_no)<<24
        mask=0xff<<24
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_c1=(dst_port, mask),
                                     nsh_ttl=0)
        self.delete_flow(datapath, flow_match)

        # Para el puerto 0xf9: puerto 1 es el último salto y hay alternativo => nsh_ttl debe ser 1
        dst_port=(0xf0+8+port_no)<<24
        mask=0xff<<24
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_c1=(dst_port, mask),
                                     nsh_ttl=1)
        self.delete_flow(datapath, flow_match)

        



        
    def add_flows_table_routing(self,ipv4_addr, port_no, physical_port, eth_src, eth_dst, table_routing):
        """ Sends flows to activate port_no in table 
        """
        self.logger.debug("-----------   add_flows_table_routing in table %s to: %s, port: %s, physical_port: %s, eth_src: %s, eth_dst: %s", table_routing, ipv4_addr, port_no, physical_port, eth_src, eth_dst) 

        datapath = self.config.getProperty(ipv4_addr, "datapath")

        parser = datapath.ofproto_parser

        
        # En tabla 1: 
        # Para puerto 1, los cuatro bits del camino =0x1 (no hay camino alternativo)
        output_port=port_no
        dst_port=port_no<<28 
        mask=0xf0000000
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_c1=(dst_port, mask))
                                     #reg3=output_port)                                     

        flow_actions =  [parser.OFPActionSetField(reg1=0xfff)]
        flow_actions += [parser.NXActionRegMove(src_field="reg1", dst_field="in_port", n_bits=16, src_ofs=0, dst_ofs=0)]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c1", dst_field="xxreg1", n_bits=28, src_ofs=0, dst_ofs=100)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c2", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=68)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c3", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=36)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c4", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=4)]
        flow_actions += [parser.NXActionRegLoad(ofs_nbits=nicira_ext.ofs_nbits(0,3), dst="xxreg1", value=0x0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c1", n_bits=32, src_ofs=96, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c2", n_bits=32, src_ofs=64, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c3", n_bits=32, src_ofs=32, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c4", n_bits=32, src_ofs=0, dst_ofs=0)]
        flow_actions += [parser.NXActionEncapEther()]
        flow_actions += [parser.OFPActionSetField(eth_dst=eth_dst)]
        flow_actions += [parser.OFPActionSetField(eth_src=eth_src)]                
        flow_actions += [parser.OFPActionOutput(physical_port)]

 
        self.logger.debug("--------------------------------------------------------------------------")
        self.add_flow(datapath, 57997, flow_match, flow_actions, table_id=table_routing)






        # Para el puerto 0x9: puerto 1 con alternativo, y  ttl=1:
        # desencapsular)
        output_port=port_no
        dst_port=(8+port_no)<<28
        mask=0xf0000000
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_c1=(dst_port, mask),
                                     nsh_ttl=1,
                                     reg3=output_port)
        
        flow_actions = [parser.OFPActionSetField(reg1=0xfff)]
        flow_actions += [parser.NXActionRegMove(src_field="reg1", dst_field="in_port", n_bits=16, src_ofs=0, dst_ofs=0)]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c1", dst_field="xxreg1", n_bits=28, src_ofs=0, dst_ofs=100)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c2", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=68)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c3", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=36)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c4", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=4)]
        flow_actions += [parser.NXActionRegLoad(ofs_nbits=nicira_ext.ofs_nbits(0,3), dst="xxreg1", value=0x0)]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionEncapNsh()]
        flow_actions += [parser.OFPActionSetField(nsh_spi=self.NSH_SPI)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c1", n_bits=32, src_ofs=96, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c2", n_bits=32, src_ofs=64, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c3", n_bits=32, src_ofs=32, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c4", n_bits=32, src_ofs=0, dst_ofs=0)]
        flow_actions += [parser.OFPActionSetField(nsh_ttl=0x0)]
        flow_actions += [parser.NXActionEncapEther()]
        flow_actions += [parser.OFPActionSetField(eth_dst=eth_dst)]        
        flow_actions += [parser.OFPActionSetField(eth_src=eth_src)]
        flow_actions += [parser.OFPActionOutput(physical_port)]


 
        self.logger.debug("--------------------------------------------------------------------------")
        self.add_flow(datapath, 57999, flow_match, flow_actions, table_id=table_routing)





        
       
        # Para el puerto 0x9: puerto 1 con alternativo y ttl>1: desencapsular, quitar
        # alternativo y reencapsular
        output_port=port_no
        dst_port=(8+port_no)<<28
        mask=0xf0000000
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_c1=(dst_port, mask),
                                     reg3=output_port)        

        flow_actions = [parser.OFPActionSetField(reg1=0xfff)]
        flow_actions += [parser.NXActionRegMove(src_field="reg1", dst_field="in_port", n_bits=16, src_ofs=0, dst_ofs=0)]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c1", dst_field="xxreg1", n_bits=28, src_ofs=0, dst_ofs=100)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c2", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=68)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c3", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=36)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c4", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=4)]
        flow_actions += [parser.NXActionRegLoad(ofs_nbits=nicira_ext.ofs_nbits(0,3), dst="xxreg1", value=0x0)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_ttl", dst_field="reg0", n_bits=8, src_ofs=0, dst_ofs=0)]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionEncapNsh()]
        flow_actions += [parser.OFPActionSetField(nsh_spi=self.NSH_SPI)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c1", n_bits=32, src_ofs=96, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c2", n_bits=32, src_ofs=64, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c3", n_bits=32, src_ofs=32, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c4", n_bits=32, src_ofs=0, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="reg0", dst_field="nsh_ttl", n_bits=8, src_ofs=0, dst_ofs=0)]
        flow_actions += [parser.NXActionDecNshTtl()]
        flow_actions += [parser.NXActionEncapEther()]
        flow_actions += [parser.OFPActionSetField(eth_dst=eth_dst)]        
        flow_actions += [parser.OFPActionSetField(eth_src=eth_src)]
        flow_actions += [parser.OFPActionOutput(physical_port)]

 
        self.logger.debug("--------------------------------------------------------------------------")
        self.add_flow(datapath, 57998, flow_match, flow_actions, table_id=table_routing)






        
        # Para el puerto 0xf1: puerto 1 es el último salto y no hay alternativo => nsh_ttl
        # debe ser 0
        output_port=port_no
        dst_port=(0xf0+port_no)<<24
        mask=0xff<<24
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_c1=(dst_port, mask),
                                     nsh_ttl=0)
                                     # reg3=output_port)

                                     

        flow_actions =  [parser.OFPActionSetField(reg1=0xfff)]
        flow_actions += [parser.NXActionRegMove(src_field="reg1", dst_field="in_port", n_bits=16, src_ofs=0, dst_ofs=0)]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionEncapEther()]
        flow_actions += [parser.OFPActionSetField(eth_dst=eth_dst)]        
        flow_actions += [parser.OFPActionSetField(eth_src=eth_src)]
        flow_actions += [parser.OFPActionOutput(physical_port)]

        self.logger.debug("--------------------------------------------------------------------------")
        self.add_flow(datapath, 57999, flow_match, flow_actions, table_id=table_routing)





        
        # Para el puerto 0xf9: puerto 1 es el último salto y hay alternativo => nsh_ttl debe ser 1
        output_port=port_no
        dst_port=(0xf0+8+port_no)<<24
        mask=0xff<<24
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_c1=(dst_port, mask),
                                     nsh_ttl=1,
                                     reg3=output_port)
                                     
        flow_actions =  [parser.OFPActionSetField(reg1=0xfff)]
        flow_actions += [parser.NXActionRegMove(src_field="reg1", dst_field="in_port", n_bits=16, src_ofs=0, dst_ofs=0)]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionEncapEther()]
        flow_actions += [parser.OFPActionSetField(eth_dst=eth_dst)]        
        flow_actions += [parser.OFPActionSetField(eth_src=eth_src)]
        flow_actions += [parser.OFPActionOutput(physical_port)]

        self.logger.debug("--------------------------------------------------------------------------")
        self.add_flow(datapath, 57998, flow_match, flow_actions, table_id=table_routing)



    def add_flows_when_no_port(self,ipv4_addr, table_routing, table_decap):
        """ Adds flows in table 1 for what to do when a port is not active in this table
        """
        self.logger.debug("-----------   add_flows_when_no_port to: %s", ipv4_addr) 

        datapath = self.config.getProperty(ipv4_addr, "datapath")
        parser = datapath.ofproto_parser

        #####################
        # NO HAY PUERTO 
        #####################
        # si no ha hecho match antes es porque su puerto no está activo
        # si TTL==0 no hay camino alternativo
        #ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=40000, dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_ttl=0,actions=drop"
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_ttl=0)
        flow_actions = []  #drop
        self.add_flow(datapath, 40000, flow_match, flow_actions, table_id=table_routing)



        
        # si TTL>0 y de los 4 bits del puerto el más significativo es 0, no hay camino alternativo para este salto, lo hay para otros
        # ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=39999, dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[31..31]=0,actions=drop"
        mask=0x80000000
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_c1=(0x0, mask))
        flow_actions = []  #drop
        self.add_flow(datapath, 39999, flow_match, flow_actions, table_id=table_routing)



        
        # si TTL>0 y estoy en último salto nsh_c1[28..31]=0xf -> el bit más significativo del puerto DEBE SER 1
        # hay camino alternativo para este salto, ponerlo como principal y quitar todos los demás alternativos, para lo cual hacemos resubmit a tabla 2,
        # guardando en reg0 el TTL.
        # ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=39999, dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0xf,nsh_c1[27..27]=1,actions=push:dl_src,push:dl_dst,decap(),move:nsh_ttl->reg0[0..7],decap(),encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,2)"

        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_c1=(0xf8000000,0xf8000000))


        flow_actions = [parser.NXActionStackPush(field="eth_src", start=0, end=48)]
        flow_actions += [parser.NXActionStackPush(field="eth_dst", start=0, end=48)]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_ttl", dst_field="reg0", n_bits=8, src_ofs=0, dst_ofs=0)]        
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionEncapEther()]
        flow_actions += [parser.NXActionStackPop(field="eth_dst", start=0, end=48)]
        flow_actions += [parser.NXActionStackPop(field="eth_src", start=0, end=48)]
        flow_actions += [parser.NXActionResubmitTable(table_id=table_decap)]

        self.add_flow(datapath, 39999, flow_match, flow_actions, table_id=table_routing)



        
        # si TTL>0 
        # hay camino alternativo para este salto, ponerlo como principal y quitar todos los demás alternativos, para lo cual hacemos resubmit a tabla 2,
        # guardando en reg0 el TTL.
        # ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=39998, dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[31..31]=1,actions=push:dl_src,push:dl_dst,decap(),move:nsh_ttl->reg0[0..7],decap(),encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,2)"
        
        
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     nsh_c1=(0x80000000,0x80000000))


        flow_actions = [parser.NXActionStackPush(field="eth_src", start=0, end=48)]
        flow_actions += [parser.NXActionStackPush(field="eth_dst", start=0, end=48)]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_ttl", dst_field="reg0", n_bits=8, src_ofs=0, dst_ofs=0)]        
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionEncapEther()]
        flow_actions += [parser.NXActionStackPop(field="eth_dst", start=0, end=48)]
        flow_actions += [parser.NXActionStackPop(field="eth_src", start=0, end=48)]
        flow_actions += [parser.NXActionResubmitTable(table_id=table_decap)]

        self.add_flow(datapath, 39998, flow_match, flow_actions, table_id=table_routing)


        
    def add_flows_decap (self, ipv4_addr, table_decap, learning_flows_table, second_table_decap):
        datapath = self.config.getProperty(ipv4_addr, "datapath")
        parser = datapath.ofproto_parser
        self.logger.debug(" add_flows_decap switch=%s", ipv4_addr)


        ##
        ## -------------------------------------------------------------------------------------
        ## tabla 2 comprobar si el alternativo es NULL y si no, almacenar alternativo en reg0
        ## -------------------------------------------------------------------------------------
        ##
        
        # Dejo el camino alternativo en xxreg1, ya tenía almacenado en reg0 el TTL y lo mando a la tabla 3 para que quite todos los nsh alternativos
        # ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME  "table=2,priority=60000,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,actions=push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..31]->xxreg1[0..31],move:nsh_c2[0..31]->xxreg1[32..63],move:nsh_c3[0..31]->xxreg1[64..95],move:nsh_c4[0..31]->xxreg1[96..127],encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,3)"
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI)


        flow_actions = [parser.NXActionStackPush(field="eth_src", start=0, end=48)]
        flow_actions += [parser.NXActionStackPush(field="eth_dst", start=0, end=48)]
        flow_actions += [parser.NXActionDecap()]
        
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c1", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=96)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c2", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=64)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c3", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=32)]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_c4", dst_field="xxreg1", n_bits=32, src_ofs=0, dst_ofs=0)]

        flow_actions += [parser.NXActionEncapEther()]
        flow_actions += [parser.NXActionStackPop(field="eth_dst", start=0, end=48)]
        flow_actions += [parser.NXActionStackPop(field="eth_src", start=0, end=48)]
        flow_actions += [parser.NXActionResubmitTable(table_id=second_table_decap)]

        self.add_flow(datapath, 60000, flow_match, flow_actions, table_id=table_decap)


        
        ## -------------------------------------------------------------------------------------
        ## tabla 3 de desencapsulamiento de paths alternativos
        ## -------------------------------------------------------------------------------------
        # si nsh_ttl = reg0 = 1 desencapsulo, quedando sin ningún nsh, así que recuperamos el alternativo de xxreg1 y le ponemos ttl=0. Finalmente, resubmit a tabla 1 para forward
        # ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME  "table=3,priority=60000, dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,reg0=1,actions=push:dl_src,push:dl_dst,decap(),decap(),encap(nsh(md_type=1)),load:0x1234->nsh_spi,load:0->nsh_ttl,move:xxreg1[0..31]->nsh_c1[0..31],move:xxreg1[32..63]->nsh_c2[0..31],move:xxreg1[64..95]->nsh_c3[0..31],move:xxreg1[96..127]->nsh_c4[0..31],encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,1)"

        
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI,
                                     reg0=(0x00000001,0xffffffff))


        flow_actions = [parser.NXActionStackPush(field="eth_src", start=0, end=48)]
        flow_actions += [parser.NXActionStackPush(field="eth_dst", start=0, end=48)]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionEncapNsh()]
        flow_actions += [parser.OFPActionSetField(nsh_spi=self.NSH_SPI)]
        flow_actions += [parser.OFPActionSetField(nsh_ttl=0x0)]        
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c1", n_bits=32, src_ofs=96, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c2", n_bits=32, src_ofs=64, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c3", n_bits=32, src_ofs=32, dst_ofs=0)]
        flow_actions += [parser.NXActionRegMove(src_field="xxreg1", dst_field="nsh_c4", n_bits=32, src_ofs=0, dst_ofs=0)]
        flow_actions += [parser.NXActionEncapEther()]
        flow_actions += [parser.NXActionStackPop(field="eth_dst", start=0, end=48)]
        flow_actions += [parser.NXActionStackPop(field="eth_src", start=0, end=48)]

        # reset all regs so it is correctly processed in learning_flows_table
        flow_actions += [parser.NXActionRegLoad(ofs_nbits=nicira_ext.ofs_nbits(0,31), dst="reg2", value=0)]
        flow_actions += [parser.NXActionRegLoad(ofs_nbits=nicira_ext.ofs_nbits(0,31), dst="reg3", value=0)]
        flow_actions += [parser.NXActionResubmitTable(table_id=learning_flows_table)]
        
        self.add_flow(datapath, 60000, flow_match, flow_actions, table_id=second_table_decap)

        
        # si nsh_ttl > 1 (si fuera 1, la regla anterior de mayor prioridad se habría cumplido nsh_ttl=1) recupero reg0->nsh_ttl, nsh_ttl_dec y lo vuelvo a dejar en reg0, decap y resubmit para seguir desencapsulando
        # ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME  "table=3, priority=59999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,actions=push:dl_src,push:dl_dst,decap(),move:reg0[0..7]->nsh_ttl,dec_nsh_ttl,move:nsh_ttl->reg0[0..7],decap(),encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,3)"

        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI)


        flow_actions = [parser.NXActionStackPush(field="eth_src", start=0, end=48)]
        flow_actions += [parser.NXActionStackPush(field="eth_dst", start=0, end=48)]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionRegMove(src_field="reg0", dst_field="nsh_ttl", n_bits=8, src_ofs=0, dst_ofs=0)]
        flow_actions += [parser.NXActionDecNshTtl()]
        flow_actions += [parser.NXActionRegMove(src_field="nsh_ttl", dst_field="reg0", n_bits=8, src_ofs=0, dst_ofs=0)]
        flow_actions += [parser.NXActionDecap()]
        flow_actions += [parser.NXActionEncapEther()]
        flow_actions += [parser.NXActionStackPop(field="eth_dst", start=0, end=48)]
        flow_actions += [parser.NXActionStackPop(field="eth_src", start=0, end=48)]
        flow_actions += [parser.NXActionResubmitTable(table_id=second_table_decap)]
        self.add_flow(datapath, 59999, flow_match, flow_actions, table_id=second_table_decap)


        
        datapath.send_barrier()
        

    def add_flow_initial_table (self, ipv4_addr, initial_table, table_routing):
        datapath = self.config.getProperty(ipv4_addr, "datapath")
        parser = datapath.ofproto_parser
        self.logger.debug(" add_flow_for_graph_routing switch=%s", ipv4_addr)

        if self.config.use_bfd():
            # añado flujo en tabla 2 para pasar por el grupo
            # En tabla: Paquetes recibidos con grafo -> pasar por grupo si hay alternativo 
            for e in self.config.DG().edges(datapath.address[0]):
                p = self.config.DG().get_edge_data(*e)["port"]
                if p != datapath.ofproto.OFPP_LOCAL:
                    flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                                 nsh_mdtype=self.NSH_MD_TYPE, 
                                                 nsh_spi=self.NSH_SPI, 
                                                 reg2=8+p)
                    flow_actions = [parser.OFPActionGroup(group_id=10+p)]
                    flow_actions += [parser.NXActionStackPop(field="reg3", start=0, end=32)]
                    flow_actions += [parser.NXActionResubmitTable(table_id=table_routing)]
                    self.add_flow(datapath, 41100, flow_match, flow_actions, table_id=initial_table)
                    datapath.send_barrier()


        # En tabla: Paquetes recibidos con grafo -> resubmit tabla=table_routing
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, nsh_mdtype=self.NSH_MD_TYPE, nsh_spi=self.NSH_SPI)
        flow_actions = [parser.NXActionResubmitTable(table_id=table_routing)]
        self.add_flow(datapath, 41000, flow_match, flow_actions, table_id=initial_table)
        datapath.send_barrier()


    def add_flows_to_learning_flows_table_A(self, ipv4_addr, learning_flows_table, active_ports_table, initial_table):
        datapath = self.config.getProperty(ipv4_addr, "datapath")
        parser = datapath.ofproto_parser
        self.logger.debug(" add_flows_to_learning_flows_table_A for switch %s", ipv4_addr)

        ###
        ### When packet is generated by the switch (LOCAL), and it has
        ### already its NSH headers, resubmit to tables active_ports_table and initial_table, withouth passing through table 
        ###
        parser = datapath.ofproto_parser

        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI, 
                                     in_port=datapath.ofproto.OFPP_LOCAL)


        flow_actions = []
        flow_actions += [parser.NXActionResubmitTable(table_id=active_ports_table)]

        flow_actions += [parser.NXActionResubmitTable(table_id=initial_table)]
        self.add_flow(datapath, 40000, flow_match, flow_actions, table_id=learning_flows_table)




        ###
        ### add flow for nsh traffic, store its port "fn"  in reg2
        ###
        flow_match = parser.OFPMatch(reg2=0,
                                     eth_type=ether_types.ETH_TYPE_NSH, 
                                     nsh_mdtype=self.NSH_MD_TYPE, 
                                     nsh_spi=self.NSH_SPI, 
                                     nsh_c1=(0xf0000000, 0xf0000000))
        # load port into reg2
        flow_actions = [parser.NXActionRegMove(src_field="nsh_c1", dst_field="reg2", n_bits=4, src_ofs=24, dst_ofs=0)]
        flow_actions += [parser.NXActionResubmitTable(table_id=learning_flows_table)]
        
        self.add_flow(datapath, 65535, flow_match, flow_actions, table_id=learning_flows_table)
        datapath.send_barrier()

        ###
        ### add flow for nsh traffic, store its port "n"  in reg2
        ###
        flow_match = parser.OFPMatch(reg2=0,eth_type=ether_types.ETH_TYPE_NSH, nsh_mdtype=self.NSH_MD_TYPE, nsh_spi=self.NSH_SPI)
        # load port into reg2
        flow_actions = [parser.NXActionRegMove(src_field="nsh_c1", dst_field="reg2", n_bits=4, src_ofs=28, dst_ofs=0)]
        flow_actions += [parser.NXActionResubmitTable(table_id=learning_flows_table)]
        
        self.add_flow(datapath, 65534, flow_match, flow_actions, table_id=learning_flows_table)


        ###
        ### all traffic resubmitted to ports_w_backup_table, then active_ports_table and then to initial_table
        ###
        flow_match = parser.OFPMatch()
        flow_actions += [parser.NXActionResubmitTable(table_id=self.ACTIVE_PORTS_TABLE)]
        flow_actions += [parser.NXActionResubmitTable(table_id=self.INITIAL_TABLE)]

        self.add_flow(datapath, 65532, flow_match, flow_actions, table_id=learning_flows_table)

        datapath.send_barrier()




    def add_learning_flow_for_port (self, ipv4_addr, port, physical_port, eth_address, learning_flows_table, active_ports_table, initial_table):
        """if traffic enters through port, add flow with learn action so that
        it is declared as active in active_ports_table

        """
#        datapath = self.config.DG().nodes[ipv4_addr]["datapath"]
        datapath = self.config.getProperty(ipv4_addr, "datapath")
        parser = datapath.ofproto_parser
        self.logger.debug(" add_learning_flow_for_port for switch %s, port %s", ipv4_addr, port)
        

        ## delete static flow for this port
        # match = parser.OFPMatch(reg2=(port,0xffff))
        # self.delete_flow(datapath, match)

        
        ## add ephemeral flow to active_ports_table through a learn action to
        ## activate there output port from in_port
        ## no match: any traffic on in_port activates such port as output
        alt_port=8+port
        mask=0x0000000f

        flow_match = parser.OFPMatch(eth_src_nxm=eth_address)
        flow_actions = []
        if self.config.use_bfd():
            flow_actions+=[
                parser.NXActionLearn(table_id=active_ports_table,
                                     specs=[
                                         parser.NXFlowSpecMatch(src=0x800,
                                                                dst=('eth_type_nxm', 0),
                                                                n_bits=16),
                                         parser.NXFlowSpecMatch(src=17,
                                                                dst=('ip_proto_nxm', 0),
                                                                n_bits=8),
                                         parser.NXFlowSpecMatch(src=3784,
                                                                dst=('udp_dst_nxm', 0),
                                                                n_bits=16),
                                         parser.NXFlowSpecMatch(src=alt_port,
                                                                dst=("reg2",0),
                                                                n_bits=32),
                                         parser.NXFlowSpecLoad(src=port,
                                                               dst=("reg3", 0),
                                                               n_bits=32)
                                     ],
                                     hard_timeout=self.LEARNT_ACTION_TIMEOUT,
                                     priority=40000,
                                     cookie=self.COOKIE,
                                     flags=datapath.ofproto.OFPFF_SEND_FLOW_REM)
            ]
        else:
            flow_actions+=[
                parser.NXActionLearn(table_id=active_ports_table,
                                     specs=[
                                         parser.NXFlowSpecMatch(src=alt_port,
                                                                dst=("reg2",0),
                                                                n_bits=32),
                                         parser.NXFlowSpecLoad(src=port,
                                                               dst=("reg3", 0),
                                                               n_bits=32)
                                     ],
                                     hard_timeout=self.LEARNT_ACTION_TIMEOUT,
                                     priority=40000,
                                     cookie=self.COOKIE,
                                     flags=datapath.ofproto.OFPFF_SEND_FLOW_REM)
            ]


        flow_actions += [parser.NXActionResubmitTable(table_id=self.ACTIVE_PORTS_TABLE)]
        flow_actions += [parser.NXActionResubmitTable(table_id=self.INITIAL_TABLE)]
        self.add_flow(datapath, 65533, flow_match, flow_actions, table_id=learning_flows_table)

        

        
    def add_flows_for_forwarding_graph(self, ipv4_addr):
        self.logger.debug("add_flows_for_forwarding_graph %s", ipv4_addr)



        self.add_flows_to_learning_flows_table_A(ipv4_addr,
                                                 learning_flows_table = self.LEARNING_FLOWS_TABLE,
                                                 active_ports_table = self.ACTIVE_PORTS_TABLE,
                                                 initial_table = self.INITIAL_TABLE)

        self.add_flows_when_no_port(ipv4_addr,
                                    table_routing=self.TABLE_ROUTING,
                                    table_decap=self.TABLE_DECAP)

        self.add_flows_decap(ipv4_addr,
                             table_decap=self.TABLE_DECAP,
                             learning_flows_table=self.LEARNING_FLOWS_TABLE,
                             second_table_decap=self.SECOND_TABLE_DECAP)


        if ipv4_addr != self.CONTROLLER_IP and self.config.use_bfd():
            self.add_group_for_port (datapath)
        
        self.add_flow_initial_table(ipv4_addr,
                                    initial_table=self.INITIAL_TABLE,
                                    table_routing=self.TABLE_ROUTING)



    def add_group_for_port(self, datapath):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # Create group, its action is check port number resubmit a new table (11)
        for e in self.config.DG().edges(datapath.address[0]):
            p = self.config.DG().get_edge_data(*e)["port"]
            if p != datapath.ofproto.OFPP_LOCAL:
                buckets = []
                bucket_action = [parser.NXActionResubmitTable(table_id=11)]
                buckets.append(parser.OFPBucket(watch_port=p,actions=bucket_action))
                req = parser.OFPGroupMod(datapath, ofproto.OFPGC_ADD, ofproto.OFPGT_SELECT, 10+p, buckets)
                datapath.send_msg(req)

                # Create a new flow in table 11 to restore reg3 which is stored on the stack
                flow_match = parser.OFPMatch(reg2=8+p)
                flow_actions = [parser.NXActionRegLoad(ofs_nbits=nicira_ext.ofs_nbits(0,3), dst="reg3", value=p)]
                flow_actions += [parser.NXActionStackPush(field="reg3", start=0, end=32)]

                self.logger.debug("--------------------------------------------------------------------------")
                self.add_flow(datapath, 25000, flow_match, flow_actions, table_id=11)
        
