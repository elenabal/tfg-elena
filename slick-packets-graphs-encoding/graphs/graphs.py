""" Graph library
"""

import networkx as nx
import matplotlib.pyplot as plt 
from bitarray import bitarray




def attributes(DG, path, edge_attribute):
    """
    :param DG:             A directed graph
    :param path:           List of nodes in DG
    :param edge_attribute: The name of an attribute of edges in DG
    """
    attributes = [-1]*(len(path) - 1)
    for i in range(0, len(path) - 1):
        attributes[i]=DG[path[i]][path[i+1]][edge_attribute]
    return attributes
        
    
def alt_path(DG, dp):
    """
    :param DG: A directed graph
    :param dp: A path (list of nodes of DG)
    :return:   Alternate path to dp after removing first edge dp[0]-dp[1]
    """
    edges = list(DG.edges())
    edges.remove((dp[0],dp[1]))
    dg = DG.edge_subgraph(edges)
    return list(nx.dijkstra_path(dg, dp[0], dp[-1]))


def graph (DG, src, dst):
    """
    :param DG: A directed graph
    :param src: node in DG
    :param dst: node in DG
    :return:    A list of paths. First element is primary_path from node src to node dst
    The rest are alternate paths for each node in primary_path[0] .. primary_path[-1]
    A path is a list of nodes in DG
    """
    primary_path = list(nx.dijkstra_path(DG, src, dst))
    paths=[[]] * len(primary_path)

    paths[0] = primary_path
    for i in range(0, len(primary_path)-1):
        paths[i+1] = alt_path(DG, primary_path[i:])

    return paths


def toBitarray(i,nbits):
    """
    :param i: An integer
    :param nbits: Number of bits to be used in the binary representation of i
    :return: A bitarray with i converted to bin using nbits bits left-padded with 0s 
    """
    return bitarray(format(i,"0"+str(nbits)+"b"))

# def toInt(b):
#     return int(b.to01(),2)


def encode_path(p, nbits):
    """
    :param p:     A list of ints
    :param nbits: Number of bits to represent each int in p
    :return:      A bitarray with each int in p converted bot bin using nbits bits for each
    """
    ba = (len(p) * nbits) * bitarray('0')

    start = 0
    for i in range(0, len(p)):
        pb = toBitarray(p[i], nbits)
        ba[start:start+nbits] = pb
        start += nbits
        
    return ba


def encode_graph(DG, g, nbits, nbitslength):
    """
    :param DG: A directed graph
    :param g:  A list of paths as returned by graph(). First element is primary_path from 
               node src to node dst. The rest are alternate paths for each node in 
               primary_path[0] .. primary_path[-1]
               A path is a list of nodes in DG
    :param nbits: size of each port
    :param nbitslength: size of the length field
    :return: A bitarray with 
             [port_in_primary_path|n_edges_of_alternate_path|encoded_ports_alternate_path]*
             field port_in_primary_path has size nbits
             field n_edges_of_alternate_path has size nbitslength
             encoded_ports_alternate_path has size nbits * number of edges in alternate_path
    """
    portsPrimaryPath = attributes(DG, g[0], "port")

    size = len(portsPrimaryPath) * (nbits + nbitslength) 

    # generate list of encoded alternate paths to calculate sizes
    encoded_alt_paths = []
    for i in range(1,len(g)):
        ep = encode_path(attributes(DG, g[i], "port"), nbits)
        encoded_alt_paths.append(ep)
        size += len(ep)
    
    ba = size * bitarray('0')

    i = 0
    # for each port in primary_path
    for j in range(len(portsPrimaryPath)):
        # encode port in primary path
        ba[i:i+nbits] = toBitarray(portsPrimaryPath[j], nbits) 
        i += nbits

        # encode number of edges of alternate path
        nedges = len (g[j+1]) - 1
        ba[i:i+nbitslength] = toBitarray(nedges, nbitslength)
        i += nbitslength

        # encode ports on alternate path
        ba[i:i+len(encoded_alt_paths[j])] = encoded_alt_paths[j]
        i += len(encoded_alt_paths[j])

    return ba

    
def draw(DG):
    """https://stackoverflow.com/questions/22785849/drawing-multiple-edges-between-two-nodes-with-networkx
    """
    pos = nx.spring_layout(DG)
    nx.draw(DG,pos,with_labels=True, node_size=4000, node_shape='h', arrowstyle='-')
    edge_labels = nx.get_edge_attributes(DG, 'port')
    nx.draw_networkx_edge_labels(DG, pos, edge_labels, label_pos=0.7)
    plt.draw()
    plt.show()

    
def main():
    node_r1 = "R1"
    node_r2 = "R2"
    node_r3 = "R3"
    node_r4 = "R4"
    node_r5 = "R5"

    DG = nx.DiGraph()

    DG.add_edges_from([(node_r1, node_r2)], weight=1, port=0)
    DG.add_edges_from([(node_r1, node_r3)], weight=1, port=1)
    DG.add_edges_from([(node_r1, node_r4)], weight=1, port=2)

    DG.add_edges_from([(node_r2, node_r1)], weight=1, port=0)
    DG.add_edges_from([(node_r2, node_r5)], weight=1, port=1)
    DG.add_edges_from([(node_r2, node_r4)], weight=1, port=2)
    DG.add_edges_from([(node_r2, node_r3)], weight=1, port=3)

    DG.add_edges_from([(node_r3, node_r1)], weight=1, port=0)
    DG.add_edges_from([(node_r3, node_r2)], weight=1, port=1)
    DG.add_edges_from([(node_r3, node_r4)], weight=1, port=2)

    DG.add_edges_from([(node_r4, node_r2)], weight=1, port=0)
    DG.add_edges_from([(node_r4, node_r3)], weight=1, port=1)
    DG.add_edges_from([(node_r4, node_r1)], weight=1, port=2)
    DG.add_edges_from([(node_r4, node_r5)], weight=1, port=3)

    DG.add_edges_from([(node_r5, node_r2)], weight=1, port=0)
    DG.add_edges_from([(node_r5, node_r4)], weight=1, port=1)

    draw(DG)


    # edges
    print("DG edges:", list(DG.edges))

    print("** Paths ", node_r1, "->", node_r5)
    g = graph(DG, node_r1, node_r5)
    print(g)
    # get ports list for each path in graph g
    for p in g:
        print(attributes(DG, p, "port"))

    print("** Graph ", node_r5, "->", node_r1)
    g = graph(DG, node_r5, node_r1)
    print(g)
    # get ports list for each path in graph g
    for p in g:
        print(attributes(DG, p, "port"))
    
    print("** Graph ", node_r3, "->", node_r4)
    g = graph(DG, node_r3, node_r4)
    print(g)
    # get ports list for each path in graph g
    for p in g:
        print(attributes(DG, p, "port"))
    
    print("** Graph ", node_r5, "->", node_r3)
    g = graph(DG, node_r5, node_r3)
    print(g)
    # get ports list for each path in graph g
    for p in g:
        print(attributes(DG, p, "port"))
    




if __name__ == "__main__":
    main()
