# Maqueta de bootstrap con controlador sin grafos

Topología definida en el fichero escenario.py

 10.0.0.1      10.0.0.101      10.0.0.102         10.0.0.103       10.0.0.2
    s0 ----------- s1 -------------- s2 -------------- s3 ------------ h2
                    \
                     \
                      \
                       --------------s4 ------------- h3
                               10.0.0.104          10.0.0.3


Los switches se arrancan en modo secure, out-of-band i
Nosotros queremos un plano de control in-band, pero en ovs out-of-band significa sin entradas en tablas de flujos. El modo in-band de ovs implica que mete entradas de las tablas para poder alcanzar al controlador. Nosotros metemos las nuetras por lo que queremos el modo out-of-band

## Modo de arranque

- En xterm de s0  arrancar controlador:
   sudo ryu-manager --verbse ./miControlador.py 

- En xterm de s0:
   s0:~> ./init-s0.sh; ./reglas-s0.sh

- En xterm de s1:
   s1:~> ./init-s1.sh; ./reglas-s1.sh

- En xterm de s2:
   s2:~> ./init-s2.sh; ./reglas-s2.sh

- En xterm de s3:
   s3:~> ./init-s3.sh; ./reglas-s3.sh

- En xterm de s4:
   s4:~> ./init-s4.sh; ./reglas-s4.sh


## Para parar lo switches, desde el terminal del switch que queremos parar, por ejemplo s1:
   s1:~> ./down-s1.sh

## Consultar estado

Para consultar la información de un switch, por ejemplo s1, ejecutamos en su terminal:
   s1:~> /dumpAllFlows.sh s1

