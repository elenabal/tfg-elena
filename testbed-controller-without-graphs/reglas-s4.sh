ovs-ofctl add-flow s4 "table=0,in_port=LOCAL,ipv4,nw_dst=10.0.0.1,tcp,tp_dst=6633,actions=ALL"
ovs-ofctl add-flow s4 "table=0,ipv4,nw_src=10.0.0.1,nw_dst=10.0.0.104,tcp,tp_src=6633,actions=LOCAL"


#ARP
ovs-ofctl add-flow s4 "table=0,in_port=LOCAL,arp,dl_dst=ff:ff:ff:ff:ff:ff,actions=output:ALL"
ovs-ofctl add-flow s4 "table=0,arp,arp_tpa=10.0.0.104,actions=LOCAL"

#Cuando envíe mensajes IPv6mcast_node_04 lo tira
ovs-ofctl add-flow s4 "table=0,dl_dst=33:33:00:00:00:04,actions=drop"
ovs-ofctl add-flow s4 "table=0,dl_dst=33:33:00:00:00:16,actions=drop"
