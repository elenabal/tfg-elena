# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ipv4
from ryu.lib.packet import tcp
from ryu.lib.packet import ether_types
from ryu.lib.packet import arp
from ryu.ofproto import ether
from ryu.ofproto import inet
from ryu.lib.packet import udp

from ryu.services.protocols.vrrp import api as vrrp_api
from ryu.services.protocols.vrrp import event as vrrp_event
from ryu.services.protocols.vrrp import utils

import os


# Info for every managed switch
class SwitchInfo:
    port_to_controller = ""
    anchor_datapath = "NONE"
    datapath = ""
    anchor_output_port = "NONE"
    address = ""

class InBandController(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION] # 1.5?

    # our ip addr. TBD: ask for it, don't hardwire it
    CONTROLLER_IP = '10.0.0.1'
    TCP_CONTROLLER_PORT = 6633

    # used to reply to ARP requests of non managed switches
    CONTROLLER_MAC='00:00:00:00:00:01' 
    
    SRC_PORT_PROBE = 11111
    DST_PORT_PROBE = 22222

    # anchor_and_port_by_IPnode
    # {ipNode, [anchor_datapath, out_port_to_IPnode]}
    anchor_and_port_by_IPnode = {}

    # out_port_to_controller_by_IPnode
    # {ipNode, out_port_to_controller}
    out_port_to_controller_by_IPnode = {}

    # switch info by IP {IP: SwitchInfo}
    switch_info_by_IP = {}

    def __init__(self, *args, **kwargs):
        super(InBandController, self).__init__(*args, **kwargs)


    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)


        self.logger.info("-----------Datapath address:: add %s", ev.msg.datapath.address[0])
        if (ev.msg.datapath.address[0] == self.CONTROLLER_IP):
            # This is the switch of the controller. We add it to switch_info here. The rest
            # of switches are added when their anchor sends us a PacketIn with their SYN
            switch_info = SwitchInfo()
            
            switch_info.port_to_controller = ofproto.OFPP_LOCAL
            switch_info.datapath = ev.msg.datapath
            switch_info.address = self.CONTROLLER_IP
            switch_info.anchor_datapath = "NONE"
            self.switch_info_by_IP[self.CONTROLLER_IP] = switch_info
            self.logger.info("-----------SWITCH [%s]:: add anchor=%s out_port=%d TO CONTROLLER", 
                            self.CONTROLLER_IP, switch_info.anchor_datapath, ofproto.OFPP_LOCAL)

        else:
            # This is a new switch, distint from the switch of the controller:
            # send probe to know its port to controller
            self.send_probe(datapath)
            
            
    def send_probe(self, datapath):
        dst = datapath.address[0]
        answer_pkt = packet.Packet()

        # we don't care about eth addresses: we use broadcast
        # addr. Could use the src addr of the datapath
        answer_e = ethernet.ethernet(self.CONTROLLER_MAC, 'ff:ff:ff:ff:ff:ff', ether.ETH_TYPE_IP)
        answer_i = ipv4.ipv4(proto=inet.IPPROTO_UDP, src=self.CONTROLLER_IP, dst=dst)
        answer_u = udp.udp(src_port=self.SRC_PORT_PROBE, dst_port=self.DST_PORT_PROBE)

        answer_pkt.add_protocol(answer_e)
        answer_pkt.add_protocol(answer_i)
        answer_pkt.add_protocol(answer_u)
        answer_pkt.serialize()

        anchor_dpath = self.switch_info_by_IP[dst].anchor_datapath
        answer_out_port = self.switch_info_by_IP[dst].anchor_output_port

        answer_parser= anchor_dpath.ofproto_parser
        answer_ofproto = anchor_dpath.ofproto
        answer_in_port = answer_ofproto.OFPP_CONTROLLER
        actions = [answer_parser.OFPActionOutput(port=answer_out_port)]
        self.logger.info("----------- SENDING PROBE anchor=%s outport=%d dst=%s", 
                         anchor_dpath.address[0], answer_out_port, dst)
        out = answer_parser.OFPPacketOut(datapath=anchor_dpath, buffer_id=answer_ofproto.OFP_NO_BUFFER,
                                         in_port=answer_in_port, actions=actions, data=answer_pkt)
        anchor_dpath.send_msg(out)

        
    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

        
    def bcastARP(self, dpath, in_port, eth, arp_req):
        self.logger.info("-----------bcastARP:: datapath %s received ARP in in_port=%s", dpath, in_port)
        ofproto = dpath.ofproto
        parser = dpath.ofproto_parser

        answer_dst_mac = eth.src
        answer_src_mac = self.CONTROLLER_MAC
        answer_ether_proto = ether.ETH_TYPE_ARP

        answer_hwtype = 1
        answer_arp_proto = ether.ETH_TYPE_IP
        answer_hlen = 6
        answer_plen = 4
        answer_dst_mac = arp_req.src_mac
        answer_dst_ip = arp_req.src_ip
        answer_src_ip = arp_req.dst_ip
        answer_output_port = in_port
        answer_in_port = ofproto.OFPP_CONTROLLER
        answer_arp_code = arp.ARP_REPLY

        answer_pkt = packet.Packet()

        answer_e = ethernet.ethernet(answer_dst_mac, answer_src_mac, answer_ether_proto)
        answer_a = arp.arp(answer_hwtype, answer_arp_proto, answer_hlen, answer_plen, answer_arp_code,
                           answer_src_mac, answer_src_ip, answer_dst_mac, answer_dst_ip)

        answer_pkt.add_protocol(answer_e)
        answer_pkt.add_protocol(answer_a)

        answer_pkt.serialize()
            
        answer_in_port = ofproto.OFPP_CONTROLLER
        actions = [parser.OFPActionOutput(port=answer_output_port)]
        self.logger.info("-----------ARP_HANDLER:: build packet anchor=%s", dpath.address[0])
        out = parser.OFPPacketOut(datapath=dpath, buffer_id=ofproto.OFP_NO_BUFFER,
                                  in_port=answer_in_port, actions=actions, data=answer_pkt)
        self.logger.info("-----------ARP_HANDLER:: send packet")
        dpath.send_msg(out)

        # añade en la caché de ARP para que el gratuitous ARP que haga el controlador no se envíe
        os.system('arp -s ' + answer_dst_ip + ' ' + answer_dst_mac)
 
 
    def syn_ack_pkt_out(self, dpath, output_port, pkt):
        self.logger.info("-----------TCP SYN_ACK:: datapath %s received TCP SYN ACK", dpath)
        ofproto = dpath.ofproto

        recv_eth = pkt.get_protocol(ethernet.ethernet)
        recv_ipv4 = pkt.get_protocol(ipv4.ipv4)
        recv_tcp = pkt.get_protocol(tcp.tcp)

        answer_pkt = packet.Packet()
        answer_pkt.add_protocol(recv_eth)
        answer_pkt.add_protocol(recv_ipv4)
        answer_pkt.add_protocol(recv_tcp)
        answer_pkt.serialize()
            
        # get anchor datapath for nodeIP
        answer_in_port = ofproto.OFPP_CONTROLLER
        answer_datapath = self.switch_info_by_IP[recv_ipv4.dst].anchor_datapath
        answer_output_port = self.switch_info_by_IP[recv_ipv4.dst].anchor_output_port
        answer_parser = answer_datapath.ofproto_parser
        actions = [answer_parser.OFPActionOutput(port=answer_output_port)]

        self.logger.info("-----------TCP_SYN_ACK:: build packet from %s to %s anchor=%s", 
                         recv_ipv4.src, recv_ipv4.dst, answer_datapath.address[0])
        out = answer_parser.OFPPacketOut(datapath=answer_datapath, buffer_id=ofproto.OFP_NO_BUFFER,
                                         in_port=answer_in_port, actions=actions, data=answer_pkt)
        self.logger.info("-----------TCP_SYN_ACK:: send packet")
        answer_datapath.send_msg(out)
 

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        pkt_eth = pkt.get_protocol(ethernet.ethernet)

        if pkt_eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return

        self.logger.info("----- NEW PACKET_IN:: eth_src=%s eth_dst=%s in_port=%s", 
                         pkt_eth.src, pkt_eth.dst, in_port)

        pkt_arp = pkt.get_protocol(arp.arp) 
        # ARP
        if pkt_arp and pkt_arp.opcode == arp.ARP_REQUEST and pkt_arp.dst_ip == self.CONTROLLER_IP:
            self.logger.info("   ----- PACKET_IN:: ARP REQUEST")
            self.bcastARP(dpath=datapath, in_port=in_port, eth=pkt_eth, arp_req=pkt_arp)
            return

        pkt_ipv4 = pkt.get_protocol(ipv4.ipv4)
        pkt_tcp = pkt.get_protocol(tcp.tcp)
        pkt_udp = pkt.get_protocol(udp.udp)

        # TCP SYN from switch
        if pkt_ipv4 and pkt_ipv4.dst == self.CONTROLLER_IP:
            if pkt_tcp:
                if pkt_tcp.dst_port == self.TCP_CONTROLLER_PORT and pkt_tcp.has_flags(tcp.TCP_SYN):
                    self.logger.info("   ----- PACKET_IN:: SYN       from=%s", pkt_ipv4.src)
                    self.rcv_syn(anchor_datapath=datapath, ip_src=pkt_ipv4.src, ip_dst=pkt_ipv4.dst, in_port=in_port, pkt=pkt)
                    return

        if pkt_ipv4 and pkt_ipv4.src == self.CONTROLLER_IP:
            # TCP SYN_ACK to switch
            if pkt_tcp and pkt_tcp.src_port == self.TCP_CONTROLLER_PORT and pkt_tcp.has_flags(tcp.TCP_SYN, tcp.TCP_ACK):
                self.logger.info("   ----- PACKET_IN:: SYN_ACK     to=%s", pkt_ipv4.dst)
                self.syn_ack_rcv(datapath=datapath, ip_src=pkt_ipv4.src, ip_dst=pkt_ipv4.dst, in_port=in_port, pkt=pkt)
                return

            # UDP PROBE from switch
            if pkt_udp and pkt_udp.src_port == self.SRC_PORT_PROBE and pkt_udp.dst_port == self.DST_PORT_PROBE:
                self.logger.info("   ----- PACKET_IN:: PROBE")
                self.udp_probe_rcvd(datapath=datapath, ip_src=pkt_ipv4.src, ip_dst=pkt_ipv4.dst, in_port=in_port)
                return



    def rcv_syn(self, anchor_datapath, ip_src, ip_dst, in_port, pkt):
        switch_info = None

        if ip_src not in self.switch_info_by_IP.keys():
        # guardo información del nuevo switch y de su switch anchor
            switch_info = SwitchInfo()
            switch_info.address = ip_src
            switch_info.anchor_datapath = anchor_datapath
            switch_info.anchor_output_port = in_port
            
            self.switch_info_by_IP[ip_src] = switch_info
            self.logger.info("          -----------SWITCH[%s]:: add anchor=%s out_port=%d to IPnode=%s", 
                             ip_src, anchor_datapath.address[0], in_port, ip_src)
        else:
            switch_info = self.switch_info_by_IP[ip_src]


        self.add_flows_to_reach_new_node(datapath=anchor_datapath, ipv4_src=ip_dst, ipv4_dst=ip_src, switch_info=switch_info)

        self.add_flows_to_reach_controller_from_new_node(datapath=anchor_datapath, 
                                                         ipv4_src=ip_src, ipv4_dst=ip_dst, switch_info=switch_info)

        self.send_syn_pkt_out(pkt=pkt)


    def add_flows_to_reach_new_node(self, datapath, ipv4_src, ipv4_dst, switch_info):
        # default prio=32768
        # Los siguientes paquetes hacia la IP de ese nuevo nodo se encaminan 
        # hay que añadir reglas en todos los nodos
        answer_datapath = datapath
        parser = answer_datapath.ofproto_parser
        anchor_port_to_IPnode = switch_info.anchor_output_port
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP,
                                     ipv4_src=ipv4_src, 
                                     ipv4_dst=ipv4_dst, 
                                     ip_proto=inet.IPPROTO_TCP)
        while (anchor_port_to_IPnode != "NONE"):
            flow_actions = [parser.OFPActionOutput(anchor_port_to_IPnode)]
            self.logger.info("-----------          ADD FLOW en switch=%s out_port=%s to IPnode=%s", 
                             answer_datapath.address[0], anchor_port_to_IPnode, ipv4_dst) 
            self.add_flow(answer_datapath, 40000, flow_match, flow_actions)

            switch_info = self.switch_info_by_IP[answer_datapath.address[0]]
            anchor_port_to_IPnode = switch_info.anchor_output_port
            answer_datapath=switch_info.anchor_datapath
            
 
    def add_flows_to_reach_controller_from_new_node(self, datapath, ipv4_src, ipv4_dst, switch_info):
        # En cada salto hay que añadir regla para que encamine al controlador 
        # los paquetes de este origen
        parser = datapath.ofproto_parser
        answer_datapath = datapath
        switch_info = self.switch_info_by_IP[answer_datapath.address[0]]
        port_to_controller = switch_info.port_to_controller
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP,
                                     ipv4_src=ipv4_src, 
                                     ipv4_dst=ipv4_dst, 
                                     ip_proto=inet.IPPROTO_TCP)

        # mientras no estoy en el primer nodo añado regla
        while (switch_info.anchor_datapath != "NONE"):
            flow_actions = [parser.OFPActionOutput(port_to_controller)]
            self.logger.info("-----------          ADD FLOW switch=%s output_port=%s to CONTROLLER", 
                             answer_datapath.address[0], port_to_controller) 
            self.add_flow(answer_datapath, 40000, flow_match, flow_actions)
            answer_datapath=switch_info.anchor_datapath
            switch_info = self.switch_info_by_IP[answer_datapath.address[0]]
            port_to_controller = switch_info.port_to_controller
            
        # en el primer nodo añado regla
        flow_actions = [parser.OFPActionOutput(port_to_controller)]
        self.logger.info("-----------          ADD FLOW FIRST switch=%s output_port=%s to CONTROLLER", 
                         answer_datapath.address[0], port_to_controller)
        self.add_flow(answer_datapath, 40000, flow_match, flow_actions)

        
    def send_syn_pkt_out(self, pkt):
        dpath = self.switch_info_by_IP[self.CONTROLLER_IP].datapath
        ofproto = dpath.ofproto
        parser = dpath.ofproto_parser

        recv_eth = pkt.get_protocol(ethernet.ethernet)
        recv_ipv4 = pkt.get_protocol(ipv4.ipv4)
        recv_tcp = pkt.get_protocol(tcp.tcp)

        answer_pkt = packet.Packet()
        answer_pkt.add_protocol(recv_eth)
        answer_pkt.add_protocol(recv_ipv4)
        answer_pkt.add_protocol(recv_tcp)
        answer_pkt.serialize()
            
        #Cualquier packet IN de TCP SYN envío pkt out al switch LOCAL
        answer_in_port = ofproto.OFPP_CONTROLLER
        answer_output_port = ofproto.OFPP_LOCAL

        actions = [parser.OFPActionOutput(port=answer_output_port)]

        out = parser.OFPPacketOut(datapath=dpath, buffer_id=ofproto.OFP_NO_BUFFER,
                                  in_port=answer_in_port, actions=actions, data=answer_pkt)
        self.logger.info("----------- send_syn_pkt_out: send SYN to TCP/IP stack of Controller")
        dpath.send_msg(out)
 

    def syn_ack_rcv(self, datapath, ip_src, ip_dst, in_port, pkt):

        # default prio=32768
        switch_info = self.switch_info_by_IP[ip_dst]
        answer_datapath = switch_info.anchor_datapath
        answer_output_port = switch_info.anchor_output_port
        answer_parser = answer_datapath.ofproto_parser
        answer_ofproto = answer_datapath.ofproto
        self.logger.info("        ----- PACKET syn_ack_rcv:: datapath=%s output_port=%d", answer_datapath, answer_output_port)

        # Regla en el anchor para el envio de mensajes a ese switch, SYN_ACK...etc
        flow_match = answer_parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP,
                                ipv4_src=ip_src, 
                                ipv4_dst=ip_dst, ip_proto=inet.IPPROTO_TCP) 
        flow_actions = [answer_parser.OFPActionOutput(answer_output_port)]
        self.add_flow(answer_datapath, 40000, flow_match, flow_actions)

        # Regla en el anchor para el envio de mensajes a ese switch,  PROBE...etc
        flow_match = answer_parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP,
                                ipv4_src=ip_src, 
                                ipv4_dst=ip_dst, ip_proto=inet.IPPROTO_UDP) 
        flow_actions = [answer_parser.OFPActionOutput(answer_output_port)]
        self.add_flow(answer_datapath, 40000, flow_match, flow_actions)

        # envía syn_ack pkt out
        self.syn_ack_pkt_out(dpath=answer_datapath, output_port=answer_output_port, pkt=pkt)
 

    def udp_probe_rcvd(self, datapath, ip_src, ip_dst, in_port):
        parser = datapath.ofproto_parser
        answer_output_port=in_port
        ofproto = datapath.ofproto

        switch_info = self.switch_info_by_IP[ip_dst]
        switch_info.datapath = datapath
        switch_info.port_to_controller = in_port

        # Regla para evitar inundacion ARP hacia el controlador
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_ARP, in_port=ofproto.OFPP_LOCAL,
                                     arp_tpa=ip_src) 
        flow_actions = [parser.OFPActionOutput(answer_output_port)]
        self.add_flow(datapath, 40000, flow_match, flow_actions)

        # En el nuevo nodo hay que añadir regla para que encamine al controlador 
        # los paquetes de este origen
        answer_datapath = datapath
        port_to_controller = switch_info.port_to_controller
        flow_match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP,
                                     ipv4_src=ip_dst, 
                                     ipv4_dst=ip_src, 
                                     ip_proto=inet.IPPROTO_TCP)
        flow_actions = [parser.OFPActionOutput(port_to_controller)]
        self.logger.info("-----------   UDP_PROBE") 
        self.logger.info("-----------          ADD FLOW switch=%s output=%s TO CONTROLLER", 
                         switch_info.datapath.address[0], switch_info.port_to_controller) 
        self.add_flow(answer_datapath, 40000, flow_match, flow_actions)

 
                
 
