

# Activar modo secure, el switch  no funciona como L2 learning switch
# Quedan alguna reglas programadas para contactar con el controlador
ovs-vsctl --db=unix:/tmp/mininet-s1/db.sock set-fail-mode s1 secure

# Desactivar el modo in-band, el switch ya no tiene reglas programadas
ovs-vsctl --db=unix:/tmp/mininet-s1/db.sock set bridge s1 other-config:disable-in-band=true

# Programar reglas openFlow
ovs-ofctl add-flow s1 "table=0,dl_dst=ff:ff:ff:ff:ff:ff,actions=output:ALL"
ovs-ofctl add-flow s1 "table=0,dl_dst=32:92:d2:eb:11:a6,actions=s1-eth0"
ovs-ofctl add-flow s1 "table=0,ipv4,nw_dst=10.0.0.1,tcp,tp_dst=6633,actions=s1-eth0"
ovs-ofctl add-flow s1 "table=0,ipv4,nw_src=10.0.0.1,tcp,tp_src=6633,actions=LOCAL,s1-eth1"
ovs-ofctl add-flow s1 "table=0,arp,dl_dst=32:a5:2d:c5:b4:47,actions=LOCAL"
ovs-ofctl add-flow s1 "table=0,ipv4,nw_src=10.0.0.1,tcp,tp_src=6633,actions=LOCAL,mod_dl_dst:00:00:00:00:00:11,output:s1-eth1"

# Programar modificar Ethernet.

# ovs-ofctl dump-flows s1
# cookie=0x0, duration=6760.718s, table=0, n_packets=3434, n_bytes=144228, dl_dst=ff:ff:ff:ff:ff:ff actions=ALL
# cookie=0x0, duration=6661.995s, table=0, n_packets=1745, n_bytes=133146, dl_dst=32:92:d2:eb:11:a6 actions=output:"s1-eth0"
# cookie=0x0, duration=6242.701s, table=0, n_packets=0, n_bytes=0, tcp,nw_dst=10.0.0.1,tp_dst=6633 actions=output:"s1-eth0"
# cookie=0x0, duration=119.596s, table=0, n_packets=2, n_bytes=84, arp,dl_dst=32:a5:2d:c5:b4:47 actions=LOCAL
# cookie=0x0, duration=71.033s, table=0, n_packets=26, n_bytes=2702, tcp,nw_src=10.0.0.1,tp_src=6633 actions=LOCAL,output:"s1-eth1"


~        
~        
