#!/bin/bash

if [ ! $# -eq 2 ]
then
    echo "Usage error: $0 <switchName> <numberInterfaces>"
    exit -1
fi

SWITCH_NAME=$1
N_IFACES=$2

echo Delete OVS bridge for $SWITCH_NAME
val=`expr $N_IFACES - 1`

for i in $(seq 0 $val);
do
   ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock del-port $SWITCH_NAME $SWITCH_NAME-eth$i
done
ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock del-br $SWITCH_NAME


