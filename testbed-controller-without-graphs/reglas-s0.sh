
# la solicitud de ARP se la enviamos al controlador para que conozca a un nuevo switch
ovs-ofctl add-flow s0 "table=0,arp,dl_dst=ff:ff:ff:ff:ff:ff,arp_tpa=10.0.0.1,actions=CONTROLLER"

# gratuitous ARP, luego ya conocemos al switch que lo ha generado: que responda la pila TCP/IP
ovs-ofctl add-flow s0 "table=0,arp,arp_tpa=10.0.0.1,actions=LOCAL"

# respuesta de ARP la genera el controllador y se la manda a s0 como pkt_out, 
# s0 debe enviarla por su interfaz s0-eth0
ovs-ofctl add-flow s0 "table=0,arp,arp_spa=10.0.0.1,actions=s0-eth0"

#Cuando envíe mensajes IPv6mcast_node_00 lo tira
ovs-ofctl add-flow s0 "table=0,dl_dst=33:33:00:00:00:00,actions=drop"
ovs-ofctl add-flow s0 "table=0,dl_dst=33:33:00:00:00:16,actions=drop"

# Cuando nos llegue el SYN generamos PacketIn para el controlador
ovs-ofctl add-flow s0 "table=0,ipv4,nw_dst=10.0.0.1,tcp,tp_dst=6633,actions=CONTROLLER"
