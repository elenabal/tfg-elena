#!/usr/bin/python

import sys
import os

from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info


# Topologia
#                                                              
#       1   10.   1    2     11.     1   2     12.     1    2   13.    1
#    s0 ----------- s1 --------------- s2 -------------- s3 ------------ h2
# 10.0.0.1           \ 3            3 /                               10.0.0.2
#                     \          16. /
#                 14.  \          3 /    15.    1
#                       -------- s4 ------------- h3
#                              1    2            10.0.0.3
#
# Lanzaremos un controlador independiente en s0

def ovsns(batch = False):


    "Create an empty network and add nodes to it."

    net = Mininet( topo=None,
                   build=False)

    my_path = os.path.dirname(os.path.abspath(__file__))

    private_dirs = [('/etc/quagga', my_path + '/quagga/')]

    s0 = net.addHost( 's0', privateDirs=private_dirs, ip='10.0.0.1', mac='00:00:00:00:00:01' )
    s1 = net.addHost( 's1', privateDirs=private_dirs, ip='10.0.0.11',  mac='00:00:00:00:11:10' )
    s2 = net.addHost( 's2', privateDirs=private_dirs, ip='11.0.0.12',  mac='00:00:00:00:22:20' )
    s3 = net.addHost( 's3', privateDirs=private_dirs, ip='12.0.0.13',  mac='00:00:00:00:33:30' )
    s4 = net.addHost( 's4', privateDirs=private_dirs, ip='14.0.0.14',  mac='00:00:00:00:44:40' )
    h2 = net.addHost( 'h2', privateDirs=private_dirs, ip='13.0.0.200', mac='00:00:00:00:00:02' )
    h3 = net.addHost( 'h3', privateDirs=private_dirs, ip='15.0.0.200', mac='00:00:00:00:00:03' )

    net.addLink( s0, s1, 0, 0 )
    net.addLink( s1, s2, 1, 0 )
    net.addLink( s1, s4, 2, 0 )
    net.addLink( s2, s3, 1, 0 )
    net.addLink( s3, h2, 1, 0 )
    net.addLink( s4, h3, 1, 0 )
    net.addLink( s2, s4, 2, 2 )

    s1.cmd("ip link set s1-eth0 address 00:00:00:00:11:10")
    s1.cmd("ip link set s1-eth1 address 00:00:00:00:11:11")
    s1.cmd("ip link set s1-eth2 address 00:00:00:00:11:12")
    s2.cmd("ip link set s2-eth1 address 00:00:00:00:22:21")
    s2.cmd("ip link set s2-eth2 address 00:00:00:00:22:22")
    s3.cmd("ip link set s3-eth1 address 00:00:00:00:33:31")
    s4.cmd("ip link set s4-eth1 address 00:00:00:00:44:41")
    s4.cmd("ip link set s4-eth2 address 00:00:00:00:44:42")


    s0.cmd("ip address add dev s0-eth0 10.0.0.1/24")
    s1.cmd("ip address add dev s1-eth0 10.0.0.11/24")
    s1.cmd("ip address add dev s1-eth1 11.0.0.11/24")
    s1.cmd("ip address add dev s1-eth2 14.0.0.11/24")
    s2.cmd("ip address add dev s2-eth0 11.0.0.12/24")
    s2.cmd("ip address add dev s2-eth1 12.0.0.12/24")
    s2.cmd("ip address add dev s2-eth2 16.0.0.12/24")
    s3.cmd("ip address add dev s3-eth0 12.0.0.13/24")
    s3.cmd("ip address add dev s3-eth1 13.0.0.13/24")
    s4.cmd("ip address add dev s4-eth0 14.0.0.14/24")
    s4.cmd("ip address add dev s4-eth1 15.0.0.14/24")
    s4.cmd("ip address add dev s4-eth2 16.0.0.14/24")



    if not batch:
        net.start()
        CLI( net )        
        net.stop()
    else: # batch
        return net, s0



if __name__ == '__main__':
    setLogLevel( 'info' )
    ovsns()
