#!/bin/bash

if [ ! $# -eq 1 ]
then
    echo "Usage error: $0 <switchName>"
    exit -1
fi

SWITCH_NAME=$1

ZEBRA_PID=`cat /tmp/quagga/zebra-$SWITCH_NAME.pid`
OSPFD_PID=`cat /tmp/quagga/ospfd-$SWITCH_NAME.pid`


kill -9 $ZEBRA_PID $OSPFD_PID
