

#!/usr/bin/python

from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info

# Escenario
#
#    1  1   2   1    3  1
# h1 ---- s1 ---- s2 ---- h2
#       3 |       | 2
#         |       |
#       1 |       | 3
#         s3 ---- s4 ---- h3
#           2   1   2  1
#


def ovsns():

    "Create an empty network and add nodes to it."

    net = Mininet( topo=None,
                   build=False)

    h1 = net.addHost( 'h1', ip='10.0.0.1', mac='00:00:00:00:00:01' )
    s1 = net.addHost( 's1', ip='0.0.0.0',  mac='00:00:00:00:11:10' )
    s2 = net.addHost( 's2', ip='0.0.0.0',  mac='00:00:00:00:22:20' )
    s3 = net.addHost( 's3', ip='0.0.0.0',  mac='00:00:00:00:33:30' )
    s4 = net.addHost( 's4', ip='0.0.0.0',  mac='00:00:00:00:44:40' )
    h2 = net.addHost( 'h2', ip='10.0.0.2', mac='00:00:00:00:00:02' )
    h3 = net.addHost( 'h3', ip='10.0.0.3', mac='00:00:00:00:00:03' )

    net.addLink( h1, s1, 0, 0 )
    net.addLink( s1, s2, 1, 0 )
    net.addLink( s1, s3, 2, 0 )
    net.addLink( s2, s4, 1, 2 )
    net.addLink( s2, h2, 2, 0 )
    net.addLink( s3, s4, 1, 0 )
    net.addLink( s4, h3, 1, 0 )

    # Direciones Ethernet amigables
    s1.cmd("ip link set s1-eth1 address 00:00:00:00:11:11")
    s1.cmd("ip link set s1-eth2 address 00:00:00:00:11:12")
    s2.cmd("ip link set s2-eth1 address 00:00:00:00:22:21")
    s2.cmd("ip link set s2-eth2 address 00:00:00:00:22:22")
    s3.cmd("ip link set s3-eth1 address 00:00:00:00:33:31")
    s4.cmd("ip link set s4-eth1 address 00:00:00:00:44:41")
    s4.cmd("ip link set s4-eth2 address 00:00:00:00:44:42")

    net.start()
    CLI( net )
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    ovsns()
