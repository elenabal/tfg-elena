#!/bin/bash

if [ ! $# -eq 1 ]
then
    echo "Usage error: $0 <switchName>"
    exit -1
fi

echo Dump all flows for $1

pid_ovs_switchd=`cat /tmp/mininet-$1/ovs-vswitchd.pid`

ovs-appctl --target=/var/run/openvswitch/ovs-vswitchd.${pid_ovs_switchd}.ctl bridge/dump-flows $1
