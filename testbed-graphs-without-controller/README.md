# OF-in-band-control-plane

## What
A proof of concept, hard-coded testbed for graph-basd source routing.

We have applied slick packets paper to OpenFlow (OF): main path and alternative paths are coded as nsh headers

The main objective is to design an in-band control plane implemented with OF using graph-based source routing

But this code just shows how to configure OF flow entries
Next step: bootstrap in-band control plane implemented with a Ryu controller


## Ficheros:
   - escenario.py: escenario de mininet que lanza la siguiente topología
   
   ```                   1   2   1    3  
           h1 --------- s1 ---- s2 --------- h2
      (10.0.0.1)      3 |       | 2          (10.0.0.2)
                        |       |
                      1 |       | 3
                        s3 ---- s4 --------- h3
                          2   1   2          (10.0.0.3)
   ```
   
   Los switches están creados como máquinas finales para poder arrancar ovswitch en cada network namespace.

   - init-s?.sh: init-s1.sh, init-s2.sh, init-s3, init-s4.sh
        - Creación de ovsDB, ovs e interfaces en modo set-fail-mode, out-of-band, disable-in-band
        - Usa los scripts: startOvsDb.sh, startOvs.sh y createBrSdn.sh
        - Asigna dirección IP al switch: s1 (10.0.0.101), s2 (10.0.0.102), s3 (10.0.0.103) y s4 (10.0.0.104)
        - Rellena caché de ARP con las direcciones del resto de máquinas y switches
   - init-h?.sh: init-h1.sh, init-h2.sh, init-h3
        - Rellena caché de ARP con las direcciones del resto de máquinas y switches
   - down-sn.sh: down-s1.sh, down-s2.sh, down-s3, down-s4.sh
        - Borrado de ovsDB, ovs e interfaces en modo set-fail-mode, out-of-band, disable-in-band
        - Usa los scripts: stopOvsDb.sh, stopOvs.sh y deleteBrSdn.sh
   - reglas-s?.sh: reglas-s1.sh, reglas-s2.sh, reglas-s3.sh, reglas-d4.sh
        - Instalación de reglas en cada switch

## Uso:
   - Arrancar escenario:
        sudo python escenario.py

   - Lanzar terminales para cada switch y desde cada uno de ellos configurar ovswitch y las reglas
        s1~:> ./init-s1.sh; ./reglas-s1.sh
        s2~:> ./init-s2.sh; ./reglas-s2.sh
        s3~:> ./init-s3.sh; ./reglas-s3.sh
        s4~:> ./init-s4.sh; ./reglas-s4.sh

   - Insertar en las cachés de ARP las direcciones Ethernet de las máquinas/switches que van hacer ping. 
        h1~:> ./init-h1.sh
        h2~:> ./init-h2.sh
        h3~:> ./init-h3.sh

   - La configuración de inserción de grafo debe hacerse en el fichero
     reglas-sn.sh del switch sn correspondiente. 
     
     **La configuración para desconectar un puerto se realiza comentando las reglas de ese puerto, para que se use el camino alternativo que traerá el grafo.** 

     Los ficheros están configurados para que funcione ping entre h1, h2 y h3, y ping h1=>s4

  - Arrancando wireshark en las diferentes interfaces se puede ver la
     encapsulación nsh del grafo

## Descripción
  -  En las cabeceras nsh más externas está el camino principal. En el
     siguiente encapsulamiento nsh está el camino alternativo para el
     primer enlace, etc.

     En cada salto se elimina el encapsulamiento del camino
     alternativo correpondiente al enlace en el que se está

     Si en un salto no está activado el puerto correspondiente se
     substituye el camino principal por el alternativo correspondiente

     Si al cambiar al alternativo este es nulo se termina

     El destino puede ser un switch o un host. Si es un host al último
     switch llega el puerto f? siendo ? el puerto hacia el host. Si el
     destino es el switch, el puerto es ff, significando que se
     entrega a LOCAL

     El TTL de nsh indica el número de caminos alternativos que viajan
     en el grafo. Es 0 si se está encaminando ya por uno alternativo,
     si se está encaminando por el principal pero ya se está en el
     último salto



     

