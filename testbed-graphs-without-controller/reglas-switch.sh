#!/bin/bash

if [ ! $# -eq 1 ]
then
   echo "Usage error: $0 <switchName>"
   exit -1
fi

SWITCH_NAME=$1



#ya viene con grafo
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=0,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,actions=resubmit(,1)"

##
## -------------------------------------
## Tabla 1: reglas de forwarding en $SWITCH_NAME
## -------------------------------------
##


# al enviar por un puerto n hay que usar "actions=output:n". Si n=in_port no lo envía. Por eso,
# siempre asignamos a in_port un número de puerto que no se use (0xffff), para que no se cumpla n=in_port.

###########
# PUERTO 1
###########
# puerto 1:  estas reglas hacen match en el puerto (nsh_c1[28..31]=0x1), el puerto 0x1 está activo

# puerto 1: si ttl = 0 (va por camino alternativo)
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=58000,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_ttl=0,nsh_c1[28..31]=0x1,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->nsh_c1[4..31],load:0x0->nsh_c1[0..3],encap(ethernet),pop:dl_dst,pop:dl_src,output:1"

# puerto 1: ttl=1, al decrementar el ttl se hace cero y el mensaje no se envía. No hay que decrementa, sino asignar.
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x1,nsh_ttl=1,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->reg0[4..31],load:0x0->reg0[0..3],decap(),decap(),encap(nsh(md_type=1)),set_field:0x1234->nsh_spi,move:reg0->nsh_c1,load:0x0->nsh_ttl,encap(ethernet),pop:dl_dst,pop:dl_src,output:1"

# puerto 1: si ttl>1, decrementar ttl, seguro que no se hace cero y se envía,
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57998,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x1,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->reg0[4..31],load:0x0->reg0[0..3],move:nsh_ttl->reg1[0..7],decap(),decap(),encap(nsh(md_type=1)),set_field:0x1234->nsh_spi,move:reg0->nsh_c1,move:reg1[0..7]->nsh_ttl,dec_nsh_ttl,encap(ethernet),pop:dl_dst,pop:dl_src,output:1"

###########
# PUERTO 2
###########
# puerto 2:  estas reglas hacen match en el puerto (nsh_c1[28..31]=0x2), el puerto 0x2 está activo

# puerto 2: si ttl = 0 (va por camino alternativo)
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=58000,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_ttl=0,nsh_c1[28..31]=0x2,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->nsh_c1[4..31],load:0x0->nsh_c1[0..3],encap(ethernet),pop:dl_dst,pop:dl_src,output:2"

# puerto 2: ttl=1, al decrementar el ttl se hace cero y el mensaje no se envía. No hay que decrementa, sino asignar.
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x2,nsh_ttl=1,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->reg0[4..31],load:0x0->reg0[0..3],decap(),decap(),encap(nsh(md_type=1)),set_field:0x1234->nsh_spi,move:reg0->nsh_c1,load:0x0->nsh_ttl,encap(ethernet),pop:dl_dst,pop:dl_src,output:2"

# puerto 2: si ttl>1, decrementar ttl, seguro que no se hace cero y se envía,
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57998,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x2,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->reg0[4..31],load:0x0->reg0[0..3],move:nsh_ttl->reg1[0..7],decap(),decap(),encap(nsh(md_type=1)),set_field:0x1234->nsh_spi,move:reg0->nsh_c1,move:reg1[0..7]->nsh_ttl,dec_nsh_ttl,encap(ethernet),pop:dl_dst,pop:dl_src,output:2"


###########
# PUERTO 3
###########
# puerto 3:  estas reglas hacen match en el puerto (nsh_c1[28..31]=0x3), el puerto 0x3 está activo

# puerto 3: si ttl = 0 (va por camino alternativo)
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=58000,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_ttl=0,nsh_c1[28..31]=0x3,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->nsh_c1[4..31],load:0x0->nsh_c1[0..3],encap(ethernet),pop:dl_dst,pop:dl_src,output:3"

# puerto 3: ttl=1, al decrementar el ttl se hace cero y el mensaje no se envía. No hay que decrementa, sino asignar.
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x3,nsh_ttl=1,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->reg0[4..31],load:0x0->reg0[0..3],decap(),decap(),encap(nsh(md_type=1)),set_field:0x1234->nsh_spi,move:reg0->nsh_c1,load:0x0->nsh_ttl,encap(ethernet),pop:dl_dst,pop:dl_src,output:3"

# puerto 3: si ttl>1, decrementar ttl, seguro que no se hace cero y se envía,
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57998,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0x3,actions=load:0xffff->NXM_OF_IN_PORT[],push:dl_src,push:dl_dst,decap(),move:nsh_c1[0..27]->reg0[4..31],load:0x0->reg0[0..3],move:nsh_ttl->reg1[0..7],decap(),decap(),encap(nsh(md_type=1)),set_field:0x1234->nsh_spi,move:reg0->nsh_c1,move:reg1[0..7]->nsh_ttl,dec_nsh_ttl,encap(ethernet),pop:dl_dst,pop:dl_src,output:3"



##############
# PUERTO 0xff
##############
# puerto 0xff: último salto, el puerto hacia el destino final es f==LOCAL. ttl=0 venga o no por camino principal, ya que el controlador cuando el último salto es ff no le pone camino alternativo. Desencapsulamos y enviamos a local
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=58000,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_ttl=0,nsh_c1[24..31]=0xff,actions=push:dl_src,push:dl_dst,decap(),decap(),encap(ethernet),pop:dl_dst,pop:dl_src,output:LOCAL"


##############
# PUERTO 0xf?
##############
#puerto 0xf?: último salto, el puerto hacia el destino final es ?. ttl=1 si es camino principal, ttl=0 si es camino alternativo. Desencapsulamos y enviamos al destino final por el puerto X que está almacenado en in_port
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0xf,nsh_ttl=0,actions=push:dl_src,push:dl_dst,decap(),move:nsh_c1[24..27]->in_port[0..3],decap(),encap(ethernet),pop:dl_dst,pop:dl_src,output:in_port"
# Si ttl=1, desencapsulo aquí, quitando NSH del alternativo, en vez de enviarlo a tablas 2 y 3 para desencapsular
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=57998,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1[28..31]=0xf,actions=push:dl_src,push:dl_dst,decap(),move:nsh_c1[24..27]->in_port[0..3],decap(),decap(),encap(ethernet),pop:dl_dst,pop:dl_src,output:in_port"


#####################
# NO HAY PUERTO 
#####################
# si no ha hecho match antes es porque su puerto no está activo
# si TTL==0 no hay camino alternativo
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=40000, dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_ttl=0,actions=drop"

# si TTL!=0 hay que poner camino alternativo y quitar todos los demás alternativos, para lo cual hacemos resubmit a tabla 2, guardando
# en reg1 el TTL.
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME "table=1,priority=39999, dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,actions=push:dl_src,push:dl_dst,decap(),move:nsh_ttl->reg1[0..7],decap(),encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,2)"


##
## -------------------------------------------------------------------------------------
## tabla 2 comprobar si el alternativo es NULL y si no, almacenar alternativo en reg0
## -------------------------------------------------------------------------------------
##

# en reg0 tenemos el path alternativo que se va a convertir en definitivo tras desencapsular los otros alternativos

# Si el camino alternativo en reg0 es 0xffffffff, drop pues eso significa que no hay alternativo
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME  "table=2,priority=60000,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,nsh_c1=0xffffffff,actions=drop"

# Dejo el camino alternativo en reg0, ya tenía almacenado en reg1 el TTL y lo mando a la tabla 3 para que quite todos los nsh alternativos
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME  "table=2,priority=59999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,actions=push:dl_src,push:dl_dst,decap(),move:nsh_c1->reg0,encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,3)"


## -------------------------------------------------------------------------------------
## tabla 3 de desencapsulamiento de paths alternativos
## -------------------------------------------------------------------------------------
# si nsh_ttl = reg1 = 1 desencapsulo, quedando sin ningún nsh, así que recuperamos el alternativo de reg0 y le ponemos ttl=0. Finalmente, resubmit a tabla 1 para forward
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME  "table=3,priority=60000, dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,reg1=1,actions=push:dl_src,push:dl_dst,decap(),decap(),encap(nsh(md_type=1)),load:0x1234->nsh_spi,load:0->nsh_ttl,move:reg0->nsh_c1,encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,1)"


# si nsh_ttl > 1 (si fuera 1, la regla anterior de mayor prioridad se habría cumplido nsh_ttl=1) recupero reg1->nsh_ttl, nsh_ttl_dec y lo vuelvo a dejar en reg1, decap y resubmit para seguir desencapsulando
ovs-ofctl -O OpenFlow15 add-flow $SWITCH_NAME  "table=3, priority=59999,dl_type=0x894f,nsh_mdtype=1,nsh_spi=0x1234,actions=push:dl_src,push:dl_dst,decap(),move:reg1[0..7]->nsh_ttl,dec_nsh_ttl,move:nsh_ttl->reg1[0..7],decap(),encap(ethernet),pop:dl_dst,pop:dl_src,resubmit(,3)"

