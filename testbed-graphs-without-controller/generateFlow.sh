#!/bin/bash

if [ ! $# -eq 1 ]
then
    echo "Usage error: $0 <switchName>"
    exit -1
fi

echo Generate flow for $1


pid_ovs_switchd=`cat /tmp/mininet-$1/ovs-vswitchd.pid`

ovs-appctl --target=/var/run/openvswitch/ovs-vswitchd.${pid_ovs_switchd}.ctl ofproto/trace $1 in_port=1,dl_type=0x0800,dl_src=00:00:00:00:00:03,dl_dst=00:00:00:00:00:01,nw_src=10.0.0.3,nw_dst=10.0.0.1 -generate
