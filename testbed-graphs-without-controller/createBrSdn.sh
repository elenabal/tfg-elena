#!/bin/bash

if [ ! $# -eq 3 ]
then
    echo "Usage error: $0 <switchName> <numberofInterfaces> <controllerIP>"
    exit -1
fi

SWITCH_NAME=$1
N_IFACES=$2
CONTROLLER_IP=$3

echo Configure OVS for $SWITCH_NAME

ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock add-br $SWITCH_NAME
ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock set bridge $SWITCH_NAME datapath_type=netdev
ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock set bridge $SWITCH_NAME protocols=OpenFlow10,OpenFlow11,OpenFlow12,OpenFlow13,OpenFlow14,OpenFlow15

val=`expr $N_IFACES - 1`

for i in $(seq 0 $val);
do
   ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock add-port $SWITCH_NAME $SWITCH_NAME-eth$i
done

ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock set-controller $SWITCH_NAME tcp:$CONTROLLER_IP:6633
ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock set-fail-mode $SWITCH_NAME secure
ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock set controller $SWITCH_NAME connection-mode=out-of-band
ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock set bridge $SWITCH_NAME other-config:disable-in-band=true
ovs-vsctl --db=unix:/tmp/mininet-$SWITCH_NAME/db.sock show


